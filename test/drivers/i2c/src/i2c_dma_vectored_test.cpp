#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/clock.h"
#include "spl/drivers/dma/mock/dma.h"
#include "spl/drivers/i2c/concepts/i2c.h"
#include "spl/drivers/i2c/detail/i2c_dma_vectored.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/i2c/mock/i2c.h"

namespace {
using namespace ::testing;
using namespace spl::drivers::i2c;
using namespace satos::chrono_literals;

constexpr auto rx_channel = spl::peripherals::dma::channel1;
constexpr auto tx_channel = spl::peripherals::dma::channel2;

using i2c_type = detail::i2c_dma_vectored<spl::peripherals::i2c::mock::i2c,
                                          spl::drivers::dma::mock::dma,
                                          spl::peripherals::dma::peripheral::i2c1_rx,
                                          spl::peripherals::dma::peripheral::i2c1_tx,
                                          rx_channel,
                                          tx_channel,
                                          spl::peripherals::dma::request1>;

static_assert(concepts::i2c_vectored<i2c_type>);

struct I2CDmaVectoredTest : Test {
    I2CDmaVectoredTest();
    NiceMock<spl::peripherals::i2c::mock::i2c> i2c_;
    NiceMock<spl::drivers::dma::mock::dma> dma_;
    StrictMock<spl::drivers::dma::mock::detail::dma_channel> tx_dma_channel_;
    StrictMock<spl::drivers::dma::mock::detail::dma_channel> rx_dma_channel_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
    static constexpr std::uint8_t i2c_address = 0xaa;
};

I2CDmaVectoredTest::I2CDmaVectoredTest() {
    spl::peripherals::i2c::mock::setup_default_return_value(i2c_);
}

TEST_F(I2CDmaVectoredTest, ReadSingleSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));

    auto result = i2c.read(i2c_address, output);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(I2CDmaVectoredTest, ReadMultipleSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(first_buffer.size() + second_buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(second_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(second_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));

    auto result = i2c.read(i2c_address, output);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(I2CDmaVectoredTest, ReadBufferEmptyFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};
    auto empty = std::span(output.data(), 0);

    auto result = i2c.read(i2c_address, empty);
    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(I2CDmaVectoredTest, ReadBufferSizeOutOfRangeFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 100> buffer1{};
    std::array<std::byte, 100> buffer2{};
    std::array<std::byte, 100> buffer3{};
    std::array<std::span<std::byte>, 3> output = {buffer1, buffer2, buffer3};

    auto result = i2c.read(i2c_address, output);
    ASSERT_THAT(result, Eq(status::buffer_size_out_of_range));
}

TEST_F(I2CDmaVectoredTest, ReadRequestChannelTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = i2c.read(i2c_address, output, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, ReadTransferTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));

    auto result = i2c.read(i2c_address, output, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, ReadMultipleTransferTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(first_buffer.size() + second_buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));

    auto result = i2c.read(i2c_address, output, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, ReadTransferErrorFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));

    auto result = i2c.read(i2c_address, output);
    ASSERT_THAT(result, Eq(status::transfer_error));
}

TEST_F(I2CDmaVectoredTest, WriteSingleSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write(i2c_address, input);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(I2CDmaVectoredTest, WriteMultipleSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<const std::byte>, 2> input = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(first_buffer.size() + second_buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(second_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(second_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write(i2c_address, input);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(I2CDmaVectoredTest, WriteBufferEmptyFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};
    auto empty = std::span(input.data(), 0);

    auto result = i2c.write(i2c_address, empty);
    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(I2CDmaVectoredTest, WriteBufferSizeOutOfRangeFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 100> buffer1{};
    std::array<std::byte, 100> buffer2{};
    std::array<std::byte, 100> buffer3{};
    std::array<std::span<const std::byte>, 3> input = {buffer1, buffer2, buffer3};

    auto result = i2c.write(i2c_address, input);
    ASSERT_THAT(result, Eq(status::buffer_size_out_of_range));
}

TEST_F(I2CDmaVectoredTest, WriteRequestChannelTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = i2c.write(i2c_address, output, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteTransferTimeoutSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write(i2c_address, input, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteMultipleTransferTimeoutSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write(i2c_address, input, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteTransferErrorFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(buffer.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write(i2c_address, input);
    ASSERT_THAT(result, Eq(status::transfer_error));
}

TEST_F(I2CDmaVectoredTest, WriteReadSingleSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(output.size()));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(output.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(output.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(I2CDmaVectoredTest, WriteReadMultipleSuccess) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output1{};
    std::array<std::byte, 8> output2{};
    std::array<std::span<std::byte>, 2> out = {output1, output2};
    std::array<std::byte, 4> input1{};
    std::array<std::byte, 8> input2{};
    std::array<std::span<const std::byte>, 2> in = {input1, input2};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input1.size() + input2.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input1.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input1.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input2.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input2.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(output1.size() + output2.size()));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(output1.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(output1.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(output2.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(output2.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(I2CDmaVectoredTest, WriteReadOutputBufferEmptyFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    auto empty = std::span(out.data(), 0);
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    auto result = i2c.write_read(i2c_address, in, empty);
    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(I2CDmaVectoredTest, WriteReadInputBufferEmptyFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};
    auto empty = std::span(in.data(), 0);

    auto result = i2c.write_read(i2c_address, empty, out);
    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(I2CDmaVectoredTest, WriteReadOutputBufferSizeOutOfRangeFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 100> buffer1{};
    std::array<std::byte, 100> buffer2{};
    std::array<std::byte, 100> buffer3{};
    std::array<std::span<std::byte>, 3> out = {buffer1, buffer2, buffer3};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    auto result = i2c.write_read(i2c_address, in, out);
    ASSERT_THAT(result, Eq(status::buffer_size_out_of_range));
}

TEST_F(I2CDmaVectoredTest, WriteReadInputBufferSizeOutOfRangeFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 100> buffer1{};
    std::array<std::byte, 100> buffer2{};
    std::array<std::byte, 100> buffer3{};
    std::array<std::span<const std::byte>, 3> in = {buffer1, buffer2, buffer3};

    auto result = i2c.write_read(i2c_address, in, out);
    ASSERT_THAT(result, Eq(status::buffer_size_out_of_range));
}

TEST_F(I2CDmaVectoredTest, WriteReadTxRequestChannelTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = i2c.write_read(i2c_address, in, out, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteReadRxRequestChannelTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = i2c.write_read(i2c_address, in, out, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteReadTxTransferTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteReadMultipleTxTransferTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input1{};
    std::array<std::byte, 8> input2{};
    std::array<std::span<const std::byte>, 2> in = {input1, input2};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input1.size() + input2.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input1.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input1.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteReadRxTransferTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(output.size()));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(output.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(output.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteReadMultipleRxTransferTimeoutFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output1{};
    std::array<std::byte, 8> output2{};
    std::array<std::span<std::byte>, 2> out = {output1, output2};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(output1.size() + output2.size()));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(output1.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(output1.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out, 1000_ms);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(I2CDmaVectoredTest, WriteReadTxTransferErrorFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out);
    ASSERT_THAT(result, Eq(status::transfer_error));
}

TEST_F(I2CDmaVectoredTest, WriteReadRxTransferErrorFailure) {
    i2c_type i2c{i2c_, dma_};

    std::array<std::byte, 4> output{};
    std::array<std::span<std::byte>, 1> out = {output};
    std::array<std::byte, 4> input{};
    std::array<std::span<const std::byte>, 1> in = {input};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_tx));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_,
                set_peripheral_address(spl::peripherals::dma::peripheral::i2c1_rx));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(i2c_, enable());
    EXPECT_CALL(i2c_, set_7bit_address(i2c_address));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::write));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(input.size()));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::tx));

    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(input.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(input.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));
    EXPECT_CALL(i2c_, set_bytes_to_transfer(output.size()));
    EXPECT_CALL(i2c_, set_transfer_dir(spl::peripherals::i2c::concepts::direction::read));
    EXPECT_CALL(i2c_, enable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, send_start());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(output.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(output.size()));
    EXPECT_CALL(rx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(i2c_, send_stop());
    EXPECT_CALL(i2c_, disable());
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::rx));
    EXPECT_CALL(i2c_, disable_dma(spl::peripherals::i2c::concepts::dma::tx));

    auto result = i2c.write_read(i2c_address, in, out);
    ASSERT_THAT(result, Eq(status::transfer_error));
}
} // namespace
