#include <array>
#include <vector>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/clock.h"
#include "satos/mock/event.h"
#include "satos/mock/mutex.h"
#include "spl/drivers/i2c/i2c.h"
#include "spl/peripherals/i2c/mock/i2c.h"
#include "spl/peripherals/nvic/enums.h"
#include "spl/peripherals/nvic/mock/nvic.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::i2c;
using namespace satos::chrono_literals;

constexpr uint8_t address = 0b01010101;

using i2c_type = detail::i2c<spl::peripherals::i2c::mock::i2c,
                             spl::peripherals::nvic::mock::nvic,
                             spl::peripherals::nvic::irq::i2c1_ev,
                             spl::peripherals::nvic::irq::i2c1_er>;

struct I2CTest : Test {
    I2CTest();
    NiceMock<spl::peripherals::i2c::mock::i2c> i2c_;
    NiceMock<spl::peripherals::nvic::mock::nvic> nvic_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

I2CTest::I2CTest() {
    spl::peripherals::i2c::mock::setup_default_return_value(i2c_);
    spl::peripherals::nvic::mock::setup_default_return_value(nvic_);
    satos::mock::clock::reset();
}

TEST_F(I2CTest, ReadSuccess) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::nack_received));
    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::rx_not_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.read(address, buffer);
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(I2CTest, ReadEmptyBuffer) {
    i2c_type i2c(i2c_, nvic_);

    std::array<std::byte, 2> buffer{};

    auto result = i2c.read(address, std::span(buffer.data(), 0));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::buffer_is_empty));
}

TEST_F(I2CTest, ReadMutexTimeout) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.read(address, buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(I2CTest, ReadEventTimeout) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::nack_received));
    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::rx_not_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.read(address, buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(I2CTest, WriteSuccess) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::nack_received));
    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::tx_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.write(address, buffer);
    ASSERT_THAT(result, Eq(spl::drivers::i2c::status::ok));
}

TEST_F(I2CTest, WriteMutexTimeout) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.write(address, buffer, 1_s);
    ASSERT_THAT(result, Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(I2CTest, WriteEventTimeout) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::nack_received));
    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::tx_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.write(address, buffer, 1_s);
    ASSERT_THAT(result, Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(I2CTest, WriteReadSuccess) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::nack_received));
    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::tx_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    std::array<const std::byte, 2> tx_buffer{};
    std::array<std::byte, 2> rx_buffer{};

    auto result = i2c.write_read(address, tx_buffer, rx_buffer);
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(I2CTest, WriteReadMutexTimeout) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<const std::byte, 2> tx_buffer{};
    std::array<std::byte, 2> rx_buffer{};

    auto result = i2c.write_read(address, tx_buffer, rx_buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(I2CTest, WriteReadEventTimeout) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::nack_received));
    EXPECT_CALL(i2c_, enable_interrupt(spl::peripherals::i2c::irq::tx_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));

    std::array<const std::byte, 2> tx_buffer{};
    std::array<std::byte, 2> rx_buffer{};

    auto result = i2c.write_read(address, tx_buffer, rx_buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::timeout));
}

TEST_F(I2CTest, ReadBusy) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(i2c_, is_busy()).WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.read(address, buffer);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::busy));
}

TEST_F(I2CTest, WriteBusy) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(i2c_, is_busy()).WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = i2c.write(address, buffer);
    ASSERT_THAT(result, Eq(spl::drivers::i2c::status::busy));
}

TEST_F(I2CTest, WriteReadBusy) {
    i2c_type i2c(i2c_, nvic_);

    EXPECT_CALL(i2c_, is_busy()).WillOnce(Return(true));

    std::array<const std::byte, 2> tx_buffer{};
    std::array<std::byte, 2> rx_buffer{};

    auto result = i2c.write_read(address, tx_buffer, rx_buffer);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::i2c::status::busy));
}

} // namespace
