#include "satos/mock/flags.h"
#include "spl/drivers/dma/detail/dma.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/dma/mock/dma.h"
#include "spl/peripherals/nvic/mock/nvic.h"

namespace {

using namespace spl;
using namespace ::testing;
using namespace satos::mock;
using namespace satos::chrono_literals;

using dma_type =
    drivers::dma::detail::dma<peripherals::dma::mock::dma, peripherals::nvic::mock::nvic>;

constexpr std::size_t channel_index = 0;
constexpr std::size_t notify_index = 1;

struct DMATest : public Test {
    StrictMock<peripherals::dma::mock::dma> dma_ll_;
    StrictMock<peripherals::nvic::mock::nvic> nvic_ll_;
    dma_type dma_{dma_ll_, nvic_ll_};

    DMATest();
};

DMATest::DMATest() {
    peripherals::dma::mock::setup_default_return_value(dma_ll_);
    peripherals::nvic::mock::setup_default_return_value(nvic_ll_);
}

TEST_F(DMATest, Initialize) {
    EXPECT_CALL(*flags::flags_vector[channel_index], set(0xffffff));

    dma_.initialize();
}

TEST_F(DMATest, RequestSuccess) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));
}

TEST_F(DMATest, RequestTimeout) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, 1000_ms))
        .WillOnce(Return(std::nullopt));

    auto result = dma_.request(peripherals::dma::channel3, 1000_ms);
    EXPECT_THAT(result.has_value(), Eq(false));
    EXPECT_THAT(result.error(), Eq(drivers::dma::status::timeout));
}

TEST_F(DMATest, SetChannelPeripheralAddress) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(
        dma_ll_,
        set_peripheral_address(peripherals::dma::channel3, peripherals::dma::peripheral::spi2));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_peripheral_address(peripherals::dma::peripheral::spi2);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelMemoryAddress) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_memory_address(peripherals::dma::channel3, 0xdeadc0de));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_memory_address(0xdeadc0de);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelPointerMemoryAddress) {
    std::array<std::uint32_t, 10> data{};
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(
        dma_ll_,
        set_memory_address(peripherals::dma::channel3, std::bit_cast<std::uint32_t>(data.data())));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_memory_address(data.data());
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelTransferSize) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_transfer_size(peripherals::dma::channel3, 1234));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_transfer_size(1234);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelMemoryIncrement) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_memory_increment(peripherals::dma::channel3, true));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_memory_increment(true);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelPeripheralIncrement) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_peripheral_increment(peripherals::dma::channel3, true));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_peripheral_increment(true);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelDirection) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_,
                set_direction(peripherals::dma::channel3,
                              peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_direction(peripherals::dma::direction::memory_to_peripheral);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelRequestMapping) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_,
                set_request_mapping(peripherals::dma::channel3, peripherals::dma::request1));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_request_mapping(peripherals::dma::request1);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelSetM2M) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_m2m(peripherals::dma::channel3, true));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_m2m(true);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelSetM2MSource) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_m2m_source(peripherals::dma::channel3, 0xdeadc0de));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_m2m_source(0xdeadc0de);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelSetM2MPointerSource) {
    std::array<std::uint32_t, 10> data{};
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(
        dma_ll_,
        set_m2m_source(peripherals::dma::channel3, std::bit_cast<std::uint32_t>(data.data())));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_m2m_source(data.data());
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelSetM2MDestination) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_, set_m2m_destination(peripherals::dma::channel3, 0xdeadc0de));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_m2m_destination(0xdeadc0de);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, SetChannelSetM2MPointerDestination) {
    std::array<std::uint32_t, 10> data{};
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(
        dma_ll_,
        set_m2m_destination(peripherals::dma::channel3, std::bit_cast<std::uint32_t>(data.data())));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.set_m2m_destination(data.data());
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, EnableChannelIrq) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_,
                enable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.enable_irq(peripherals::dma::irq::transfer_completed);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, DisableChannelIrq) {
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));
    EXPECT_CALL(dma_ll_,
                disable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto& channel_result = channel.disable_irq(peripherals::dma::irq::transfer_completed);
    EXPECT_THAT(&channel, Eq(&channel_result));
}

TEST_F(DMATest, ChannelTransferSuccess) {
    InSequence s;
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));

    EXPECT_CALL(dma_ll_,
                enable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(dma_ll_,
                enable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_error));
    EXPECT_CALL(dma_ll_, enable_channel(peripherals::dma::channel3));

    EXPECT_CALL(*flags::flags_vector[notify_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));

    EXPECT_CALL(dma_ll_, disable_channel(peripherals::dma::channel3));
    EXPECT_CALL(dma_ll_,
                disable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(dma_ll_,
                disable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_error));

    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto transfer_result = channel.transfer();
    EXPECT_THAT(transfer_result, Eq(drivers::dma::status::ok));
}

TEST_F(DMATest, ChannelTransferTimeout) {
    InSequence s;
    EXPECT_CALL(*flags::flags_vector[channel_index], wait(0x4, true, satos::clock::duration::max()))
        .WillOnce(Return(0x4));

    EXPECT_CALL(dma_ll_,
                enable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(dma_ll_,
                enable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_error));
    EXPECT_CALL(dma_ll_, enable_channel(peripherals::dma::channel3));

    EXPECT_CALL(*flags::flags_vector[notify_index], wait(0x4, true, 1000_ms))
        .WillOnce(Return(std::nullopt));

    EXPECT_CALL(dma_ll_, disable_channel(peripherals::dma::channel3));
    EXPECT_CALL(dma_ll_,
                disable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(dma_ll_,
                disable_irq(peripherals::dma::channel3, peripherals::dma::irq::transfer_error));

    EXPECT_CALL(dma_ll_, reset_channel(peripherals::dma::channel3));

    auto result = dma_.request(peripherals::dma::channel3);
    EXPECT_THAT(result.has_value(), Eq(true));

    auto channel = std::move(result.value());
    auto transfer_result = channel.transfer(1000_ms);
    EXPECT_THAT(transfer_result, Eq(drivers::dma::status::timeout));
}

} // namespace
