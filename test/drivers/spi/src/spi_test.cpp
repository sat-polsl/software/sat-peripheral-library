#include <array>
#include <vector>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/clock.h"
#include "satos/mock/event.h"
#include "satos/mock/mutex.h"
#include "satos/mock/this_thread.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/drivers/spi/detail/spi.h"
#include "spl/drivers/spi/enums.h"
#include "spl/peripherals/gpio/mock/gpio.h"
#include "spl/peripherals/nvic/enums.h"
#include "spl/peripherals/nvic/mock/nvic.h"
#include "spl/peripherals/spi/mock/spi.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::spi;
using namespace satos::chrono_literals;

using spi_type = spl::drivers::spi::detail::spi<spl::peripherals::spi::mock::spi,
                                                spl::peripherals::gpio::mock::gpio,
                                                spl::peripherals::nvic::mock::nvic,
                                                spl::peripherals::nvic::irq::spi2>;

struct SpiTest : Test {
    SpiTest();

    NiceMock<spl::peripherals::spi::mock::spi> spi_;
    NiceMock<spl::peripherals::nvic::mock::nvic> nvic_;
    NiceMock<spl::peripherals::gpio::mock::gpio> gpio_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
    satos::mock::this_thread& this_thread_{satos::mock::this_thread::instance()};
};

SpiTest::SpiTest() {
    spl::peripherals::spi::mock::setup_default_return_value(spi_);
    spl::peripherals::nvic::mock::setup_default_return_value(nvic_);
    satos::mock::clock::reset();
    satos::mock::this_thread::reset();
}

TEST_F(SpiTest, Initialize) {
    spi_type spi{spi_, nvic_};

    EXPECT_CALL(spi_, set_slave_management(spl::peripherals::spi::slave_management::software));
    EXPECT_CALL(spi_, set_mode(spl::peripherals::spi::mode::master));
    EXPECT_CALL(spi_, set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8));
    EXPECT_CALL(spi_, set_internal_slave_select(true));

    spi.initialize();
}

TEST_F(SpiTest, ReadSuccess) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));
    EXPECT_CALL(gpio_, set());

    std::array<std::byte, 2> buffer{};

    auto result = spi.read(gpio_, buffer);
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(SpiTest, ReadEmptyBuffer) {
    spi_type spi(spi_, nvic_);

    std::array<std::byte, 2> buffer{};

    auto result = spi.read(gpio_, std::span(buffer.data(), 0));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(SpiTest, ReadEventTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));
    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));

    std::array<std::byte, 2> buffer{};

    auto result = spi.read(gpio_, buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiTest, ReadMutexTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = spi.read(gpio_, buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(SpiTest, WriteSuccess) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    EXPECT_CALL(this_thread_, sleep_for(10_ms));

    EXPECT_CALL(gpio_, set());
    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = spi.write(gpio_, buffer);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiTest, WriteMutexTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = spi.write(gpio_, buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(SpiTest, WriteEventTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::tx_buffer_empty));

    std::array<std::byte, 2> buffer{};

    auto result = spi.write(gpio_, buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(SpiTest, WriteReadSuccess) {
    spi_type spi(spi_, nvic_);

    InSequence s;

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    EXPECT_CALL(this_thread_, sleep_for(10_ms));
    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));
    EXPECT_CALL(gpio_, set());

    std::array<std::byte, 2> write_buffer{};
    std::array<std::byte, 2> read_buffer{};

    auto result = spi.write_read(gpio_, write_buffer, read_buffer);
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(SpiTest, WriteReadReadBufferEmpty) {
    spi_type spi(spi_, nvic_);
    std::array<std::byte, 2> write_buffer{};

    auto result = spi.write_read(gpio_, write_buffer, std::span(write_buffer.data(), 0));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(SpiTest, WriteReadWriteBufferEmpty) {
    spi_type spi(spi_, nvic_);
    std::array<std::byte, 2> read_buffer{};

    auto result = spi.write_read(gpio_, std::span(read_buffer.data(), 0), read_buffer);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(SpiTest, WriteReadMutexTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> write_buffer{};
    std::array<std::byte, 2> read_buffer{};

    auto result = spi.write_read(gpio_, write_buffer, read_buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result, Eq(satext::unexpected(status::timeout)));
}

TEST_F(SpiTest, WriteReadEnableEventTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));

    std::array<std::byte, 2> write_buffer{};
    std::array<std::byte, 2> read_buffer{};

    auto result = spi.write_read(gpio_, write_buffer, read_buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result, Eq(satext::unexpected(status::timeout)));
}

TEST_F(SpiTest, WriteReadIrqEventTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(true));

    EXPECT_CALL(this_thread_, sleep_for(10_ms));
    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));
    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));

    std::array<std::byte, 2> write_buffer{};
    std::array<std::byte, 2> read_buffer{};

    auto result = spi.write_read(gpio_, write_buffer, read_buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result, Eq(satext::unexpected(status::timeout)));
}

TEST_F(SpiTest, TransferSuccess) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));
    EXPECT_CALL(gpio_, set());

    std::array<std::byte, 2> buffer{};

    auto result = spi.transfer(gpio_, buffer);
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(SpiTest, TransferBufferEmpty) {
    spi_type spi(spi_, nvic_);

    std::array<std::byte, 2> buffer{};

    auto result = spi.transfer(gpio_, std::span<std::byte>(buffer.data(), 0));
    ASSERT_THAT(result, Eq(satext::unexpected(status::buffer_is_empty)));
}

TEST_F(SpiTest, TransferMutexTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = spi.transfer(gpio_, buffer, 1_s);
    ASSERT_THAT(result, Eq(satext::unexpected(status::timeout)));
}

TEST_F(SpiTest, TransferEventTimeout) {
    spi_type spi(spi_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[0], try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*satos::mock::event::events[0], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty));
    EXPECT_CALL(spi_, disable_irq(spl::peripherals::spi::irq::tx_buffer_empty));
    EXPECT_CALL(spi_, disable());

    std::array<std::byte, 2> buffer{};

    auto result = spi.transfer(gpio_, buffer, satos::chrono::seconds(1));
    ASSERT_THAT(result, Eq(satext::unexpected(status::timeout)));
}

} // namespace
