#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/clock.h"
#include "spl/drivers/dma/mock/dma.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/drivers/spi/detail/spi_dma_vectored.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/gpio/mock/gpio.h"
#include "spl/peripherals/spi/enums.h"
#include "spl/peripherals/spi/mock/spi.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::spi;
using namespace satos::chrono_literals;

constexpr auto rx_channel = spl::peripherals::dma::channel1;
constexpr auto tx_channel = spl::peripherals::dma::channel2;

using spi_type = detail::spi_dma_vectored<spl::peripherals::spi::mock::spi,
                                          spl::peripherals::gpio::mock::gpio,
                                          spl::drivers::dma::mock::dma,
                                          spl::peripherals::dma::peripheral::spi2,
                                          rx_channel,
                                          tx_channel,
                                          spl::peripherals::dma::request1>;

static_assert(concepts::spi_vectored<spi_type, spl::peripherals::gpio::mock::gpio>);

struct SpiDmaVectoredTest : Test {
    SpiDmaVectoredTest();
    NiceMock<spl::peripherals::spi::mock::spi> spi_;
    NiceMock<spl::drivers::dma::mock::dma> dma_;
    NiceMock<spl::peripherals::gpio::mock::gpio> gpio_;
    StrictMock<spl::drivers::dma::mock::detail::dma_channel> tx_dma_channel_;
    StrictMock<spl::drivers::dma::mock::detail::dma_channel> rx_dma_channel_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

SpiDmaVectoredTest::SpiDmaVectoredTest() {
    spl::peripherals::spi::mock::setup_default_return_value(spi_);
}

TEST_F(SpiDmaVectoredTest, Initialize) {
    spi_type spi{spi_, dma_};

    InSequence s;
    EXPECT_CALL(spi_, set_slave_management(spl::peripherals::spi::slave_management::software));
    EXPECT_CALL(spi_, set_mode(spl::peripherals::spi::mode::master));
    EXPECT_CALL(spi_, set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8));
    EXPECT_CALL(spi_, set_internal_slave_select(true));

    spi.initialize();
}

TEST_F(SpiDmaVectoredTest, WriteSingleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    auto result = spi.write(gpio_, input);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, WriteMultipleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<const std::byte>, 2> input = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(second_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(second_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    auto result = spi.write(gpio_, input);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, WriteBufferEmptyFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};
    auto empty = std::span(input.data(), 0);

    auto result = spi.write(gpio_, empty);

    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaVectoredTest, WriteRequestTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.write(gpio_, input, 1000_ms);

    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteTransferTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    auto result = spi.write(gpio_, input, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteMultipleTransferTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<const std::byte>, 2> input = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    auto result = spi.write(gpio_, input, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteTransferErrorFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    auto result = spi.write(gpio_, input);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::transfer_error));
}

TEST_F(SpiDmaVectoredTest, ReadSingleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.read(gpio_, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, ReadMultipleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed))
        .Times(1);
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error)).Times(1);
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(second_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(second_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed))
        .Times(1);
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error)).Times(1);
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(second_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.read(gpio_, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, ReadBufferEmptyFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input = {buffer};
    auto empty = std::span(input.data(), 0);

    auto result = spi.read(gpio_, empty);

    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaVectoredTest, ReadRequestTxChannelTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.read(gpio_, input, 1000_ms);

    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(SpiDmaVectoredTest, ReadRequestRxChannelTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.read(gpio_, input, 1000_ms);

    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(SpiDmaVectoredTest, ReadTransferTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.read(gpio_, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, ReadMultipleTransferTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.read(gpio_, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, ReadTransferErrorFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.read(gpio_, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::transfer_error));
}

TEST_F(SpiDmaVectoredTest, WriteReadSingleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(out_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(out_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(out_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, WriteReadMultipleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer1{};
    std::array<std::byte, 8> out_buffer2{};
    std::array<std::span<std::byte>, 2> output = {out_buffer1, out_buffer2};
    std::array<std::byte, 6> in_buffer1{};
    std::array<std::byte, 10> in_buffer2{};
    std::array<std::span<const std::byte>, 2> input = {in_buffer1, in_buffer2};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer1.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer1.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer2.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer2.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(out_buffer1.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(out_buffer1.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(out_buffer1.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(out_buffer2.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(out_buffer2.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(out_buffer2.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, WriteReadBufferEmptyFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};
    auto empty = std::span(input.data(), 0);

    auto result = spi.write_read(gpio_, empty, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::buffer_is_empty));
}

TEST_F(SpiDmaVectoredTest, WriteReadTxChannelRequestTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.write_read(gpio_, input, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteReadRxChannelRequestTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));
    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.write_read(gpio_, input, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteReadSingleWriteTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteReadMultipleWriteTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer1{};
    std::array<std::byte, 8> out_buffer2{};
    std::array<std::span<std::byte>, 2> output = {out_buffer1, out_buffer2};
    std::array<std::byte, 6> in_buffer1{};
    std::array<std::byte, 10> in_buffer2{};
    std::array<std::span<const std::byte>, 2> input = {in_buffer1, in_buffer2};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer1.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer1.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteReadSingleReadTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());

    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(out_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(out_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(out_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteReadMultipleReadTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer1{};
    std::array<std::byte, 8> out_buffer2{};
    std::array<std::span<std::byte>, 2> output = {out_buffer1, out_buffer2};
    std::array<std::byte, 6> in_buffer1{};
    std::array<std::byte, 10> in_buffer2{};
    std::array<std::span<const std::byte>, 2> input = {in_buffer1, in_buffer2};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());

    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer1.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer1.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer2.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer2.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(out_buffer1.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(out_buffer1.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(out_buffer1.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, WriteReadSingleWriteErrorFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::transfer_error));
}

TEST_F(SpiDmaVectoredTest, WriteReadSingleReadErrorFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());

    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(in_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(in_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(tx_dma_channel_, set_memory_address(_));
    EXPECT_CALL(tx_dma_channel_, set_memory_increment(false));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, enable()).Times(1);

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(out_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(out_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_completed));
    EXPECT_CALL(rx_dma_channel_, enable_irq(spl::peripherals::dma::irq::transfer_error));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(out_buffer.size()));
    EXPECT_CALL(tx_dma_channel_, enable());

    EXPECT_CALL(rx_dma_channel_, wait_for_completion(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(tx_dma_channel_, disable());
    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.write_read(gpio_, input, output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::transfer_error));
}

TEST_F(SpiDmaVectoredTest, TransferSingleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));

    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(false));

    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.transfer(gpio_, input_output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, TransferMultipleSuccess) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> input_output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable()).Times(1);
    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));

    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(false));

    EXPECT_CALL(rx_dma_channel_, disable()).Times(1);

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(second_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(second_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable()).Times(1);
    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(second_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(second_buffer.size()));

    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::ok));

    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));

    EXPECT_CALL(rx_dma_channel_, disable()).Times(1);

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.transfer(gpio_, input_output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::ok));
}

TEST_F(SpiDmaVectoredTest, TransferBufferEmptyFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};
    auto empty = std::span(input_output.data(), 0);

    auto result = spi.transfer(gpio_, empty);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::buffer_is_empty));
}

TEST_F(SpiDmaVectoredTest, TransferRequestTxChannelTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};
    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.transfer(gpio_, input_output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, TransferRequestRxChannelTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};
    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [](auto, auto) { return satext::unexpected{spl::drivers::dma::status::timeout}; }));

    auto result = spi.transfer(gpio_, input_output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, TransferTransferTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};
    InSequence s;

    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));

    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(false));

    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.transfer(gpio_, input_output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, TransferMultipleTransferTimeoutFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> input_output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(clock_, now()).WillOnce(Return(satos::clock::time_point{1000_ms}));
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point{2000_ms}))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(first_buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_,
                set_memory_address(std::bit_cast<std::uint32_t>(first_buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(first_buffer.size()));

    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point{2000_ms}))
        .WillOnce(Return(spl::drivers::dma::status::timeout));

    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(false));

    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.transfer(gpio_, input_output, 1000_ms);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::timeout));
}

TEST_F(SpiDmaVectoredTest, TransferTransferErrorFailure) {
    spi_type spi{spi_, dma_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};

    InSequence s;
    EXPECT_CALL(dma_, request(tx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&tx_dma_channel_}; }));

    EXPECT_CALL(dma_, request(rx_channel, satos::clock::time_point::max()))
        .WillOnce(Invoke(
            [this](auto, auto) { return spl::drivers::dma::mock::dma_channel{&rx_dma_channel_}; }));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));

    EXPECT_CALL(tx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(tx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(tx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(tx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(rx_dma_channel_, set_memory_increment(true));
    EXPECT_CALL(rx_dma_channel_, set_peripheral_address(spl::peripherals::dma::peripheral::spi2));
    EXPECT_CALL(rx_dma_channel_,
                set_direction(spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(rx_dma_channel_, set_request_mapping(spl::peripherals::dma::request1));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(rx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(rx_dma_channel_, set_transfer_size(buffer.size()));
    EXPECT_CALL(rx_dma_channel_, enable());
    EXPECT_CALL(tx_dma_channel_, set_memory_address(std::bit_cast<std::uint32_t>(buffer.data())));
    EXPECT_CALL(tx_dma_channel_, set_transfer_size(buffer.size()));

    EXPECT_CALL(tx_dma_channel_, transfer(satos::clock::time_point::max()))
        .WillOnce(Return(spl::drivers::dma::status::transfer_error));

    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).Times(1).WillOnce(Return(false));

    EXPECT_CALL(rx_dma_channel_, disable());

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable()).Times(1);
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));

    auto result = spi.transfer(gpio_, input_output);
    ASSERT_THAT(result, Eq(spl::drivers::spi::status::transfer_error));
}

} // namespace
