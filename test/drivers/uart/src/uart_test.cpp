#include <array>
#include <vector>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/chrono.h"
#include "satos/mock/clock.h"
#include "satos/mock/event.h"
#include "satos/mock/mutex.h"
#include "spl/drivers/uart/uart.h"
#include "spl/peripherals/nvic/mock/nvic.h"
#include "spl/peripherals/uart/mock/uart.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::uart;
using namespace satos::chrono_literals;

static constexpr std::size_t write_index = 0;
static constexpr std::size_t read_index = 1;

using uart_type = detail::uart<spl::peripherals::uart::mock::uart,
                               spl::peripherals::nvic::mock::nvic,
                               spl::peripherals::nvic::irq::uart1>;

struct UartTest : Test {
    UartTest();

    NiceMock<spl::peripherals::uart::mock::uart> uart_;
    NiceMock<spl::peripherals::nvic::mock::nvic> nvic_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};

UartTest::UartTest() {
    spl::peripherals::uart::mock::setup_default_return_value(uart_);
    spl::peripherals::nvic::mock::setup_default_return_value(nvic_);
    satos::mock::clock::reset();
}

TEST_F(UartTest, ReadSuccess) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[read_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(uart_, enable_irq(spl::peripherals::uart::irq::rx_not_empty));
    EXPECT_CALL(*satos::mock::event::events[read_index],
                wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = uart.read(buffer);
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(UartTest, ReadEmptyBuffer) {
    uart_type uart(uart_, nvic_);

    std::array<std::byte, 2> buffer{};

    auto result = uart.read(std::span(buffer.data(), 0));
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(UartTest, ReadMutexTimeout) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[read_index],
                try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = uart.read(buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(UartTest, ReadEventTimeout) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[read_index],
                try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(uart_, enable_irq(spl::peripherals::uart::irq::rx_not_empty));
    EXPECT_CALL(*satos::mock::event::events[read_index], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));

    std::array<std::byte, 2> buffer{};

    auto result = uart.read(buffer, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(UartTest, ReadUntilSuccess) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[read_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(uart_, enable_irq(spl::peripherals::uart::irq::rx_not_empty));
    EXPECT_CALL(*satos::mock::event::events[read_index],
                wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = uart.read_until(buffer, std::byte{});
    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(UartTest, ReadUntilEmptyBuffer) {
    uart_type uart(uart_, nvic_);

    std::array<std::byte, 2> buffer{};

    auto result = uart.read_until(std::span(buffer.data(), 0), std::byte{});
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(UartTest, ReadUntilMutexTimeout) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[read_index],
                try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = uart.read_until(buffer, std::byte{}, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(UartTest, ReadUntilEventTimeout) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[read_index],
                try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(uart_, enable_irq(spl::peripherals::uart::irq::rx_not_empty));
    EXPECT_CALL(*satos::mock::event::events[read_index], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));

    std::array<std::byte, 2> buffer{};

    auto result = uart.read_until(buffer, std::byte{}, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(UartTest, WriteSuccess) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[write_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(uart_, enable_irq(spl::peripherals::uart::irq::tx_empty));
    EXPECT_CALL(*satos::mock::event::events[write_index],
                wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));

    std::array<std::byte, 2> buffer{};

    auto result = uart.write(buffer);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(UartTest, WriteMutexTimeout) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[write_index],
                try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return false; }));

    std::array<std::byte, 2> buffer{};

    auto result = uart.write(buffer, 1_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(UartTest, WriteEventTimeout) {
    uart_type uart(uart_, nvic_);

    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[write_index],
                try_lock_until(satos::clock::time_point(1_s)))
        .WillOnce(Invoke([](const auto& timeout) { return true; }));

    EXPECT_CALL(uart_, enable_irq(spl::peripherals::uart::irq::tx_empty));
    EXPECT_CALL(*satos::mock::event::events[write_index], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));

    std::array<std::byte, 2> buffer{};

    auto result = uart.write(buffer, 1_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

} // namespace
