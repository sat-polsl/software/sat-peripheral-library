#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "baremetal/mock/irq_event.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/drivers/spi/detail/spi_dma_lite.h"
#include "spl/peripherals/dma/mock/dma.h"
#include "spl/peripherals/gpio/mock/gpio.h"
#include "spl/peripherals/nvic/enums.h"
#include "spl/peripherals/nvic/mock/nvic.h"
#include "spl/peripherals/spi/mock/spi.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::spi;

constexpr auto rx_channel = spl::peripherals::dma::channel1;
constexpr auto tx_channel = spl::peripherals::dma::channel2;
constexpr auto rx_irq = spl::peripherals::nvic::irq::dma1_channel1;
constexpr auto tx_irq = spl::peripherals::nvic::irq::dma1_channel2;

using spi_type = detail::spi_dma_lite<spl::peripherals::spi::mock::spi,
                                      spl::peripherals::dma::mock::dma,
                                      spl::peripherals::gpio::mock::gpio,
                                      spl::peripherals::nvic::mock::nvic,
                                      baremetal::mock::irq_event,
                                      spl::peripherals::dma::peripheral::spi1,
                                      rx_channel,
                                      tx_channel,
                                      rx_irq,
                                      tx_irq,
                                      spl::peripherals::dma::request1>;

static_assert(concepts::spi<spi_type, spl::peripherals::gpio::mock::gpio>);

struct SpiDmaLiteTest : Test {
    SpiDmaLiteTest();

    NiceMock<spl::peripherals::spi::mock::spi> spi_;
    NiceMock<spl::peripherals::nvic::mock::nvic> nvic_;
    NiceMock<spl::peripherals::dma::mock::dma> dma_;
    NiceMock<spl::peripherals::gpio::mock::gpio> gpio_;
};

SpiDmaLiteTest::SpiDmaLiteTest() {
    spl::peripherals::spi::mock::setup_default_return_value(spi_);
    spl::peripherals::nvic::mock::setup_default_return_value(nvic_);
    spl::peripherals::dma::mock::setup_default_return_value(dma_);
}

TEST_F(SpiDmaLiteTest, Initialize) {
    spi_type spi{spi_, dma_, nvic_};

    InSequence s;
    EXPECT_CALL(spi_, set_slave_management(spl::peripherals::spi::slave_management::software));
    EXPECT_CALL(spi_, set_mode(spl::peripherals::spi::mode::master));
    EXPECT_CALL(spi_, set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8));
    EXPECT_CALL(spi_, set_internal_slave_select(true));

    spi.initialize();
}

TEST_F(SpiDmaLiteTest, WriteSuccess) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 4> buffer{};

    InSequence s;
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, buffer.size()));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto status = spi.write(gpio_, buffer);

    ASSERT_THAT(status, Eq(status::ok));
}

TEST_F(SpiDmaLiteTest, WriteBufferEmpty) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 1> buffer{};

    auto status = spi.write(gpio_, std::span(buffer.data(), 0));

    ASSERT_THAT(status, Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteTest, ReadSuccess) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 4> buffer{};

    InSequence s;
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, buffer.size()));
    EXPECT_CALL(nvic_, enable_irq(rx_irq));
    EXPECT_CALL(spi_, set_receive_only_mode());

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, set_full_duplex_mode());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(dma_, reset_channel(rx_channel));

    auto result = spi.read(gpio_, buffer);

    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(SpiDmaLiteTest, ReadBufferEmpty) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 1> buffer{};

    auto result = spi.read(gpio_, std::span(buffer.data(), 0));

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteTest, WriteReadSuccess) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 2> input_buffer{};
    std::array<std::byte, 2> output_buffer{};

    InSequence s;
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, input_buffer.size()));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, output_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));
    EXPECT_CALL(nvic_, enable_irq(rx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(spi_, set_receive_only_mode());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, set_full_duplex_mode());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.write_read(gpio_, input_buffer, output_buffer);

    ASSERT_THAT(result.has_value(), Eq(true));
}

TEST_F(SpiDmaLiteTest, WriteReadInputBufferEmpty) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 1> buffer{};

    auto result = spi.write_read(gpio_, std::span(buffer.data(), 0), std::span(buffer.data(), 1));

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteTest, WriteReadOutputBufferEmpty) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 1> buffer{};

    auto result = spi.write_read(gpio_, std::span(buffer.data(), 1), std::span(buffer.data(), 0));

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteTest, TransferSuccess) {
    spi_type spi{spi_, dma_, nvic_};
    std::array<std::byte, 4> buffer{};

    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));

    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));

    EXPECT_CALL(nvic_, enable_irq(rx_irq));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.transfer(gpio_, buffer);

    ASSERT_THAT(result.has_value(), Eq(true));
}

} // namespace
