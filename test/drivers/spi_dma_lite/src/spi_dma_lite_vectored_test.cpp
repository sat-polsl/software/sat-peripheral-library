#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "baremetal/mock/irq_event.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/drivers/spi/detail/spi_dma_lite_vectored.h"
#include "spl/peripherals/dma/mock/dma.h"
#include "spl/peripherals/gpio/mock/gpio.h"
#include "spl/peripherals/nvic/enums.h"
#include "spl/peripherals/nvic/mock/nvic.h"
#include "spl/peripherals/spi/mock/spi.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::spi;

constexpr auto rx_channel = spl::peripherals::dma::channel1;
constexpr auto tx_channel = spl::peripherals::dma::channel2;
constexpr auto rx_irq = spl::peripherals::nvic::irq::dma1_channel1;
constexpr auto tx_irq = spl::peripherals::nvic::irq::dma1_channel2;

using spi_type = detail::spi_dma_lite_vectored<spl::peripherals::spi::mock::spi,
                                               spl::peripherals::dma::mock::dma,
                                               spl::peripherals::gpio::mock::gpio,
                                               spl::peripherals::nvic::mock::nvic,
                                               baremetal::mock::irq_event,
                                               spl::peripherals::dma::peripheral::spi1,
                                               rx_channel,
                                               tx_channel,
                                               rx_irq,
                                               tx_irq,
                                               spl::peripherals::dma::request1>;

static_assert(concepts::spi_vectored<spi_type, spl::peripherals::gpio::mock::gpio>);

struct SpiDmaLiteVectoredTest : Test {
    SpiDmaLiteVectoredTest();

    NiceMock<spl::peripherals::spi::mock::spi> spi_;
    NiceMock<spl::peripherals::nvic::mock::nvic> nvic_;
    NiceMock<spl::peripherals::dma::mock::dma> dma_;
    NiceMock<spl::peripherals::gpio::mock::gpio> gpio_;
};

SpiDmaLiteVectoredTest::SpiDmaLiteVectoredTest() {
    spl::peripherals::spi::mock::setup_default_return_value(spi_);
    spl::peripherals::nvic::mock::setup_default_return_value(nvic_);
    spl::peripherals::dma::mock::setup_default_return_value(dma_);
}

TEST_F(SpiDmaLiteVectoredTest, Initialize) {
    spi_type spi{spi_, dma_, nvic_};

    InSequence s;
    EXPECT_CALL(spi_, set_slave_management(spl::peripherals::spi::slave_management::software));
    EXPECT_CALL(spi_, set_mode(spl::peripherals::spi::mode::master));
    EXPECT_CALL(spi_, set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8));
    EXPECT_CALL(spi_, set_internal_slave_select(true));

    spi.initialize();
}

TEST_F(SpiDmaLiteVectoredTest, WriteSingleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};

    InSequence s;
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(nvic_, disable_irq(tx_irq));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.write(gpio_, input);

    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, WriteMultipleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<const std::byte>, 2> input = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, first_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(true));
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, second_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(nvic_, disable_irq(tx_irq));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.write(gpio_, input);

    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, WriteBufferEmptyFailure) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<const std::byte>, 1> input = {buffer};
    auto empty = std::span(input.data(), 0);

    auto result = spi.write(gpio_, empty);

    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteVectoredTest, ReadSingleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};

    InSequence s;
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_, set_memory_increment(rx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, false));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(rx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(dma_, disable_channel(tx_channel));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(nvic_, disable_irq(rx_irq));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.read(gpio_, output);

    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, ReadMultipleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_, set_memory_increment(rx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, false));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(rx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, first_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, first_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(dma_, disable_channel(tx_channel));
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, second_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, second_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(dma_, disable_channel(tx_channel));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(nvic_, disable_irq(rx_irq));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.read(gpio_, output);

    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, ReadBufferEmptyFailure) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> output = {buffer};
    auto empty = std::span(output.data(), 0);

    auto result = spi.read(gpio_, empty);

    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteVectoredTest, WriteReadSingleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    InSequence s;
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_memory_increment(rx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(dma_, set_transfer_size(tx_channel, in_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(nvic_, disable_irq(tx_irq));
    EXPECT_CALL(nvic_, enable_irq(rx_irq));
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, false));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(dma_, set_transfer_size(rx_channel, out_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, out_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(dma_, disable_channel(tx_channel));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(nvic_, disable_irq(rx_irq));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.write_read(gpio_, input, output);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, WriteReadMultipleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> out_buffer1{};
    std::array<std::byte, 8> out_buffer2{};
    std::array<std::span<std::byte>, 2> output = {out_buffer1, out_buffer2};
    std::array<std::byte, 6> in_buffer1{};
    std::array<std::byte, 10> in_buffer2{};
    std::array<std::span<const std::byte>, 2> input = {in_buffer1, in_buffer2};

    InSequence s;
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(dma_, set_memory_increment(rx_channel, true));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(dma_, set_transfer_size(tx_channel, in_buffer1.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, in_buffer2.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(false));
    EXPECT_CALL(spi_, read());
    EXPECT_CALL(spi_, is_rx_fifo_empty()).WillOnce(Return(true));
    EXPECT_CALL(nvic_, disable_irq(tx_irq));
    EXPECT_CALL(nvic_, enable_irq(rx_irq));
    EXPECT_CALL(dma_, set_memory_increment(tx_channel, false));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::concepts::dma::rx));
    EXPECT_CALL(spi_, enable());

    EXPECT_CALL(dma_, set_transfer_size(rx_channel, out_buffer1.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, out_buffer1.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(dma_, disable_channel(tx_channel));
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, out_buffer2.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, out_buffer2.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(dma_, disable_channel(tx_channel));

    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(nvic_, disable_irq(rx_irq));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.write_read(gpio_, input, output);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, WriteReadEmptyBufferFailure) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> out_buffer{};
    std::array<std::span<std::byte>, 1> output = {out_buffer};
    auto empty = std::span(output.data(), 0);
    std::array<std::byte, 6> in_buffer{};
    std::array<std::span<const std::byte>, 1> input = {in_buffer};

    auto result = spi.write_read(gpio_, input, empty);
    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

TEST_F(SpiDmaLiteVectoredTest, TransferSingleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};

    InSequence s;
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(nvic_, disable_irq(tx_irq));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.transfer(gpio_, input_output);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, TransferMultipleSuccess) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> first_buffer{};
    std::array<std::byte, 8> second_buffer{};
    std::array<std::span<std::byte>, 2> input_output = {first_buffer, second_buffer};

    InSequence s;
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::rx));
    EXPECT_CALL(dma_,
                set_direction(rx_channel, spl::peripherals::dma::direction::peripheral_to_memory));
    EXPECT_CALL(dma_,
                set_direction(tx_channel, spl::peripherals::dma::direction::memory_to_peripheral));
    EXPECT_CALL(spi_, enable_dma(spl::peripherals::spi::dma::tx));
    EXPECT_CALL(nvic_, enable_irq(tx_irq));

    EXPECT_CALL(gpio_, clear());
    EXPECT_CALL(spi_, enable());
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, first_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, first_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(dma_, set_transfer_size(rx_channel, second_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(rx_channel));
    EXPECT_CALL(dma_, set_transfer_size(tx_channel, second_buffer.size()));
    EXPECT_CALL(dma_, enable_channel(tx_channel));
    EXPECT_CALL(*baremetal::mock::irq_event::events[0], wait());
    EXPECT_CALL(spi_, is_busy()).WillOnce(Return(false));
    EXPECT_CALL(gpio_, set());

    EXPECT_CALL(spi_, disable());
    EXPECT_CALL(nvic_, disable_irq(tx_irq));
    EXPECT_CALL(dma_, reset_channel(rx_channel));
    EXPECT_CALL(dma_, reset_channel(tx_channel));

    auto result = spi.transfer(gpio_, input_output);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(SpiDmaLiteVectoredTest, TransferEmptyBufferFailure) {
    spi_type spi{spi_, dma_, nvic_};

    std::array<std::byte, 4> buffer{};
    std::array<std::span<std::byte>, 1> input_output = {buffer};
    auto empty = std::span(input_output.data(), 0);

    auto result = spi.transfer(gpio_, empty);
    ASSERT_THAT(result, Eq(status::buffer_is_empty));
}

} // namespace
