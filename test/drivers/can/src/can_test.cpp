#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satos/mock/clock.h"
#include "satos/mock/event.h"
#include "satos/mock/mutex.h"
#include "spl/drivers/can/concepts/can.h"
#include "spl/drivers/can/detail/can.h"
#include "spl/peripherals/can/enums.h"
#include "spl/peripherals/can/mock/can.h"
#include "spl/peripherals/can/types.h"
#include "spl/peripherals/nvic/enums.h"
#include "spl/peripherals/nvic/mock/nvic.h"

namespace {

using namespace ::testing;
using namespace spl::drivers::can;
using namespace satos::chrono_literals;

constexpr uint8_t tx_index = 0;
constexpr uint8_t rx0_index = 1;
constexpr uint8_t rx1_index = 2;

using can_type = detail::can<spl::peripherals::can::mock::can,
                             spl::peripherals::nvic::mock::nvic,
                             spl::peripherals::nvic::irq::can1_tx,
                             spl::peripherals::nvic::irq::can1_rx0,
                             spl::peripherals::nvic::irq::can1_rx1>;

struct CANTest : Test {
    CANTest();
    NiceMock<spl::peripherals::can::mock::can> can_;
    NiceMock<spl::peripherals::nvic::mock::nvic> nvic_;
    satos::mock::clock& clock_{satos::mock::clock::instance()};
};
CANTest::CANTest() {
    spl::peripherals::nvic::mock::setup_default_return_value(nvic_);
    satos::mock::clock::reset();
}

TEST_F(CANTest, WriteSuccessMailboxFull) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[tx_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(can_, is_tx_mailbox_empty()).WillOnce(Return(false));
    EXPECT_CALL(can_, enable_irq(spl::peripherals::can::irq::tx_mailbox_released));
    EXPECT_CALL(*satos::mock::event::events[tx_index], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));
    EXPECT_CALL(can_, disable_irq(spl::peripherals::can::irq::tx_mailbox_released));

    EXPECT_CALL(can_, write(_)).WillOnce(Invoke([](const spl::peripherals::can::message& msg) {
        EXPECT_THAT(msg.id, Eq(0xdead));
        EXPECT_THAT(msg.size, Eq(2));
        EXPECT_THAT(msg.is_extended, Eq(true));
        EXPECT_THAT(msg.data[0], Eq(std::byte{0xc0}));
        EXPECT_THAT(msg.data[1], Eq(std::byte{0xde}));
        return true;
    }));

    spl::peripherals::can::message msg{
        .id = 0xdead, .data = {std::byte{0xc0}, std::byte{0xde}}, .size = 2, .is_extended = true};
    auto result = can.write(msg);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(CANTest, WriteSuccessMailboxEmpty) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[tx_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(can_, is_tx_mailbox_empty()).WillOnce(Return(true));

    EXPECT_CALL(can_, write(_)).WillOnce(Invoke([](const spl::peripherals::can::message& msg) {
        EXPECT_THAT(msg.id, Eq(0xdead));
        EXPECT_THAT(msg.size, Eq(2));
        EXPECT_THAT(msg.is_extended, Eq(true));
        EXPECT_THAT(msg.data[0], Eq(std::byte{0xc0}));
        EXPECT_THAT(msg.data[1], Eq(std::byte{0xde}));
        return true;
    }));

    spl::peripherals::can::message msg{
        .id = 0xdead, .data = {std::byte{0xc0}, std::byte{0xde}}, .size = 2, .is_extended = true};
    auto result = can.write(msg);
    ASSERT_THAT(result, Eq(status::ok));
}

TEST_F(CANTest, WriteFailureMutexTimeout) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[tx_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout, Eq(satos::clock::time_point(1_s)));
            return false;
        }));

    spl::peripherals::can::message msg{
        .id = 0xdead, .data = {std::byte{0xc0}, std::byte{0xde}}, .size = 2, .is_extended = true};
    auto result = can.write(msg, 1_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(CANTest, WriteFailureEventTimeout) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[tx_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout, Eq(satos::clock::time_point(1_s)));
            return true;
        }));

    EXPECT_CALL(can_, is_tx_mailbox_empty()).WillOnce(Return(false));
    EXPECT_CALL(can_, enable_irq(spl::peripherals::can::irq::tx_mailbox_released));
    EXPECT_CALL(*satos::mock::event::events[tx_index], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(can_, disable_irq(spl::peripherals::can::irq::tx_mailbox_released));

    spl::peripherals::can::message msg{
        .id = 0xdead, .data = {std::byte{0xc0}, std::byte{0xde}}, .size = 2, .is_extended = true};
    auto result = can.write(msg, 1_s);
    ASSERT_THAT(result, Eq(status::timeout));
}

TEST_F(CANTest, ReadSuccessFifo0) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[rx0_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(can_, enable_irq(spl::peripherals::can::irq::rx0_pending));
    EXPECT_CALL(*satos::mock::event::events[rx0_index], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));
    EXPECT_CALL(can_, disable_irq(spl::peripherals::can::irq::rx0_pending));

    EXPECT_CALL(can_, read(_)).WillOnce(Invoke([](spl::peripherals::can::concepts::rx_fifo fifo) {
        EXPECT_THAT(fifo, Eq(spl::peripherals::can::rx_fifo0));
        return spl::peripherals::can::message{.id = 0xdead,
                                              .data = {std::byte{0xc0}, std::byte{0xde}},
                                              .size = 2,
                                              .is_extended = true,
                                              .matched_filter = spl::peripherals::can::filter0};
    }));

    auto result = can.read(spl::peripherals::can::rx_fifo0);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->id, Eq(0xdead));
    ASSERT_THAT(result->size, Eq(2));
    ASSERT_THAT(result->data[0], Eq(std::byte{0xc0}));
    ASSERT_THAT(result->data[1], Eq(std::byte{0xde}));
    ASSERT_THAT(result->is_extended, Eq(true));
    ASSERT_THAT(result->matched_filter, Eq(spl::peripherals::can::filter0));
}

TEST_F(CANTest, ReadSuccessFifo1) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(satos::clock::duration::max()))
        .WillOnce(Return(satos::clock::time_point::max()));

    EXPECT_CALL(*satos::mock::mutex::mutexes[rx1_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout.time_since_epoch(), Eq(satos::chrono::milliseconds::max()));
            return true;
        }));

    EXPECT_CALL(can_, enable_irq(spl::peripherals::can::irq::rx1_pending));
    EXPECT_CALL(*satos::mock::event::events[rx1_index], wait_until(satos::clock::time_point::max()))
        .WillOnce(Return(true));
    EXPECT_CALL(can_, disable_irq(spl::peripherals::can::irq::rx1_pending));

    EXPECT_CALL(can_, read(_)).WillOnce(Invoke([](spl::peripherals::can::concepts::rx_fifo fifo) {
        EXPECT_THAT(fifo, Eq(spl::peripherals::can::rx_fifo1));
        return spl::peripherals::can::message{.id = 0xdead,
                                              .data = {std::byte{0xc0}, std::byte{0xde}},
                                              .size = 2,
                                              .is_extended = true,
                                              .matched_filter = spl::peripherals::can::filter0};
    }));

    auto result = can.read(spl::peripherals::can::rx_fifo1);
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result->id, Eq(0xdead));
    ASSERT_THAT(result->size, Eq(2));
    ASSERT_THAT(result->data[0], Eq(std::byte{0xc0}));
    ASSERT_THAT(result->data[1], Eq(std::byte{0xde}));
    ASSERT_THAT(result->is_extended, Eq(true));
    ASSERT_THAT(result->matched_filter, Eq(spl::peripherals::can::filter0));
}

TEST_F(CANTest, ReadFailureMutexTimeout) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[rx0_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout, Eq(satos::clock::time_point(1_s)));
            return false;
        }));

    auto result = can.read(spl::peripherals::can::rx_fifo0, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

TEST_F(CANTest, ReadFailureEventTimeout) {
    can_type can(can_, nvic_);

    InSequence s;
    EXPECT_CALL(clock_, from_now(1000_ms)).WillOnce(Return(satos::clock::time_point(1_s)));

    EXPECT_CALL(*satos::mock::mutex::mutexes[rx0_index], try_lock_until(_))
        .WillOnce(Invoke([](const auto& timeout) {
            EXPECT_THAT(timeout, Eq(satos::clock::time_point(1_s)));
            return true;
        }));

    EXPECT_CALL(can_, enable_irq(spl::peripherals::can::irq::rx0_pending));
    EXPECT_CALL(*satos::mock::event::events[rx0_index], wait_until(satos::clock::time_point(1_s)))
        .WillOnce(Return(false));
    EXPECT_CALL(can_, disable_irq(spl::peripherals::can::irq::rx0_pending));

    auto result = can.read(spl::peripherals::can::rx_fifo0, 1_s);
    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(status::timeout));
}

} // namespace
