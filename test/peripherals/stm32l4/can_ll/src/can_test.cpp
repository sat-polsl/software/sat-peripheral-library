#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/can.h"
#include "spl/peripherals/can/can.h"

DEFINE_FFF_GLOBALS
FAKE_VALUE_FUNC(int,
                can_init,
                std::uint32_t,
                bool,
                bool,
                bool,
                bool,
                bool,
                bool,
                std::uint32_t,
                std::uint32_t,
                std::uint32_t,
                std::uint32_t,
                bool,
                bool)
FAKE_VOID_FUNC(can_enable_irq, std::uint32_t, std::uint32_t)
FAKE_VOID_FUNC(can_disable_irq, std::uint32_t, std::uint32_t)
FAKE_VALUE_FUNC(
    int, can_transmit, std::uint32_t, std::uint32_t, bool, bool, std::uint8_t, std::uint8_t*)
FAKE_VOID_FUNC(can_receive,
               std::uint32_t,
               std::uint8_t,
               bool,
               std::uint32_t*,
               bool*,
               bool*,
               std::uint8_t*,
               std::uint8_t*,
               std::uint8_t*,
               std::uint16_t*)
FAKE_VOID_FUNC(
    can_filter_id_mask_32bit_init, std::uint32_t, std::uint32_t, std::uint32_t, std::uint32_t, bool)

namespace {

void can_receive_call(uint32_t canport,
                      uint8_t fifo,
                      bool release,
                      uint32_t* id,
                      bool* ext,
                      bool* rtr,
                      uint8_t* fmi,
                      uint8_t* length,
                      uint8_t* data,
                      uint16_t* timestamp) {
    *id = 0xdead;
    *ext = true;
    *fmi = 5;
    *length = 4;
    data[0] = 0xde;
    data[1] = 0xad;
    data[2] = 0xc0;
    data[3] = 0xde;
}

using namespace spl::peripherals::can;
using namespace testing;

struct CANTest : Test {
    CANTest(){
        RESET_FAKE(can_init)                      //
        RESET_FAKE(can_enable_irq)                //
        RESET_FAKE(can_disable_irq)               //
        RESET_FAKE(can_transmit)                  //
        RESET_FAKE(can_receive)                   //
        RESET_FAKE(can_filter_id_mask_32bit_init) //
    }

    can can_{instance::can2};
    static constexpr auto instance_ = CAN2;
};

TEST_F(CANTest, Initialize) {
    can_.initialize(timings{.sjw = 1, .ts1 = 8, .ts2 = 2, .brp = 4}, options{});
    ASSERT_THAT(can_init_fake.call_count, Eq(1));
    ASSERT_THAT(can_init_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_init_fake.arg1_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg2_val, Eq(true));
    ASSERT_THAT(can_init_fake.arg3_val, Eq(true));
    ASSERT_THAT(can_init_fake.arg4_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg5_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg6_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg7_val, Eq(CAN_BTR_SJW_1TQ));
    ASSERT_THAT(can_init_fake.arg8_val, Eq(CAN_BTR_TS1_8TQ));
    ASSERT_THAT(can_init_fake.arg9_val, Eq(CAN_BTR_TS2_2TQ));
    ASSERT_THAT(can_init_fake.arg10_val, Eq(4));
    ASSERT_THAT(can_init_fake.arg11_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg12_val, Eq(false));
}

TEST_F(CANTest, InitializeOptions) {
    can_.initialize(timings{.sjw = 2, .ts1 = 10, .ts2 = 3, .brp = 5},
                    options{.loopback = loopback_mode::on,
                            .silent = silent_mode::on,
                            .busoff = automatic_busoff::off,
                            .wakeup = automatic_wakeup::off,
                            .retransmission = automatic_retransmission::off,
                            .rx_lock = rx_fifo_lock::on,
                            .tx_priority = tx_fifo_priority::chronological});
    ASSERT_THAT(can_init_fake.call_count, Eq(1));
    ASSERT_THAT(can_init_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_init_fake.arg1_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg2_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg3_val, Eq(false));
    ASSERT_THAT(can_init_fake.arg4_val, Eq(true));
    ASSERT_THAT(can_init_fake.arg5_val, Eq(true));
    ASSERT_THAT(can_init_fake.arg6_val, Eq(true));
    ASSERT_THAT(can_init_fake.arg7_val, Eq(CAN_BTR_SJW_2TQ));
    ASSERT_THAT(can_init_fake.arg8_val, Eq(CAN_BTR_TS1_10TQ));
    ASSERT_THAT(can_init_fake.arg9_val, Eq(CAN_BTR_TS2_3TQ));
    ASSERT_THAT(can_init_fake.arg10_val, Eq(5));
    ASSERT_THAT(can_init_fake.arg11_val, Eq(true));
    ASSERT_THAT(can_init_fake.arg12_val, Eq(true));
}

TEST_F(CANTest, EnableIrq) {
    can_.enable_irq(irq::rx1_pending);
    ASSERT_THAT(can_enable_irq_fake.call_count, Eq(1));
    ASSERT_THAT(can_enable_irq_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_enable_irq_fake.arg1_val, Eq(CAN_IER_FMPIE1));
}

TEST_F(CANTest, DisableIrq) {
    can_.disable_irq(irq::rx1_pending);
    ASSERT_THAT(can_disable_irq_fake.call_count, Eq(1));
    ASSERT_THAT(can_disable_irq_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_disable_irq_fake.arg1_val, Eq(CAN_IER_FMPIE1));
}

TEST_F(CANTest, WriteSuccess) {
    message msg{.id = 0xdead, .size = 4, .is_extended = true};
    can_transmit_fake.return_val = 1;

    auto result = can_.write(msg);
    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(can_transmit_fake.call_count, Eq(1));
    ASSERT_THAT(can_transmit_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_transmit_fake.arg1_val, Eq(msg.id));
    ASSERT_THAT(can_transmit_fake.arg2_val, Eq(msg.is_extended));
    ASSERT_THAT(can_transmit_fake.arg3_val, Eq(msg.is_rtr));
    ASSERT_THAT(can_transmit_fake.arg4_val, Eq(msg.size));
    ASSERT_THAT(can_transmit_fake.arg5_val, Eq(reinterpret_cast<std::uint8_t*>(msg.data.data())));
}

TEST_F(CANTest, WriteFailure) {
    message msg{.id = 0xdead, .size = 4, .is_extended = true};
    can_transmit_fake.return_val = -1;

    auto result = can_.write(msg);
    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(can_transmit_fake.call_count, Eq(1));
    ASSERT_THAT(can_transmit_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_transmit_fake.arg1_val, Eq(msg.id));
    ASSERT_THAT(can_transmit_fake.arg2_val, Eq(msg.is_extended));
    ASSERT_THAT(can_transmit_fake.arg3_val, Eq(msg.is_rtr));
    ASSERT_THAT(can_transmit_fake.arg4_val, Eq(msg.size));
    ASSERT_THAT(can_transmit_fake.arg5_val, Eq(reinterpret_cast<std::uint8_t*>(msg.data.data())));
}

TEST_F(CANTest, Read) {
    can_receive_fake.custom_fake = can_receive_call;

    auto msg = can_.read(rx_fifo1);
    ASSERT_THAT(can_receive_fake.call_count, Eq(1));
    ASSERT_THAT(can_receive_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(can_receive_fake.arg1_val, Eq(1));
    ASSERT_THAT(can_receive_fake.arg2_val, Eq(true));
    ASSERT_THAT(msg.is_extended, Eq(true));
    ASSERT_THAT(msg.is_rtr, Eq(false));
    ASSERT_THAT(msg.id, Eq(0xdead));
    ASSERT_THAT(msg.size, Eq(4));
    ASSERT_THAT(msg.matched_filter, Eq(filter5));
    ASSERT_THAT(msg.data[0], Eq(std::byte{0xde}));
    ASSERT_THAT(msg.data[1], Eq(std::byte{0xad}));
    ASSERT_THAT(msg.data[2], Eq(std::byte{0xc0}));
    ASSERT_THAT(msg.data[3], Eq(std::byte{0xde}));
}

TEST_F(CANTest, SetupFilter) {
    can_.setup_filter(filter10, 0xdead, 0xffff, rx_fifo1, true);
    ASSERT_THAT(can_filter_id_mask_32bit_init_fake.call_count, Eq(1));
    ASSERT_THAT(can_filter_id_mask_32bit_init_fake.arg0_val, Eq(23));
    ASSERT_THAT(can_filter_id_mask_32bit_init_fake.arg1_val, Eq(0xdead << 3));
    ASSERT_THAT(can_filter_id_mask_32bit_init_fake.arg2_val, Eq(0xffff << 3));
    ASSERT_THAT(can_filter_id_mask_32bit_init_fake.arg3_val, Eq(1));
    ASSERT_THAT(can_filter_id_mask_32bit_init_fake.arg4_val, Eq(true));
}

} // namespace
