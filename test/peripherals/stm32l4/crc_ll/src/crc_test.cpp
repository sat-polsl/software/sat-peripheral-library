#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/crc.h"
#include "spl/peripherals/crc/crc.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(crc_reset)
FAKE_VOID_FUNC(crc_set_reverse_input, std::uint32_t);
FAKE_VOID_FUNC(crc_reverse_output_enable);
FAKE_VOID_FUNC(crc_reverse_output_disable);
FAKE_VALUE_FUNC(std::uint32_t, crc_feed, std::uint8_t)
FAKE_VALUE_FUNC(std::uint32_t, crc_feed_block, std::uint8_t*, int)

namespace {

using namespace spl::peripherals::crc;
using namespace testing;

struct CRCTest : Test {
    CRCTest(){
        RESET_FAKE(crc_reset)                  //
        RESET_FAKE(crc_feed)                   //
        RESET_FAKE(crc_feed_block)             //
        RESET_FAKE(crc_set_reverse_input)      //
        RESET_FAKE(crc_reverse_output_enable)  //
        RESET_FAKE(crc_reverse_output_disable) //
    }

    crc crc_;
};

TEST_F(CRCTest, Reset) {
    auto& result = crc_.reset();
    ASSERT_THAT(&result, Eq(&crc_));
    ASSERT_THAT(crc_reset_fake.call_count, Eq(1));
}

TEST_F(CRCTest, Feed) {
    crc_feed_fake.return_val = 321;
    auto data = std::byte{123};
    uint32_t result = crc_.feed(data);
    ASSERT_THAT(crc_feed_fake.call_count, Eq(1));
    ASSERT_THAT(crc_feed_fake.arg0_val, Eq(static_cast<std::uint8_t>(data)));
    ASSERT_THAT(result, Eq(321));
}

TEST_F(CRCTest, FeedSpan) {
    crc_feed_block_fake.return_val = 321;
    std::array<std::byte, 4> buffer{};
    uint32_t result = crc_.feed(buffer);
    ASSERT_THAT(result, Eq(321));
    ASSERT_THAT(crc_feed_block_fake.call_count, Eq(1));
    ASSERT_THAT(crc_feed_block_fake.arg0_val,
                Eq(reinterpret_cast<std::uint8_t*>(const_cast<std::byte*>(buffer.data()))));
    ASSERT_THAT(crc_feed_block_fake.arg1_val, Eq(buffer.size()));
}

TEST_F(CRCTest, EnableReverseInput) {
    auto& result = crc_.set_reverse_input(true);
    ASSERT_THAT(&result, Eq(&crc_));
    ASSERT_THAT(crc_set_reverse_input_fake.call_count, Eq(1));
    ASSERT_THAT(crc_set_reverse_input_fake.arg0_val, Eq(CRC_CR_REV_IN_WORD));
}

TEST_F(CRCTest, DisableReverseInput) {
    auto& result = crc_.set_reverse_input(false);
    ASSERT_THAT(&result, Eq(&crc_));
    ASSERT_THAT(crc_set_reverse_input_fake.call_count, Eq(1));
    ASSERT_THAT(crc_set_reverse_input_fake.arg0_val, Eq(CRC_CR_REV_IN_NONE));
}

TEST_F(CRCTest, EnableReverseOutput) {
    auto& result = crc_.set_reverse_output(true);
    ASSERT_THAT(&result, Eq(&crc_));
    ASSERT_THAT(crc_reverse_output_enable_fake.call_count, Eq(1));
}

TEST_F(CRCTest, DisableReverseOutput) {
    auto& result = crc_.set_reverse_output(false);
    ASSERT_THAT(&result, Eq(&crc_));
    ASSERT_THAT(crc_reverse_output_disable_fake.call_count, Eq(1));
}

} // namespace
