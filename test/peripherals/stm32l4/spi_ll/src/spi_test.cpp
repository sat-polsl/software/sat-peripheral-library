#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/spi.h"
#include "spl/peripherals/spi/spi.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(spi_enable, std::uint32_t)
FAKE_VOID_FUNC(spi_disable, std::uint32_t)
FAKE_VOID_FUNC(spi_send_byte, std::uint32_t, std::uint8_t)
FAKE_VALUE_FUNC(std::uint8_t, spi_read_byte, std::uint32_t)
FAKE_VOID_FUNC(spi_set_full_duplex_mode, std::uint32_t)
FAKE_VOID_FUNC(spi_set_receive_only_mode, std::uint32_t)
FAKE_VOID_FUNC(spi_disable_software_slave_management, std::uint32_t)
FAKE_VOID_FUNC(spi_enable_software_slave_management, std::uint32_t)
FAKE_VOID_FUNC(spi_send_lsb_first, std::uint32_t)
FAKE_VOID_FUNC(spi_send_msb_first, std::uint32_t)
FAKE_VOID_FUNC(spi_set_baudrate_prescaler, std::uint32_t, uint8_t)
FAKE_VOID_FUNC(spi_set_master_mode, std::uint32_t)
FAKE_VOID_FUNC(spi_set_slave_mode, std::uint32_t)
FAKE_VOID_FUNC(spi_enable_tx_buffer_empty_interrupt, std::uint32_t)
FAKE_VOID_FUNC(spi_disable_tx_buffer_empty_interrupt, std::uint32_t)
FAKE_VOID_FUNC(spi_enable_rx_buffer_not_empty_interrupt, std::uint32_t)
FAKE_VOID_FUNC(spi_disable_rx_buffer_not_empty_interrupt, std::uint32_t)
FAKE_VOID_FUNC(spi_enable_error_interrupt, std::uint32_t)
FAKE_VOID_FUNC(spi_disable_error_interrupt, std::uint32_t)
FAKE_VOID_FUNC(spi_enable_tx_dma, std::uint32_t)
FAKE_VOID_FUNC(spi_disable_tx_dma, std::uint32_t)
FAKE_VOID_FUNC(spi_enable_rx_dma, std::uint32_t)
FAKE_VOID_FUNC(spi_disable_rx_dma, std::uint32_t)
FAKE_VOID_FUNC(spi_set_standard_mode, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(spi_set_data_size, std::uint32_t, std::uint16_t)
FAKE_VOID_FUNC(spi_fifo_reception_threshold_8bit, std::uint32_t)
FAKE_VOID_FUNC(spi_fifo_reception_threshold_16bit, std::uint32_t)
FAKE_VOID_FUNC(spi_set_nss_high, std::uint32_t)
FAKE_VOID_FUNC(spi_set_nss_low, std::uint32_t)
FAKE_VALUE_FUNC(bool, spi_get_status, std::uint32_t, std::uint32_t)
FAKE_VALUE_FUNC(bool, spi_is_tx_buffer_empty_interrupt_enabled, std::uint32_t)
FAKE_VALUE_FUNC(bool, spi_is_rx_buffer_not_empty_interrupt_enabled, std::uint32_t)
FAKE_VALUE_FUNC(bool, spi_is_error_interrupt_enabled, std::uint32_t)

namespace {
using namespace spl::peripherals::spi;
using namespace testing;

struct SpiTest : Test {
    SpiTest() {
        RESET_FAKE(spi_enable)                                //
        RESET_FAKE(spi_disable)                               //
        RESET_FAKE(spi_send_byte)                             //
        RESET_FAKE(spi_read_byte)                             //
        RESET_FAKE(spi_set_full_duplex_mode)                  //
        RESET_FAKE(spi_set_receive_only_mode)                 //
        RESET_FAKE(spi_enable_software_slave_management)      //
        RESET_FAKE(spi_disable_software_slave_management)     //
        RESET_FAKE(spi_send_msb_first)                        //
        RESET_FAKE(spi_send_lsb_first)                        //
        RESET_FAKE(spi_set_baudrate_prescaler)                //
        RESET_FAKE(spi_set_slave_mode)                        //
        RESET_FAKE(spi_set_master_mode)                       //
        RESET_FAKE(spi_enable_tx_buffer_empty_interrupt)      //
        RESET_FAKE(spi_disable_tx_buffer_empty_interrupt)     //
        RESET_FAKE(spi_enable_rx_buffer_not_empty_interrupt)  //
        RESET_FAKE(spi_disable_rx_buffer_not_empty_interrupt) //
        RESET_FAKE(spi_enable_error_interrupt)                //
        RESET_FAKE(spi_disable_error_interrupt)               //
        RESET_FAKE(spi_enable_tx_dma)                         //
        RESET_FAKE(spi_disable_tx_dma)                        //
        RESET_FAKE(spi_enable_rx_dma)                         //
        RESET_FAKE(spi_disable_rx_dma)                        //
        RESET_FAKE(spi_set_standard_mode)                     //
        RESET_FAKE(spi_set_data_size)                         //
        RESET_FAKE(spi_fifo_reception_threshold_8bit)         //
        RESET_FAKE(spi_fifo_reception_threshold_16bit)        //
        RESET_FAKE(spi_get_status)                            //
        RESET_FAKE(spi_set_nss_high)                          //
        RESET_FAKE(spi_set_nss_low)                           //
        RESET_FAKE(spi_get_status)
        RESET_FAKE(spi_is_tx_buffer_empty_interrupt_enabled)
        RESET_FAKE(spi_is_rx_buffer_not_empty_interrupt_enabled)
        RESET_FAKE(spi_is_error_interrupt_enabled)
    }
    static constexpr std::uint32_t target_bus = SPI1;
    spi spi_{instance::spi1};
};

TEST_F(SpiTest, Enable) {
    spi_.enable();
    ASSERT_THAT(spi_enable_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, Disable) {
    spi_.disable();
    ASSERT_THAT(spi_disable_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, Write) {
    uint16_t d = 0xaa;
    spi_.write(d);
    ASSERT_THAT(spi_send_byte_fake.call_count, Eq(1));
    ASSERT_THAT(spi_send_byte_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_send_byte_fake.arg1_val, Eq(d));
}

TEST_F(SpiTest, Read) {
    spi_read_byte_fake.return_val = 0xaa;
    std::uint16_t x = spi_.read();
    ASSERT_THAT(spi_read_byte_fake.call_count, Eq(1));
    ASSERT_THAT(spi_read_byte_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(x, Eq(0xaa));
}

TEST_F(SpiTest, xfer) {
    spi_read_byte_fake.return_val = 0xaa;
    uint16_t d = 0xbb;
    std::uint16_t y = spi_.xfer(d);
    ASSERT_THAT(spi_send_byte_fake.call_count, Eq(1));
    ASSERT_THAT(spi_send_byte_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_send_byte_fake.arg1_val, Eq(d));
    ASSERT_THAT(spi_read_byte_fake.call_count, Eq(1));
    ASSERT_THAT(spi_read_byte_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(y, Eq(0xaa));
}

TEST_F(SpiTest, SetFullDuplexMode) {
    spi_.set_full_duplex_mode();
    ASSERT_THAT(spi_set_full_duplex_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_full_duplex_mode_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetReceiveOnlyMode) {
    spi_.set_receive_only_mode();
    ASSERT_THAT(spi_set_receive_only_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_receive_only_mode_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetSlaveManagementHardware) {
    spi_.set_slave_management(slave_management::hardware);
    ASSERT_THAT(spi_disable_software_slave_management_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_software_slave_management_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetSlaveManagementSoftware) {
    spi_.set_slave_management(slave_management::software);
    ASSERT_THAT(spi_enable_software_slave_management_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_software_slave_management_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetSendOrderMsb) {
    spi_.set_sending_order(send_order::msb_first);
    ASSERT_THAT(spi_send_msb_first_fake.call_count, Eq(1));
    ASSERT_THAT(spi_send_msb_first_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetSendOrderLsb) {
    spi_.set_sending_order(send_order::lsb_first);
    ASSERT_THAT(spi_send_lsb_first_fake.call_count, Eq(1));
    ASSERT_THAT(spi_send_lsb_first_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetBaudratePrescaler) {
    spi_.set_baudrate_prescaler(baudrate_prescaler::div32);
    ASSERT_THAT(spi_set_baudrate_prescaler_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_baudrate_prescaler_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_baudrate_prescaler_fake.arg1_val, Eq(SPI_CR1_BR_FPCLK_DIV_32));
}

TEST_F(SpiTest, SetHierarchyModeMaster) {
    spi_.set_mode(mode::master);
    ASSERT_THAT(spi_set_master_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_master_mode_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetHierarchyModeSlave) {
    spi_.set_mode(mode::slave);
    ASSERT_THAT(spi_set_slave_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_slave_mode_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, EnableIrqTx) {
    spi_.enable_irq(irq::tx_buffer_empty);
    ASSERT_THAT(spi_enable_tx_buffer_empty_interrupt_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_tx_buffer_empty_interrupt_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, EnableIrqRx) {
    spi_.enable_irq(irq::rx_buffer_not_empty);
    ASSERT_THAT(spi_enable_rx_buffer_not_empty_interrupt_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_rx_buffer_not_empty_interrupt_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, EnableIrqError) {
    spi_.enable_irq(irq::error);
    ASSERT_THAT(spi_enable_error_interrupt_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_error_interrupt_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, DisableIrqTx) {
    spi_.disable_irq(irq::tx_buffer_empty);
    ASSERT_THAT(spi_disable_tx_buffer_empty_interrupt_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_tx_buffer_empty_interrupt_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, DisableIrqRx) {
    spi_.disable_irq(irq::rx_buffer_not_empty);
    ASSERT_THAT(spi_disable_rx_buffer_not_empty_interrupt_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_rx_buffer_not_empty_interrupt_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, DisableIrqError) {
    spi_.disable_irq(irq::error);
    ASSERT_THAT(spi_disable_error_interrupt_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_error_interrupt_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, EnableDmaTx) {
    spi_.enable_dma(dma::tx);
    ASSERT_THAT(spi_enable_tx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_tx_dma_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, EnableDmaRx) {
    spi_.enable_dma(dma::rx);
    ASSERT_THAT(spi_enable_rx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(spi_enable_rx_dma_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, DisableDmaTx) {
    spi_.disable_dma(dma::tx);
    ASSERT_THAT(spi_disable_tx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_tx_dma_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, DisableDmaRx) {
    spi_.disable_dma(dma::rx);
    ASSERT_THAT(spi_disable_rx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(spi_disable_rx_dma_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetStandardMode0) {
    spi_.set_clock_mode(clock_mode::mode0);
    ASSERT_THAT(spi_set_standard_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_standard_mode_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_standard_mode_fake.arg1_val, Eq(0));
}

TEST_F(SpiTest, SetStandardMode1) {
    spi_.set_clock_mode(clock_mode::mode1);
    ASSERT_THAT(spi_set_standard_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_standard_mode_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_standard_mode_fake.arg1_val, Eq(1));
}

TEST_F(SpiTest, SetStandardMode2) {
    spi_.set_clock_mode(clock_mode::mode2);
    ASSERT_THAT(spi_set_standard_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_standard_mode_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_standard_mode_fake.arg1_val, Eq(2));
}

TEST_F(SpiTest, SetStandardMode3) {
    spi_.set_clock_mode(clock_mode::mode3);
    ASSERT_THAT(spi_set_standard_mode_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_standard_mode_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_standard_mode_fake.arg1_val, Eq(3));
}

TEST_F(SpiTest, SetDataSize8bit) {
    spi_.set_data_size(data_size::bit8);
    ASSERT_THAT(spi_set_data_size_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_data_size_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_data_size_fake.arg1_val, Eq(8));
}

TEST_F(SpiTest, SetDataSize16bit) {
    spi_.set_data_size(data_size::bit16);
    ASSERT_THAT(spi_set_data_size_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_data_size_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_data_size_fake.arg1_val, Eq(16));
}

TEST_F(SpiTest, SetFifoReceptionThreshold8) {
    spi_.set_fifo_reception_threshold(fifo_threshold::bit8);
    ASSERT_THAT(spi_fifo_reception_threshold_8bit_fake.call_count, Eq(1));
    ASSERT_THAT(spi_fifo_reception_threshold_8bit_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, SetFifoReceptionThreshold16) {
    spi_.set_fifo_reception_threshold(fifo_threshold::bit16);
    ASSERT_THAT(spi_fifo_reception_threshold_16bit_fake.call_count, Eq(1));
    ASSERT_THAT(spi_fifo_reception_threshold_16bit_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, IsBusyFalse) {
    spi_get_status_fake.return_val = false;
    auto result = spi_.is_busy();
    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_BSY));
}

TEST_F(SpiTest, IsBusyTrue) {
    spi_get_status_fake.return_val = true;
    auto result = spi_.is_busy();
    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_BSY));
}

TEST_F(SpiTest, IsTxEmptyFalse) {
    spi_get_status_fake.return_val = false;
    auto result = spi_.is_tx_empty();
    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_TXE));
}

TEST_F(SpiTest, IsTxEmptyTrue) {
    spi_get_status_fake.return_val = true;
    auto result = spi_.is_tx_empty();
    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_TXE));
}

TEST_F(SpiTest, IsRxNotEmptyFalse) {
    spi_get_status_fake.return_val = false;
    auto result = spi_.is_rx_not_empty();
    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_RXNE));
}

TEST_F(SpiTest, IsRxNotEmptyTrue) {
    spi_get_status_fake.return_val = true;
    auto result = spi_.is_rx_not_empty();
    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_RXNE));
}

TEST_F(SpiTest, OverrunErrorFalse) {
    spi_get_status_fake.return_val = false;
    auto result = spi_.overrun_error();
    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_OVR));
}

TEST_F(SpiTest, OverrunErrorTrue) {
    spi_get_status_fake.return_val = true;
    auto result = spi_.overrun_error();
    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(spi_get_status_fake.call_count, Eq(1));
    ASSERT_THAT(spi_get_status_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_get_status_fake.arg1_val, Eq(SPI_SR_OVR));
}

TEST_F(SpiTest, SetInternalSlaveSelectTrue) {
    spi_.set_internal_slave_select(true);
    ASSERT_THAT(spi_set_nss_high_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_nss_high_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_nss_low_fake.call_count, Eq(0));
}

TEST_F(SpiTest, SetInternalSlaveSelectFalse) {
    spi_.set_internal_slave_select(false);
    ASSERT_THAT(spi_set_nss_low_fake.call_count, Eq(1));
    ASSERT_THAT(spi_set_nss_low_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(spi_set_nss_high_fake.call_count, Eq(0));
}

TEST_F(SpiTest, IfRxBufNotEmptyIrq) {
    bool res = spi_.is_irq_enabled(irq::rx_buffer_not_empty);
    ASSERT_THAT(res, Eq(SPI_CR2(target_bus) & SPI_CR2_RXNEIE));
    ASSERT_THAT(spi_is_rx_buffer_not_empty_interrupt_enabled_fake.call_count, Eq(1));
    ASSERT_THAT(spi_is_rx_buffer_not_empty_interrupt_enabled_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, IfTxBufEmptyIrq) {
    bool res = spi_.is_irq_enabled(irq::tx_buffer_empty);
    ASSERT_THAT(res, Eq(SPI_CR2(target_bus) & SPI_CR2_TXEIE));
    ASSERT_THAT(spi_is_tx_buffer_empty_interrupt_enabled_fake.call_count, Eq(1));
    ASSERT_THAT(spi_is_tx_buffer_empty_interrupt_enabled_fake.arg0_val, Eq(target_bus));
}

TEST_F(SpiTest, IfErrorIrq) {
    bool res = spi_.is_irq_enabled(irq::error);
    ASSERT_THAT(spi_is_error_interrupt_enabled_fake.call_count, Eq(1));
    ASSERT_THAT(spi_is_error_interrupt_enabled_fake.arg0_val, Eq(target_bus));
    ASSERT_THAT(res, Eq(SPI_CR2(target_bus) & SPI_CR2_ERRIE));
}

} // namespace
