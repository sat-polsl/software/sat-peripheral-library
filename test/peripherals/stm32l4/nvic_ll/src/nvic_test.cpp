#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/l4/nvic.h"
#include "spl/peripherals/nvic/nvic.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(nvic_enable_irq, std::uint8_t)
FAKE_VOID_FUNC(nvic_disable_irq, std::uint8_t)
FAKE_VOID_FUNC(nvic_set_priority, std::uint8_t, std::uint8_t)
FAKE_VOID_FUNC(nvic_clear_pending_irq, std::uint8_t)

namespace {

using namespace spl::peripherals::nvic;
using namespace testing;

struct nvic_params {
    spl::peripherals::nvic::concepts::irq irq;
    std::uint8_t expected;
};

struct NvicTest : TestWithParam<nvic_params> {
    NvicTest(){RESET_FAKE(nvic_enable_irq) RESET_FAKE(nvic_disable_irq)
                   RESET_FAKE(nvic_set_priority) RESET_FAKE(nvic_clear_pending_irq)}

    nvic nvic_;
};

TEST_P(NvicTest, EnableIrq) {
    auto params = GetParam();
    auto& result = nvic_.enable_irq(params.irq);

    ASSERT_THAT(&result, Eq(&nvic_));
    ASSERT_THAT(nvic_enable_irq_fake.call_count, Eq(1));
    ASSERT_THAT(nvic_enable_irq_fake.arg0_val, Eq(params.expected));
}

TEST_P(NvicTest, DisableIrq) {
    auto params = GetParam();
    auto& result = nvic_.disable_irq(params.irq);

    ASSERT_THAT(&result, Eq(&nvic_));
    ASSERT_THAT(nvic_disable_irq_fake.call_count, Eq(1));
    ASSERT_THAT(nvic_disable_irq_fake.arg0_val, Eq(params.expected));
}

TEST_P(NvicTest, SetPriority) {
    auto params = GetParam();
    auto& result = nvic_.set_priority(params.irq, 5);

    ASSERT_THAT(&result, Eq(&nvic_));
    ASSERT_THAT(nvic_set_priority_fake.call_count, Eq(1));
    ASSERT_THAT(nvic_set_priority_fake.arg0_val, Eq(params.expected));
    ASSERT_THAT(nvic_set_priority_fake.arg1_val, Eq(5 << 4));
}

TEST_P(NvicTest, ClearPendingIrq) {
    auto params = GetParam();
    auto& result = nvic_.clear_pending_irq(params.irq);

    ASSERT_THAT(&result, Eq(&nvic_));
    ASSERT_THAT(nvic_clear_pending_irq_fake.call_count, Eq(1));
    ASSERT_THAT(nvic_clear_pending_irq_fake.arg0_val, Eq(params.expected));
}

INSTANTIATE_TEST_CASE_P(NvicIrqTest,
                        NvicTest,
                        ::testing::Values(nvic_params{irq::uart1, NVIC_USART1_IRQ},
                                          nvic_params{irq::uart2, NVIC_USART2_IRQ},
                                          nvic_params{irq::uart3, NVIC_USART3_IRQ},
                                          nvic_params{irq::dma1_channel1, NVIC_DMA1_CHANNEL1_IRQ},
                                          nvic_params{irq::dma1_channel2, NVIC_DMA1_CHANNEL2_IRQ},
                                          nvic_params{irq::dma1_channel3, NVIC_DMA1_CHANNEL3_IRQ},
                                          nvic_params{irq::dma1_channel4, NVIC_DMA1_CHANNEL4_IRQ},
                                          nvic_params{irq::dma1_channel5, NVIC_DMA1_CHANNEL5_IRQ},
                                          nvic_params{irq::dma1_channel6, NVIC_DMA1_CHANNEL6_IRQ},
                                          nvic_params{irq::dma1_channel7, NVIC_DMA1_CHANNEL7_IRQ},
                                          nvic_params{irq::dma2_channel1, NVIC_DMA2_CHANNEL1_IRQ},
                                          nvic_params{irq::dma2_channel2, NVIC_DMA2_CHANNEL2_IRQ},
                                          nvic_params{irq::dma2_channel3, NVIC_DMA2_CHANNEL3_IRQ},
                                          nvic_params{irq::dma2_channel4, NVIC_DMA2_CHANNEL4_IRQ},
                                          nvic_params{irq::dma2_channel5, NVIC_DMA2_CHANNEL5_IRQ},
                                          nvic_params{irq::dma2_channel6, NVIC_DMA2_CHANNEL6_IRQ},
                                          nvic_params{irq::dma2_channel7, NVIC_DMA2_CHANNEL7_IRQ}));

} // namespace
