#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "satext/units.h"
#include "spl/peripherals/flash/enums.h"
#include "spl/peripherals/flash/mock/flash.h"
#include "spl/peripherals/rcc/mock/rcc.h"
#include "spl/peripherals/sysclk/sysclk.h"

namespace {

using namespace testing;
using namespace spl::peripherals::sysclk;

using sysclk_type = sysclk<spl::peripherals::rcc::mock::rcc, spl::peripherals::flash::mock::flash>;

struct SysclkTest : Test {
    SysclkTest();
    NiceMock<spl::peripherals::rcc::mock::rcc> rcc_;
    NiceMock<spl::peripherals::flash::mock::flash> flash_;
};

SysclkTest::SysclkTest() {
    spl::peripherals::rcc::mock::setup_default_return_value(rcc_);
    spl::peripherals::flash::mock::setup_default_return_value(flash_);
}

TEST_F(SysclkTest, Setup) {
    sysclk_type sysclk{rcc_, flash_};

    static constexpr spl::peripherals::sysclk::configuration sysclk_cfg{
        .sysclk_src = spl::peripherals::rcc::clock_source::pll,
        .sysclk_hz = 50_MHz,
        .pll_clock_src = spl::peripherals::rcc::pll_clock_source::hsi,
        .pll_input_clock_hz = 16_MHz,
        .pll_input_prescaler = spl::peripherals::rcc::pll_input_prescaler::div4,
        .pll_vco_multiplier = 50,
        .pll_output_prescaler = spl::peripherals::rcc::pll_prescaler::div4};

    EXPECT_CALL(flash_, set_wait_states(spl::peripherals::flash::ws3));
    EXPECT_CALL(rcc_, enable_oscillator(spl::peripherals::rcc::oscillator::hsi));
    EXPECT_CALL(rcc_, is_oscillator_ready(spl::peripherals::rcc::oscillator::hsi))
        .WillOnce(Return(true));
    EXPECT_CALL(rcc_, set_pll_source(spl::peripherals::rcc::pll_clock_source::hsi));

    EXPECT_CALL(rcc_,
                set_pll_input_clock_prescaler(spl::peripherals::rcc::pll_input_prescaler::div4));
    EXPECT_CALL(rcc_, set_pll_vco_multiplier(50));
    EXPECT_CALL(rcc_, set_pll_clock_prescaler(spl::peripherals::rcc::pll_prescaler::div4));
    EXPECT_CALL(rcc_, enable_oscillator(spl::peripherals::rcc::oscillator::pll));
    EXPECT_CALL(rcc_, is_oscillator_ready(spl::peripherals::rcc::oscillator::pll))
        .WillOnce(Return(true));

    EXPECT_CALL(rcc_, enable_pll_output(spl::peripherals::rcc::pll_output::pllclk));

    EXPECT_CALL(rcc_, set_ahb_frequency(50_MHz));
    EXPECT_CALL(rcc_, set_apb1_frequency(50_MHz));
    EXPECT_CALL(rcc_, set_apb2_frequency(50_MHz));
    EXPECT_CALL(rcc_, set_sysclk_source(spl::peripherals::rcc::clock_source::pll));

    sysclk.setup<sysclk_cfg>();
}

} // namespace
