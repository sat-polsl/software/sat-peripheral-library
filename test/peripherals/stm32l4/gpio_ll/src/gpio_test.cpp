#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/gpio.h"
#include "spl/peripherals/gpio/gpio.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(gpio_mode_setup, std::uint32_t, std::uint8_t, std::uint8_t, std::uint16_t)
FAKE_VOID_FUNC(gpio_set_af, std::uint32_t, std::uint8_t, std::uint16_t)
FAKE_VOID_FUNC(gpio_clear, std::uint32_t, std::uint16_t)
FAKE_VOID_FUNC(gpio_set, std::uint32_t, std::uint16_t)
FAKE_VOID_FUNC(gpio_toggle, std::uint32_t, std::uint16_t)
FAKE_VALUE_FUNC(std::uint16_t, gpio_port_read, std::uint32_t)

namespace {

using namespace spl::peripherals::gpio;
using namespace testing;

struct GpioTest : testing::Test {
    GpioTest() {
        RESET_FAKE(gpio_mode_setup)
        RESET_FAKE(gpio_set_af)
        RESET_FAKE(gpio_clear)
        RESET_FAKE(gpio_set)
        RESET_FAKE(gpio_toggle)
        RESET_FAKE(gpio_port_read)
    }
};

TEST_F(GpioTest, ModeSetup) {
    auto g = gpio(port::a, pin5);
    g.setup(mode::output, pupd::none);

    ASSERT_THAT(gpio_mode_setup_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_mode_setup_fake.arg0_val, Eq(GPIOA));
    ASSERT_THAT(gpio_mode_setup_fake.arg1_val, Eq(GPIO_MODE_OUTPUT));
    ASSERT_THAT(gpio_mode_setup_fake.arg2_val, Eq(GPIO_PUPD_NONE));
    ASSERT_THAT(gpio_mode_setup_fake.arg3_val, Eq(GPIO5));
}

TEST_F(GpioTest, ModeSetupAlternate) {
    auto g = gpio(port::b, pin10);
    g.setup(mode::af, pupd::pullup, af6);

    ASSERT_THAT(gpio_mode_setup_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_mode_setup_fake.arg0_val, Eq(GPIOB));
    ASSERT_THAT(gpio_mode_setup_fake.arg1_val, Eq(GPIO_MODE_AF));
    ASSERT_THAT(gpio_mode_setup_fake.arg2_val, Eq(GPIO_PUPD_PULLUP));
    ASSERT_THAT(gpio_mode_setup_fake.arg3_val, Eq(GPIO10));

    ASSERT_THAT(gpio_set_af_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_set_af_fake.arg0_val, Eq(GPIOB));
    ASSERT_THAT(gpio_set_af_fake.arg1_val, Eq(GPIO_AF6));
    ASSERT_THAT(gpio_set_af_fake.arg2_val, Eq(GPIO10));
}

TEST_F(GpioTest, Set) {
    auto g = gpio(port::c, pin0);
    g.set();

    ASSERT_THAT(gpio_set_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_set_fake.arg0_val, Eq(GPIOC));
    ASSERT_THAT(gpio_set_fake.arg1_val, Eq(GPIO0));
}

TEST_F(GpioTest, Clear) {
    auto g = gpio(port::d, pin11);
    g.clear();

    ASSERT_THAT(gpio_clear_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_clear_fake.arg0_val, Eq(GPIOD));
    ASSERT_THAT(gpio_clear_fake.arg1_val, Eq(GPIO11));
}

TEST_F(GpioTest, Toggle) {
    auto g = gpio(port::a, pin8);
    g.toggle();

    ASSERT_THAT(gpio_toggle_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_toggle_fake.arg0_val, Eq(GPIOA));
    ASSERT_THAT(gpio_toggle_fake.arg1_val, Eq(GPIO8));
}

TEST_F(GpioTest, ReadTrue) {
    auto g = gpio(port::b, pin3);
    gpio_port_read_fake.return_val = 0x0008;

    auto result = g.read();

    ASSERT_THAT(gpio_port_read_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_port_read_fake.arg0_val, Eq(GPIOB));
    ASSERT_THAT(result, Eq(true));
}

TEST_F(GpioTest, ReadFalse) {
    auto g = gpio(port::c, pin9);
    gpio_port_read_fake.return_val = 0;

    auto result = g.read();

    ASSERT_THAT(gpio_port_read_fake.call_count, Eq(1));
    ASSERT_THAT(gpio_port_read_fake.arg0_val, Eq(GPIOC));
    ASSERT_THAT(result, Eq(false));
}

} // namespace
