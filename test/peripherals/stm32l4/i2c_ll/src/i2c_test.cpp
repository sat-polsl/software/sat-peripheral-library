#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/i2c.h"
#include "spl/peripherals/i2c/i2c.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(i2c_reset, uint32_t)
FAKE_VOID_FUNC(i2c_peripheral_enable, uint32_t)
FAKE_VOID_FUNC(i2c_peripheral_disable, uint32_t)
FAKE_VOID_FUNC(i2c_send_start, uint32_t)
FAKE_VOID_FUNC(i2c_send_stop, uint32_t)
FAKE_VOID_FUNC(i2c_clear_stop, uint32_t)
FAKE_VOID_FUNC(i2c_set_7bit_addr_mode, uint32_t)
FAKE_VOID_FUNC(i2c_set_write_transfer_dir, uint32_t)
FAKE_VOID_FUNC(i2c_set_read_transfer_dir, uint32_t)
FAKE_VOID_FUNC(i2c_enable_autoend, uint32_t)
FAKE_VOID_FUNC(i2c_disable_autoend, uint32_t)
FAKE_VOID_FUNC(i2c_enable_rxdma, uint32_t)
FAKE_VOID_FUNC(i2c_disable_rxdma, uint32_t)
FAKE_VOID_FUNC(i2c_enable_txdma, uint32_t)
FAKE_VOID_FUNC(i2c_disable_txdma, uint32_t)
FAKE_VOID_FUNC(i2c_set_own_7bit_slave_address, uint32_t, uint8_t)
FAKE_VOID_FUNC(i2c_send_data, uint32_t, uint8_t)
FAKE_VOID_FUNC(i2c_set_prescaler, uint32_t, uint8_t)
FAKE_VOID_FUNC(i2c_set_scl_high_period, uint32_t, uint8_t)
FAKE_VOID_FUNC(i2c_set_scl_low_period, uint32_t, uint8_t)
FAKE_VOID_FUNC(i2c_set_7bit_address, uint32_t, uint8_t)
FAKE_VOID_FUNC(i2c_set_bytes_to_transfer, uint32_t, uint32_t)
FAKE_VOID_FUNC(i2c_enable_interrupt, uint32_t, uint32_t)
FAKE_VOID_FUNC(i2c_disable_interrupt, uint32_t, uint32_t)
FAKE_VOID_FUNC(i2c_set_speed, uint32_t, i2c_speeds, uint32_t)
FAKE_VALUE_FUNC(std::uint8_t, i2c_get_data, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_is_start, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_nack, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_busy, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_transmit_int_status, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_transfer_complete, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_received_data, uint32_t)
FAKE_VALUE_FUNC(bool, i2c_is_interrupt_enabled, uint32_t, uint32_t)

namespace {

using namespace spl::peripherals::i2c;
using namespace testing;

struct I2CTest : Test {
    I2CTest(){
        RESET_FAKE(i2c_reset)                      //
        RESET_FAKE(i2c_peripheral_enable)          //
        RESET_FAKE(i2c_peripheral_disable)         //
        RESET_FAKE(i2c_send_start)                 //
        RESET_FAKE(i2c_send_stop)                  //
        RESET_FAKE(i2c_clear_stop)                 //
        RESET_FAKE(i2c_set_own_7bit_slave_address) //
        RESET_FAKE(i2c_send_data)                  //
        RESET_FAKE(i2c_get_data)                   //
        RESET_FAKE(i2c_set_prescaler)              //
        RESET_FAKE(i2c_set_scl_high_period)        //
        RESET_FAKE(i2c_set_scl_low_period)         //
        RESET_FAKE(i2c_set_7bit_addr_mode)         //
        RESET_FAKE(i2c_set_7bit_address)           //
        RESET_FAKE(i2c_set_write_transfer_dir)     //
        RESET_FAKE(i2c_set_read_transfer_dir)      //
        RESET_FAKE(i2c_set_bytes_to_transfer)      //
        RESET_FAKE(i2c_is_start)                   //
        RESET_FAKE(i2c_enable_autoend)             //
        RESET_FAKE(i2c_disable_autoend)            //
        RESET_FAKE(i2c_nack)                       //
        RESET_FAKE(i2c_busy)                       //
        RESET_FAKE(i2c_transmit_int_status)        //
        RESET_FAKE(i2c_transfer_complete)          //
        RESET_FAKE(i2c_received_data)              //
        RESET_FAKE(i2c_enable_interrupt)           //
        RESET_FAKE(i2c_disable_interrupt)          //
        RESET_FAKE(i2c_enable_rxdma)               //
        RESET_FAKE(i2c_disable_rxdma)              //
        RESET_FAKE(i2c_enable_txdma)               //
        RESET_FAKE(i2c_disable_txdma)              //
        RESET_FAKE(i2c_set_speed)                  //
        RESET_FAKE(i2c_is_interrupt_enabled)       //
    }

    spl::peripherals::i2c::i2c i2c_{instance::i2c1};
    static constexpr auto instance_ = I2C1;
};

TEST_F(I2CTest, Enable) {
    auto& result = i2c_.enable();

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_peripheral_enable_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_peripheral_enable_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, Disable) {
    auto& result = i2c_.disable();

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_peripheral_disable_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_peripheral_disable_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, SendStart) {
    auto& result = i2c_.send_start();

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_send_start_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_send_start_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, SendStop) {
    auto& result = i2c_.send_stop();

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_send_stop_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_send_stop_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, ClearStop) {
    auto& result = i2c_.clear_stop();

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_clear_stop_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_clear_stop_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, SetTransferDir) {
    {
        auto& result = i2c_.set_transfer_dir(direction::read);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_set_read_transfer_dir_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_set_write_transfer_dir_fake.call_count, Eq(0));
        ASSERT_THAT(i2c_set_read_transfer_dir_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = i2c_.set_transfer_dir(direction::write);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_set_read_transfer_dir_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_set_write_transfer_dir_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_set_write_transfer_dir_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(I2CTest, EnableDMA) {
    {
        auto& result = i2c_.enable_dma(dma::rx);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_txdma_fake.call_count, Eq(0));
        ASSERT_THAT(i2c_enable_rxdma_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_enable_rxdma_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = i2c_.enable_dma(dma::tx);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_txdma_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_enable_rxdma_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_enable_txdma_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(I2CTest, DisableDMA) {
    {
        auto& result = i2c_.disable_dma(dma::rx);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_txdma_fake.call_count, Eq(0));
        ASSERT_THAT(i2c_disable_rxdma_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_disable_rxdma_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = i2c_.disable_dma(dma::tx);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_txdma_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_disable_rxdma_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_disable_txdma_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(I2CTest, Write) {
    i2c_.write(0xab);

    ASSERT_THAT(i2c_send_data_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_send_data_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(i2c_send_data_fake.arg1_val, Eq(0xab));
}

TEST_F(I2CTest, Set7bitAddress) {
    auto& result = i2c_.set_7bit_address(234);

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_set_7bit_address_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_set_7bit_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(i2c_set_7bit_address_fake.arg1_val, Eq(234));
}

TEST_F(I2CTest, SetBytesToTransfer) {
    auto& result = i2c_.set_bytes_to_transfer(4123456789);

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_set_bytes_to_transfer_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_set_bytes_to_transfer_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(i2c_set_bytes_to_transfer_fake.arg1_val, Eq(4123456789));
}

TEST_F(I2CTest, EnableInterrupt) {
    {
        auto& result = i2c_.enable_interrupt(irq::error);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_ERRIE));
    }
    {
        auto& result = i2c_.enable_interrupt(irq::transfer_completed);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(2));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_TCIE));
    }
    {
        auto& result = i2c_.enable_interrupt(irq::stop_detected);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(3));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_STOPIE));
    }
    {
        auto& result = i2c_.enable_interrupt(irq::nack_received);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(4));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_NACKIE));
    }
    {
        auto& result = i2c_.enable_interrupt(irq::address_matched);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(5));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_ADDRIE));
    }
    {
        auto& result = i2c_.enable_interrupt(irq::rx_not_empty);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(6));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_RXIE));
    }
    {
        auto& result = i2c_.enable_interrupt(irq::tx_empty);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_enable_interrupt_fake.call_count, Eq(7));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_enable_interrupt_fake.arg1_val, Eq(I2C_CR1_TXIE));
    }
}

TEST_F(I2CTest, DisableInterrupt) {
    {
        auto& result = i2c_.disable_interrupt(irq::error);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_ERRIE));
    }
    {
        auto& result = i2c_.disable_interrupt(irq::transfer_completed);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(2));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_TCIE));
    }
    {
        auto& result = i2c_.disable_interrupt(irq::stop_detected);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(3));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_STOPIE));
    }
    {
        auto& result = i2c_.disable_interrupt(irq::nack_received);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(4));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_NACKIE));
    }
    {
        auto& result = i2c_.disable_interrupt(irq::address_matched);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(5));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_ADDRIE));
    }
    {
        auto& result = i2c_.disable_interrupt(irq::rx_not_empty);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(6));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_RXIE));
    }
    {
        auto& result = i2c_.disable_interrupt(irq::tx_empty);

        ASSERT_THAT(&i2c_, Eq(&result));
        ASSERT_THAT(i2c_disable_interrupt_fake.call_count, Eq(7));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_disable_interrupt_fake.arg1_val, Eq(I2C_CR1_TXIE));
    }
}

TEST_F(I2CTest, IsInterruptEnabled) {
    {
        i2c_is_interrupt_enabled_fake.return_val = false;
        auto result = i2c_.is_interrupt_enabled(irq::error);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_ERRIE));
    }
    {
        i2c_is_interrupt_enabled_fake.return_val = true;
        auto result = i2c_.is_interrupt_enabled(irq::transfer_completed);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(2));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_TCIE));
    }
    {
        i2c_is_interrupt_enabled_fake.return_val = false;
        auto result = i2c_.is_interrupt_enabled(irq::stop_detected);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(3));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_STOPIE));
    }
    {
        i2c_is_interrupt_enabled_fake.return_val = true;
        auto result = i2c_.is_interrupt_enabled(irq::nack_received);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(4));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_NACKIE));
    }
    {
        i2c_is_interrupt_enabled_fake.return_val = false;
        auto result = i2c_.is_interrupt_enabled(irq::address_matched);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(5));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_ADDRIE));
    }
    {
        i2c_is_interrupt_enabled_fake.return_val = true;
        auto result = i2c_.is_interrupt_enabled(irq::rx_not_empty);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(6));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_RXIE));
    }
    {
        i2c_is_interrupt_enabled_fake.return_val = false;
        auto result = i2c_.is_interrupt_enabled(irq::tx_empty);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.call_count, Eq(7));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(i2c_is_interrupt_enabled_fake.arg1_val, Eq(I2C_CR1_TXIE));
    }
}

TEST_F(I2CTest, SetSpeed) {
    auto& result = i2c_.set_speed(speed::i2c_400k, 4123456789);

    ASSERT_THAT(&i2c_, Eq(&result));
    ASSERT_THAT(i2c_set_speed_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_set_speed_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(i2c_set_speed_fake.arg1_val, Eq(i2c_speed_fm_400k));
    ASSERT_THAT(i2c_set_speed_fake.arg2_val, Eq(4123456789));
}

TEST_F(I2CTest, Read) {
    i2c_get_data_fake.return_val = 234;
    auto result = i2c_.read();

    ASSERT_THAT(result, Eq(234));
    ASSERT_THAT(i2c_get_data_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_get_data_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, StartGeneration) {
    i2c_is_start_fake.return_val = true;
    auto result = i2c_.start_generation();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(i2c_is_start_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_is_start_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, NackReceived) {
    i2c_nack_fake.return_val = false;
    auto result = i2c_.nack_received();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(i2c_nack_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_nack_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, IsBusy) {
    i2c_busy_fake.return_val = true;
    auto result = i2c_.is_busy();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(i2c_busy_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_busy_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, DataTransmitted) {
    i2c_transmit_int_status_fake.return_val = false;
    auto result = i2c_.is_tx_empty();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(i2c_transmit_int_status_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_transmit_int_status_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, TransferCompleted) {
    i2c_transfer_complete_fake.return_val = true;
    auto result = i2c_.transfer_completed();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(i2c_transfer_complete_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_transfer_complete_fake.arg0_val, Eq(instance_));
}

TEST_F(I2CTest, DataReceived) {
    i2c_received_data_fake.return_val = false;
    auto result = i2c_.is_rx_not_empty();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(i2c_received_data_fake.call_count, Eq(1));
    ASSERT_THAT(i2c_received_data_fake.arg0_val, Eq(instance_));
}

} // namespace
