#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/crc.h"
#include "libopencm3/stm32/dma.h"
#include "spl/peripherals/dma/dma.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(dma_enable_channel, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_channel, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_channel_reset, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_set_peripheral_address, std::uint32_t, std::uint8_t, std::uint32_t)
FAKE_VOID_FUNC(dma_set_memory_address, std::uint32_t, std::uint8_t, std::uint32_t)
FAKE_VOID_FUNC(dma_set_number_of_data, std::uint32_t, std::uint8_t, std::uint16_t)
FAKE_VOID_FUNC(dma_enable_memory_increment_mode, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_memory_increment_mode, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_enable_peripheral_increment_mode, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_peripheral_increment_mode, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_enable_half_transfer_interrupt, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_enable_transfer_complete_interrupt, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_enable_transfer_error_interrupt, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_half_transfer_interrupt, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_transfer_complete_interrupt, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_transfer_error_interrupt, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_clear_interrupt_flags, std::uint32_t, std::uint8_t, std::uint32_t)
FAKE_VALUE_FUNC(bool, dma_get_interrupt_flag, std::uint32_t, std::uint8_t, std::uint32_t)
FAKE_VOID_FUNC(dma_set_read_from_memory, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_set_read_from_peripheral, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_set_channel_request, std::uint32_t, std::uint8_t, std::uint8_t)
FAKE_VOID_FUNC(dma_enable_mem2mem_mode, std::uint32_t, std::uint8_t)
FAKE_VOID_FUNC(dma_disable_mem2mem_mode, std::uint32_t, std::uint8_t)

namespace {

using namespace spl::peripherals::dma;
using namespace testing;

struct DMATest : Test {
    DMATest(){
        RESET_FAKE(dma_enable_channel)                      //
        RESET_FAKE(dma_disable_channel)                     //
        RESET_FAKE(dma_channel_reset)                       //
        RESET_FAKE(dma_set_peripheral_address)              //
        RESET_FAKE(dma_set_memory_address)                  //
        RESET_FAKE(dma_set_number_of_data)                  //
        RESET_FAKE(dma_enable_memory_increment_mode)        //
        RESET_FAKE(dma_disable_memory_increment_mode)       //
        RESET_FAKE(dma_enable_peripheral_increment_mode)    //
        RESET_FAKE(dma_disable_peripheral_increment_mode)   //
        RESET_FAKE(dma_enable_half_transfer_interrupt)      //
        RESET_FAKE(dma_enable_transfer_complete_interrupt)  //
        RESET_FAKE(dma_enable_transfer_error_interrupt)     //
        RESET_FAKE(dma_disable_half_transfer_interrupt)     //
        RESET_FAKE(dma_disable_transfer_complete_interrupt) //
        RESET_FAKE(dma_disable_transfer_error_interrupt)    //
        RESET_FAKE(dma_clear_interrupt_flags)               //
        RESET_FAKE(dma_get_interrupt_flag)                  //
        RESET_FAKE(dma_set_read_from_memory)                //
        RESET_FAKE(dma_set_read_from_peripheral)            //
        RESET_FAKE(dma_set_channel_request)                 //
        RESET_FAKE(dma_enable_mem2mem_mode)                 //
        RESET_FAKE(dma_disable_mem2mem_mode)                //
    }

    dma dma_{instance::dma1};
    static constexpr auto instance_ = DMA1;
};

TEST_F(DMATest, EnableChannel) {
    auto& result = dma_.enable_channel(channel1);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_enable_channel_fake.call_count, Eq(1));
    ASSERT_THAT(dma_enable_channel_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_enable_channel_fake.arg1_val, Eq(DMA_CHANNEL1));
}

TEST_F(DMATest, DisableChannel) {
    auto& result = dma_.disable_channel(channel2);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_disable_channel_fake.call_count, Eq(1));
    ASSERT_THAT(dma_disable_channel_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_disable_channel_fake.arg1_val, Eq(DMA_CHANNEL2));
}

TEST_F(DMATest, ResetChannel) {
    auto& result = dma_.reset_channel(channel3);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_channel_reset_fake.call_count, Eq(1));
    ASSERT_THAT(dma_channel_reset_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_channel_reset_fake.arg1_val, Eq(DMA_CHANNEL3));
}

TEST_F(DMATest, SetPeripheralAddress) {
    auto& result = dma_.set_peripheral_address(channel4, peripheral::crc);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_peripheral_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg1_val, Eq(DMA_CHANNEL4));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg2_val, Eq(CRC_BASE));
}

TEST_F(DMATest, SetMemoryAddress) {
    auto& result = dma_.set_memory_address(channel5, 0xDEADBEEF);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_memory_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_memory_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_memory_address_fake.arg1_val, Eq(DMA_CHANNEL5));
    ASSERT_THAT(dma_set_memory_address_fake.arg2_val, Eq(0xDEADBEEF));
}

TEST_F(DMATest, SetMemoryAddressPointer) {
    std::array<std::uint32_t, 1> test{};
    auto& result = dma_.set_memory_address(channel5, test.data());

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_memory_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_memory_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_memory_address_fake.arg1_val, Eq(DMA_CHANNEL5));
    ASSERT_THAT(dma_set_memory_address_fake.arg2_val,
                Eq(reinterpret_cast<std::uint32_t>(test.data())));
}

TEST_F(DMATest, SetTransferSize) {
    auto& result = dma_.set_transfer_size(channel6, 0xBAAD);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_number_of_data_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_number_of_data_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_number_of_data_fake.arg1_val, Eq(DMA_CHANNEL6));
    ASSERT_THAT(dma_set_number_of_data_fake.arg2_val, Eq(0xBAAD));
}

TEST_F(DMATest, EnableMemoryIncrement) {
    auto& result = dma_.set_memory_increment(channel7, true);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_enable_memory_increment_mode_fake.call_count, Eq(1));
    ASSERT_THAT(dma_enable_memory_increment_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_enable_memory_increment_mode_fake.arg1_val, Eq(DMA_CHANNEL7));

    ASSERT_THAT(dma_disable_memory_increment_mode_fake.call_count, Eq(0));
}

TEST_F(DMATest, DisableMemoryIncrement) {
    auto& result = dma_.set_memory_increment(channel1, false);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_disable_memory_increment_mode_fake.call_count, Eq(1));
    ASSERT_THAT(dma_disable_memory_increment_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_disable_memory_increment_mode_fake.arg1_val, Eq(DMA_CHANNEL1));

    ASSERT_THAT(dma_enable_memory_increment_mode_fake.call_count, Eq(0));
}

TEST_F(DMATest, EnablePeripheralIncrement) {
    auto& result = dma_.set_peripheral_increment(channel2, true);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_enable_peripheral_increment_mode_fake.call_count, Eq(1));
    ASSERT_THAT(dma_enable_peripheral_increment_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_enable_peripheral_increment_mode_fake.arg1_val, Eq(DMA_CHANNEL2));

    ASSERT_THAT(dma_disable_peripheral_increment_mode_fake.call_count, Eq(0));
}

TEST_F(DMATest, DisablePeripheralIncrement) {
    auto& result = dma_.set_peripheral_increment(channel3, false);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_disable_peripheral_increment_mode_fake.call_count, Eq(1));
    ASSERT_THAT(dma_disable_peripheral_increment_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_disable_peripheral_increment_mode_fake.arg1_val, Eq(DMA_CHANNEL3));

    ASSERT_THAT(dma_enable_peripheral_increment_mode_fake.call_count, Eq(0));
}

TEST_F(DMATest, EnableIrq) {
    {
        auto& result = dma_.enable_irq(channel4, irq::transfer_error);

        ASSERT_THAT(&dma_, Eq(&result));
        ASSERT_THAT(dma_enable_transfer_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_enable_transfer_error_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(dma_enable_transfer_error_interrupt_fake.arg1_val, Eq(DMA_CHANNEL4));

        ASSERT_THAT(dma_enable_transfer_complete_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(dma_enable_half_transfer_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = dma_.enable_irq(channel5, irq::transfer_completed);

        ASSERT_THAT(&dma_, Eq(&result));
        ASSERT_THAT(dma_enable_transfer_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_enable_transfer_complete_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(dma_enable_transfer_complete_interrupt_fake.arg1_val, Eq(DMA_CHANNEL5));

        ASSERT_THAT(dma_enable_transfer_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_enable_half_transfer_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = dma_.enable_irq(channel6, irq::half_transfer);

        ASSERT_THAT(&dma_, Eq(&result));
        ASSERT_THAT(dma_enable_half_transfer_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_enable_half_transfer_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(dma_enable_half_transfer_interrupt_fake.arg1_val, Eq(DMA_CHANNEL6));

        ASSERT_THAT(dma_enable_transfer_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_enable_transfer_complete_interrupt_fake.call_count, Eq(1));
    }
}

TEST_F(DMATest, DisableIrq) {
    {
        auto& result = dma_.disable_irq(channel4, irq::transfer_error);

        ASSERT_THAT(&dma_, Eq(&result));
        ASSERT_THAT(dma_disable_transfer_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_disable_transfer_error_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(dma_disable_transfer_error_interrupt_fake.arg1_val, Eq(DMA_CHANNEL4));

        ASSERT_THAT(dma_disable_transfer_complete_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(dma_disable_half_transfer_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = dma_.disable_irq(channel5, irq::transfer_completed);

        ASSERT_THAT(&dma_, Eq(&result));
        ASSERT_THAT(dma_disable_transfer_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_disable_transfer_complete_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(dma_disable_transfer_complete_interrupt_fake.arg1_val, Eq(DMA_CHANNEL5));

        ASSERT_THAT(dma_disable_transfer_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_disable_half_transfer_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = dma_.disable_irq(channel6, irq::half_transfer);

        ASSERT_THAT(&dma_, Eq(&result));
        ASSERT_THAT(dma_disable_half_transfer_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_disable_half_transfer_interrupt_fake.arg0_val, Eq(instance_));
        ASSERT_THAT(dma_disable_half_transfer_interrupt_fake.arg1_val, Eq(DMA_CHANNEL6));

        ASSERT_THAT(dma_disable_transfer_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(dma_disable_transfer_complete_interrupt_fake.call_count, Eq(1));
    }
}

TEST_F(DMATest, ClearIrqTransferComplete) {
    auto& result = dma_.clear_irq(channel5, irq::transfer_completed);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.call_count, Eq(1));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg1_val, Eq(DMA_CHANNEL5));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg2_val, Eq(DMA_TCIF));
}

TEST_F(DMATest, ClearIrqTransferError) {
    auto& result = dma_.clear_irq(channel4, irq::transfer_error);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.call_count, Eq(1));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg1_val, Eq(DMA_CHANNEL4));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg2_val, Eq(DMA_TEIF));
}

TEST_F(DMATest, ClearIrqHalfTransfer) {
    auto& result = dma_.clear_irq(channel7, irq::half_transfer);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.call_count, Eq(1));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg1_val, Eq(DMA_CHANNEL7));
    ASSERT_THAT(dma_clear_interrupt_flags_fake.arg2_val, Eq(DMA_HTIF));
}

TEST_F(DMATest, GetIrqStatusTransferCompleteTrue) {
    dma_get_interrupt_flag_fake.return_val = true;

    auto result = dma_.get_irq_status(channel2, irq::transfer_completed);

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(dma_get_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg1_val, Eq(DMA_CHANNEL2));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg2_val, Eq(DMA_TCIF));
}

TEST_F(DMATest, GetIrqStatusTransferErrorTrue) {
    dma_get_interrupt_flag_fake.return_val = true;

    auto result = dma_.get_irq_status(channel3, irq::transfer_error);

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(dma_get_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg1_val, Eq(DMA_CHANNEL3));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg2_val, Eq(DMA_TEIF));
}

TEST_F(DMATest, ClearIrqStatusHalfTransferTrue) {
    dma_get_interrupt_flag_fake.return_val = true;

    auto result = dma_.get_irq_status(channel4, irq::half_transfer);

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(dma_get_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg1_val, Eq(DMA_CHANNEL4));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg2_val, Eq(DMA_HTIF));
}

TEST_F(DMATest, GetIrqStatusTransferCompleteFalse) {
    dma_get_interrupt_flag_fake.return_val = false;

    auto result = dma_.get_irq_status(channel2, irq::transfer_completed);

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(dma_get_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg1_val, Eq(DMA_CHANNEL2));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg2_val, Eq(DMA_TCIF));
}

TEST_F(DMATest, GetIrqStatusTransferErrorFalse) {
    dma_get_interrupt_flag_fake.return_val = false;

    auto result = dma_.get_irq_status(channel3, irq::transfer_error);

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(dma_get_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg1_val, Eq(DMA_CHANNEL3));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg2_val, Eq(DMA_TEIF));
}

TEST_F(DMATest, ClearIrqStatusHalfTransferFalse) {
    dma_get_interrupt_flag_fake.return_val = false;

    auto result = dma_.get_irq_status(channel4, irq::half_transfer);

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(dma_get_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg1_val, Eq(DMA_CHANNEL4));
    ASSERT_THAT(dma_get_interrupt_flag_fake.arg2_val, Eq(DMA_HTIF));
}

TEST_F(DMATest, SetDirectionPeripheralToMemory) {
    auto& result = dma_.set_direction(channel6, direction::peripheral_to_memory);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_read_from_peripheral_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_read_from_peripheral_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_read_from_peripheral_fake.arg1_val, Eq(DMA_CHANNEL6));

    ASSERT_THAT(dma_set_read_from_memory_fake.call_count, Eq(0));
}

TEST_F(DMATest, SetDirectionMemoryToPeripheral) {
    auto& result = dma_.set_direction(channel7, direction::memory_to_peripheral);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_read_from_memory_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_read_from_memory_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_read_from_memory_fake.arg1_val, Eq(DMA_CHANNEL7));

    ASSERT_THAT(dma_set_read_from_peripheral_fake.call_count, Eq(0));
}

TEST_F(DMATest, SetRequestMapping) {
    auto& result = dma_.set_request_mapping(channel4, request6);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_channel_request_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_channel_request_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_channel_request_fake.arg1_val, Eq(DMA_CHANNEL4));
    ASSERT_THAT(dma_set_channel_request_fake.arg2_val, Eq(0x5));
}

TEST_F(DMATest, SetM2MEnable) {
    auto& result = dma_.set_m2m(channel1, true);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_enable_mem2mem_mode_fake.call_count, Eq(1));
    ASSERT_THAT(dma_enable_mem2mem_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_enable_mem2mem_mode_fake.arg1_val, Eq(DMA_CHANNEL1));
}

TEST_F(DMATest, SetM2MDisable) {
    auto& result = dma_.set_m2m(channel7, false);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_disable_mem2mem_mode_fake.call_count, Eq(1));
    ASSERT_THAT(dma_disable_mem2mem_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_disable_mem2mem_mode_fake.arg1_val, Eq(DMA_CHANNEL7));
}

TEST_F(DMATest, SetM2MSource) {
    auto& result = dma_.set_m2m_source(channel5, 0xDEADBEEF);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_peripheral_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg1_val, Eq(DMA_CHANNEL5));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg2_val, Eq(0xDEADBEEF));
}

TEST_F(DMATest, SetM2MSourcePointer) {
    std::array<std::uint32_t, 1> test{};
    auto& result = dma_.set_m2m_source(channel5, test.data());

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_peripheral_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg1_val, Eq(DMA_CHANNEL5));
    ASSERT_THAT(dma_set_peripheral_address_fake.arg2_val,
                Eq(reinterpret_cast<std::uint32_t>(test.data())));
}

TEST_F(DMATest, SetM2MDestination) {
    auto& result = dma_.set_m2m_destination(channel3, 0xDEADC0DE);

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_memory_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_memory_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_memory_address_fake.arg1_val, Eq(DMA_CHANNEL3));
    ASSERT_THAT(dma_set_memory_address_fake.arg2_val, Eq(0xDEADC0DE));
}

TEST_F(DMATest, SetM2MDestinationPointer) {
    std::array<std::uint32_t, 1> test{};
    auto& result = dma_.set_m2m_destination(channel3, test.data());

    ASSERT_THAT(&dma_, Eq(&result));
    ASSERT_THAT(dma_set_memory_address_fake.call_count, Eq(1));
    ASSERT_THAT(dma_set_memory_address_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(dma_set_memory_address_fake.arg1_val, Eq(DMA_CHANNEL3));
    ASSERT_THAT(dma_set_memory_address_fake.arg2_val,
                Eq(reinterpret_cast<std::uint32_t>(test.data())));
}

} // namespace
