#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/usart.h"
#include "spl/peripherals/uart/uart.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(usart_enable, std::uint32_t)
FAKE_VOID_FUNC(usart_disable, std::uint32_t)
FAKE_VOID_FUNC(usart_set_baudrate, std::uint32_t, std::uint32_t)
FAKE_VOID_FUNC(usart_set_mode, std::uint32_t, std::uint32_t)
FAKE_VOID_FUNC(usart_enable_tx_complete_interrupt, std::uint32_t)
FAKE_VOID_FUNC(usart_disable_tx_complete_interrupt, std::uint32_t)
FAKE_VALUE_FUNC(bool, usart_tx_complete_interrupt_enabled, std::uint32_t)
FAKE_VOID_FUNC(usart_enable_tx_interrupt, std::uint32_t)
FAKE_VOID_FUNC(usart_disable_tx_interrupt, std::uint32_t)
FAKE_VALUE_FUNC(bool, usart_tx_interrupt_enabled, std::uint32_t)
FAKE_VOID_FUNC(usart_enable_rx_interrupt, std::uint32_t)
FAKE_VOID_FUNC(usart_disable_rx_interrupt, std::uint32_t)
FAKE_VALUE_FUNC(bool, usart_rx_interrupt_enabled, std::uint32_t)
FAKE_VOID_FUNC(usart_send, std::uint32_t, std::uint16_t)
FAKE_VALUE_FUNC(std::uint16_t, usart_recv, std::uint32_t)
FAKE_VALUE_FUNC(bool, usart_get_flag, std::uint32_t, std::uint32_t)
FAKE_VOID_FUNC(usart_clear_interrupt_flag, std::uint32_t, std::uint32_t)
FAKE_VOID_FUNC(usart_enable_tx_dma, std::uint32_t)
FAKE_VOID_FUNC(usart_disable_tx_dma, std::uint32_t)
FAKE_VOID_FUNC(usart_enable_rx_dma, std::uint32_t)
FAKE_VOID_FUNC(usart_disable_rx_dma, std::uint32_t)

namespace {

using namespace spl::peripherals::uart;
using namespace testing;

struct UartTest : Test {
    UartTest(){
        RESET_FAKE(usart_enable)                          //
        RESET_FAKE(usart_disable)                         //
        RESET_FAKE(usart_set_baudrate)                    //
        RESET_FAKE(usart_set_mode)                        //
        RESET_FAKE(usart_enable_tx_complete_interrupt)    //
        RESET_FAKE(usart_disable_tx_complete_interrupt)   //
        RESET_FAKE(usart_tx_complete_interrupt_enabled)   //
        RESET_FAKE(usart_enable_tx_interrupt)             //
        RESET_FAKE(usart_disable_tx_interrupt)            //
        RESET_FAKE(usart_tx_interrupt_enabled)            //
        RESET_FAKE(usart_enable_rx_interrupt)             //
        RESET_FAKE(usart_disable_rx_interrupt)            //
        RESET_FAKE(usart_rx_interrupt_enabled)            //
        RESET_FAKE(usart_send)                            //
        RESET_FAKE(usart_recv) RESET_FAKE(usart_get_flag) //
        RESET_FAKE(usart_clear_interrupt_flag)            //
        RESET_FAKE(usart_enable_tx_dma)                   //
        RESET_FAKE(usart_disable_tx_dma)                  //
        RESET_FAKE(usart_enable_rx_dma)                   //
        RESET_FAKE(usart_disable_rx_dma)                  //
    }

    uart uart_{instance::uart1};
    static constexpr auto instance_ = USART1;
};

TEST_F(UartTest, Enable) {
    auto& result = uart_.enable();

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_enable_fake.call_count, Eq(1));
    ASSERT_THAT(usart_enable_fake.arg0_val, Eq(instance_));
}

TEST_F(UartTest, Disable) {
    auto& result = uart_.disable();

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_disable_fake.call_count, Eq(1));
    ASSERT_THAT(usart_disable_fake.arg0_val, Eq(instance_));
}

TEST_F(UartTest, SetBaudrate) {
    auto& result = uart_.set_baudrate(115200);

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_set_baudrate_fake.call_count, Eq(1));
    ASSERT_THAT(usart_set_baudrate_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_set_baudrate_fake.arg1_val, Eq(115200));
}

TEST_F(UartTest, SetMode) {
    auto& result = uart_.set_mode(mode::rx_tx);

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_set_mode_fake.call_count, Eq(1));
    ASSERT_THAT(usart_set_mode_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_set_mode_fake.arg1_val, Eq(USART_MODE_TX_RX));
}

TEST_F(UartTest, EnableIrq) {
    {
        auto& result = uart_.enable_irq(irq::tx_complete);

        ASSERT_THAT(&uart_, Eq(&result));
        ASSERT_THAT(usart_enable_tx_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_enable_tx_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(usart_enable_rx_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(usart_enable_tx_complete_interrupt_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = uart_.enable_irq(irq::tx_empty);

        ASSERT_THAT(&uart_, Eq(&result));
        ASSERT_THAT(usart_enable_tx_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_enable_tx_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_enable_rx_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(usart_enable_tx_interrupt_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = uart_.enable_irq(irq::rx_not_empty);

        ASSERT_THAT(&uart_, Eq(&result));
        ASSERT_THAT(usart_enable_tx_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_enable_tx_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_enable_rx_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_enable_rx_interrupt_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(UartTest, DisableIrq) {
    {
        auto& result = uart_.disable_irq(irq::tx_complete);

        ASSERT_THAT(&uart_, Eq(&result));
        ASSERT_THAT(usart_disable_tx_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_disable_tx_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(usart_disable_rx_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(usart_disable_tx_complete_interrupt_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = uart_.disable_irq(irq::tx_empty);

        ASSERT_THAT(&uart_, Eq(&result));
        ASSERT_THAT(usart_disable_tx_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_disable_tx_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_disable_rx_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(usart_disable_tx_interrupt_fake.arg0_val, Eq(instance_));
    }
    {
        auto& result = uart_.disable_irq(irq::rx_not_empty);

        ASSERT_THAT(&uart_, Eq(&result));
        ASSERT_THAT(usart_disable_tx_complete_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_disable_tx_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_disable_rx_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(usart_disable_rx_interrupt_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(UartTest, IsIrqEnabledTrue) {
    {
        usart_tx_complete_interrupt_enabled_fake.return_val = true;
        auto result = uart_.is_irq_enabled(irq::tx_complete);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.arg0_val, Eq(instance_));
    }
    {
        usart_tx_interrupt_enabled_fake.return_val = true;
        auto result = uart_.is_irq_enabled(irq::tx_empty);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.arg0_val, Eq(instance_));
    }
    {
        usart_rx_interrupt_enabled_fake.return_val = true;
        auto result = uart_.is_irq_enabled(irq::rx_not_empty);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(UartTest, IsIrqEnabledFalse) {
    {
        usart_tx_complete_interrupt_enabled_fake.return_val = false;
        auto result = uart_.is_irq_enabled(irq::tx_complete);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.arg0_val, Eq(instance_));
    }
    {
        usart_tx_interrupt_enabled_fake.return_val = false;
        auto result = uart_.is_irq_enabled(irq::tx_empty);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.arg0_val, Eq(instance_));
    }
    {
        usart_rx_interrupt_enabled_fake.return_val = false;
        auto result = uart_.is_irq_enabled(irq::rx_not_empty);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(usart_tx_complete_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_tx_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(usart_rx_interrupt_enabled_fake.arg0_val, Eq(instance_));
    }
}

TEST_F(UartTest, IsTxEmptyTrue) {
    usart_get_flag_fake.return_val = true;

    auto result = uart_.is_tx_empty();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(usart_get_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_get_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_get_flag_fake.arg1_val, Eq(USART_FLAG_TXE));
}

TEST_F(UartTest, IsTxEmptyFalse) {
    usart_get_flag_fake.return_val = false;

    auto result = uart_.is_tx_empty();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(usart_get_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_get_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_get_flag_fake.arg1_val, Eq(USART_FLAG_TXE));
}

TEST_F(UartTest, IsRxNotEmptyTrue) {
    usart_get_flag_fake.return_val = true;

    auto result = uart_.is_rx_not_empty();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(usart_get_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_get_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_get_flag_fake.arg1_val, Eq(USART_FLAG_RXNE));
}

TEST_F(UartTest, IsRxNotEmptyFalse) {
    usart_get_flag_fake.return_val = false;

    auto result = uart_.is_rx_not_empty();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(usart_get_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_get_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_get_flag_fake.arg1_val, Eq(USART_FLAG_RXNE));
}

TEST_F(UartTest, OverrunErrorTrue) {
    usart_get_flag_fake.return_val = true;

    auto result = uart_.overrun_error();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(usart_get_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_get_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_get_flag_fake.arg1_val, Eq(USART_FLAG_ORE));
}

TEST_F(UartTest, OverrunErrorFalse) {
    usart_get_flag_fake.return_val = false;

    auto result = uart_.overrun_error();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(usart_get_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_get_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_get_flag_fake.arg1_val, Eq(USART_FLAG_ORE));
}

TEST_F(UartTest, ClearIrqFlag) {
    uart_.clear_irq_flag(clear_irq::overrun);

    ASSERT_THAT(usart_clear_interrupt_flag_fake.call_count, Eq(1));
    ASSERT_THAT(usart_clear_interrupt_flag_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_clear_interrupt_flag_fake.arg1_val, Eq(USART_ICR_ORECF));
}

TEST_F(UartTest, Write) {
    uart_.write(0xdead);

    ASSERT_THAT(usart_send_fake.call_count, Eq(1));
    ASSERT_THAT(usart_send_fake.arg0_val, Eq(instance_));
    ASSERT_THAT(usart_send_fake.arg1_val, Eq(0xdead));
}

TEST_F(UartTest, Read) {
    usart_recv_fake.return_val = 0xbeef;

    auto result = uart_.read();

    ASSERT_THAT(result, Eq(0xbeef));
    ASSERT_THAT(usart_recv_fake.call_count, Eq(1));
    ASSERT_THAT(usart_recv_fake.arg0_val, Eq(instance_));
}

TEST_F(UartTest, EnableTxDMA) {
    auto& result = uart_.enable_dma(dma::tx);

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_enable_tx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(usart_enable_tx_dma_fake.arg0_val, Eq(instance_));
}

TEST_F(UartTest, DisableTxDMA) {
    auto& result = uart_.disable_dma(dma::tx);

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_disable_tx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(usart_disable_tx_dma_fake.arg0_val, Eq(instance_));
}

TEST_F(UartTest, EnableRxDMA) {
    auto& result = uart_.enable_dma(dma::rx);

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_enable_rx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(usart_enable_rx_dma_fake.arg0_val, Eq(instance_));
}

TEST_F(UartTest, DisableRxDMA) {
    auto& result = uart_.disable_dma(dma::rx);

    ASSERT_THAT(&uart_, Eq(&result));
    ASSERT_THAT(usart_disable_rx_dma_fake.call_count, Eq(1));
    ASSERT_THAT(usart_disable_rx_dma_fake.arg0_val, Eq(instance_));
}

} // namespace
