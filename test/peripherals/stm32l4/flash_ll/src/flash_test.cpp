#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/flash.h"
#include "spl/peripherals/flash/flash.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(flash_set_ws, std::uint32_t)
FAKE_VOID_FUNC(flash_lock)
FAKE_VOID_FUNC(flash_unlock)
FAKE_VOID_FUNC(flash_program_double_word, std::uint32_t, std::uint64_t)
FAKE_VOID_FUNC(flash_program, std::uint32_t, std::uint8_t*, std::uint32_t)
FAKE_VOID_FUNC(flash_erase_page, std::uint32_t)
FAKE_VOID_FUNC(flash_erase_all_pages)
FAKE_VOID_FUNC(flash_enable_end_of_operation_interrupt)
FAKE_VOID_FUNC(flash_disable_end_of_operation_interrupt)
FAKE_VOID_FUNC(flash_clear_end_of_operation_interrupt)
FAKE_VOID_FUNC(flash_enable_operation_error_interrupt)
FAKE_VOID_FUNC(flash_disable_operation_error_interrupt)
FAKE_VOID_FUNC(flash_clear_operation_error_interrupt)
FAKE_VOID_FUNC(flash_enable_read_error_interrupt)
FAKE_VOID_FUNC(flash_disable_read_error_interrupt)
FAKE_VOID_FUNC(flash_clear_read_error_interrupt)
FAKE_VOID_FUNC(flash_enable_ecc_correction_interrupt)
FAKE_VOID_FUNC(flash_disable_ecc_correction_interrupt)
FAKE_VOID_FUNC(flash_clear_ecc_correction_interrupt)
FAKE_VALUE_FUNC(bool, flash_is_busy)
FAKE_VALUE_FUNC(bool, flash_is_end_of_operation_interrupt_enabled)
FAKE_VALUE_FUNC(bool, flash_is_operation_error_interrupt_enabled)
FAKE_VALUE_FUNC(bool, flash_is_read_error_interrupt_enabled)
FAKE_VALUE_FUNC(bool, flash_is_ecc_correction_interrupt_enabled)
FAKE_VALUE_FUNC(bool, flash_end_of_operation)
FAKE_VALUE_FUNC(bool, flash_operation_error)
FAKE_VALUE_FUNC(bool, flash_read_error)
FAKE_VALUE_FUNC(bool, flash_ecc_correction)

namespace {

using namespace spl::peripherals::flash;
using namespace testing;

struct FlashTest : Test {
    FlashTest(){
        RESET_FAKE(flash_set_ws)                                //
        RESET_FAKE(flash_lock)                                  //
        RESET_FAKE(flash_unlock)                                //
        RESET_FAKE(flash_program_double_word)                   //
        RESET_FAKE(flash_program)                               //
        RESET_FAKE(flash_erase_page)                            //
        RESET_FAKE(flash_erase_all_pages)                       //
        RESET_FAKE(flash_enable_end_of_operation_interrupt)     //
        RESET_FAKE(flash_disable_end_of_operation_interrupt)    //
        RESET_FAKE(flash_clear_end_of_operation_interrupt)      //
        RESET_FAKE(flash_enable_operation_error_interrupt)      //
        RESET_FAKE(flash_disable_operation_error_interrupt)     //
        RESET_FAKE(flash_clear_operation_error_interrupt)       //
        RESET_FAKE(flash_enable_read_error_interrupt)           //
        RESET_FAKE(flash_disable_read_error_interrupt)          //
        RESET_FAKE(flash_clear_read_error_interrupt)            //
        RESET_FAKE(flash_enable_ecc_correction_interrupt)       //
        RESET_FAKE(flash_disable_ecc_correction_interrupt)      //
        RESET_FAKE(flash_clear_ecc_correction_interrupt)        //
        RESET_FAKE(flash_is_busy)                               //
        RESET_FAKE(flash_is_end_of_operation_interrupt_enabled) //
        RESET_FAKE(flash_is_operation_error_interrupt_enabled)  //
        RESET_FAKE(flash_is_read_error_interrupt_enabled)       //
        RESET_FAKE(flash_is_ecc_correction_interrupt_enabled)   //
        RESET_FAKE(flash_end_of_operation)                      //
        RESET_FAKE(flash_operation_error)                       //
        RESET_FAKE(flash_read_error)                            //
        RESET_FAKE(flash_ecc_correction)                        //
    }

    flash flash_;
};

TEST_F(FlashTest, SetWaitState) {
    auto& result = flash_.set_wait_states(ws3);

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_set_ws_fake.call_count, Eq(1));
    ASSERT_THAT(flash_set_ws_fake.arg0_val, Eq(3));
}

TEST_F(FlashTest, Lock) {
    auto& result = flash_.lock();

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_lock_fake.call_count, Eq(1));
}

TEST_F(FlashTest, Unlock) {
    auto& result = flash_.unlock();

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_unlock_fake.call_count, Eq(1));
}

TEST_F(FlashTest, ProgramU64) {
    auto& result = flash_.program_u64(0xdeadc0de, 0xdeadbeefcafebabe);

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_program_double_word_fake.call_count, Eq(1));
    ASSERT_THAT(flash_program_double_word_fake.arg0_val, Eq(0xdeadc0de));
    ASSERT_THAT(flash_program_double_word_fake.arg1_val, Eq(0xdeadbeefcafebabe));
}

TEST_F(FlashTest, ProgramBuffer) {
    std::array<std::byte, 4> buffer{};
    auto& result = flash_.program_buffer(0xdeadc0de, buffer);

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_program_fake.call_count, Eq(1));
    ASSERT_THAT(flash_program_fake.arg0_val, Eq(0xdeadc0de));
    ASSERT_THAT(flash_program_fake.arg1_val, Eq(reinterpret_cast<std::uint8_t*>(buffer.data())));
    ASSERT_THAT(flash_program_fake.arg2_val, Eq(buffer.size()));
}

TEST_F(FlashTest, ErasePage) {
    auto& result = flash_.erase_page(10);

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_erase_page_fake.call_count, Eq(1));
    ASSERT_THAT(flash_erase_page_fake.arg0_val, Eq(10));
}

TEST_F(FlashTest, MassErase) {
    auto& result = flash_.mass_erase();

    ASSERT_THAT(&result, Eq(&flash_));
    ASSERT_THAT(flash_erase_all_pages_fake.call_count, Eq(1));
}

TEST_F(FlashTest, IsBusyTrue) {
    flash_is_busy_fake.return_val = true;
    auto result = flash_.is_busy();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(flash_is_busy_fake.call_count, Eq(1));
}

TEST_F(FlashTest, IsBusyFalse) {
    flash_is_busy_fake.return_val = false;
    auto result = flash_.is_busy();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(flash_is_busy_fake.call_count, Eq(1));
}

TEST_F(FlashTest, EnableIrq) {
    {
        auto& result = flash_.enable_irq(irq::read_error);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_enable_ecc_correction_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_enable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_operation_error_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_enable_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.enable_irq(irq::ecc_correction);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_enable_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_operation_error_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_enable_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.enable_irq(irq::operation_error);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_enable_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_operation_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.enable_irq(irq::end_of_operation);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_enable_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_operation_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_enable_end_of_operation_interrupt_fake.call_count, Eq(1));
    }
}

TEST_F(FlashTest, DisableIrq) {
    {
        auto& result = flash_.disable_irq(irq::read_error);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_disable_ecc_correction_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_disable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_operation_error_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_disable_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.disable_irq(irq::ecc_correction);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_disable_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_operation_error_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_disable_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.disable_irq(irq::operation_error);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_disable_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_operation_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.disable_irq(irq::end_of_operation);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_disable_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_operation_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_disable_end_of_operation_interrupt_fake.call_count, Eq(1));
    }
}

TEST_F(FlashTest, ClearIrq) {
    {
        auto& result = flash_.clear_irq_flag(irq::read_error);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_clear_ecc_correction_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_clear_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_operation_error_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_clear_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.clear_irq_flag(irq::ecc_correction);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_clear_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_operation_error_interrupt_fake.call_count, Eq(0));
        ASSERT_THAT(flash_clear_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.clear_irq_flag(irq::operation_error);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_clear_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_operation_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_end_of_operation_interrupt_fake.call_count, Eq(0));
    }
    {
        auto& result = flash_.clear_irq_flag(irq::end_of_operation);

        ASSERT_THAT(&result, Eq(&flash_));
        ASSERT_THAT(flash_clear_ecc_correction_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_read_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_operation_error_interrupt_fake.call_count, Eq(1));
        ASSERT_THAT(flash_clear_end_of_operation_interrupt_fake.call_count, Eq(1));
    }
}

TEST_F(FlashTest, IsIrqEnabledTrue) {
    {
        flash_is_read_error_interrupt_enabled_fake.return_val = true;
        auto result = flash_.is_irq_enabled(irq::read_error);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(0));
    }
    {
        flash_is_ecc_correction_interrupt_enabled_fake.return_val = true;
        auto result = flash_.is_irq_enabled(irq::ecc_correction);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(0));
    }
    {
        flash_is_operation_error_interrupt_enabled_fake.return_val = true;
        auto result = flash_.is_irq_enabled(irq::operation_error);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(0));
    }
    {
        flash_is_end_of_operation_interrupt_enabled_fake.return_val = true;
        auto result = flash_.is_irq_enabled(irq::end_of_operation);

        ASSERT_THAT(result, Eq(true));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(1));
    }
}

TEST_F(FlashTest, IsIrqEnabledFalse) {
    {
        flash_is_read_error_interrupt_enabled_fake.return_val = false;
        auto result = flash_.is_irq_enabled(irq::read_error);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(0));
    }
    {
        flash_is_ecc_correction_interrupt_enabled_fake.return_val = false;
        auto result = flash_.is_irq_enabled(irq::ecc_correction);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(0));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(0));
    }
    {
        flash_is_operation_error_interrupt_enabled_fake.return_val = false;
        auto result = flash_.is_irq_enabled(irq::operation_error);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(0));
    }
    {
        flash_is_end_of_operation_interrupt_enabled_fake.return_val = false;
        auto result = flash_.is_irq_enabled(irq::end_of_operation);

        ASSERT_THAT(result, Eq(false));
        ASSERT_THAT(flash_is_ecc_correction_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_read_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_operation_error_interrupt_enabled_fake.call_count, Eq(1));
        ASSERT_THAT(flash_is_end_of_operation_interrupt_enabled_fake.call_count, Eq(1));
    }
}

TEST_F(FlashTest, EndOfOperationTrue) {
    flash_end_of_operation_fake.return_val = true;
    auto result = flash_.end_of_operation();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(flash_end_of_operation_fake.call_count, Eq(1));
}

TEST_F(FlashTest, EndOfOperationFalse) {
    flash_end_of_operation_fake.return_val = false;
    auto result = flash_.end_of_operation();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(flash_end_of_operation_fake.call_count, Eq(1));
}

TEST_F(FlashTest, OperationErrorTrue) {
    flash_operation_error_fake.return_val = true;
    auto result = flash_.operation_error();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(flash_operation_error_fake.call_count, Eq(1));
}

TEST_F(FlashTest, OperationErrorFalse) {
    flash_operation_error_fake.return_val = false;
    auto result = flash_.operation_error();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(flash_operation_error_fake.call_count, Eq(1));
}

TEST_F(FlashTest, ReadErrorTrue) {
    flash_read_error_fake.return_val = true;
    auto result = flash_.read_error();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(flash_read_error_fake.call_count, Eq(1));
}

TEST_F(FlashTest, ReadErrorFalse) {
    flash_read_error_fake.return_val = false;
    auto result = flash_.read_error();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(flash_read_error_fake.call_count, Eq(1));
}

TEST_F(FlashTest, ECCCorectionTrue) {
    flash_ecc_correction_fake.return_val = true;
    auto result = flash_.ecc_correction();

    ASSERT_THAT(result, Eq(true));
    ASSERT_THAT(flash_ecc_correction_fake.call_count, Eq(1));
}

TEST_F(FlashTest, ECCCorectionFalse) {
    flash_ecc_correction_fake.return_val = false;
    auto result = flash_.ecc_correction();

    ASSERT_THAT(result, Eq(false));
    ASSERT_THAT(flash_ecc_correction_fake.call_count, Eq(1));
}

} // namespace
