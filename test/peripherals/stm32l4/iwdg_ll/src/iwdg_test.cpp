#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/iwdg.h"
#include "spl/peripherals/iwdg/iwdg.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(iwdg_start)
FAKE_VOID_FUNC(iwdg_set_period_ms, std::uint32_t)
FAKE_VOID_FUNC(iwdg_reset)

namespace {

using namespace spl::peripherals::iwdg;
using namespace testing;

struct IWDGTest : Test {
    IWDGTest(){
        RESET_FAKE(iwdg_start)         //
        RESET_FAKE(iwdg_set_period_ms) //
        RESET_FAKE(iwdg_reset)         //
    }

    iwdg iwdg_;
};

TEST_F(IWDGTest, Initialize) {
    auto& result = iwdg_.initialize();
    ASSERT_THAT(&result, Eq(&iwdg_));
    ASSERT_THAT(iwdg_start_fake.call_count, Eq(1));
}

TEST_F(IWDGTest, SetPeriodMs) {
    auto& result = iwdg_.set_period_ms(1234);
    ASSERT_THAT(&result, Eq(&iwdg_));
    ASSERT_THAT(iwdg_set_period_ms_fake.call_count, Eq(1));
    ASSERT_THAT(iwdg_set_period_ms_fake.arg0_val, Eq(1234));
}

TEST_F(IWDGTest, Kick) {
    auto& result = iwdg_.kick();
    ASSERT_THAT(&result, Eq(&iwdg_));
    ASSERT_THAT(iwdg_reset_fake.call_count, Eq(1));
}

} // namespace
