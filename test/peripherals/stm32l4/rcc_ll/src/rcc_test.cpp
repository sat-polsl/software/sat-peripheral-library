#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "fff.h"
#include "libopencm3/stm32/rcc.h"
#include "spl/peripherals/rcc/rcc.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(rcc_periph_clock_enable, rcc_periph_clken)
FAKE_VOID_FUNC(rcc_periph_clock_disable, rcc_periph_clken)
FAKE_VOID_FUNC(rcc_periph_reset_pulse, rcc_periph_rst)
FAKE_VOID_FUNC(rcc_set_sysclk_source, std::uint32_t)
FAKE_VALUE_FUNC(std::uint32_t, rcc_system_clock_source)
FAKE_VOID_FUNC(rcc_osc_on, rcc_osc)
FAKE_VOID_FUNC(rcc_osc_off, rcc_osc)
FAKE_VOID_FUNC(rcc_set_pll_source, std::uint32_t)
FAKE_VALUE_FUNC(bool, rcc_is_osc_ready, rcc_osc)
FAKE_VOID_FUNC(rcc_pll_output_enable, std::uint32_t)
FAKE_VOID_FUNC(rcc_pll_output_disable, std::uint32_t)
FAKE_VOID_FUNC(rcc_set_ahb_frequency, std::uint32_t)
FAKE_VOID_FUNC(rcc_set_apb1_frequency, std::uint32_t)
FAKE_VOID_FUNC(rcc_set_apb2_frequency, std::uint32_t)
FAKE_VOID_FUNC(rcc_set_plln, std::uint32_t)
FAKE_VOID_FUNC(rcc_set_pllm, std::uint32_t)
FAKE_VOID_FUNC(rcc_set_pllr, std::uint32_t)
FAKE_VALUE_FUNC(std::uint32_t, rcc_get_ahb_frequency)
FAKE_VALUE_FUNC(std::uint32_t, rcc_get_apb1_frequency)
FAKE_VALUE_FUNC(std::uint32_t, rcc_get_apb2_frequency)

namespace {

using namespace spl::peripherals::rcc;
using namespace testing;

struct RccTest : Test {
    RccTest(){
        RESET_FAKE(rcc_periph_clock_enable)  //
        RESET_FAKE(rcc_periph_clock_disable) //
        RESET_FAKE(rcc_periph_reset_pulse)   //
        RESET_FAKE(rcc_set_sysclk_source)    //
        RESET_FAKE(rcc_system_clock_source)  //
        RESET_FAKE(rcc_osc_on)               //
        RESET_FAKE(rcc_osc_off)              //
        RESET_FAKE(rcc_set_pll_source)       //
        RESET_FAKE(rcc_is_osc_ready)         //
        RESET_FAKE(rcc_set_plln)             //
        RESET_FAKE(rcc_set_pllm)             //
        RESET_FAKE(rcc_set_pllr)             //
        RESET_FAKE(rcc_pll_output_enable)    //
        RESET_FAKE(rcc_pll_output_disable)   //
        RESET_FAKE(rcc_set_ahb_frequency)    //
        RESET_FAKE(rcc_set_apb1_frequency)   //
        RESET_FAKE(rcc_set_apb2_frequency)   //
        RESET_FAKE(rcc_get_ahb_frequency)    //
        RESET_FAKE(rcc_get_apb1_frequency)   //
        RESET_FAKE(rcc_get_apb2_frequency)   //
    }

    rcc rcc_;
    static constexpr std::uint32_t ahb_freq = 30'000;
    static constexpr std::uint32_t apb1_freq = 30'000;
    static constexpr std::uint32_t apb2_freq = 30'000;
};

TEST_F(RccTest, EnableClock) {
    auto& result = rcc_.enable_clock(peripheral::dma1);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_periph_clock_enable_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_periph_clock_enable_fake.arg0_val, Eq(RCC_DMA1));
}

TEST_F(RccTest, DisableClock) {
    auto& result = rcc_.disable_clock(peripheral::can1);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_periph_clock_disable_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_periph_clock_disable_fake.arg0_val, Eq(RCC_CAN1));
}

TEST_F(RccTest, ResetPeripheral) {
    auto& result = rcc_.pulse_reset(peripheral::uart2);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_periph_reset_pulse_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_periph_reset_pulse_fake.arg0_val, Eq(RST_USART2));
}

TEST_F(RccTest, SetSysclkSource) {
    auto& result = rcc_.set_sysclk_source(clock_source::hse);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_sysclk_source_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_set_sysclk_source_fake.arg0_val, Eq(RCC_CFGR_SW_HSE));
}

TEST_F(RccTest, GetSysclkSource) {
    rcc_.get_sysclk_source();

    ASSERT_THAT(rcc_system_clock_source_fake.call_count, Eq(1));
}

TEST_F(RccTest, EnableOscillator) {
    auto& result = rcc_.enable_oscillator(oscillator::hse);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_osc_on_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_osc_on_fake.arg0_val, Eq(RCC_HSE));
}

TEST_F(RccTest, DisableOscillator) {
    auto& result = rcc_.disable_oscillator(oscillator::pll);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_osc_off_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_osc_off_fake.arg0_val, Eq(RCC_PLL));
}

TEST_F(RccTest, SetPllSource) {
    auto& result = rcc_.set_pll_source(pll_clock_source::hse);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_pll_source_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_set_pll_source_fake.arg0_val, Eq(RCC_PLLCFGR_PLLSRC_HSE));
}

TEST_F(RccTest, SetPllClockPrescaler) {
    auto& result = rcc_.set_pll_clock_prescaler(pll_prescaler::div4);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_pllr_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_set_pllr_fake.arg0_val, Eq(0x01));
}

TEST_F(RccTest, SetPllVcoMultiplier) {
    auto& result = rcc_.set_pll_vco_multiplier(80);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_plln_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_set_plln_fake.arg0_val, Eq(80));
}

TEST_F(RccTest, SetPllInputClockPrescaler) {
    auto& result = rcc_.set_pll_input_clock_prescaler(pll_input_prescaler::div8);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_pllm_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_set_pllm_fake.arg0_val, Eq(0x07));
}

TEST_F(RccTest, IsHseOscillatorReady) {
    rcc_.is_oscillator_ready(concepts::oscillator::hsi);

    ASSERT_THAT(rcc_is_osc_ready_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_is_osc_ready_fake.arg0_val, Eq(RCC_HSI16));
}

TEST_F(RccTest, SetAhbFrequency) {
    auto& result = rcc_.set_ahb_frequency(ahb_freq);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_ahb_frequency_fake.arg0_val, Eq(ahb_freq));
    ASSERT_THAT(rcc_set_ahb_frequency_fake.call_count, Eq(1));
}

TEST_F(RccTest, SetApb1Frequency) {
    auto& result = rcc_.set_apb1_frequency(apb1_freq);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_apb1_frequency_fake.arg0_val, Eq(ahb_freq));
    ASSERT_THAT(rcc_set_apb1_frequency_fake.call_count, Eq(1));
}

TEST_F(RccTest, SetApb2Frequency) {
    auto& result = rcc_.set_apb2_frequency(apb2_freq);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_set_apb2_frequency_fake.arg0_val, Eq(apb2_freq));
    ASSERT_THAT(rcc_set_apb2_frequency_fake.call_count, Eq(1));
}

TEST_F(RccTest, GetAhbFrequency) {
    rcc_.get_ahb_frequency();

    ASSERT_THAT(rcc_get_ahb_frequency_fake.call_count, Eq(1));
}

TEST_F(RccTest, GetApb1Frequency) {
    rcc_.get_apb1_frequency();

    ASSERT_THAT(rcc_get_apb1_frequency_fake.call_count, Eq(1));
}

TEST_F(RccTest, GetApb2Frequency) {
    rcc_.get_apb2_frequency();

    ASSERT_THAT(rcc_get_apb2_frequency_fake.call_count, Eq(1));
}

TEST_F(RccTest, EnablePllOutputPllclk) {
    auto& result = rcc_.enable_pll_output(pll_output::pllclk);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_pll_output_enable_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_pll_output_enable_fake.arg0_val, Eq(RCC_PLLCFGR_PLLREN));
}

TEST_F(RccTest, DisablePllOutputPllclk) {
    auto& result = rcc_.disable_pll_output(pll_output::pllclk);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_pll_output_disable_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_pll_output_disable_fake.arg0_val, Eq(RCC_PLLCFGR_PLLREN));
}

TEST_F(RccTest, EnablePllOutputPll48m1clk) {
    auto& result = rcc_.enable_pll_output(pll_output::pll48m1clk);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_pll_output_enable_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_pll_output_enable_fake.arg0_val, Eq(RCC_PLLCFGR_PLLQEN));
}

TEST_F(RccTest, DisablePllOutputPll48m1clk) {
    auto& result = rcc_.enable_pll_output(pll_output::pll48m1clk);

    ASSERT_THAT(&result, Eq(&rcc_));
    ASSERT_THAT(rcc_pll_output_enable_fake.call_count, Eq(1));
    ASSERT_THAT(rcc_pll_output_enable_fake.arg0_val, Eq(RCC_PLLCFGR_PLLQEN));
}
} // namespace
