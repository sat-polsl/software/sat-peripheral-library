CPMAddPackage("gl:sat-polsl/software/packages/arm-none-eabi-gcc@13.2.1")
arm_none_eabi_gcc_setup()

CPMAddPackage(
    NAME semihosting
    GITLAB_REPOSITORY sat-polsl/software/semihosting
    VERSION 1.2.0
    OPTIONS
    "SEMIHOSTING_COVERAGE_FILTER spl"
)

CPMAddPackage("gl:sat-polsl/software/packages/doxygen@1.9.6")
CPMAddPackage("gl:sat-polsl/software/libopencm3#sat-main")
CPMAddPackage("gl:sat-polsl/software/satext@2.4.1")
CPMAddPackage("gl:sat-polsl/software/baremetal@1.1.0")
CPMAddPackage("gl:sat-polsl/software/satos@5.2.0")

CPMAddPackage(
    NAME fff
    GITHUB_REPOSITORY silesian-aerospace-technologies/fff
    GIT_TAG master
    OPTIONS
    "FFF_BUILD_TESTS OFF"
    "FFF_BUILD_EXAMPLES OFF"
    "FFF_GENERATE OFF"
    EXCLUDE_FROM_ALL YES
)

CPMAddPackage(
    NAME googletest
    GITHUB_REPOSITORY google/googletest
    GIT_TAG v1.13.0
    OPTIONS
    "gtest_disable_pthreads ON"
    EXCLUDE_FROM_ALL YES
)

set_target_properties(gtest gmock gmock_main
    PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON
    )

target_compile_definitions(gtest
    PUBLIC
    _POSIX_PATH_MAX=128
    GTEST_HAS_POSIX_RE=0
    )

target_compile_definitions(gmock
    PUBLIC
    GTEST_HAS_POSIX_RE=0
)

