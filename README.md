[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_sat-peripheral-library&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_sat-peripheral-library)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_sat-peripheral-library&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_sat-peripheral-library)
[![Pipeline](https://gitlab.com/sat-polsl/software/sat-peripheral-library/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/sat-peripheral-library/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39874673%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/sat-peripheral-library/-/tags)

# Sat Peripheral Library

C++ Hardware Abstraction Library built upon [LibOpenCM3](https://libopencm3.org)

# Table of contents

- [Supported MCUs](#supported-mcus)
- [Reporitng an Issue](#reporting-an-issue)
- [Working with repsitory](#working-with-repository)
    - [Contribution flow](#contribution-flow)
    - [Commit message format](#commit-message-format)
- [Coding style](#coding-style)
- [Useful links](#useful-links)

## Supported MCUs

- STM32L4
 
## Reporting an issue
Bugs are tracked with [GitLab Issue Board](https://gitlab.com/sat-polsl/software/sat-peripheral-library/-/boards)

Explain the problem and include additional details to help maintainers reproduce the problem.

## Working with repository
### Contribution flow

1. Branch out from `main` branch and name your branch `spl<issue_number>-<shortdescription>`.
2. Make commits to your branch.
3. Make sure to pass all tests and add new if needed.
4. Push your work to remote branch
5. Submit a Merge Request to `main` branch

### Commit message format
Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Coding style
Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Useful links

- [Doxygen](https://sat-polsl.gitlab.io/software/sat-peripheral-library/index.html)

