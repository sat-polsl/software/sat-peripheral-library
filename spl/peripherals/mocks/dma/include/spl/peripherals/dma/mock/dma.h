#pragma once
#include <bit>
#include "gmock/gmock.h"
#include "spl/peripherals/dma/concepts/dma.h"

namespace spl::peripherals::dma::mock {
struct dma {
    MOCK_METHOD(const dma&, enable_channel, (concepts::channel), (const));
    MOCK_METHOD(const dma&, disable_channel, (concepts::channel), (const));
    MOCK_METHOD(const dma&, reset_channel, (concepts::channel), (const));
    MOCK_METHOD(const dma&,
                set_peripheral_address,
                (concepts::channel, concepts::peripheral),
                (const));
    MOCK_METHOD(const dma&, set_memory_address, (concepts::channel, std::uint32_t), (const));

    template<typename T>
    const dma& set_memory_address(concepts::channel ch, const T* ptr) const {
        return set_memory_address(ch, std::bit_cast<std::uint32_t>(ptr));
    }

    MOCK_METHOD(const dma&, set_transfer_size, (concepts::channel, std::uint16_t), (const));
    MOCK_METHOD(const dma&, set_peripheral_increment, (concepts::channel, bool), (const));
    MOCK_METHOD(const dma&, set_memory_increment, (concepts::channel, bool), (const));
    MOCK_METHOD(const dma&, enable_irq, (concepts::channel, concepts::irq), (const));
    MOCK_METHOD(const dma&, disable_irq, (concepts::channel, concepts::irq), (const));
    MOCK_METHOD(const dma&, clear_irq, (concepts::channel, concepts::irq), (const));
    MOCK_METHOD(bool, get_irq_status, (concepts::channel, concepts::irq), (const));
    MOCK_METHOD(const dma&, set_direction, (concepts::channel, concepts::direction), (const));
    MOCK_METHOD(const dma&, set_request_mapping, (concepts::channel, concepts::request), (const));
    MOCK_METHOD(const dma&, set_m2m, (concepts::channel, bool), (const));
    MOCK_METHOD(const dma&, set_m2m_source, (concepts::channel, std::uint32_t), (const));

    template<typename T>
    const dma& set_m2m_source(concepts::channel ch, const T* ptr) const {
        return set_m2m_source(ch, std::bit_cast<std::uint32_t>(ptr));
    }

    MOCK_METHOD(const dma&, set_m2m_destination, (concepts::channel, std::uint32_t), (const));

    template<typename T>
    const dma& set_m2m_destination(concepts::channel ch, const T* ptr) const {
        return set_m2m_destination(ch, std::bit_cast<std::uint32_t>(ptr));
    }
};

static_assert(concepts::dma<dma>);

inline void setup_default_return_value(dma& dma_) {
    ON_CALL(dma_, enable_channel).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, disable_channel).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, reset_channel).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_peripheral_address).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_memory_address).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_transfer_size).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_peripheral_increment).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_memory_increment).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, enable_irq).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, disable_irq).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, clear_irq).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_direction).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_request_mapping).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_m2m).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_m2m_source).WillByDefault(testing::ReturnRef(dma_));
    ON_CALL(dma_, set_m2m_destination).WillByDefault(testing::ReturnRef(dma_));
}

} // namespace spl::peripherals::dma::mock
