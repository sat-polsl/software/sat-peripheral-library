#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/nvic/concepts/nvic.h"

namespace spl::peripherals::nvic::mock {
struct nvic {
    MOCK_METHOD(const nvic&, enable_irq, (concepts::irq), (const));
    MOCK_METHOD(const nvic&, disable_irq, (concepts::irq), (const));
    MOCK_METHOD(const nvic&, set_priority, (concepts::irq, std::uint8_t), (const));
    MOCK_METHOD(const nvic&, clear_pending_irq, (concepts::irq), (const));
};

static_assert(concepts::nvic<nvic>);

inline void setup_default_return_value(nvic& nvic_) {
    ON_CALL(nvic_, enable_irq).WillByDefault(testing::ReturnRef(nvic_));
    ON_CALL(nvic_, disable_irq).WillByDefault(testing::ReturnRef(nvic_));
    ON_CALL(nvic_, set_priority).WillByDefault(testing::ReturnRef(nvic_));
    ON_CALL(nvic_, clear_pending_irq).WillByDefault(testing::ReturnRef(nvic_));
}

} // namespace spl::peripherals::nvic::mock
