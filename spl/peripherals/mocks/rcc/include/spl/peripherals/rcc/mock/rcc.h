#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/rcc/concepts/rcc.h"

namespace spl::peripherals::rcc::mock {
struct rcc {
    MOCK_METHOD(const rcc&, enable_clock, (concepts::peripheral), (const));
    MOCK_METHOD(const rcc&, disable_clock, (concepts::peripheral), (const));
    MOCK_METHOD(const rcc&, pulse_reset, (concepts::peripheral), (const));
    MOCK_METHOD(const rcc&, set_sysclk_source, (concepts::clock_source), (const));
    MOCK_METHOD(concepts::clock_source, get_sysclk_source, (), (const));
    MOCK_METHOD(const rcc&, enable_oscillator, (concepts::oscillator), (const));
    MOCK_METHOD(const rcc&, disable_oscillator, (concepts::oscillator), (const));
    MOCK_METHOD(bool, is_oscillator_ready, (concepts::oscillator), (const));
    MOCK_METHOD(const rcc&, set_pll_source, (concepts::pll_clock_source), (const));
    MOCK_METHOD(const rcc&, set_pll_clock_prescaler, (concepts::pll_prescaler), (const));
    MOCK_METHOD(const rcc&, set_pll_vco_multiplier, (std::int8_t), (const));
    MOCK_METHOD(const rcc&,
                set_pll_input_clock_prescaler,
                (concepts::pll_input_prescaler),
                (const));
    MOCK_METHOD(const rcc&, enable_pll_output, (concepts::pll_output), (const));
    MOCK_METHOD(const rcc&, disable_pll_output, (concepts::pll_output), (const));
    MOCK_METHOD(const rcc&, set_ahb_frequency, (std::uint32_t), (const));
    MOCK_METHOD(const rcc&, set_apb1_frequency, (std::uint32_t), (const));
    MOCK_METHOD(const rcc&, set_apb2_frequency, (std::uint32_t), (const));
    MOCK_METHOD(std::uint32_t, get_ahb_frequency, (), (const));
    MOCK_METHOD(std::uint32_t, get_apb1_frequency, (), (const));
    MOCK_METHOD(std::uint32_t, get_apb2_frequency, (), (const));
};

static_assert(concepts::rcc<rcc>);

inline void setup_default_return_value(rcc& rcc_) {
    ON_CALL(rcc_, enable_clock).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, disable_clock).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, pulse_reset).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_sysclk_source).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, enable_oscillator).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, disable_oscillator).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_pll_source).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_pll_clock_prescaler).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_pll_vco_multiplier).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_pll_input_clock_prescaler).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, enable_pll_output).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, disable_pll_output).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_ahb_frequency).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_apb1_frequency).WillByDefault(testing::ReturnRef(rcc_));
    ON_CALL(rcc_, set_apb2_frequency).WillByDefault(testing::ReturnRef(rcc_));
}

} // namespace spl::peripherals::rcc::mock
