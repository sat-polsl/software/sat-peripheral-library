add_mock_library(rcc_ll_mock
    LIBS
    spl::rcc_ll_concept
    INCLUDES
    include
    SOURCES
    include/spl/peripherals/rcc/mock/rcc.h
    )

add_library(spl::rcc_ll_mock ALIAS rcc_ll_mock)
