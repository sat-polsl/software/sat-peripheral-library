#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/flash/concepts/flash.h"

namespace spl::peripherals::flash::mock {
struct flash {
    MOCK_METHOD(const flash&, set_wait_states, (concepts::wait_states), (const));
    MOCK_METHOD(const flash&, lock, (), (const));
    MOCK_METHOD(const flash&, unlock, (), (const));
    MOCK_METHOD(const flash&, program_u64, (std::uint32_t, std::uint64_t), (const));
    MOCK_METHOD(const flash&, program_buffer, (std::uint32_t, std::span<const std::byte>), (const));
    MOCK_METHOD(const flash&, erase_page, (std::uint8_t), (const));
    MOCK_METHOD(const flash&, mass_erase, (), (const));
    MOCK_METHOD(const flash&, enable_irq, (concepts::irq), (const));
    MOCK_METHOD(const flash&, disable_irq, (concepts::irq), (const));
    MOCK_METHOD(const flash&, clear_irq_flag, (concepts::irq), (const));
    MOCK_METHOD(bool, is_irq_enabled, (concepts::irq), (const));
    MOCK_METHOD(bool, is_busy, (), (const));
    MOCK_METHOD(bool, end_of_operation, (), (const));
    MOCK_METHOD(bool, operation_error, (), (const));
    MOCK_METHOD(bool, read_error, (), (const));
    MOCK_METHOD(bool, ecc_correction, (), (const));
    static constexpr auto page_size = 128;
    static constexpr auto memory_base = 0x08000000;
};

static_assert(concepts::flash<flash>);

inline void setup_default_return_value(flash& flash_) {
    ON_CALL(flash_, set_wait_states).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, lock).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, unlock).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, program_u64).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, program_buffer).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, erase_page).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, mass_erase).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, enable_irq).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, disable_irq).WillByDefault(testing::ReturnRef(flash_));
    ON_CALL(flash_, clear_irq_flag).WillByDefault(testing::ReturnRef(flash_));
}

} // namespace spl::peripherals::flash::mock
