#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace spl::peripherals::gpio::mock {
struct gpio {
    MOCK_METHOD(void, set, (), (const));
    MOCK_METHOD(void, clear, (), (const));
    MOCK_METHOD(void, toggle, (), (const));
    MOCK_METHOD(bool, read, (), (const));
};

static_assert(concepts::gpio<gpio>);

} // namespace spl::peripherals::gpio::mock
