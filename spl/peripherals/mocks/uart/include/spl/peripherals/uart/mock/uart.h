#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/uart/concepts/uart.h"

namespace spl::peripherals::uart::mock {
struct uart {
    MOCK_METHOD(const uart&, set_baudrate, (std::uint32_t), (const));
    MOCK_METHOD(const uart&, set_mode, (concepts::mode), (const));
    MOCK_METHOD(const uart&, enable_irq, (concepts::irq), (const));
    MOCK_METHOD(const uart&, disable_irq, (concepts::irq), (const));
    MOCK_METHOD(bool, is_irq_enabled, (concepts::irq), (const));
    MOCK_METHOD(const uart&, enable, (), (const));
    MOCK_METHOD(const uart&, disable, (), (const));
    MOCK_METHOD(void, write, (std::uint16_t), (const));
    MOCK_METHOD(std::uint16_t, read, (), (const));
    MOCK_METHOD(bool, is_tx_empty, (), (const));
    MOCK_METHOD(bool, is_rx_not_empty, (), (const));
    MOCK_METHOD(bool, overrun_error, (), (const));
    MOCK_METHOD(void, clear_irq_flag, (concepts::clear_irq), (const));
    MOCK_METHOD(const uart&, enable_dma, (concepts::dma), (const));
    MOCK_METHOD(const uart&, disable_dma, (concepts::dma), (const));
};

static_assert(concepts::uart<uart>);

inline void setup_default_return_value(uart& uart_) {
    ON_CALL(uart_, set_baudrate).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, set_mode).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, enable_irq).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, disable_irq).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, enable).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, disable).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, enable_dma).WillByDefault(testing::ReturnRef(uart_));
    ON_CALL(uart_, disable_dma).WillByDefault(testing::ReturnRef(uart_));
}
} // namespace spl::peripherals::uart::mock
