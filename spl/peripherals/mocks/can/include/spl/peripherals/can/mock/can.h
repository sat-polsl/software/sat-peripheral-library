#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/can/concepts/can.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace spl::peripherals::can::mock {
struct can {
    MOCK_METHOD(void, initialize, (concepts::timings, concepts::options), (const));
    MOCK_METHOD(void, enable_irq, (concepts::irq), (const));
    MOCK_METHOD(void, disable_irq, (concepts::irq), (const));
    MOCK_METHOD(bool, is_irq_enabled, (concepts::irq), (const));
    MOCK_METHOD(bool, write, (const concepts::message&), (const));
    MOCK_METHOD(concepts::message, read, (concepts::rx_fifo), (const));
    MOCK_METHOD(void,
                setup_filter,
                (concepts::filter, std::uint32_t, std::uint32_t, concepts::rx_fifo, bool),
                (const));
    MOCK_METHOD(bool, is_tx_mailbox_empty, (), (const));
    MOCK_METHOD(bool, is_rx0_fifo_not_empty, (), (const));
    MOCK_METHOD(bool, is_rx1_fifo_not_empty, (), (const));
    MOCK_METHOD(bool, tx_mailbox_released, (), (const));
    MOCK_METHOD(void, clear_tx_mailbox_released, (), (const));
};

static_assert(concepts::can<can>);
} // namespace spl::peripherals::can::mock
