#pragma once
#include <cstdint>
#include "gmock/gmock.h"
#include "spl/peripherals/iwdg/concepts/iwdg.h"

namespace spl::peripherals::iwdg::mock {
struct iwdg {
    MOCK_METHOD(const iwdg&, initialize, (), (const));
    MOCK_METHOD(const iwdg&, set_period_ms, (std::uint32_t), (const));
    MOCK_METHOD(const iwdg&, kick, (), (const));
};

static_assert(concepts::iwdg<iwdg>);

inline void setup_default_return_value(iwdg& iwdg_) {
    ON_CALL(iwdg_, initialize).WillByDefault(testing::ReturnRef(iwdg_));
    ON_CALL(iwdg_, set_period_ms).WillByDefault(testing::ReturnRef(iwdg_));
    ON_CALL(iwdg_, kick).WillByDefault(testing::ReturnRef(iwdg_));
}

} // namespace spl::peripherals::iwdg::mock
