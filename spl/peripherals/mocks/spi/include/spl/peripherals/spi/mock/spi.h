#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/spi/concepts/spi.h"

namespace spl::peripherals::spi::mock {
struct spi {
    MOCK_METHOD(const spi&, enable, (), (const));
    MOCK_METHOD(const spi&, disable, (), (const));
    MOCK_METHOD(void, write, (std::uint16_t), (const));
    MOCK_METHOD(std::uint8_t, read, (), (const));
    MOCK_METHOD(std::uint8_t, xfer, (std::uint8_t), (const));
    MOCK_METHOD(const spi&, set_full_duplex_mode, (), (const));
    MOCK_METHOD(const spi&, set_receive_only_mode, (), (const));
    MOCK_METHOD(const spi&, set_slave_management, (concepts::slave_management), (const));
    MOCK_METHOD(const spi&, set_sending_order, (concepts::send_order), (const));
    MOCK_METHOD(const spi&, set_baudrate_prescaler, (concepts::baudrate_prescaler), (const));
    MOCK_METHOD(const spi&, set_mode, (concepts::mode), (const));
    MOCK_METHOD(const spi&, enable_irq, (concepts::irq), (const));
    MOCK_METHOD(const spi&, disable_irq, (concepts::irq), (const));
    MOCK_METHOD(const spi&, enable_dma, (concepts::dma), (const));
    MOCK_METHOD(const spi&, disable_dma, (concepts::dma), (const));
    MOCK_METHOD(const spi&, set_clock_mode, (concepts::clock_mode), (const));
    MOCK_METHOD(const spi&, set_data_size, (concepts::data_size), (const));
    MOCK_METHOD(const spi&, set_fifo_reception_threshold, (concepts::fifo_threshold), (const));
    MOCK_METHOD(const spi&, set_internal_slave_select, (bool), (const));
    MOCK_METHOD(bool, is_busy, (), (const));
    MOCK_METHOD(bool, is_tx_empty, (), (const));
    MOCK_METHOD(bool, is_rx_not_empty, (), (const));
    MOCK_METHOD(bool, overrun_error, (), (const));
    MOCK_METHOD(bool, is_rx_fifo_empty, (), (const));
};

static_assert(concepts::spi<spi>);

inline void setup_default_return_value(spi& spi_) {
    ON_CALL(spi_, enable).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, disable).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_full_duplex_mode).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_receive_only_mode).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_slave_management).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_sending_order).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_baudrate_prescaler).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_mode).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, enable_irq).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, disable_irq).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, enable_dma).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, disable_dma).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_clock_mode).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_data_size).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_fifo_reception_threshold).WillByDefault(testing::ReturnRef(spi_));
    ON_CALL(spi_, set_internal_slave_select).WillByDefault(testing::ReturnRef(spi_));
}
} // namespace spl::peripherals::spi::mock
