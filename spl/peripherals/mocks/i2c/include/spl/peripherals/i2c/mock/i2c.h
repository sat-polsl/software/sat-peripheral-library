#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/i2c/concepts/i2c.h"

namespace spl::peripherals::i2c::mock {
struct i2c {
    MOCK_METHOD(const i2c&, enable, (), (const));
    MOCK_METHOD(const i2c&, disable, (), (const));
    MOCK_METHOD(const i2c&, send_start, (), (const));
    MOCK_METHOD(const i2c&, send_stop, (), (const));
    MOCK_METHOD(const i2c&, clear_stop, (), (const));
    MOCK_METHOD(const i2c&, set_transfer_dir, (concepts::direction), (const));
    MOCK_METHOD(const i2c&, enable_dma, (concepts::dma), (const));
    MOCK_METHOD(const i2c&, disable_dma, (concepts::dma), (const));
    MOCK_METHOD(void, write, (std::uint8_t), (const));
    MOCK_METHOD(std::uint8_t, read, (), (const));
    MOCK_METHOD(const i2c&, set_7bit_address, (std::uint8_t), (const));
    MOCK_METHOD(const i2c&, set_bytes_to_transfer, (std::uint32_t), (const));
    MOCK_METHOD(const i2c&, enable_interrupt, (concepts::irq), (const));
    MOCK_METHOD(const i2c&, disable_interrupt, (concepts::irq), (const));
    MOCK_METHOD(const i2c&, set_speed, (concepts::speed, std::uint32_t), (const));
    MOCK_METHOD(bool, start_generation, (), (const));
    MOCK_METHOD(bool, nack_received, (), (const));
    MOCK_METHOD(bool, is_busy, (), (const));
    MOCK_METHOD(bool, is_tx_empty, (), (const));
    MOCK_METHOD(bool, transfer_completed, (), (const));
    MOCK_METHOD(bool, is_rx_not_empty, (), (const));
    MOCK_METHOD(bool, is_interrupt_enabled, (concepts::irq), (const));
};

static_assert(concepts::i2c<i2c>);

inline void setup_default_return_value(i2c& i2c_) {
    ON_CALL(i2c_, enable).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, disable).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, send_start).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, send_stop).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, clear_stop).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, set_transfer_dir).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, enable_dma).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, disable_dma).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, set_7bit_address).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, set_bytes_to_transfer).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, enable_interrupt).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, disable_interrupt).WillByDefault(testing::ReturnRef(i2c_));
    ON_CALL(i2c_, set_speed).WillByDefault(testing::ReturnRef(i2c_));
}

} // namespace spl::peripherals::i2c::mock
