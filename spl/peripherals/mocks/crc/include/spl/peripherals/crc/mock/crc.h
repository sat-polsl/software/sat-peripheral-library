#pragma once
#include "gmock/gmock.h"
#include "spl/peripherals/crc/concepts/crc.h"

namespace spl::peripherals::crc::mock {
struct crc {
    MOCK_METHOD(const crc&, reset, (), (const));
    MOCK_METHOD(std::uint32_t, feed_byte, (std::byte), (const));
    MOCK_METHOD(std::uint32_t, feed_span, (std::span<const std::byte>), (const));

    std::uint32_t feed(std::byte val) const { return feed_byte(val); }

    std::uint32_t feed(std::span<const std::byte> buffer) const { return feed_span(buffer); }

    MOCK_METHOD(const crc&, set_reverse_input, (bool), (const));
    MOCK_METHOD(const crc&, set_reverse_output, (bool), (const));
    MOCK_METHOD(const crc&, set_final_xor_value, (std::uint32_t), ());
};

static_assert(concepts::crc<crc>);

inline void setup_default_return_value(crc& crc_) {
    ON_CALL(crc_, reset).WillByDefault(testing::ReturnRef(crc_));
    ON_CALL(crc_, set_reverse_input).WillByDefault(testing::ReturnRef(crc_));
    ON_CALL(crc_, set_reverse_output).WillByDefault(testing::ReturnRef(crc_));
    ON_CALL(crc_, set_final_xor_value).WillByDefault(testing::ReturnRef(crc_));
}
} // namespace spl::peripherals::crc::mock
