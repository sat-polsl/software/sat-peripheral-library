#pragma once
#include <concepts>
#include <cstdint>
#include <span>

namespace spl::peripherals::crc::concepts {
// clang-format off
template<typename T>
concept crc = requires(T crc, std::byte data, std::span<const std::byte> datap,
                       std::int32_t size, bool bool_value, std::uint32_t xor_value) {
    { crc.reset() } -> std::same_as<const T&>;
    { crc.feed(data) } -> std::same_as<std::uint32_t>;
    { crc.feed(datap)} -> std::same_as<std::uint32_t>;
    { crc.set_reverse_input(bool_value)} -> std::same_as<const T&>;
    { crc.set_reverse_output(bool_value)} -> std::same_as<const T&>;
    { crc.set_final_xor_value(xor_value)} -> std::same_as<const T&>;
};
// clang-format on
} // namespace spl::peripherals::crc::concepts
