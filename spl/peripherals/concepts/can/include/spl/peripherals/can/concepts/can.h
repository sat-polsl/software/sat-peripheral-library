#pragma once
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace spl::peripherals::can::concepts {

// clang-format off
template<typename T>
concept can = requires(T can, timings timings, options opts, message msg, irq can_irq,
                       rx_fifo fifo, filter filter_id, bool enabled, std::uint32_t u32) {
    { can.initialize(timings, opts) } -> std::same_as<void>;
    { can.enable_irq(can_irq) } -> std::same_as<void>;
    { can.disable_irq(can_irq) } -> std::same_as<void>;
    { can.is_irq_enabled(can_irq) } -> std::same_as<bool>;
    { can.write(msg) } -> std::same_as<bool>;
    { can.read(fifo) } -> std::same_as<message>;
    { can.setup_filter(filter_id, u32, u32, fifo, enabled) } -> std::same_as<void>;
    { can.is_tx_mailbox_empty() } -> std::same_as<bool>;
    { can.is_rx0_fifo_not_empty() } -> std::same_as<bool>;
    { can.is_rx1_fifo_not_empty() } -> std::same_as<bool>;
    { can.tx_mailbox_released() } -> std::same_as<bool>;
    { can.clear_tx_mailbox_released() } -> std::same_as<void>;
};
// clang-format on
} // namespace spl::peripherals::can::concepts
