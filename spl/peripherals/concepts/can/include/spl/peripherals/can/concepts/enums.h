#pragma once
#include <array>
#include <cstdint>

namespace spl::peripherals::can::concepts {

/**
 * @addtogroup can_ll
 * @{
 */

/**
 * @brief CAN ID mask
 */
constexpr std::uint32_t id_mask = 0x1fffffff;

/**
 * @brief CAN IRQ enumeration
 */
enum class irq {};

/**
 * @brief CAN RX FIFO enumeration
 */
enum class rx_fifo : std::uint8_t {};

/**
 * @brief CAN Filter Id enumeration
 */
enum class filter : std::uint8_t {};

/**
 * @brief CAN Automatic Bus-off management enumeration
 */
enum class automatic_busoff { off, on };

/**
 * @brief CAN Automatic wakeup management enumeration
 */
enum class automatic_wakeup { off, on };

/**
 * @brief CAN Automatic retransmission enumeration
 */
enum class automatic_retransmission { off, on };

/**
 * @brief CAN RX FIFO lock enumeration
 */
enum class rx_fifo_lock { off, on };

/**
 * @brief CAN TX FIFO priority enumeration
 */
enum class tx_fifo_priority { identifier, chronological };

/**
 * @brief CAN Loopback mode enumeration
 */
enum class loopback_mode { off, on };

/**
 * @brief CAN Silent mode enumeration
 */
enum class silent_mode { off, on };

/** @} */
} // namespace spl::peripherals::can::concepts
