#pragma once
#include <array>
#include <cstdint>
#include "spl/peripherals/can/concepts/enums.h"

namespace spl::peripherals::can::concepts {

/**
 * @addtogroup can_ll
 * @{
 */

/**
 * @brief CAN Timings
 */
struct timings {
    /**
     * @brief Resynchronization Jump Width
     */
    std::uint32_t sjw;

    /**
     * @brief Bit segment 1
     */
    std::uint32_t ts1;

    /**
     * @brief Bit segment 2
     */
    std::uint32_t ts2;

    /**
     * @brief Baud rate prescaler
     */
    std::uint32_t brp;
};

/**
 * @brief CAN Options
 */
struct options {
    loopback_mode loopback{loopback_mode::off};
    silent_mode silent{silent_mode::off};
    automatic_busoff busoff{automatic_busoff::on};
    automatic_wakeup wakeup{automatic_wakeup::on};
    automatic_retransmission retransmission{automatic_retransmission::on};
    rx_fifo_lock rx_lock{rx_fifo_lock::off};
    tx_fifo_priority tx_priority{tx_fifo_priority::identifier};
};

/**
 * @brief CAN Message
 */
struct message {
    std::uint32_t id{};
    std::array<std::byte, 8> data{};
    std::uint8_t size{};
    bool is_extended{};
    bool is_rtr{};
    filter matched_filter{};
};

/** @} */
} // namespace spl::peripherals::can::concepts
