#pragma once
#include <cstdint>

namespace spl::peripherals::nvic::concepts {

/**
 * @addtogroup nvic_ll
 * @{
 */

/**
 * @brief IRQ enumeration
 */
enum class irq : std::int32_t {};

/** @} */
} // namespace spl::peripherals::nvic::concepts
