#pragma once
#include <concepts>
#include "spl/peripherals/nvic/concepts/enums.h"

namespace spl::peripherals::nvic::concepts {
// clang-format off
template<typename T>
concept nvic= requires(T n, irq nvic_irq, std::uint8_t priority) {
    { n.enable_irq(nvic_irq) } -> std::same_as<const T&>;
    { n.disable_irq(nvic_irq) } -> std::same_as<const T&>;
    { n.set_priority(nvic_irq, priority) } -> std::same_as<const T&>;
    { n.clear_pending_irq(nvic_irq) } -> std::same_as<const T&>;
};
// clang-format on
} // namespace spl::peripherals::nvic::concepts
