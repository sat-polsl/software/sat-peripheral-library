#pragma once
#include <concepts>
#include "spl/peripherals/spi/concepts/enums.h"

namespace spl::peripherals::spi::concepts {

// clang-format off
template<typename T>
concept spi = requires(T spi, std::uint8_t data, slave_management method, send_order order,
                       baudrate_prescaler baudrate, mode mode, irq spi_irq, dma spi_dma,
                       clock_mode spi_mode, data_size size, fifo_threshold threshold, bool select) {
    { spi.enable() } -> std::same_as<const T&>;
    { spi.disable() } -> std::same_as<const T&>;
    { spi.write(data) } -> std::same_as<void>;
    { spi.read() } -> std::same_as<std::uint8_t>;
    { spi.xfer(data) } -> std::same_as<std::uint8_t>;
    { spi.set_full_duplex_mode() } -> std::same_as<const T&>;
    { spi.set_receive_only_mode() } -> std::same_as<const T&>;
    { spi.set_slave_management(method) } -> std::same_as<const T&>;
    { spi.set_sending_order(order) } -> std::same_as<const T&>;
    { spi.set_baudrate_prescaler(baudrate) } -> std::same_as<const T&>;
    { spi.set_mode(mode) } -> std::same_as<const T&>;
    { spi.enable_irq(spi_irq) } -> std::same_as<const T&>;
    { spi.disable_irq(spi_irq) } -> std::same_as<const T&>;
    { spi.enable_dma(spi_dma) } -> std::same_as<const T&>;
    { spi.disable_dma(spi_dma) } -> std::same_as<const T&>;
    { spi.set_clock_mode(spi_mode) } -> std::same_as<const T&>;
    { spi.set_data_size(size) } -> std::same_as<const T&>;
    { spi.set_fifo_reception_threshold(threshold) } -> std::same_as<const T&>;
    { spi.set_internal_slave_select(select) } -> std::same_as<const T&>;
    { spi.is_rx_fifo_empty() } -> std::same_as<bool>;
};
// clang-format on

} // namespace spl::peripherals::spi::concepts
