#pragma once
#include <cstdint>

namespace spl::peripherals::spi::concepts {

/**
 * @addtogroup spi_ll
 * @{
 */

/**
 * @brief Slave management enumeration
 * @details Used to set software or hardware slave management
 * @see set_slave_management()
 */
enum class slave_management { software, hardware };

/**
 * @brief Send order enumeration.
 * @details Used to set from which bit data is sent - most
 *          significant bit or least significant bit.
 * @see set_sending_order()
 */
enum class send_order { msb_first, lsb_first };

/**
 * @brief Mode enumeration.
 * @details Used to set in which mode the device will be
 *          working - as master or as a slave.
 * @see set_mode()
 */
enum class mode { master, slave };

/**
 * @brief Interrupt enumeration.
 * @details Used to enable/disable specific interrupts
 * @see enable_irq(), disable_irq()
 */
enum class irq {};

/**
 * @brief Data size enumeration.
 * @details Used to set the size of sent data frame.
 * @sa spi::set_data_size()
 */
enum class data_size { bit8, bit16 };

/**
 * @brief SPI clock mode enumeration.
 * @details Used to set the standard clock mode in which SPI
 *          works.
 *          \n mode0 - Clock polarity: 0, Clock phase: 0,
 *          \n mode1 - Clock polarity: 0, Clock phase: 1,
 *          \n mode2 - Clock polarity: 1, Clock phase: 0,
 *          \n mode3 - Clock polarity: 1, Clock phase: 1.
 * @sa spi::set_clock_mode(clock_mode) const
 */
enum class clock_mode { mode0, mode1, mode2, mode3 };

/*
 * @brief SPI baudrate prescaler
 */
enum class baudrate_prescaler {};

/**
 * @brief DMA enumeration.
 * @details Used to specify the target for DMA.
 * @sa spi::enable_dma(dma) const, spi::disable_dma(dma) const
 */
enum class dma { rx, tx };

/**
 * @brief FIFO threshold enumeration.
 * @details Used to specify the target for DMA.
 * @see spi::set_fifo_reception_threshold(fifo_threshold) const,
 */
enum class fifo_threshold {};

enum class error {};

/** @} */

} // namespace spl::peripherals::spi::concepts
