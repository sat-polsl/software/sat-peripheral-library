#pragma once
#include <concepts>
#include "spl/peripherals/dma/concepts/enums.h"

namespace spl::peripherals::dma::concepts {
// clang-format off
template<typename T>
concept dma = requires(T dma, channel dma_channel, irq dma_irq, direction dma_direction,
                       request dma_request, std::uint32_t address, std::uint16_t size,
                       bool increment, peripheral peripheral_address, bool enable) {
    { dma.enable_channel(dma_channel) } -> std::same_as<const T&>;
    { dma.disable_channel(dma_channel) } -> std::same_as<const T&>;
    { dma.reset_channel(dma_channel) } -> std::same_as<const T&>;
    { dma.set_peripheral_address(dma_channel, peripheral_address) } -> std::same_as<const T&>;
    { dma.set_memory_address(dma_channel, address) } -> std::same_as<const T&>;
    { dma.set_transfer_size(dma_channel, size) } -> std::same_as<const T&>;
    { dma.set_peripheral_increment(dma_channel, increment) } -> std::same_as<const T&>;
    { dma.set_memory_increment(dma_channel, increment) } -> std::same_as<const T&>;
    { dma.enable_irq(dma_channel, dma_irq) } -> std::same_as<const T&>;
    { dma.disable_irq(dma_channel, dma_irq) } -> std::same_as<const T&>;
    { dma.clear_irq(dma_channel, dma_irq) } -> std::same_as<const T&>;
    { dma.get_irq_status(dma_channel, dma_irq) } -> std::same_as<bool>;
    { dma.set_direction(dma_channel, dma_direction) } -> std::same_as<const T&>;
    { dma.set_request_mapping(dma_channel, dma_request) } -> std::same_as<const T&>;
    { dma.set_m2m(dma_channel, enable) } -> std::same_as<const T&>;
    { dma.set_m2m_source(dma_channel, address) } -> std::same_as<const T&>;
    { dma.set_m2m_destination(dma_channel, address) } -> std::same_as<const T&>;
};
// clang-format on

} // namespace spl::peripherals::dma::concepts
