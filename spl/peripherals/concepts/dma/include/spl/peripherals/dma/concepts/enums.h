#pragma once
#include <cstdint>

namespace spl::peripherals::dma::concepts {

/**
 * @addtogroup dma_ll
 * @{
 */

/**
 * @brief DMA channel enumeration
 */
enum class channel : std::uint8_t {};

/**
 * @brief DMA IRQ enumeration
 */
enum class irq {};

/**
 * @brief DMA direction enumeration
 */
enum class direction { peripheral_to_memory, memory_to_peripheral, memory_to_memory };

/**
 * @brief DMA requset enumeration
 */
enum class request : std::uint8_t {};

enum class peripheral : std::uint32_t {};
/** @} */
} // namespace spl::peripherals::dma::concepts
