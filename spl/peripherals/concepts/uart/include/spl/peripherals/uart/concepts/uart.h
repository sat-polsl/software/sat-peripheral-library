#pragma once
#include <concepts>
#include "spl/peripherals/uart/concepts/enums.h"

namespace spl::peripherals::uart::concepts {
// clang-format off
template<typename T>
concept uart = requires(T uart, mode uart_mode, std::uint32_t baudrate, irq uart_irq,
                        std::uint16_t data, clear_irq irq, dma uart_dma) {
    { uart.set_baudrate(baudrate) } -> std::same_as<const T&>;
    { uart.set_mode(uart_mode) } -> std::same_as<const T&>;
    { uart.enable_irq(uart_irq) } -> std::same_as<const T&>;
    { uart.disable_irq(uart_irq) } -> std::same_as<const T&>;
    { uart.is_irq_enabled(uart_irq) } -> std::same_as<bool>;
    { uart.enable() } -> std::same_as<const T&>;
    { uart.disable() } -> std::same_as<const T&>;
    { uart.enable_dma(uart_dma) } -> std::same_as<const T&>;
    { uart.disable_dma(uart_dma) } -> std::same_as<const T&>;
    { uart.write(data) } -> std::same_as<void>;
    { uart.read() } -> std::same_as<std::uint16_t>;
    { uart.is_tx_empty() } -> std::same_as<bool>;
    { uart.is_rx_not_empty() } -> std::same_as<bool>;
    { uart.overrun_error() } -> std::same_as<bool>;
    { uart.clear_irq_flag(irq) } -> std::same_as<void>;
};
// clang-format on

} // namespace spl::peripherals::uart::concepts
