#pragma once
#include <cstdint>

namespace spl::peripherals::uart::concepts {

/**
 * @addtogroup uart_ll
 * @{
 */

/**
 * @brief UART mode enumeration
 */
enum class mode : std::int32_t { rx, tx, rx_tx };

/**
 * @brief UART irq enumeration
 */
enum class irq : std::int32_t {};

/**
 * @brief UART clear irq enumeration
 */
enum class clear_irq : std::int32_t {};

/**
 * @brief UART dma enumeration
 */
enum class dma : std::int32_t { rx, tx };

/** @} */
} // namespace spl::peripherals::uart::concepts
