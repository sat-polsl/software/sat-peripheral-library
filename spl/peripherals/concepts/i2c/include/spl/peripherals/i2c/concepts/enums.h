#pragma once
#include <cstdint>

namespace spl::peripherals::i2c::concepts {

/**
 * @addtogroup i2c_ll I2C peripheral
 * @{
 */

/**
 * @brief I2C speed enumeration
 * @see set_speed()
 */
enum class speed : std::int32_t {};

/**
 * @brief I2C transfer direction enumeration
 * @see set_transfer_dir()
 */
enum class direction : std::int32_t { write, read };

/**
 * @brief I2C interrupt enumeration
 * @see enable_interrupt(), disable_interrupt()
 */
enum class irq : std::int32_t {};

/**
 * @brief I2C DMA enumeration
 * @details Used to specify the target for DMA.
 * @see enable_dma(), disable_dma()
 */
enum class dma : std::int32_t { tx, rx };

/** @} */
} // namespace spl::peripherals::i2c::concepts
