#pragma once
#include <concepts>
#include "spl/peripherals/i2c/concepts/enums.h"

namespace spl::peripherals::i2c::concepts {
// clang-format off
template<typename T>
concept i2c = requires(T i2c, speed spd, std::uint8_t data, direction dir,
                       std::uint8_t addr, std::uint32_t n_bytes, irq intr,
                       std::uint32_t clock_megahz, dma target) {
    { i2c.enable() } -> std::same_as<const T&>;
    { i2c.disable() } -> std::same_as<const T&>;
    { i2c.send_start() } -> std::same_as<const T&>;
    { i2c.send_stop() } -> std::same_as<const T&>;
    { i2c.clear_stop() } -> std::same_as<const T&>;
    { i2c.set_transfer_dir(dir) } -> std::same_as<const T&>;
    { i2c.enable_dma(target) } -> std::same_as<const T&>;
    { i2c.disable_dma(target) } -> std::same_as<const T&>;
    { i2c.write(data) } -> std::same_as<void>;
    { i2c.set_7bit_address(addr) } -> std::same_as<const T&>;
    { i2c.set_bytes_to_transfer(n_bytes) } -> std::same_as<const T&>;
    { i2c.enable_interrupt(intr) } -> std::same_as<const T&>;
    { i2c.disable_interrupt(intr) } -> std::same_as<const T&>;
    { i2c.set_speed(spd, clock_megahz) } -> std::same_as<const T&>;
    { i2c.read() } -> std::same_as<std::uint8_t>;
    { i2c.start_generation() } -> std::same_as<bool>;
    { i2c.nack_received() } -> std::same_as<bool>;
    { i2c.is_busy() } -> std::same_as<bool>;
    { i2c.is_tx_empty() } -> std::same_as<bool>;
    { i2c.transfer_completed() } -> std::same_as<bool>;
    { i2c.is_rx_not_empty() } -> std::same_as<bool>;
    { i2c.is_interrupt_enabled(intr) } -> std::same_as<bool>;
};
// clang-format on
} // namespace spl::peripherals::i2c::concepts
