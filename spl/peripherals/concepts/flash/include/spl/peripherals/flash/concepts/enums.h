#pragma once
#include <cstdint>

namespace spl::peripherals::flash::concepts {

/**
 * @addtogroup flash_ll
 * @{
 */

/**
 * @brief Flash Wait States
 */
enum class wait_states : std::uint32_t;

/**
 * @brief Flash IRQ enumeration
 */
enum class irq : std::uint32_t {};

/** @} */

} // namespace spl::peripherals::flash::concepts
