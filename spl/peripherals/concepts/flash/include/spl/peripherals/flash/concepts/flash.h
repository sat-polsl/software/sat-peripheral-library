#pragma once
#include <concepts>
#include <span>
#include "spl/peripherals/flash/concepts/enums.h"

namespace spl::peripherals::flash::concepts {
// clang-format off
template<typename T>
concept flash = requires(T flash, wait_states ws,  irq flash_irq, std::uint8_t u8,
                        std::uint32_t u32, std::uint64_t u64, std::span<const std::byte> buffer) {
    { flash.set_wait_states(ws) } -> std::same_as<const T&>;
    { flash.lock() } -> std::same_as<const T&>;
    { flash.unlock() } -> std::same_as<const T&>;
    { flash.program_u64(u32, u64) } -> std::same_as<const T&>;
    { flash.program_buffer(u32, buffer) } -> std::same_as<const T&>;
    { flash.erase_page(u8) } -> std::same_as<const T&>;
    { flash.mass_erase() } -> std::same_as<const T&>;
    { flash.enable_irq(flash_irq) } -> std::same_as<const T&>;
    { flash.disable_irq(flash_irq) } -> std::same_as<const T&>;
    { flash.clear_irq_flag(flash_irq) } -> std::same_as<const T&>;
    { flash.is_irq_enabled(flash_irq) } -> std::same_as<bool>;
    { flash.is_busy() } -> std::same_as<bool>;
    { flash.end_of_operation() } -> std::same_as<bool>;
    { flash.operation_error() } -> std::same_as<bool>;
    { flash.read_error() } -> std::same_as<bool>;
    { flash.ecc_correction() } -> std::same_as<bool>;
    T::page_size;
    T::memory_base;
};
// clang-format on
} // namespace spl::peripherals::flash::concepts
