#pragma once
#include <concepts>
#include <cstdint>
#include <span>

namespace spl::peripherals::iwdg::concepts {
// clang-format off
template<typename T>
concept iwdg = requires(T iwdg, std::uint32_t period) {
    { iwdg.initialize() } -> std::same_as<const T&>;
    { iwdg.set_period_ms(period) } -> std::same_as<const T&>;
    { iwdg.kick()} -> std::same_as<const T&>;
};
// clang-format on
} // namespace spl::peripherals::iwdg::concepts
