#pragma once
#include <concepts>
#include "spl/peripherals/gpio/concepts/enums.h"

namespace spl::peripherals::gpio::concepts {
// clang-format off
template<typename T>
concept gpio = requires(T gpio, mode gpio_mode, af gpio_af, pupd gpio_pupd) {
    { gpio.set() } -> std::same_as<void>;
    { gpio.clear() } -> std::same_as<void>;
    { gpio.toggle() } -> std::same_as<void>;
    { gpio.read() } -> std::same_as<bool>;
};
// clang-format on
} // namespace spl::peripherals::gpio::concepts
