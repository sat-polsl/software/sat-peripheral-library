#pragma once
#include <cstdint>

namespace spl::peripherals::gpio::concepts {

/**
 * @addtogroup gpio_ll
 * @{
 */

/**
 * @brief GPIO Port enumeration
 */
enum class port : std::int32_t {};

/**
 * @brief GPIO Pin type
 */
enum class pin : std::int32_t;

/**
 * @brief GPIO Mode enumeration
 */
enum class mode : std::int32_t { input, output, analog, af };

/**
 * @brief GPIO PullUp/PullDown enumeration
 */
enum class pupd : std::int32_t { none, pullup, pulldown };

/**
 * @brief GPIO Alternate functions type
 */
enum class af : std::int32_t {};

/**
 * @brief GPIO output type enumeration
 */
enum class output_type { push_pull, open_drain };

/**
 * @brief GPIO output speed enumeration
 */
enum class output_speed {};

/** @} */

} // namespace spl::peripherals::gpio::concepts
