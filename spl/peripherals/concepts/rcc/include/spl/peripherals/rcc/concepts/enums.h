#pragma once
#include <cstdint>

namespace spl::peripherals::rcc::concepts {

/**
 * @addtogroup rcc_ll
 * @{
 */

/**
 * @brief Peripheral clock enumeration
 */
enum class peripheral : std::int32_t {};

enum class clock_source { hse, hsi, pll, msi };

enum class oscillator { hse, hsi, pll, msi };

enum class pll_clock_source { hse, hsi, msi };

enum class pll_prescaler { div2 = 0x0, div4 = 0x1, div6 = 0x2, div8 = 0x3 };

enum class pll_input_prescaler {
    div1 = 0x0,
    div2 = 0x1,
    div3 = 0x2,
    div4 = 0x3,
    div5 = 0x4,
    div6 = 0x5,
    div7 = 0x6,
    div8 = 0x7
};

enum class pll_output { pllclk, pll48m1clk };

/** @} */
} // namespace spl::peripherals::rcc::concepts
