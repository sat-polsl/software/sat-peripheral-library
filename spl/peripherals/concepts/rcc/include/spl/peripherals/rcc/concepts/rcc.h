#pragma once
#include <concepts>
#include "spl/peripherals/rcc/concepts/enums.h"

namespace spl::peripherals::rcc::concepts {
// clang-format off
template<typename T>
concept rcc = requires(T r, peripheral rcc_clock, peripheral reset, clock_source source,
                       pll_clock_source src, pll_prescaler prescaler,
                       std::int8_t multiplier, pll_input_prescaler presc,
                       std::uint32_t frequency, pll_output output, oscillator osc) {
    { r.enable_clock(rcc_clock) } -> std::same_as<const T&>;
    { r.disable_clock(rcc_clock) } -> std::same_as<const T&>;
    { r.pulse_reset(reset) } -> std::same_as<const T&>;
    { r.set_sysclk_source(source) } -> std::same_as<const T&>;
    { r.get_sysclk_source() } -> std::same_as<clock_source>;
    { r.enable_oscillator(osc) } -> std::same_as<const T&>;
    { r.disable_oscillator(osc) } -> std::same_as<const T&>;
    { r.set_pll_source(src) } -> std::same_as<const T&>;
    { r.set_pll_clock_prescaler(prescaler) } -> std::same_as<const T&>;
    { r.set_pll_vco_multiplier(multiplier) } -> std::same_as<const T&>;
    { r.set_pll_input_clock_prescaler(presc) } -> std::same_as<const T&>;
    { r.is_oscillator_ready(osc) } -> std::same_as<bool>;
    { r.set_ahb_frequency(frequency) } -> std::same_as<const T&>;
    { r.set_apb1_frequency(frequency) } -> std::same_as<const T&>;
    { r.set_apb2_frequency(frequency) } -> std::same_as<const T&>;
    { r.get_ahb_frequency() } -> std::same_as<std::uint32_t>;
    { r.get_apb1_frequency() } -> std::same_as<std::uint32_t>;
    { r.get_apb2_frequency() } -> std::same_as<std::uint32_t>;
    { r.enable_pll_output(output) } -> std::same_as<const T&>;
    { r.disable_pll_output(output) } -> std::same_as<const T&>;
};
// clang-format on
} // namespace spl::peripherals::rcc::concepts
