#pragma once
#include <cstdint>
#include "spl/peripherals/i2c/concepts/enums.h"

namespace spl::peripherals::i2c {

/**
 * @addtogroup i2c_ll I2C peripheral
 * @{
 */

/**
 * @brief I2C instance enumeration
 */
enum class instance : std::int32_t { i2c1, i2c2, i2c3 };

struct speed {
    static constexpr auto i2c_100k = spl::peripherals::i2c::concepts::speed{0};
    static constexpr auto i2c_400k = spl::peripherals::i2c::concepts::speed{1};
    static constexpr auto i2c_1m = spl::peripherals::i2c::concepts::speed{2};
    static constexpr auto i2c_uk = spl::peripherals::i2c::concepts::speed{3};
};

struct irq {
    static constexpr auto error = spl::peripherals::i2c::concepts::irq{0};
    static constexpr auto transfer_completed = spl::peripherals::i2c::concepts::irq{1};
    static constexpr auto stop_detected = spl::peripherals::i2c::concepts::irq{2};
    static constexpr auto nack_received = spl::peripherals::i2c::concepts::irq{3};
    static constexpr auto address_matched = spl::peripherals::i2c::concepts::irq{4};
    static constexpr auto rx_not_empty = spl::peripherals::i2c::concepts::irq{5};
    static constexpr auto tx_empty = spl::peripherals::i2c::concepts::irq{6};
};

using concepts::direction;
using concepts::dma;

/** @} */
} // namespace spl::peripherals::i2c
