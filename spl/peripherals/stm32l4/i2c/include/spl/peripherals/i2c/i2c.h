#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/i2c/concepts/enums.h"
#include "spl/peripherals/i2c/concepts/i2c.h"
#include "spl/peripherals/i2c/enums.h"

namespace spl::peripherals::i2c {

/**
 * @addtogroup i2c_ll I2C peripheral
 * @{
 */

/**
 * @brief I2C low level driver class
 */
class i2c : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param i2c_instance I2C instance
     */
    explicit constexpr i2c(instance i2c_instance) : instance_(i2c_instance){};

    /**
     * @brief Enable I2C
     * @return const i2c_base&
     */
    const i2c& enable() const;

    /**
     * @brief Disable I2C
     * @return const i2c_base&
     */
    const i2c& disable() const;

    /**
     * @brief Send Start Condition
     * @return const i2c_base&
     */
    const i2c& send_start() const;

    /**
     * @brief Send Stop Condition
     * @return const i2c_base&
     */
    const i2c& send_stop() const;

    /**
     * @brief Clear Stop Flag
     * @return const i2c_base&
     */
    const i2c& clear_stop() const;

    /**
     * @brief Set the transfer direction
     * @param dir direction read/write
     * @return const i2c_base&
     * @see enum direction
     */
    const i2c& set_transfer_dir(concepts::direction dir) const;

    /**
     * @brief Enable reception DMA
     * @return const i2c_base&
     * @see enum dma
     */
    const i2c& enable_dma(concepts::dma target) const;

    /**
     * @brief Disable reception DMA
     * @return const i2c_base&
     * @see enum dma
     */
    const i2c& disable_dma(concepts::dma target) const;

    /**
     * @brief Write data to transmit register
     * @param data byte to send
     */
    void write(std::uint8_t data) const;

    /**
     * @brief Set the 7bit address
     * @param addr address to set
     * @return const i2c_base&
     */
    const i2c& set_7bit_address(std::uint8_t addr) const;

    /**
     * @brief Set the bytes to transfer
     * @param n_bytes number of bytes to be transmitted/received
     * @return const i2c_base&
     */
    const i2c& set_bytes_to_transfer(std::uint32_t n_bytes) const;

    /**
     * @brief Enable I2C interrupt
     * @param intr interrupt to enable
     * @return const i2c_base&
     * @see enum interrupt
     */
    const i2c& enable_interrupt(concepts::irq intr) const;

    /**
     * @brief Disable I2C interrupt
     * @param intr interrupt to disable
     * @return const i2c_base&
     * @see enum interrupt
     */
    const i2c& disable_interrupt(concepts::irq intr) const;

    /**
     * @brief Set the I2C communication speed
     * @param spd speed mode
     * @param clock_megahz I2C peripheral clock speed in MHz
     * @return const i2c_base&
     * @see enum speed
     */
    const i2c& set_speed(concepts::speed spd, std::uint32_t clock_megahz) const;

    /**
     * @brief Read data from receive register
     * @return std::uint8_t read data
     */
    std::uint8_t read() const;

    /**
     * @brief Start generation
     * @return true restart/start generation
     * @return false no start generation
     */
    bool start_generation() const;

    /**
     * @brief NACK generation
     * @return true NACK is send after current received byte
     * @return false ACK is send after current received byte
     */
    bool nack_received() const;

    /**
     * @brief Bus busy
     * @return true communication in progress on the bus
     * @return false no communication in progress
     */
    bool is_busy() const;

    /**
     * @brief Transmit interrupt status
     * @return true transmit register is empty, data to be transmitted must be written
     * @return false data is written in the transmit register
     */
    bool is_tx_empty() const;

    /**
     * @brief Transfer complete
     * @return true data has been transfered
     * @return false data has not been transfered
     */
    bool transfer_completed() const;

    /**
     * @brief Receive data register not empty
     * @return true received data copied to receive register, ready to be read
     * @return false receive was read
     */
    bool is_rx_not_empty() const;

    /**
     * @brief Receive data register not empty
     * @return true received data copied to receive register, ready to be read
     * @return false receive was read
     */
    bool is_interrupt_enabled(concepts::irq intr) const;

private:
    instance instance_;
};

static_assert(concepts::i2c<i2c>);

/** @} */

} // namespace spl::peripherals::i2c
