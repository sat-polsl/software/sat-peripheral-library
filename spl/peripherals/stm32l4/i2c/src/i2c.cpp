#include "spl/peripherals/i2c/i2c.h"
#include <array>
#include "libopencm3/stm32/i2c.h"
#include "satext/type_traits.h"

namespace spl::peripherals::i2c {

using namespace satext;

consteval auto create_instance_lut() {
    std::array<std::uint32_t, 3> lut{};
    lut[to_underlying_type(instance::i2c1)] = I2C1;
    lut[to_underlying_type(instance::i2c2)] = I2C2;
    lut[to_underlying_type(instance::i2c3)] = I2C3;
    return lut;
}

consteval auto create_speed_lut() {
    std::array<i2c_speeds, 4> lut{};
    lut[to_underlying_type(speed::i2c_100k)] = i2c_speed_sm_100k;
    lut[to_underlying_type(speed::i2c_400k)] = i2c_speed_fm_400k;
    lut[to_underlying_type(speed::i2c_1m)] = i2c_speed_fmp_1m;
    lut[to_underlying_type(speed::i2c_uk)] = i2c_speed_unknown;
    return lut;
}

consteval auto create_interrupt_lut() {
    std::array<std::uint32_t, 7> lut{};
    lut[to_underlying_type(irq::error)] = I2C_CR1_ERRIE;
    lut[to_underlying_type(irq::transfer_completed)] = I2C_CR1_TCIE;
    lut[to_underlying_type(irq::stop_detected)] = I2C_CR1_STOPIE;
    lut[to_underlying_type(irq::nack_received)] = I2C_CR1_NACKIE;
    lut[to_underlying_type(irq::address_matched)] = I2C_CR1_ADDRIE;
    lut[to_underlying_type(irq::rx_not_empty)] = I2C_CR1_RXIE;
    lut[to_underlying_type(irq::tx_empty)] = I2C_CR1_TXIE;
    return lut;
}

constexpr auto instance_lut = create_instance_lut();
constexpr auto speed_lut = create_speed_lut();
constexpr auto interrupt_lut = create_interrupt_lut();

std::uint32_t operator*(instance i2c_instance) {
    return instance_lut[to_underlying_type(i2c_instance)];
}

i2c_speeds operator*(concepts::speed spd) { return speed_lut[to_underlying_type(spd)]; }

std::uint32_t operator*(concepts::irq intr) { return interrupt_lut[to_underlying_type(intr)]; }

const i2c& i2c::enable() const {
    i2c_peripheral_enable(*instance_);
    return *this;
}
const i2c& i2c::disable() const {
    i2c_peripheral_disable(*instance_);
    return *this;
}
const i2c& i2c::send_start() const {
    i2c_send_start(*instance_);
    return *this;
}
const i2c& i2c::send_stop() const {
    i2c_send_stop(*instance_);
    return *this;
}
const i2c& i2c::clear_stop() const {
    i2c_clear_stop(*instance_);
    return *this;
}
const i2c& i2c::set_transfer_dir(concepts::direction dir) const {
    switch (dir) {
    case concepts::direction::read:
        i2c_set_read_transfer_dir(*instance_);
        break;
    case concepts::direction::write:
        i2c_set_write_transfer_dir(*instance_);
        break;
    default:
        break;
    }
    return *this;
}
const i2c& i2c::enable_dma(concepts::dma target) const {
    switch (target) {
    case concepts::dma::rx:
        i2c_enable_rxdma(*instance_);
        break;
    case concepts::dma::tx:
        i2c_enable_txdma(*instance_);
        break;
    default:
        break;
    }
    return *this;
}
const i2c& i2c::disable_dma(concepts::dma target) const {
    switch (target) {
    case concepts::dma::rx:
        i2c_disable_rxdma(*instance_);
        break;
    case concepts::dma::tx:
        i2c_disable_txdma(*instance_);
        break;
    default:
        break;
    }
    return *this;
}
void i2c::write(std::uint8_t data) const { i2c_send_data(*instance_, data); }
const i2c& i2c::set_7bit_address(std::uint8_t addr) const {
    i2c_set_7bit_address(*instance_, addr);
    return *this;
}
const i2c& i2c::set_bytes_to_transfer(std::uint32_t n_bytes) const {
    i2c_set_bytes_to_transfer(*instance_, n_bytes);
    return *this;
}
const i2c& i2c::enable_interrupt(concepts::irq intr) const {
    i2c_enable_interrupt(*instance_, *intr);
    return *this;
}
const i2c& i2c::disable_interrupt(concepts::irq intr) const {
    i2c_disable_interrupt(*instance_, *intr);
    return *this;
}
const i2c& i2c::set_speed(concepts::speed speed, std::uint32_t clock_megahz) const {
    i2c_set_speed(*instance_, *speed, clock_megahz);
    return *this;
}
std::uint8_t i2c::read() const { return i2c_get_data(*instance_); }
bool i2c::start_generation() const { return i2c_is_start(*instance_); }
bool i2c::nack_received() const { return i2c_nack(*instance_); }
bool i2c::is_busy() const { return i2c_busy(*instance_); }
bool i2c::is_tx_empty() const { return i2c_transmit_int_status(*instance_); }
bool i2c::transfer_completed() const { return i2c_transfer_complete(*instance_); }
bool i2c::is_rx_not_empty() const { return i2c_received_data(*instance_); }
bool i2c::is_interrupt_enabled(concepts::irq intr) const {
    return i2c_is_interrupt_enabled(*instance_, *intr);
}
} // namespace spl::peripherals::i2c
