#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/can/concepts/can.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"
#include "spl/peripherals/can/enums.h"
#include "spl/peripherals/can/types.h"

namespace spl::peripherals::can {

/**
 * @addtogroup can_ll
 * @{
 */

/**
 * @brief CAN low level driver
 */
class can : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param can_instance CAN instance
     */
    explicit constexpr can(instance can_instance) : instance_{can_instance} {}

    /**
     * @brief Initializes CAN
     * @param timings CAN timings
     * @param opts CAN options
     */
    void initialize(const concepts::timings& timings, const concepts::options& opts) const;

    /**
     * @brief Enables given IRQ
     * @param can_irq CAN IRQ
     */
    void enable_irq(concepts::irq can_irq) const;

    /**
     * @brief Disables given IRQ
     * @param can_irq CAN IRQ
     */
    void disable_irq(concepts::irq can_irq) const;

    /**
     * @brief Checks whether IRQ is enabled
     * @param can_irq CAN IRQ
     * @return true if enabled, otherwise false
     */
    bool is_irq_enabled(concepts::irq can_irq) const;

    /**
     * @brief Writes message to transmit FIFO
     * @param msg CAN message
     * @return true on success, false if transmit FIFO is full
     */
    bool write(const concepts::message& msg) const;

    /**
     * @brief Reads message from given receive FIFO
     * @param fifo Receive FIFO
     * @return CAN message
     */
    concepts::message read(concepts::rx_fifo fifo) const;

    /**
     * @brief Configures filter
     * @param filter_id Filter ID
     * @param id CAN Message ID
     * @param mask CAN Message Mask
     * @param fifo Receive FIFO
     * @param enable Enable
     */
    void setup_filter(concepts::filter filter_id,
                      std::uint32_t id,
                      std::uint32_t mask,
                      concepts::rx_fifo fifo,
                      bool enable) const;

    /**
     * @brief Checks whether any of TX mailboxes is empty
     * @return true if at least TX mailbox is empty, false otherwise
     */
    bool is_tx_mailbox_empty() const;

    /**
     * @brief Checks whether TX mailbox was released
     * @return true if TX mailbox was released, false otherwise
     */
    bool tx_mailbox_released() const;

    /**
     * @brief Checks whether RX FIFO 0 is not empty
     * @return true if RX FIFO 0 is not empty, false otherwise
     */
    bool is_rx0_fifo_not_empty() const;

    /**
     * @brief Checks whether RX FIFO 1 is not empty
     * @return true if RX FIFO 1 is not empty, false otherwise
     */
    bool is_rx1_fifo_not_empty() const;

    /**
     * @brief Clears TX mailboxes released flags
     */
    void clear_tx_mailbox_released() const;

private:
    instance instance_;
};

static_assert(concepts::can<can>);

/** @} */
} // namespace spl::peripherals::can
