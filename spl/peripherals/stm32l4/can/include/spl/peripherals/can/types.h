#pragma once
#include <array>
#include <cstdint>
#include "spl/peripherals/can/concepts/types.h"

namespace spl::peripherals::can {

/**
 * @addtogroup can_ll
 * @{
 */

using concepts::message;
using concepts::options;
using concepts::timings;

/** @} */
} // namespace spl::peripherals::can
