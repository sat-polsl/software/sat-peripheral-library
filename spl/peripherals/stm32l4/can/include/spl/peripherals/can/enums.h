#pragma once
#include "spl/peripherals/can/concepts/enums.h"

namespace spl::peripherals::can {

/**
 * @addtogroup can_ll
 * @{
 */

/**
 * @brief CAN instance enumeration
 */
enum class instance { can1, can2 };

constexpr auto rx_fifo0 = concepts::rx_fifo{0};
constexpr auto rx_fifo1 = concepts::rx_fifo{1};

constexpr auto filter0 = concepts::filter{0};
constexpr auto filter1 = concepts::filter{1};
constexpr auto filter2 = concepts::filter{2};
constexpr auto filter3 = concepts::filter{3};
constexpr auto filter4 = concepts::filter{4};
constexpr auto filter5 = concepts::filter{5};
constexpr auto filter6 = concepts::filter{6};
constexpr auto filter7 = concepts::filter{7};
constexpr auto filter8 = concepts::filter{8};
constexpr auto filter9 = concepts::filter{9};
constexpr auto filter10 = concepts::filter{10};
constexpr auto filter11 = concepts::filter{11};
constexpr auto filter12 = concepts::filter{12};
constexpr auto filter13 = concepts::filter{13};

struct irq {
    static constexpr auto tx_mailbox_released = concepts::irq{0};
    static constexpr auto rx0_pending = concepts::irq{1};
    static constexpr auto rx1_pending = concepts::irq{2};
};

using concepts::automatic_busoff;
using concepts::automatic_retransmission;
using concepts::automatic_wakeup;
using concepts::loopback_mode;
using concepts::rx_fifo_lock;
using concepts::silent_mode;
using concepts::tx_fifo_priority;

/** @} */
} // namespace spl::peripherals::can
