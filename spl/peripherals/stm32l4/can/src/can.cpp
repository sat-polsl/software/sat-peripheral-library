#include "spl/peripherals/can/can.h"
#include <array>
#include "libopencm3/stm32/can.h"
#include "satext/type_traits.h"

namespace spl::peripherals::can {
using namespace satext;

consteval auto create_instance_lut() {
    std::array<std::uint32_t, 2> lut{};
    lut[to_underlying_type(instance::can1)] = CAN1;
    lut[to_underlying_type(instance::can2)] = CAN2;
    return lut;
}

consteval auto create_irq_lut() {
    std::array<std::uint32_t, 3> lut{};
    lut[to_underlying_type(irq::tx_mailbox_released)] = CAN_IER_TMEIE;
    lut[to_underlying_type(irq::rx0_pending)] = CAN_IER_FMPIE0;
    lut[to_underlying_type(irq::rx1_pending)] = CAN_IER_FMPIE1;
    return lut;
}

constexpr auto instance_lut = create_instance_lut();
constexpr auto irq_lut = create_irq_lut();

constexpr auto filter_bank_size = std::uint32_t{14};
constexpr auto filter_id_shift = std::uint32_t{3};

std::uint32_t operator*(instance can_instance) {
    return instance_lut[to_underlying_type(can_instance)];
}

std::uint32_t operator*(concepts::irq can_irq) { return irq_lut[to_underlying_type(can_irq)]; }

void can::initialize(const concepts::timings& timings, const concepts::options& opts) const {
    can_init(
        /* canport= */ *instance_,
        /* ttcm= */ false,
        /* abom= */ opts.busoff == automatic_busoff::on,
        /* awum= */ opts.wakeup == automatic_wakeup::on,
        /* nart= */ opts.retransmission == automatic_retransmission::off,
        /* rflm= */ opts.rx_lock == rx_fifo_lock::on,
        /* txfp= */ opts.tx_priority == tx_fifo_priority::chronological,
        /* sjw= */ (timings.sjw - 1) << CAN_BTR_SJW_SHIFT,
        /* ts1= */ (timings.ts1 - 1) << CAN_BTR_TS1_SHIFT,
        /* ts2= */ (timings.ts2 - 1) << CAN_BTR_TS2_SHIFT,
        /* brp= */ timings.brp,
        /* loopback= */ opts.loopback == concepts::loopback_mode::on,
        /* silent= */ opts.silent == concepts::silent_mode::on);
}
void can::enable_irq(concepts::irq can_irq) const { can_enable_irq(*instance_, *can_irq); }

void can::disable_irq(concepts::irq can_irq) const { can_disable_irq(*instance_, *can_irq); }

bool can::is_irq_enabled(concepts::irq can_irq) const {
    // TODO: implement in libopencm3
    // https://gitlab.com/sat-polsl/software/libopencm3/-/issues/8

    return (CAN_IER(*instance_) & *can_irq) != 0;
}

bool can::write(const concepts::message& msg) const {
    return can_transmit(*instance_,
                        msg.id,
                        msg.is_extended,
                        msg.is_rtr,
                        msg.size,
                        const_cast<std::uint8_t*>(
                            reinterpret_cast<const std::uint8_t*>(msg.data.data()))) != -1;
}

concepts::message can::read(concepts::rx_fifo fifo) const {
    concepts::message result{};

    can_receive(*instance_,
                to_underlying_type(fifo),
                true,
                &result.id,
                &result.is_extended,
                &result.is_rtr,
                reinterpret_cast<std::uint8_t*>(&result.matched_filter),
                &result.size,
                reinterpret_cast<std::uint8_t*>(result.data.data()),
                nullptr);

    return result;
}

void can::setup_filter(concepts::filter filter_id,
                       std::uint32_t id,
                       std::uint32_t mask,
                       concepts::rx_fifo fifo,
                       bool enable) const {
    std::uint32_t filter_base = ((instance_ == instance::can2) ? filter_bank_size - 1 : 0);
    can_filter_id_mask_32bit_init(filter_base + to_underlying_type(filter_id),
                                  id << filter_id_shift,
                                  mask << filter_id_shift,
                                  to_underlying_type(fifo),
                                  enable);
}

bool can::is_tx_mailbox_empty() const {
    static constexpr std::uint32_t tx_empty_mask = 0x3 << 26;
    return (CAN_TSR(*instance_) & tx_empty_mask) != 0;
}

bool can::is_rx0_fifo_not_empty() const { return (CAN_RF0R(*instance_) & CAN_RF0R_FMP0_MASK) != 0; }

bool can::is_rx1_fifo_not_empty() const { return (CAN_RF1R(*instance_) & CAN_RF1R_FMP1_MASK) != 0; }

bool can::tx_mailbox_released() const {
    return (CAN_TSR(*instance_) & (CAN_TSR_RQCP0 | CAN_TSR_RQCP1 | CAN_TSR_RQCP2)) != 0;
}
void can::clear_tx_mailbox_released() const {
    std::uint32_t reg = CAN_TSR(*instance_);
    reg |= CAN_TSR_RQCP0 | CAN_TSR_RQCP1 | CAN_TSR_RQCP2;
    CAN_TSR(*instance_) = reg;
}

} // namespace spl::peripherals::can
