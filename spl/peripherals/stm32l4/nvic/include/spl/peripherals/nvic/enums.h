#pragma once
#include <cstdint>
#include "spl/peripherals/nvic/concepts/enums.h"

namespace spl::peripherals::nvic {

/**
 * @addtogroup nvic_ll
 * @{
 */

struct irq {
    static constexpr auto uart1 = concepts::irq{0};
    static constexpr auto uart2 = concepts::irq{1};
    static constexpr auto uart3 = concepts::irq{2};
    static constexpr auto dma1_channel1 = concepts::irq{3};
    static constexpr auto dma1_channel2 = concepts::irq{4};
    static constexpr auto dma1_channel3 = concepts::irq{5};
    static constexpr auto dma1_channel4 = concepts::irq{6};
    static constexpr auto dma1_channel5 = concepts::irq{7};
    static constexpr auto dma1_channel6 = concepts::irq{8};
    static constexpr auto dma1_channel7 = concepts::irq{9};
    static constexpr auto dma2_channel1 = concepts::irq{10};
    static constexpr auto dma2_channel2 = concepts::irq{11};
    static constexpr auto dma2_channel3 = concepts::irq{12};
    static constexpr auto dma2_channel4 = concepts::irq{13};
    static constexpr auto dma2_channel5 = concepts::irq{14};
    static constexpr auto dma2_channel6 = concepts::irq{15};
    static constexpr auto dma2_channel7 = concepts::irq{16};
    static constexpr auto i2c1_ev = concepts::irq{17};
    static constexpr auto i2c2_ev = concepts::irq{18};
    static constexpr auto i2c3_ev = concepts::irq{19};
    static constexpr auto i2c1_er = concepts::irq{20};
    static constexpr auto i2c2_er = concepts::irq{21};
    static constexpr auto i2c3_er = concepts::irq{22};
    static constexpr auto can1_tx = concepts::irq{23};
    static constexpr auto can1_rx0 = concepts::irq{24};
    static constexpr auto can1_rx1 = concepts::irq{25};
    static constexpr auto can1_status_change = concepts::irq{26};
    static constexpr auto can2_tx = concepts::irq{27};
    static constexpr auto can2_rx0 = concepts::irq{28};
    static constexpr auto can2_rx1 = concepts::irq{29};
    static constexpr auto can2_status_change = concepts::irq{30};
    static constexpr auto spi1 = concepts::irq{31};
    static constexpr auto spi2 = concepts::irq{32};
    static constexpr auto spi3 = concepts::irq{33};
};

/** @} */
} // namespace spl::peripherals::nvic
