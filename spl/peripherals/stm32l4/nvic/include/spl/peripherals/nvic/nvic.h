#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/nvic/concepts/enums.h"
#include "spl/peripherals/nvic/concepts/nvic.h"
#include "spl/peripherals/nvic/enums.h"

namespace spl::peripherals::nvic {

/**
 * @addtogroup nvic_ll
 * @{
 */

/**
 * @brief NVIC low lever driver class
 */
struct nvic : private satext::noncopyable {
    /**
     * @brief Enable IRQ
     * @param i IRQ to enable
     */
    const nvic& enable_irq(concepts::irq i) const;

    /**
     * @brief Disable IRQ
     * @param i IRQ to disable
     */
    const nvic& disable_irq(concepts::irq i) const;

    /**
     * @brief Set IRQ priority. IRQ priority is inverted, IRQ with greater priority value
     * has lower logical priority. Also RTOS is constraining maximum possible priority.
     * @param i IRQ to set
     * @param priority priority to set
     */
    const nvic& set_priority(concepts::irq i, std::uint8_t priority) const;

    /**
     * @brief Clear pending IRQ flag
     * @param i IRQ to clear
     */
    const nvic& clear_pending_irq(concepts::irq i) const;
};

static_assert(concepts::nvic<nvic>);

/** @} */
} // namespace spl::peripherals::nvic
