#include "spl/peripherals/nvic/nvic.h"
#include <array>
#include "libopencm3/stm32/l4/nvic.h"
#include "satext/type_traits.h"

namespace spl::peripherals::nvic {

using namespace satext;

consteval auto create_irq_lut() {
    std::array<std::uint8_t, 34> lut{};
    lut[to_underlying_type(irq::uart1)] = NVIC_USART1_IRQ;
    lut[to_underlying_type(irq::uart2)] = NVIC_USART2_IRQ;
    lut[to_underlying_type(irq::uart3)] = NVIC_USART3_IRQ;
    lut[to_underlying_type(irq::dma1_channel1)] = NVIC_DMA1_CHANNEL1_IRQ;
    lut[to_underlying_type(irq::dma1_channel2)] = NVIC_DMA1_CHANNEL2_IRQ;
    lut[to_underlying_type(irq::dma1_channel3)] = NVIC_DMA1_CHANNEL3_IRQ;
    lut[to_underlying_type(irq::dma1_channel4)] = NVIC_DMA1_CHANNEL4_IRQ;
    lut[to_underlying_type(irq::dma1_channel5)] = NVIC_DMA1_CHANNEL5_IRQ;
    lut[to_underlying_type(irq::dma1_channel6)] = NVIC_DMA1_CHANNEL6_IRQ;
    lut[to_underlying_type(irq::dma1_channel7)] = NVIC_DMA1_CHANNEL7_IRQ;
    lut[to_underlying_type(irq::dma2_channel1)] = NVIC_DMA2_CHANNEL1_IRQ;
    lut[to_underlying_type(irq::dma2_channel2)] = NVIC_DMA2_CHANNEL2_IRQ;
    lut[to_underlying_type(irq::dma2_channel3)] = NVIC_DMA2_CHANNEL3_IRQ;
    lut[to_underlying_type(irq::dma2_channel4)] = NVIC_DMA2_CHANNEL4_IRQ;
    lut[to_underlying_type(irq::dma2_channel5)] = NVIC_DMA2_CHANNEL5_IRQ;
    lut[to_underlying_type(irq::dma2_channel6)] = NVIC_DMA2_CHANNEL6_IRQ;
    lut[to_underlying_type(irq::dma2_channel7)] = NVIC_DMA2_CHANNEL7_IRQ;
    lut[to_underlying_type(irq::i2c1_ev)] = NVIC_I2C1_EV_IRQ;
    lut[to_underlying_type(irq::i2c2_ev)] = NVIC_I2C2_EV_IRQ;
    lut[to_underlying_type(irq::i2c3_ev)] = NVIC_I2C3_EV_IRQ;
    lut[to_underlying_type(irq::i2c1_er)] = NVIC_I2C1_ER_IRQ;
    lut[to_underlying_type(irq::i2c2_er)] = NVIC_I2C2_ER_IRQ;
    lut[to_underlying_type(irq::i2c3_er)] = NVIC_I2C3_ER_IRQ;
    lut[to_underlying_type(irq::can1_tx)] = NVIC_CAN1_TX_IRQ;
    lut[to_underlying_type(irq::can1_rx0)] = NVIC_CAN1_RX0_IRQ;
    lut[to_underlying_type(irq::can1_rx1)] = NVIC_CAN1_RX1_IRQ;
    lut[to_underlying_type(irq::can1_status_change)] = NVIC_CAN1_SCE_IRQ;
    lut[to_underlying_type(irq::can2_tx)] = NVIC_CAN2_TX_IRQ;
    lut[to_underlying_type(irq::can2_rx0)] = NVIC_CAN2_RX0_IRQ;
    lut[to_underlying_type(irq::can2_rx1)] = NVIC_CAN2_RX1_IRQ;
    lut[to_underlying_type(irq::can2_status_change)] = NVIC_CAN2_SCE_IRQ;
    lut[to_underlying_type(irq::spi1)] = NVIC_SPI1_IRQ;
    lut[to_underlying_type(irq::spi2)] = NVIC_SPI2_IRQ;
    lut[to_underlying_type(irq::spi3)] = NVIC_SPI3_IRQ;
    return lut;
}

constexpr std::uint8_t prio_bits = 4;
constexpr std::uint8_t bits_per_byte = 8;

constexpr auto irq_lut = create_irq_lut();

std::uint8_t operator*(concepts::irq i) { return irq_lut[to_underlying_type(i)]; }

const nvic& nvic::enable_irq(concepts::irq i) const {
    nvic_enable_irq(*i);
    return *this;
}

const nvic& nvic::disable_irq(concepts::irq i) const {
    nvic_disable_irq(*i);
    return *this;
}

const nvic& nvic::set_priority(concepts::irq i, std::uint8_t priority) const {
    nvic_set_priority(*i, static_cast<std::uint8_t>(priority << (bits_per_byte - prio_bits)));
    return *this;
}

const nvic& nvic::clear_pending_irq(concepts::irq i) const {
    nvic_clear_pending_irq(*i);
    return *this;
}

} // namespace spl::peripherals::nvic
