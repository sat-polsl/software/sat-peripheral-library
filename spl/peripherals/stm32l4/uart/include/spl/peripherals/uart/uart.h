#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/uart/concepts/enums.h"
#include "spl/peripherals/uart/concepts/uart.h"
#include "spl/peripherals/uart/enums.h"

namespace spl::peripherals::uart {

/**
 * @addtogroup uart_ll
 * @{
 */

/**
 * @brief UART low level driver class
 */
class uart : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param uart_instance UART instance
     */
    explicit constexpr uart(instance uart_instance) : instance_(uart_instance){};

    /**
     * @brief Set baudrate
     * @param baudrate baudrate in bytes per second
     * @return UART base
     */
    const uart& set_baudrate(std::uint32_t baudrate) const;

    /**
     * @brief Set UART mode
     * @param uart_mode UART mode
     * @return UART base
     */
    const uart& set_mode(concepts::mode uart_mode) const;

    /**
     * @brief Enable UART IRQ
     * @param uart_irq IRQ to enable
     * @return UART base
     */
    const uart& enable_irq(concepts::irq uart_irq) const;

    /**
     * @brief Disable UART IRQ
     * @param uart_irq IRQ to disable
     * @return UART base
     */
    const uart& disable_irq(concepts::irq uart_irq) const;

    /**
     * @brief Checks if given IRQ is enabled
     * @param uart_irq UART IRQ
     * @return true if enabled, false if disabled
     */
    bool is_irq_enabled(concepts::irq uart_irq) const;

    /**
     * @brief Enable UART
     * @return UART base
     */
    const uart& enable() const;

    /**
     * @brief Disable UART
     * @return UART base
     */
    const uart& disable() const;

    /**
     * @brief Enable UART DMA
     * @param uart_dma DMA to enable
     * @return UART base
     */
    const uart& enable_dma(concepts::dma uart_dma) const;

    /**
     * @brief Disable UART DMA
     * @param uart_dma DMA to disable
     * @return UART base
     */
    const uart& disable_dma(concepts::dma uart_dma) const;

    /**
     * @brief Checks if TX register is empty
     * @return true if TX register is empty, false otherwise
     */
    bool is_tx_empty() const;

    /**
     * @brief Checks if RX register is not empty
     * @return true if RX register is not empty, false otherwise
     */
    bool is_rx_not_empty() const;

    /**
     * @brief Checks if Overrun Error occurred
     * @return true if Overrun Error occurred, false otherwise
     */
    bool overrun_error() const;

    /**
     * @brief Clear UART IRQ flag
     * @param irq flag to clear
     */
    void clear_irq_flag(concepts::clear_irq irq) const;

    /**
     * @brief Write data to TX register
     * @param data data to write
     */
    void write(std::uint16_t data) const;

    /**
     * @brief Read data from RX register
     * @return Data readed from RX register
     */
    std::uint16_t read() const;

private:
    instance instance_;
};

/** @} */

static_assert(concepts::uart<uart>);

} // namespace spl::peripherals::uart
