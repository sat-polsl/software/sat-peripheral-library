#pragma once
#include <cstdint>
#include "spl/peripherals/uart/concepts/enums.h"

namespace spl::peripherals::uart {

/**
 * @addtogroup uart_ll
 * @{
 */

/**
 * @brief UART instance enumaration
 */
enum class instance : std::int32_t { uart1, uart2, uart3 };

struct irq {
    static constexpr auto tx_empty = concepts::irq{0};
    static constexpr auto tx_complete = concepts::irq{1};
    static constexpr auto rx_not_empty = concepts::irq{2};
};

struct clear_irq {
    static constexpr auto overrun = concepts::clear_irq{0};
};

using concepts::dma;
using concepts::mode;

/** @} */
} // namespace spl::peripherals::uart
