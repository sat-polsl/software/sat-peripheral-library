#include "spl/peripherals/uart/uart.h"
#include <array>
#include "libopencm3/stm32/usart.h"
#include "satext/type_traits.h"

namespace spl::peripherals::uart {

using namespace satext;

consteval auto create_instance_lut() {
    std::array<std::uint32_t, 3> lut{};
    lut[to_underlying_type(instance::uart1)] = USART1;
    lut[to_underlying_type(instance::uart2)] = USART2;
    lut[to_underlying_type(instance::uart3)] = USART3;
    return lut;
}

consteval auto create_mode_lut() {
    std::array<std::uint32_t, 3> lut{};
    lut[to_underlying_type(concepts::mode::rx)] = USART_MODE_RX;
    lut[to_underlying_type(concepts::mode::tx)] = USART_MODE_TX;
    lut[to_underlying_type(concepts::mode::rx_tx)] = USART_MODE_TX_RX;
    return lut;
}

consteval auto create_clear_irq_lut() {
    std::array<std::uint32_t, 1> lut{};
    lut[to_underlying_type(clear_irq::overrun)] = USART_ICR_ORECF;
    return lut;
}

constexpr auto instance_lut = create_instance_lut();
constexpr auto mode_lut = create_mode_lut();
constexpr auto clear_irq_lut = create_clear_irq_lut();

std::uint32_t operator*(instance uart_instance) {
    return instance_lut[to_underlying_type(uart_instance)];
}
std::uint32_t operator*(concepts::mode uart_mode) {
    return mode_lut[to_underlying_type(uart_mode)];
}

std::uint32_t operator*(concepts::clear_irq irq) { return clear_irq_lut[to_underlying_type(irq)]; }

const uart& uart::enable() const {
    usart_enable(*instance_);
    return *this;
}

const uart& uart::disable() const {
    usart_disable(*instance_);
    return *this;
}

const uart& uart::set_baudrate(std::uint32_t baudrate) const {
    usart_set_baudrate(*instance_, baudrate);
    return *this;
}

const uart& uart::set_mode(concepts::mode uart_mode) const {
    usart_set_mode(*instance_, *uart_mode);
    return *this;
}

const uart& uart::enable_irq(concepts::irq uart_irq) const {
    switch (uart_irq) {
    case irq::tx_complete:
        usart_enable_tx_complete_interrupt(*instance_);
        break;
    case irq::tx_empty:
        usart_enable_tx_interrupt(*instance_);
        break;
    case irq::rx_not_empty:
        usart_enable_rx_interrupt(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const uart& uart::disable_irq(concepts::irq uart_irq) const {
    switch (uart_irq) {
    case irq::tx_complete:
        usart_disable_tx_complete_interrupt(*instance_);
        break;
    case irq::tx_empty:
        usart_disable_tx_interrupt(*instance_);
        break;
    case irq::rx_not_empty:
        usart_disable_rx_interrupt(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

bool uart::is_irq_enabled(concepts::irq uart_irq) const {
    switch (uart_irq) {
    case irq::tx_complete:
        return usart_tx_complete_interrupt_enabled(*instance_);
    case irq::tx_empty:
        return usart_tx_interrupt_enabled(*instance_);
    case irq::rx_not_empty:
        return usart_rx_interrupt_enabled(*instance_);
    default:
        return false;
    }
}

void uart::write(std::uint16_t data) const { usart_send(*instance_, data); }

std::uint16_t uart::read() const { return usart_recv(*instance_); }

bool uart::is_tx_empty() const { return usart_get_flag(*instance_, USART_FLAG_TXE); }

bool uart::is_rx_not_empty() const { return usart_get_flag(*instance_, USART_FLAG_RXNE); }

bool uart::overrun_error() const { return usart_get_flag(*instance_, USART_FLAG_ORE); }

void uart::clear_irq_flag(concepts::clear_irq irq) const {
    usart_clear_interrupt_flag(*instance_, *irq);
}

const uart& uart::enable_dma(concepts::dma uart_dma) const {
    if (uart_dma == concepts::dma::rx) {
        usart_enable_rx_dma(*instance_);
    } else {
        usart_enable_tx_dma(*instance_);
    }
    return *this;
}

const uart& uart::disable_dma(concepts::dma uart_dma) const {
    if (uart_dma == concepts::dma::rx) {
        usart_disable_rx_dma(*instance_);
    } else {
        usart_disable_tx_dma(*instance_);
    }
    return *this;
}

} // namespace spl::peripherals::uart
