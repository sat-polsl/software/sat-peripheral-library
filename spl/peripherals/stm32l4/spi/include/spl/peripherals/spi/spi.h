#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/spi/concepts/enums.h"
#include "spl/peripherals/spi/concepts/spi.h"
#include "spl/peripherals/spi/enums.h"

namespace spl::peripherals::spi {

/**
 * @addtogroup spi_ll
 * @{
 */

/**
 * @brief SPI low level driver class
 */
class spi : private satext::noncopyable {
public:
    /**
     * @brief constructor.
     * @param instance spi instance.
     */
    explicit constexpr spi(spl::peripherals::spi::instance instance) : instance_(instance){};

    /**
     * @brief enable spi.
     * @return const spi_base&
     */
    const spi& enable() const;

    /**
     * @brief disable spi.
     * @return const spi_base&
     */
    const spi& disable() const;

    /**
     * @brief write data to spi.
     * @param data 16-bit data to be written.
     */
    void write(std::uint8_t data) const;

    /**
     * @brief read data from spi.
     * @return std::uint16_t data received.
     */
    std::uint8_t read() const;

    /**
     * @brief write data to spi and read from it.
     * @details data is written similarly to write()
     *          method.
     * @param data 16-bit data to be written.
     * @return std::uint16_t data received.
     * @see write()
     */
    std::uint8_t xfer(std::uint8_t data) const;

    /**
     * @brief set full duplex mode.
     * @return const spi_base&
     */
    const spi& set_full_duplex_mode() const;

    /**
     * @brief set receive only mode.
     * @return const spi_base&
     */
    const spi& set_receive_only_mode() const;

    /**
     * @brief set slave management method.
     * @param method method to manage slaves.
     * @return const spi_base&
     * @see #slave_management
     */
    const spi& set_slave_management(concepts::slave_management method) const;

    /**
     * @brief set bit sending order
     * @param order bit sending order.
     * @return const spi_base&
     * @see #send_order
     */
    const spi& set_sending_order(concepts::send_order order) const;

    /**
     * @brief set baudrate prescaler
     * @param baudrate baudrate prescaler.
     * @return const spi_base&
     */
    const spi& set_baudrate_prescaler(concepts::baudrate_prescaler baudrate) const;

    /**
     * @brief set spi mode
     * @param mode mode in which spi should work.
     * @return const spi_base&
     * @see #mode
     */
    const spi& set_mode(concepts::mode mode) const;

    /**
     * @brief enable spi interrupt request.
     * @param spi_irq interrupt request to enable.
     * @return const spi_base&
     * @see  #irq
     */
    const spi& enable_irq(concepts::irq spi_irq) const;

    /**
     * @brief disable spi interrupt request.
     * @param spi_irq interrupt request to disable.
     * @return const spi_base&
     * @see #irq
     */
    const spi& disable_irq(concepts::irq spi_irq) const;

    /**
     * @brief enable dma for spi.
     * @param target target for dma.
     * @return const spi_base&
     * @see #dma
     */
    const spi& enable_dma(concepts::dma target) const;

    /**
     * @brief disable dma for spi.
     * @param target target for dma.
     * @return const spi_base&
     * @see #dma
     */
    const spi& disable_dma(concepts::dma target) const;

    /**
     * @brief set clock mode for spi.
     * @param clock_mode spi clock_mode.
     * @return const spi_base&
     * @see #clock_mode
     */
    const spi& set_clock_mode(concepts::clock_mode spi_mode) const;

    /**
     * @brief set data frame size for spi.
     * @param size data frame size.
     * @return const spi_base&
     * @sa spi::data_size(data_size)
     */
    const spi& set_data_size(concepts::data_size size) const;

    /**
     * @brief set reception threshold for spi.
     * @param threshold reception threshold.
     * @return const spi_base&
     * @sa #fifo_threshold
     */
    const spi& set_fifo_reception_threshold(concepts::fifo_threshold threshold) const;

    /**
     * @brief Checks if specified interrupt is enabled
     * @param irq interrupt to be checked
     * @return bool
     */
    bool is_irq_enabled(concepts::irq irq) const;
    /**
     * @brief checks if spi is busy
     * @return true if spi is busy, false otherwise
     */
    bool is_busy() const;

    /**
     * @brief checks if tx register is empty
     * @return true if tx register is empty, false otherwise
     */
    bool is_tx_empty() const;

    /**
     * @brief checks if rx register is not empty
     * @return true if rx register is not empty, false otherwise
     */
    bool is_rx_not_empty() const;

    /**
     * @brief checks if overrun error occurred
     * @return true if overrun error occurred, false otherwise
     */
    bool overrun_error() const;

    /**
     * @brief checks if rx fifo is empty
     * @return true if rx fifo is empty, false otherwise
     */
    bool is_rx_fifo_empty() const;

    /**
     * @brief set internal slave select
     * @param select
     * @return const spi_base&
     */
    const spi& set_internal_slave_select(bool select) const;

    const spi& clear_error_flag(concepts::error flag) const;

private:
    instance instance_;
};

/** @} */

static_assert(concepts::spi<spi>);
} // namespace spl::peripherals::spi
