#pragma once
#include <cstdint>
#include "spl/peripherals/spi/concepts/enums.h"

namespace spl::peripherals::spi {

/**
 * @addtogroup spi_ll
 * @{
 */

/**
 * @brief SPI instance enumeration.
 */
enum class instance : std::int32_t { spi1, spi2, spi3 };

struct irq {
    static constexpr auto tx_buffer_empty = concepts::irq{0};
    static constexpr auto rx_buffer_not_empty = concepts::irq{1};
    static constexpr auto error = concepts::irq{2};
};

struct baudrate_prescaler {
    static constexpr auto div2 = concepts::baudrate_prescaler{0};
    static constexpr auto div4 = concepts::baudrate_prescaler{1};
    static constexpr auto div8 = concepts::baudrate_prescaler{2};
    static constexpr auto div16 = concepts::baudrate_prescaler{3};
    static constexpr auto div32 = concepts::baudrate_prescaler{4};
    static constexpr auto div64 = concepts::baudrate_prescaler{5};
    static constexpr auto div128 = concepts::baudrate_prescaler{6};
    static constexpr auto div256 = concepts::baudrate_prescaler{7};
};

struct fifo_threshold {
    static constexpr auto bit8 = concepts::fifo_threshold{0};
    static constexpr auto bit16 = concepts::fifo_threshold{1};
};

struct error {
    static constexpr auto overrun = concepts::error{0};
    static constexpr auto mode_fault = concepts::error{1};
};

using concepts::clock_mode;
using concepts::data_size;
using concepts::dma;
using concepts::mode;
using concepts::send_order;
using concepts::slave_management;

/** @} */

} // namespace spl::peripherals::spi
