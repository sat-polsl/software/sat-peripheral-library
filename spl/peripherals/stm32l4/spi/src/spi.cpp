#include "spl/peripherals/spi/spi.h"
#include <array>
#include "libopencm3/stm32/spi.h"
#include "satext/type_traits.h"

namespace spl::peripherals::spi {
using namespace satext;

consteval auto create_spi_instance_lut() {
    std::array<std::uint32_t, 3> lut{};
    lut[to_underlying_type(instance::spi1)] = SPI1;
    lut[to_underlying_type(instance::spi2)] = SPI2;
    lut[to_underlying_type(instance::spi3)] = SPI3;
    return lut;
}
constexpr auto spi_instance_lut = create_spi_instance_lut();
std::uint32_t operator*(instance instance) {
    return spi_instance_lut[to_underlying_type(instance)];
}

const spi& spi::enable() const {
    spi_enable(*instance_);
    return *this;
}

const spi& spi::disable() const {
    spi_disable(*instance_);
    return *this;
}

void spi::write(uint8_t data) const { spi_send_byte(*instance_, data); }

std::uint8_t spi::read() const { return spi_read_byte(*instance_); }

std::uint8_t spi::xfer(uint8_t data) const {
    write(data);
    return read();
}

const spi& spi::set_full_duplex_mode() const {
    spi_set_full_duplex_mode(*instance_);
    return *this;
}

const spi& spi::set_receive_only_mode() const {
    spi_set_receive_only_mode(*instance_);
    return *this;
}

const spi& spi::set_slave_management(concepts::slave_management method) const {
    switch (method) {
    case slave_management::hardware:
        spi_disable_software_slave_management(*instance_);
        break;
    case slave_management::software:
        spi_enable_software_slave_management(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::set_sending_order(concepts::send_order order) const {
    switch (order) {
    case send_order::msb_first:
        spi_send_msb_first(*instance_);
        break;
    case send_order::lsb_first:
        spi_send_lsb_first(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::set_baudrate_prescaler(concepts::baudrate_prescaler baudrate) const {
    spi_set_baudrate_prescaler(*instance_, static_cast<std::uint8_t>(baudrate));
    return *this;
}

const spi& spi::set_mode(concepts::mode mode) const {
    switch (mode) {
    case mode::master:
        spi_set_master_mode(*instance_);
        break;
    case mode::slave:
        spi_set_slave_mode(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::enable_irq(concepts::irq spi_irq) const {
    switch (spi_irq) {
    case irq::tx_buffer_empty:
        spi_enable_tx_buffer_empty_interrupt(*instance_);
        break;
    case irq::rx_buffer_not_empty:
        spi_enable_rx_buffer_not_empty_interrupt(*instance_);
        break;
    case irq::error:
        spi_enable_error_interrupt(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::disable_irq(concepts::irq spi_irq) const {
    switch (spi_irq) {
    case irq::tx_buffer_empty:
        spi_disable_tx_buffer_empty_interrupt(*instance_);
        break;
    case irq::rx_buffer_not_empty:
        spi_disable_rx_buffer_not_empty_interrupt(*instance_);
        break;
    case irq::error:
        spi_disable_error_interrupt(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::enable_dma(concepts::dma target) const {
    switch (target) {
    case dma::tx:
        spi_enable_tx_dma(*instance_);
        break;
    case dma::rx:
        spi_enable_rx_dma(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::disable_dma(concepts::dma target) const {
    switch (target) {
    case dma::tx:
        spi_disable_tx_dma(*instance_);
        break;
    case dma::rx:
        spi_disable_rx_dma(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::set_clock_mode(concepts::clock_mode spi_mode) const {
    switch (spi_mode) {
    case clock_mode::mode0:
        spi_set_standard_mode(*instance_, 0);
        break;
    case clock_mode::mode1:
        spi_set_standard_mode(*instance_, 1);
        break;
    case clock_mode::mode2:
        spi_set_standard_mode(*instance_, 2);
        break;
    case clock_mode::mode3:
        spi_set_standard_mode(*instance_, 3);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::set_data_size(concepts::data_size size) const {
    switch (size) {
    case data_size::bit8:
        spi_set_data_size(*instance_, 8);
        break;
    case data_size::bit16:
        spi_set_data_size(*instance_, 16);
        break;
    default:
        break;
    }
    return *this;
}

const spi& spi::set_fifo_reception_threshold(concepts::fifo_threshold threshold) const {
    switch (threshold) {
    case fifo_threshold::bit8:
        spi_fifo_reception_threshold_8bit(*instance_);
        break;
    case fifo_threshold::bit16:
        spi_fifo_reception_threshold_16bit(*instance_);
        break;
    default:
        break;
    }
    return *this;
}

bool spi::is_busy() const { return spi_get_status(*instance_, SPI_SR_BSY); }

bool spi::is_tx_empty() const { return spi_get_status(*instance_, SPI_SR_TXE); }

bool spi::is_rx_not_empty() const { return spi_get_status(*instance_, SPI_SR_RXNE); }

bool spi::overrun_error() const { return spi_get_status(*instance_, SPI_SR_OVR); }

bool spi::is_rx_fifo_empty() const { return (SPI_SR(*instance_) & (0x3 << 9)) == 0; }

const spi& spi::set_internal_slave_select(bool select) const {
    if (select) {
        spi_set_nss_high(*instance_);
    } else {
        spi_set_nss_low(*instance_);
    }
    return *this;
}

bool spi::is_irq_enabled(concepts::irq irq) const {
    switch (irq) {
    case irq::rx_buffer_not_empty:
        return spi_is_rx_buffer_not_empty_interrupt_enabled(*instance_);
    case irq::tx_buffer_empty:
        return spi_is_tx_buffer_empty_interrupt_enabled(*instance_);
    case irq::error:
        return spi_is_error_interrupt_enabled(*instance_);
    default:
        return false;
    }
}

const spi& spi::clear_error_flag(concepts::error flag) const {
    switch (flag) {
    case error::overrun:
        spi_clear_overrun_flag(*instance_);
        return *this;
    case error::mode_fault:
        spi_clear_mode_fault_flag(*instance_);
        return *this;
    default:
        return *this;
    }
}

} // namespace spl::peripherals::spi
