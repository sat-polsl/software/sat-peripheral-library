#include "spl/peripherals/flash/flash.h"
#include "libopencm3/stm32/l4/flash.h"
#include "satext/type_traits.h"

namespace spl::peripherals::flash {

bool flash::is_busy() const { return flash_is_busy(); }

const flash& flash::set_wait_states(concepts::wait_states ws) const {
    flash_set_ws(satext::to_underlying_type(ws));
    return *this;
}

const flash& flash::lock() const {
    flash_lock();
    return *this;
}

const flash& flash::unlock() const {
    flash_unlock();
    return *this;
}

const flash& flash::program_u64(std::uint32_t address, std::uint64_t data) const {
    flash_program_double_word(address, data);
    return *this;
}

const flash& flash::program_buffer(std::uint32_t address, std::span<const std::byte> buffer) const {
    flash_program(address,
                  const_cast<std::uint8_t*>(reinterpret_cast<const std::uint8_t*>(buffer.data())),
                  buffer.size());
    return *this;
}

const flash& flash::erase_page(std::uint8_t page) const {
    flash_erase_page(page);
    return *this;
}

const flash& flash::mass_erase() const {
    flash_erase_all_pages();
    return *this;
}

const flash& flash::enable_irq(concepts::irq flash_irq) const {
    switch (flash_irq) {
    case irq::ecc_correction:
        flash_enable_ecc_correction_interrupt();
        break;
    case irq::end_of_operation:
        flash_enable_end_of_operation_interrupt();
        break;
    case irq::operation_error:
        flash_enable_operation_error_interrupt();
        break;
    case irq::read_error:
        flash_enable_read_error_interrupt();
        break;
    default:
        break;
    }
    return *this;
}

const flash& flash::disable_irq(concepts::irq flash_irq) const {
    switch (flash_irq) {
    case irq::ecc_correction:
        flash_disable_ecc_correction_interrupt();
        break;
    case irq::end_of_operation:
        flash_disable_end_of_operation_interrupt();
        break;
    case irq::operation_error:
        flash_disable_operation_error_interrupt();
        break;
    case irq::read_error:
        flash_disable_read_error_interrupt();
        break;
    default:
        break;
    }
    return *this;
}

bool flash::is_irq_enabled(concepts::irq flash_irq) const {
    switch (flash_irq) {
    case irq::ecc_correction:
        return flash_is_ecc_correction_interrupt_enabled();
    case irq::end_of_operation:
        return flash_is_end_of_operation_interrupt_enabled();
    case irq::operation_error:
        return flash_is_operation_error_interrupt_enabled();
    case irq::read_error:
        return flash_is_read_error_interrupt_enabled();
    default:
        return false;
    }
}

const flash& flash::clear_irq_flag(concepts::irq flash_irq) const {
    switch (flash_irq) {
    case irq::ecc_correction:
        flash_clear_ecc_correction_interrupt();
        break;
    case irq::end_of_operation:
        flash_clear_end_of_operation_interrupt();
        break;
    case irq::operation_error:
        flash_clear_operation_error_interrupt();
        break;
    case irq::read_error:
        flash_clear_read_error_interrupt();
        break;
    default:
        break;
    }
    return *this;
}

bool flash::end_of_operation() const { return flash_end_of_operation(); }

bool flash::operation_error() const { return flash_operation_error(); }

bool flash::read_error() const { return flash_read_error(); }

bool flash::ecc_correction() const { return flash_ecc_correction(); }

} // namespace spl::peripherals::flash
