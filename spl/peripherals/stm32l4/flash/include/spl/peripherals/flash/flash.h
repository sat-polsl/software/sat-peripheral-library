#pragma once
#include <span>
#include "satext/noncopyable.h"
#include "satext/units.h"
#include "spl/peripherals/flash/concepts/enums.h"
#include "spl/peripherals/flash/concepts/flash.h"
#include "spl/peripherals/flash/enums.h"

namespace spl::peripherals::flash {

/**
 * @addtogroup flash_ll
 * @{
 */

class flash : private satext::noncopyable {
public:
    /**
     * @brief Flash Page size
     */
    static constexpr auto page_size = 2_KiB;

    /**
     * @brief Flash Memory base
     */
    static constexpr std::uint32_t memory_base = 0x08000000;

    /**
     * @brief Checks whether Flash is busy
     * @return true if Flash is busy, false otherwise
     */
    bool is_busy() const;

    /**
     * @brief Sets Flash Wait States
     * @param ws wait states
     * @return Flash object
     */
    const flash& set_wait_states(concepts::wait_states ws) const;

    /**
     * @brief Locks Flash writes
     * @return Flash object
     */
    const flash& lock() const;

    /**
     * @brief Unlocks Flash writes
     * @return Flash object
     */
    const flash& unlock() const;

    /**
     * @brief Blocking method. Write 64 bit data to Flash at given address
     * @param address Address where data will be written
     * @param data Data to write
     * @return Flash object
     */
    const flash& program_u64(std::uint32_t address, std::uint64_t data) const;

    /**
     * @brief Blocking method. Writes buffer to Flash at given address
     * @param address Address where data will be written
     * @param buffer Buffer to write
     * @return Flash object
     */
    const flash& program_buffer(std::uint32_t address, std::span<const std::byte> buffer) const;

    /**
     * @brief Blocking method. Erases given Flash page
     * @param page Page to erase
     * @return Flash object
     */
    const flash& erase_page(std::uint8_t page) const;

    /**
     * @brief Blocking method. Erases all pages
     * @return Flash object
     */
    const flash& mass_erase() const;

    /**
     * @brief Enables given IRQ
     * @param flash_irq IRQ to enable
     * @return Flash object
     */
    const flash& enable_irq(concepts::irq flash_irq) const;

    /**
     * @brief Disables given IRQ
     * @param flash_irq IRQ to disable
     * @return Flash object
     */
    const flash& disable_irq(concepts::irq flash_irq) const;

    /**
     * @brief Clears IRQ flag
     * @param flash_irq IRQ to clear
     * @return Flash object
     */
    const flash& clear_irq_flag(concepts::irq flash_irq) const;

    /**
     * @brief Checks whether given IRQ is enabled
     * @param flash_irq IRQ
     * @return true if given IRQ is enabled, false otherwise
     */
    bool is_irq_enabled(concepts::irq flash_irq) const;

    /**
     * @brief Checks whether End of operation flag is set
     * @return true if flag is set, false otherwise
     */
    bool end_of_operation() const;

    /**
     * @brief Checks whether Operation Error occurred
     * @return true if Operation Error occurred, false otherwise
     */
    bool operation_error() const;

    /**
     * @brief Checks whether Read Error occurred
     * @return true if Read Error occurred, false otherwise
     */
    bool read_error() const;

    /**
     * @brief Checks whether ECC Correction occurred
     * @return true if ECC Correction occurred, false otherwise
     */
    bool ecc_correction() const;
};

static_assert(concepts::flash<flash>);

/** @} */

} // namespace spl::peripherals::flash
