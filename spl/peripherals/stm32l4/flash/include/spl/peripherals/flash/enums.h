#pragma once
#include <cstdint>
#include "spl/peripherals/flash/concepts/enums.h"

namespace spl::peripherals::flash {

/**
 * @addtogroup flash_ll
 * @{
 */

constexpr auto ws0 = concepts::wait_states(0);
constexpr auto ws1 = concepts::wait_states(1);
constexpr auto ws2 = concepts::wait_states(2);
constexpr auto ws3 = concepts::wait_states(3);
constexpr auto ws4 = concepts::wait_states(4);

struct irq {
    static constexpr auto end_of_operation = concepts::irq{0};
    static constexpr auto operation_error = concepts::irq{1};
    static constexpr auto read_error = concepts::irq{2};
    static constexpr auto ecc_correction = concepts::irq{3};
};

/** @} */

} // namespace spl::peripherals::flash
