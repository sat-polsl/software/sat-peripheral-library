#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/iwdg/concepts/iwdg.h"

namespace spl::peripherals::iwdg {

/**
 * @addtogroup iwdg_ll
 * @{
 */

/**
 * @brief Watchdog class
 */
class iwdg : private satext::noncopyable {
public:
    /**
     * @brief Initializes watchdog
     * @return IWDG object
     */
    const iwdg& initialize() const;

    /**
     * @brief Sets watchdog period
     * @param period watchdog period
     * @return IWDG object
     */
    const iwdg& set_period_ms(std::uint32_t period) const;

    /**
     * @brief Kicks watchdog
     * @return IWDG object
     */
    const iwdg& kick() const;
};

/** @} */

static_assert(concepts::iwdg<iwdg>);

} // namespace spl::peripherals::iwdg
