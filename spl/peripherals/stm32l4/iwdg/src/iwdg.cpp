#include "spl/peripherals/iwdg/iwdg.h"
#include "libopencm3/stm32/iwdg.h"

namespace spl::peripherals::iwdg {
const iwdg& iwdg::initialize() const {
    iwdg_start();
    return *this;
}

const iwdg& iwdg::set_period_ms(std::uint32_t period) const {
    iwdg_set_period_ms(period);
    return *this;
}

const iwdg& iwdg::kick() const {
    iwdg_reset();
    return *this;
}
} // namespace spl::peripherals::iwdg
