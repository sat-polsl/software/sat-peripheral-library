#include "spl/peripherals/dma/dma.h"
#include <array>
#include "libopencm3/stm32/crc.h"
#include "libopencm3/stm32/dma.h"
#include "libopencm3/stm32/i2c.h"
#include "libopencm3/stm32/spi.h"
#include "libopencm3/stm32/usart.h"
#include "satext/type_traits.h"

namespace spl::peripherals::dma {

using namespace satext;

consteval auto create_instance_lut() {
    std::array<std::uint32_t, 2> lut{};
    lut[to_underlying_type(instance::dma1)] = DMA1;
    lut[to_underlying_type(instance::dma2)] = DMA2;
    return lut;
}

consteval auto create_channel_lut() {
    std::array<std::uint8_t, 7> lut{};
    lut[to_underlying_type(channel1)] = DMA_CHANNEL1;
    lut[to_underlying_type(channel2)] = DMA_CHANNEL2;
    lut[to_underlying_type(channel3)] = DMA_CHANNEL3;
    lut[to_underlying_type(channel4)] = DMA_CHANNEL4;
    lut[to_underlying_type(channel5)] = DMA_CHANNEL5;
    lut[to_underlying_type(channel6)] = DMA_CHANNEL6;
    lut[to_underlying_type(channel7)] = DMA_CHANNEL7;
    return lut;
}

consteval auto create_peripheral_lut() {
    std::array<std::uint32_t, 16> lut{};
    constexpr std::uint32_t uart_rdr = 0x24;
    constexpr std::uint32_t uart_tdr = 0x28;
    constexpr std::uint32_t i2c_rdr = 0x24;
    constexpr std::uint32_t i2c_tdr = 0x28;
    constexpr std::uint32_t spi_dr = 0x0c;
    lut[to_underlying_type(peripheral::uart1_rx)] = USART1 + uart_rdr;
    lut[to_underlying_type(peripheral::uart1_tx)] = USART1 + uart_tdr;
    lut[to_underlying_type(peripheral::uart2_rx)] = USART2 + uart_rdr;
    lut[to_underlying_type(peripheral::uart2_tx)] = USART2 + uart_tdr;
    lut[to_underlying_type(peripheral::uart3_rx)] = USART3 + uart_rdr;
    lut[to_underlying_type(peripheral::uart3_tx)] = USART3 + uart_tdr;
    lut[to_underlying_type(peripheral::crc)] = CRC_BASE;
    lut[to_underlying_type(peripheral::i2c1_rx)] = I2C1 + i2c_rdr;
    lut[to_underlying_type(peripheral::i2c1_tx)] = I2C1 + i2c_tdr;
    lut[to_underlying_type(peripheral::i2c2_rx)] = I2C2 + i2c_rdr;
    lut[to_underlying_type(peripheral::i2c2_tx)] = I2C2 + i2c_tdr;
    lut[to_underlying_type(peripheral::i2c3_rx)] = I2C3 + i2c_rdr;
    lut[to_underlying_type(peripheral::i2c3_tx)] = I2C3 + i2c_tdr;
    lut[to_underlying_type(peripheral::spi1)] = SPI1 + spi_dr;
    lut[to_underlying_type(peripheral::spi2)] = SPI2 + spi_dr;
    lut[to_underlying_type(peripheral::spi3)] = SPI3 + spi_dr;
    return lut;
}

constexpr auto instance_lut = create_instance_lut();
constexpr auto channel_lut = create_channel_lut();
constexpr auto peripheral_lut = create_peripheral_lut();

std::uint32_t operator*(instance dma_instance) {
    return instance_lut[to_underlying_type(dma_instance)];
}

std::uint8_t operator*(concepts::channel dma_channel) {
    return channel_lut[to_underlying_type(dma_channel)];
}

std::uint8_t operator*(concepts::request req) { return to_underlying_type(req); }

std::uint32_t operator*(concepts::peripheral periph) {
    return peripheral_lut[to_underlying_type(periph)];
}

const dma& dma::enable_channel(concepts::channel dma_channel) const {
    dma_enable_channel(*instance_, *dma_channel);
    return *this;
}

const dma& dma::disable_channel(concepts::channel dma_channel) const {
    dma_disable_channel(*instance_, *dma_channel);
    return *this;
}

const dma& dma::reset_channel(concepts::channel ch) const {
    dma_channel_reset(*instance_, *ch);
    return *this;
}

const dma& dma::set_peripheral_address(concepts::channel ch, concepts::peripheral address) const {
    dma_set_peripheral_address(*instance_, *ch, *address);
    return *this;
}

const dma& dma::set_memory_address(concepts::channel ch, std::uint32_t address) const {
    dma_set_memory_address(*instance_, *ch, address);
    return *this;
}

const dma& dma::set_transfer_size(concepts::channel ch, std::uint16_t size) const {
    dma_set_number_of_data(*instance_, *ch, size);
    return *this;
}

const dma& dma::set_memory_increment(concepts::channel ch, bool increment) const {
    if (increment) {
        dma_enable_memory_increment_mode(*instance_, *ch);
    } else {
        dma_disable_memory_increment_mode(*instance_, *ch);
    }
    return *this;
}
const dma& dma::set_peripheral_increment(concepts::channel ch, bool increment) const {
    if (increment) {
        dma_enable_peripheral_increment_mode(*instance_, *ch);
    } else {
        dma_disable_peripheral_increment_mode(*instance_, *ch);
    }
    return *this;
}

const dma& dma::enable_irq(concepts::channel ch, concepts::irq dma_irq) const {
    switch (dma_irq) {
    case irq::half_transfer:
        dma_enable_half_transfer_interrupt(*instance_, *ch);
        break;
    case irq::transfer_completed:
        dma_enable_transfer_complete_interrupt(*instance_, *ch);
        break;
    case irq::transfer_error:
        dma_enable_transfer_error_interrupt(*instance_, *ch);
        break;
    default:
        break;
    }
    return *this;
}

const dma& dma::disable_irq(concepts::channel ch, concepts::irq dma_irq) const {
    switch (dma_irq) {
    case irq::half_transfer:
        dma_disable_half_transfer_interrupt(*instance_, *ch);
        break;
    case irq::transfer_completed:
        dma_disable_transfer_complete_interrupt(*instance_, *ch);
        break;
    case irq::transfer_error:
        dma_disable_transfer_error_interrupt(*instance_, *ch);
        break;
    default:
        break;
    }
    return *this;
}

const dma& dma::clear_irq(concepts::channel ch, concepts::irq dma_irq) const {
    switch (dma_irq) {
    case irq::half_transfer:
        dma_clear_interrupt_flags(*instance_, *ch, DMA_HTIF);
        break;
    case irq::transfer_completed:
        dma_clear_interrupt_flags(*instance_, *ch, DMA_TCIF);
        break;
    case irq::transfer_error:
        dma_clear_interrupt_flags(*instance_, *ch, DMA_TEIF);
        break;
    default:
        break;
    }
    return *this;
}

bool dma::get_irq_status(concepts::channel ch, concepts::irq dma_irq) const {
    switch (dma_irq) {
    case irq::half_transfer:
        return dma_get_interrupt_flag(*instance_, *ch, DMA_HTIF);
    case irq::transfer_completed:
        return dma_get_interrupt_flag(*instance_, *ch, DMA_TCIF);
    case irq::transfer_error:
        return dma_get_interrupt_flag(*instance_, *ch, DMA_TEIF);
    default:
        return false;
    }
}

const dma& dma::set_direction(concepts::channel ch, concepts::direction dir) const {
    if (dir == direction::memory_to_peripheral) {
        dma_set_read_from_memory(*instance_, *ch);
    } else {
        dma_set_read_from_peripheral(*instance_, *ch);
    }
    return *this;
}

const dma& dma::set_request_mapping(concepts::channel ch, concepts::request req) const {
    dma_set_channel_request(*instance_, *ch, *req);
    return *this;
}

const dma& dma::set_m2m(concepts::channel ch, bool m2m) const {
    if (m2m) {
        dma_enable_mem2mem_mode(*instance_, *ch);
    } else {
        dma_disable_mem2mem_mode(*instance_, *ch);
    }
    return *this;
}

const dma& dma::set_m2m_source(concepts::channel ch, std::uint32_t address) const {
    dma_set_peripheral_address(*instance_, *ch, address);
    return *this;
}

const dma& dma::set_m2m_destination(concepts::channel ch, std::uint32_t address) const {
    return set_memory_address(ch, address);
}
} // namespace spl::peripherals::dma
