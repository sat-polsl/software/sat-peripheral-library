#pragma once
#include <cstdint>
#include "spl/peripherals/dma/concepts/enums.h"

namespace spl::peripherals::dma {

/**
 * @addtogroup dma_ll
 * @{
 */

/**
 * @brief DMA instance enumeration
 */
enum class instance { dma1, dma2 };

constexpr auto channel1 = concepts::channel(0);
constexpr auto channel2 = concepts::channel(1);
constexpr auto channel3 = concepts::channel(2);
constexpr auto channel4 = concepts::channel(3);
constexpr auto channel5 = concepts::channel(4);
constexpr auto channel6 = concepts::channel(5);
constexpr auto channel7 = concepts::channel(6);

constexpr auto request1 = concepts::request(0);
constexpr auto request2 = concepts::request(1);
constexpr auto request3 = concepts::request(2);
constexpr auto request4 = concepts::request(3);
constexpr auto request5 = concepts::request(4);
constexpr auto request6 = concepts::request(5);
constexpr auto request7 = concepts::request(6);

struct irq {
    static constexpr auto transfer_error = concepts::irq{0};
    static constexpr auto half_transfer = concepts::irq{1};
    static constexpr auto transfer_completed = concepts::irq{2};
};

using concepts::direction;

struct peripheral {
    static constexpr auto uart1_tx = concepts::peripheral{0};
    static constexpr auto uart1_rx = concepts::peripheral{1};
    static constexpr auto uart2_tx = concepts::peripheral{2};
    static constexpr auto uart2_rx = concepts::peripheral{3};
    static constexpr auto uart3_tx = concepts::peripheral{4};
    static constexpr auto uart3_rx = concepts::peripheral{5};
    static constexpr auto crc = concepts::peripheral{6};
    static constexpr auto spi1 = concepts::peripheral{7};
    static constexpr auto spi2 = concepts::peripheral{8};
    static constexpr auto spi3 = concepts::peripheral{9};
    static constexpr auto i2c1_tx = concepts::peripheral{10};
    static constexpr auto i2c1_rx = concepts::peripheral{11};
    static constexpr auto i2c2_tx = concepts::peripheral{12};
    static constexpr auto i2c2_rx = concepts::peripheral{13};
    static constexpr auto i2c3_tx = concepts::peripheral{14};
    static constexpr auto i2c3_rx = concepts::peripheral{15};
};

/** @} */
} // namespace spl::peripherals::dma
