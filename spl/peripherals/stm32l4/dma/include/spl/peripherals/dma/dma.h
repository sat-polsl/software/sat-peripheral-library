#pragma once
#include <bit>
#include "satext/noncopyable.h"
#include "spl/peripherals/dma/concepts/dma.h"
#include "spl/peripherals/dma/concepts/enums.h"
#include "spl/peripherals/dma/enums.h"

namespace spl::peripherals::dma {

/**
 * @addtogroup dma_ll
 * @{
 */

/**
 * @brief DMA base class
 */
class dma : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param dma_instance DMA instance
     */
    explicit constexpr dma(instance dma_instance) : instance_(dma_instance){};

    /**
     * @brief Enable DMA channel
     * @param ch channel to enable
     * @return DMA base
     */
    const dma& enable_channel(concepts::channel ch) const;

    /**
     * @brief Disable DMA channel
     * @param ch channel to disable
     * @return DMA base
     */
    const dma& disable_channel(concepts::channel ch) const;

    /**
     * @brief Reset channel
     * @param ch channel to reset
     * @return DMA base
     */
    const dma& reset_channel(concepts::channel ch) const;

    /**
     * @brief Set peripheral address in P2M/M2P mode of given DMA channel
     * @param ch DMA channel
     * @param address Peripheral address to set
     * @return DMA base
     */
    const dma& set_peripheral_address(concepts::channel ch, concepts::peripheral address) const;

    /**
     * @brief Set memory address in P2M/M2P mode of given DMA channel
     * @param ch DMA channel
     * @param address Memory address to set
     * @return DMA base
     */
    const dma& set_memory_address(concepts::channel ch, std::uint32_t address) const;

    /**
     * @brief Set memory address in P2M/M2P mode of given DMA channel
     * @tparam T pointer type
     * @param ch DMA channel
     * @param address Pointer to memory address to set
     * @return DMA base
     */
    template<typename T>
    const dma& set_memory_address(concepts::channel ch, const T* address) const {
        return set_memory_address(ch, std::bit_cast<std::uint32_t>(address));
    }

    /**
     * @brief Set transfer size of given DMA channel
     * @param ch DMA channel
     * @param size Transfer size to set
     * @return DMA base
     */
    const dma& set_transfer_size(concepts::channel ch, std::uint16_t size) const;

    /**
     * @brief Set memory increment mode
     * @param ch DMA channel
     * @param increment true to enable increment mode, false to disable it
     * @return DMA base
     */
    const dma& set_memory_increment(concepts::channel ch, bool increment) const;

    /**
     * @brief Set peripheral increment mode
     * @param ch DMA channel
     * @param increment true to enable increment mode, false to disable it
     * @return DMA base
     */
    const dma& set_peripheral_increment(concepts::channel ch, bool increment) const;

    /**
     * @brief Enable IRQ
     * @param ch DMA channel
     * @param irq IRQ to enable
     * @return DMA base
     */
    const dma& enable_irq(concepts::channel ch, concepts::irq dma_irq) const;

    /**
     * @brief Disable IRQ
     * @param ch DMA channel
     * @param irq IRQ to disable
     * @return DMA base
     */
    const dma& disable_irq(concepts::channel ch, concepts::irq dma_irq) const;

    /**
     * @brief Clear IRQ
     * @param ch DMA channel
     * @param irq IRQ to clear
     * @return DMA base
     */
    const dma& clear_irq(concepts::channel ch, concepts::irq dma_irq) const;

    /**
     * @brief Returns IRQ status
     * @param ch DMA channel
     * @param irq IRQ to check
     * @return true if IRQ occurred, false otherwise
     */
    bool get_irq_status(concepts::channel ch, concepts::irq dma_irq) const;

    /**
     * @brief Set transfer direction
     * @param ch DMA channel
     * @param dir Transfer direction
     * @return DMA base
     */
    const dma& set_direction(concepts::channel ch, concepts::direction dir) const;

    /**
     * @brief Set request mapping
     * @param ch DMA channel
     * @param request DMA request mapping
     * @return DMA base
     */
    const dma& set_request_mapping(concepts::channel ch, concepts::request req) const;

    /**
     * @brief Set memory-to-memory DMA mode
     * @param ch DMA channel
     * @param m2m true to enable m2m, false to disable
     * @return DMA base
     */
    const dma& set_m2m(concepts::channel ch, bool m2m) const;

    /**
     * @brief Set memory-to-memory transfer source
     * @param ch DMA channel
     * @param address Source memory address
     * @return DMA base
     */
    const dma& set_m2m_source(concepts::channel ch, std::uint32_t address) const;

    /**
     * @brief Set memory-to-memory transfer source
     * @param ch DMA channel
     * @param address Pointer to source memory address
     * @return DMA base
     */
    template<typename T>
    const dma& set_m2m_source(concepts::channel ch, const T* address) const {
        return set_m2m_source(ch, std::bit_cast<std::uint32_t>(address));
    }

    /**
     * @brief Set memory-to-memory transfer destination
     * @tparam T pointer type
     * @param ch DMA channel
     * @param address Destination memory address
     * @return DMA base
     */
    const dma& set_m2m_destination(concepts::channel ch, std::uint32_t address) const;

    /**
     * @brief Set memory-to-memory transfer destination
     * @tparam T pointer type
     * @param ch DMA channel
     * @param address Pointer to destination memory address
     * @return DMA base
     */
    template<typename T>
    const dma& set_m2m_destination(concepts::channel ch, const T* address) const {
        return set_m2m_destination(ch, std::bit_cast<std::uint32_t>(address));
    }

private:
    instance instance_;
};

static_assert(concepts::dma<dma>);
/** @] */
} // namespace spl::peripherals::dma
