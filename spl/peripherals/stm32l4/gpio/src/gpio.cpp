#include "spl/peripherals/gpio/gpio.h"
#include <array>
#include <cstdint>
#include "libopencm3/stm32/gpio.h"
#include "satext/type_traits.h"

namespace spl::peripherals::gpio {

using namespace satext;

consteval auto create_port_lut() {
    std::array<std::uint32_t, 4> lut{};
    lut[to_underlying_type(port::a)] = GPIOA;
    lut[to_underlying_type(port::b)] = GPIOB;
    lut[to_underlying_type(port::c)] = GPIOC;
    lut[to_underlying_type(port::d)] = GPIOD;
    return lut;
}

consteval auto create_pin_lut() {
    std::array<std::uint16_t, 16> lut{};
    lut[to_underlying_type(pin0)] = GPIO0;
    lut[to_underlying_type(pin1)] = GPIO1;
    lut[to_underlying_type(pin2)] = GPIO2;
    lut[to_underlying_type(pin3)] = GPIO3;
    lut[to_underlying_type(pin4)] = GPIO4;
    lut[to_underlying_type(pin5)] = GPIO5;
    lut[to_underlying_type(pin6)] = GPIO6;
    lut[to_underlying_type(pin7)] = GPIO7;
    lut[to_underlying_type(pin8)] = GPIO8;
    lut[to_underlying_type(pin9)] = GPIO9;
    lut[to_underlying_type(pin10)] = GPIO10;
    lut[to_underlying_type(pin11)] = GPIO11;
    lut[to_underlying_type(pin12)] = GPIO12;
    lut[to_underlying_type(pin13)] = GPIO13;
    lut[to_underlying_type(pin14)] = GPIO14;
    lut[to_underlying_type(pin15)] = GPIO15;
    return lut;
}

consteval auto create_mode_lut() {
    std::array<std::uint8_t, 4> lut{};
    lut[to_underlying_type(mode::input)] = GPIO_MODE_INPUT;
    lut[to_underlying_type(mode::output)] = GPIO_MODE_OUTPUT;
    lut[to_underlying_type(mode::analog)] = GPIO_MODE_ANALOG;
    lut[to_underlying_type(mode::af)] = GPIO_MODE_AF;
    return lut;
}

consteval auto create_pupd_lut() {
    std::array<std::uint8_t, 3> lut{};
    lut[to_underlying_type(pupd::none)] = GPIO_PUPD_NONE;
    lut[to_underlying_type(pupd::pulldown)] = GPIO_PUPD_PULLDOWN;
    lut[to_underlying_type(pupd::pullup)] = GPIO_PUPD_PULLUP;
    return lut;
}

consteval auto create_af_lut() {
    std::array<std::uint8_t, 16> lut{};
    lut[to_underlying_type(af0)] = GPIO_AF0;
    lut[to_underlying_type(af1)] = GPIO_AF1;
    lut[to_underlying_type(af2)] = GPIO_AF2;
    lut[to_underlying_type(af3)] = GPIO_AF3;
    lut[to_underlying_type(af4)] = GPIO_AF4;
    lut[to_underlying_type(af5)] = GPIO_AF5;
    lut[to_underlying_type(af6)] = GPIO_AF6;
    lut[to_underlying_type(af7)] = GPIO_AF7;
    lut[to_underlying_type(af8)] = GPIO_AF8;
    lut[to_underlying_type(af9)] = GPIO_AF9;
    lut[to_underlying_type(af10)] = GPIO_AF10;
    lut[to_underlying_type(af11)] = GPIO_AF11;
    lut[to_underlying_type(af12)] = GPIO_AF12;
    lut[to_underlying_type(af13)] = GPIO_AF13;
    lut[to_underlying_type(af14)] = GPIO_AF14;
    lut[to_underlying_type(af15)] = GPIO_AF15;
    return lut;
}

consteval auto create_output_speeed_lut() {
    std::array<std::uint8_t, 4> lut{};
    lut[to_underlying_type(output_speed::low_2mhz)] = GPIO_OSPEED_2MHZ;
    lut[to_underlying_type(output_speed::medium_25mhz)] = GPIO_OSPEED_25MHZ;
    lut[to_underlying_type(output_speed::high_50mhz)] = GPIO_OSPEED_50MHZ;
    lut[to_underlying_type(output_speed::very_high_100mhz)] = GPIO_OSPEED_100MHZ;
    return lut;
}

constexpr auto port_lut = create_port_lut();
constexpr auto pin_lut = create_pin_lut();
constexpr auto mode_lut = create_mode_lut();
constexpr auto pupd_lut = create_pupd_lut();
constexpr auto af_lut = create_af_lut();
constexpr auto output_speed_lut = create_output_speeed_lut();

std::uint32_t operator*(concepts::port gpio_port) {
    return port_lut[to_underlying_type(gpio_port)];
}
std::uint16_t operator*(concepts::pin gpio_pin) { return pin_lut[to_underlying_type(gpio_pin)]; }
std::uint8_t operator*(concepts::mode gpio_mode) { return mode_lut[to_underlying_type(gpio_mode)]; }
std::uint8_t operator*(concepts::pupd gpio_pupd) { return pupd_lut[to_underlying_type(gpio_pupd)]; }
std::uint8_t operator*(concepts::af gpio_af) { return af_lut[to_underlying_type(gpio_af)]; }
std::uint8_t operator*(concepts::output_speed gpio_speed) {
    return output_speed_lut[to_underlying_type(gpio_speed)];
}

void gpio::setup(concepts::mode gpio_mode, concepts::pupd gpio_pupd, concepts::af gpio_af) const {
    gpio_mode_setup(*port_, *gpio_mode, *gpio_pupd, *pin_);
    if (gpio_mode == mode::af) {
        gpio_set_af(*port_, *gpio_af, *pin_);
    }
}

void gpio::clear() const { gpio_clear(*port_, *pin_); }

void gpio::set() const { gpio_set(*port_, *pin_); }

void gpio::toggle() const { gpio_toggle(*port_, *pin_); }

bool gpio::read() const { return (gpio_port_read(*port_) & *pin_) != 0; }

void gpio::set_output_type(concepts::output_type type) const {
    // TODO: implement this function in Libopencm3
    // https://gitlab.com/sat-polsl/software/libopencm3/-/issues/4
    auto reg = GPIO_OTYPER(*port_);
    if (type == output_type::open_drain) {
        reg |= *pin_;
    } else {
        reg &= ~*pin_;
    }
    GPIO_OTYPER(*port_) = reg;
}

void gpio::set_output_speed(concepts::output_speed speed) const {
    // TODO: implement this function in Libopencm3
    // https://gitlab.com/sat-polsl/software/libopencm3/-/issues/4
    auto reg = GPIO_OSPEEDR(*port_);
    reg &= ~GPIO_OSPEED_MASK(*pin_);
    reg |= GPIO_OSPEED(*pin_, *speed);
    GPIO_OSPEEDR(*port_) = reg;
}

} // namespace spl::peripherals::gpio
