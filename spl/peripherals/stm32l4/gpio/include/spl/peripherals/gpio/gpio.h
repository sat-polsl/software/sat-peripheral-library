#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/gpio/concepts/enums.h"
#include "spl/peripherals/gpio/concepts/gpio.h"
#include "spl/peripherals/gpio/enums.h"

namespace spl::peripherals::gpio {

/**
 * @addtogroup gpio_ll
 * @{
 */

/**
 * @brief GPIO driver
 */
class gpio : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param gpio_port GPIO port
     * @param gpio_pin GPIO pin
     */
    constexpr gpio(concepts::port gpio_port, concepts::pin gpio_pin) :
        port_(gpio_port),
        pin_(gpio_pin) {}

    /**
     * @brief Setup gpio
     * @param gpio_mode GPIO Mode
     * @param gpio_pupd GPIO PullUp/PullDown
     * @param gpio_af GPIO Alternate functions
     */
    void
    setup(concepts::mode gpio_mode, concepts::pupd gpio_pupd, concepts::af gpio_af = af0) const;

    /**
     * @brief Set GPIO output type
     * @param type output type
     */
    void set_output_type(concepts::output_type type) const;

    /**
     * @brief Set GPIO output speed
     * @param speed output speed
     */
    void set_output_speed(concepts::output_speed speed) const;

    /**
     * @brief Set GPIO
     */
    void set() const;

    /**
     * @brief Clear GPIO
     */
    void clear() const;

    /**
     * @brief Toggle GPIO
     */
    void toggle() const;

    /**
     * @brief Read GPIO value
     * @return true if set, false if cleared
     */
    bool read() const;

private:
    concepts::port port_;
    concepts::pin pin_;
};

static_assert(concepts::gpio<gpio>);

/** @} */
} // namespace spl::peripherals::gpio
