#pragma once
#include <cstdint>
#include "spl/peripherals/gpio/concepts/enums.h"

namespace spl::peripherals::gpio {

/**
 * @addtogroup gpio_ll
 * @{
 */

struct port {
    static constexpr auto a = concepts::port(0);
    static constexpr auto b = concepts::port(1);
    static constexpr auto c = concepts::port(2);
    static constexpr auto d = concepts::port(3);
};

constexpr auto pin0 = concepts::pin(0);
constexpr auto pin1 = concepts::pin(1);
constexpr auto pin2 = concepts::pin(2);
constexpr auto pin3 = concepts::pin(3);
constexpr auto pin4 = concepts::pin(4);
constexpr auto pin5 = concepts::pin(5);
constexpr auto pin6 = concepts::pin(6);
constexpr auto pin7 = concepts::pin(7);
constexpr auto pin8 = concepts::pin(8);
constexpr auto pin9 = concepts::pin(9);
constexpr auto pin10 = concepts::pin(10);
constexpr auto pin11 = concepts::pin(11);
constexpr auto pin12 = concepts::pin(12);
constexpr auto pin13 = concepts::pin(13);
constexpr auto pin14 = concepts::pin(14);
constexpr auto pin15 = concepts::pin(15);

constexpr auto af0 = concepts::af(0);
constexpr auto af1 = concepts::af(1);
constexpr auto af2 = concepts::af(2);
constexpr auto af3 = concepts::af(3);
constexpr auto af4 = concepts::af(4);
constexpr auto af5 = concepts::af(5);
constexpr auto af6 = concepts::af(6);
constexpr auto af7 = concepts::af(7);
constexpr auto af8 = concepts::af(8);
constexpr auto af9 = concepts::af(9);
constexpr auto af10 = concepts::af(10);
constexpr auto af11 = concepts::af(11);
constexpr auto af12 = concepts::af(12);
constexpr auto af13 = concepts::af(13);
constexpr auto af14 = concepts::af(14);
constexpr auto af15 = concepts::af(15);

using concepts::mode;
using concepts::output_type;
using concepts::pupd;

struct output_speed {
    static constexpr auto low_2mhz = concepts::output_speed{0};
    static constexpr auto medium_25mhz = concepts::output_speed{1};
    static constexpr auto high_50mhz = concepts::output_speed{2};
    static constexpr auto very_high_100mhz = concepts::output_speed{3};
};

/** @} */

} // namespace spl::peripherals::gpio
