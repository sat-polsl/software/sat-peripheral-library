#include "spl/peripherals/rcc/rcc.h"
#include <array>
#include "libopencm3/stm32/rcc.h"
#include "satext/type_traits.h"

namespace spl::peripherals::rcc {

using namespace satext;

consteval auto create_clock_lut() {
    std::array<rcc_periph_clken, 18> lut{};
    lut[to_underlying_type(peripheral::gpioa)] = RCC_GPIOA;
    lut[to_underlying_type(peripheral::gpiob)] = RCC_GPIOB;
    lut[to_underlying_type(peripheral::gpioc)] = RCC_GPIOC;
    lut[to_underlying_type(peripheral::gpiod)] = RCC_GPIOD;
    lut[to_underlying_type(peripheral::uart1)] = RCC_USART1;
    lut[to_underlying_type(peripheral::uart2)] = RCC_USART2;
    lut[to_underlying_type(peripheral::uart3)] = RCC_USART3;
    lut[to_underlying_type(peripheral::dma1)] = RCC_DMA1;
    lut[to_underlying_type(peripheral::dma2)] = RCC_DMA2;
    lut[to_underlying_type(peripheral::spi1)] = RCC_SPI1;
    lut[to_underlying_type(peripheral::spi2)] = RCC_SPI2;
    lut[to_underlying_type(peripheral::spi3)] = RCC_SPI3;
    lut[to_underlying_type(peripheral::can1)] = RCC_CAN1;
    lut[to_underlying_type(peripheral::can2)] = RCC_CAN2;
    lut[to_underlying_type(peripheral::i2c1)] = RCC_I2C1;
    lut[to_underlying_type(peripheral::i2c2)] = RCC_I2C2;
    lut[to_underlying_type(peripheral::i2c3)] = RCC_I2C3;
    lut[to_underlying_type(peripheral::crc)] = RCC_CRC;
    return lut;
}

consteval auto create_reset_lut() {
    std::array<rcc_periph_rst, 18> lut{};
    lut[to_underlying_type(peripheral::gpioa)] = RST_GPIOA;
    lut[to_underlying_type(peripheral::gpiob)] = RST_GPIOB;
    lut[to_underlying_type(peripheral::gpioc)] = RST_GPIOC;
    lut[to_underlying_type(peripheral::gpiod)] = RST_GPIOD;
    lut[to_underlying_type(peripheral::uart1)] = RST_USART1;
    lut[to_underlying_type(peripheral::uart2)] = RST_USART2;
    lut[to_underlying_type(peripheral::uart3)] = RST_USART3;
    lut[to_underlying_type(peripheral::dma1)] = RST_DMA1;
    lut[to_underlying_type(peripheral::dma2)] = RST_DMA2;
    lut[to_underlying_type(peripheral::spi1)] = RST_SPI1;
    lut[to_underlying_type(peripheral::spi2)] = RST_SPI2;
    lut[to_underlying_type(peripheral::spi3)] = RST_SPI3;
    lut[to_underlying_type(peripheral::can1)] = RST_CAN1;
    lut[to_underlying_type(peripheral::can2)] = RST_CAN2;
    lut[to_underlying_type(peripheral::i2c1)] = RST_I2C1;
    lut[to_underlying_type(peripheral::i2c2)] = RST_I2C2;
    lut[to_underlying_type(peripheral::i2c3)] = RST_I2C3;
    lut[to_underlying_type(peripheral::crc)] = RST_CRC;
    return lut;
}

consteval auto create_clock_source_lut() {
    std::array<std::int32_t, 4> lut{};
    lut[to_underlying_type(clock_source::hse)] = RCC_CFGR_SW_HSE;
    lut[to_underlying_type(clock_source::hsi)] = RCC_CFGR_SW_HSI16;
    lut[to_underlying_type(clock_source::msi)] = RCC_CFGR_SW_MSI;
    lut[to_underlying_type(clock_source::pll)] = RCC_CFGR_SW_PLL;
    return lut;
}

consteval auto create_pll_clock_source_lut() {
    std::array<std::int32_t, 3> lut{};
    lut[to_underlying_type(pll_clock_source::hse)] = RCC_PLLCFGR_PLLSRC_HSE;
    lut[to_underlying_type(pll_clock_source::hsi)] = RCC_PLLCFGR_PLLSRC_HSI16;
    lut[to_underlying_type(pll_clock_source::msi)] = RCC_PLLCFGR_PLLSRC_MSI;
    return lut;
}

consteval auto create_pll_output_lut() {
    std::array<std::int32_t, 2> lut{};
    lut[to_underlying_type(pll_output::pll48m1clk)] = RCC_PLLCFGR_PLLQEN;
    lut[to_underlying_type(pll_output::pllclk)] = RCC_PLLCFGR_PLLREN;
    return lut;
}

consteval auto create_oscillator_lut() {
    std::array<rcc_osc, 4> lut{};
    lut[to_underlying_type(oscillator::hse)] = RCC_HSE;
    lut[to_underlying_type(oscillator::hsi)] = RCC_HSI16;
    lut[to_underlying_type(oscillator::msi)] = RCC_MSI;
    lut[to_underlying_type(oscillator::pll)] = RCC_PLL;
    return lut;
}

constexpr auto clock_lut = create_clock_lut();
constexpr auto reset_lut = create_reset_lut();
constexpr auto clock_source_lut = create_clock_source_lut();
constexpr auto pll_source_lut = create_pll_clock_source_lut();
constexpr auto pll_output_lut = create_pll_output_lut();
constexpr auto oscillator_lut = create_oscillator_lut();

const rcc& rcc::enable_clock(concepts::peripheral clk) const {
    rcc_periph_clock_enable(clock_lut[to_underlying_type(clk)]);
    return *this;
}

const rcc& rcc::disable_clock(concepts::peripheral clk) const {
    rcc_periph_clock_disable(clock_lut[to_underlying_type(clk)]);
    return *this;
}

const rcc& rcc::pulse_reset(concepts::peripheral reset) const {
    rcc_periph_reset_pulse(reset_lut[to_underlying_type(reset)]);
    return *this;
}

const rcc& rcc::set_sysclk_source(concepts::clock_source source) const {
    rcc_set_sysclk_source(clock_source_lut[to_underlying_type(source)]);
    return *this;
}

concepts::clock_source rcc::get_sysclk_source() const {
    auto res = rcc_system_clock_source();
    switch (res) {
    case RCC_CFGR_SW_HSE:
        return clock_source::hse;
    case RCC_CFGR_SW_MSI:
        return clock_source::msi;
    case RCC_CFGR_SW_HSI16:
        return clock_source::hsi;
    case RCC_CFGR_SW_PLL:
        return clock_source::pll;
    default:
        return clock_source::msi;
    }
}

const rcc& rcc::enable_oscillator(concepts::oscillator osc) const {
    rcc_osc_on(oscillator_lut[to_underlying_type(osc)]);
    return *this;
}
const rcc& rcc::disable_oscillator(concepts::oscillator osc) const {
    rcc_osc_off(oscillator_lut[to_underlying_type(osc)]);
    return *this;
}

bool rcc::is_oscillator_ready(concepts::oscillator osc) const {
    return rcc_is_osc_ready(oscillator_lut[to_underlying_type(osc)]);
}

const rcc& rcc::set_pll_source(concepts::pll_clock_source source) const {
    rcc_set_pll_source(pll_source_lut[to_underlying_type(source)]);
    return *this;
}

const rcc& rcc::set_pll_clock_prescaler(concepts::pll_prescaler prescaler) const {
    rcc_set_pllr(to_underlying_type(prescaler));
    return *this;
}

const rcc& rcc::set_pll_vco_multiplier(std::uint8_t multiplier) const {
    if (multiplier >= min_vco_multiplier && multiplier <= max_vco_multiplier) {
        rcc_set_plln(multiplier);
    } else if (multiplier < min_vco_multiplier) {
        rcc_set_plln(min_vco_multiplier);
    } else {
        rcc_set_plln(max_vco_multiplier);
    }
    return *this;
}

const rcc& rcc::set_pll_input_clock_prescaler(concepts::pll_input_prescaler prescaler) const {
    rcc_set_pllm(to_underlying_type(prescaler));
    return *this;
}

const rcc& rcc::set_ahb_frequency(std::uint32_t frequency) const {
    rcc_set_ahb_frequency(frequency);
    return *this;
}

const rcc& rcc::set_apb1_frequency(std::uint32_t frequency) const {
    rcc_set_apb1_frequency(frequency);
    return *this;
}

const rcc& rcc::set_apb2_frequency(std::uint32_t frequency) const {
    rcc_set_apb2_frequency(frequency);
    return *this;
}

std::uint32_t rcc::get_ahb_frequency() const { return rcc_get_ahb_frequency(); }

std::uint32_t rcc::get_apb1_frequency() const { return rcc_get_apb1_frequency(); }

std::uint32_t rcc::get_apb2_frequency() const { return rcc_get_apb2_frequency(); }

const rcc& rcc::enable_pll_output(concepts::pll_output output) const {
    rcc_pll_output_enable(pll_output_lut[to_underlying_type(output)]);
    return *this;
}

const rcc& rcc::disable_pll_output(concepts::pll_output output) const {
    rcc_pll_output_disable(pll_output_lut[to_underlying_type(output)]);
    return *this;
}

} // namespace spl::peripherals::rcc
