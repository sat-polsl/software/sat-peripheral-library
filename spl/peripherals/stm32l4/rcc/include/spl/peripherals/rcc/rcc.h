#pragma once
#include <cstdint>
#include "satext/noncopyable.h"
#include "spl/peripherals/rcc/concepts/rcc.h"
#include "spl/peripherals/rcc/enums.h"

namespace spl::peripherals::rcc {

/**
 * @addtogroup rcc_ll
 * @{
 */

/**
 * @brief RCC low level driver
 */
struct rcc : private satext::noncopyable {
    /**
     * @brief Enable peripheral clock
     * @param clk Peripheral clock
     * @return RCC object
     */
    const rcc& enable_clock(concepts::peripheral clk) const;

    /**
     * @brief Disable peripheral clock
     * @param clk Peripheral clock
     * @return RCC object
     */
    const rcc& disable_clock(concepts::peripheral clk) const;

    /**
     * @brief Resets peripheral
     * @param reset Peripheral to reset
     * @return RCC object
     */
    const rcc& pulse_reset(concepts::peripheral reset) const;

    /**
     * @brief Set clock source of the system clock
     * @param source Clock source
     * @return RCC object
     */
    const rcc& set_sysclk_source(concepts::clock_source source) const;

    /**
     * @brief Set clock source for the system clock
     * @return clock_source enumeration
     */
    concepts::clock_source get_sysclk_source() const;

    /**
     * @brief Enables oscillator
     * @param osc oscillator
     * @return RCC object
     */
    const rcc& enable_oscillator(concepts::oscillator osc) const;

    /**
     * @brief Disables oscillator
     * @param osc oscillator
     * @return RCC object
     */
    const rcc& disable_oscillator(concepts::oscillator osc) const;

    /**
     * @brief Checks whether osciallator is ready
     * @param osc oscillator
     * @return true if ready, false otherwise
     */
    bool is_oscillator_ready(concepts::oscillator osc) const;

    /**
     * @brief Set prescaler for PLL
     * @param source Clock source
     * @return RCC object
     */
    const rcc& set_pll_source(concepts::pll_clock_source source) const;

    /**
     * @brief Set multiplier for input clock in PLL (PLLN)
     * @param multiplier Desired multiplier (between 8 and 86)
     * @return RCC object
     */
    const rcc& set_pll_vco_multiplier(std::uint8_t multiplier) const;

    /**
     * @brief Set prescaler for PLL clock (PLLR)
     * @param prescaler Desired prescaler (2, 4, 6, 8)
     * @return RCC object
     */
    const rcc& set_pll_clock_prescaler(concepts::pll_prescaler prescaler) const;

    /**
     * @brief Set prescaler for input clock for PLL (PLLM)
     * @param prescaler Desired prescaler (1-8)
     * @return RCC object
     */
    const rcc& set_pll_input_clock_prescaler(concepts::pll_input_prescaler prescaler) const;

    /**
     * @brief Set frequency for AHB bus
     * @param frequency Desired AHB frequency
     */
    const rcc& set_ahb_frequency(std::uint32_t frequency) const;

    /**
     * @brief Set frequency for AHB bus
     * @return AHB frequency
     */
    std::uint32_t get_ahb_frequency() const;

    /**
     * @brief Set frequency for APB1 bus
     * @param frequency Desired APB1 frequency
     */
    const rcc& set_apb1_frequency(std::uint32_t frequency) const;

    /**
     * @brief Set frequency for APB1 bus
     * @return APB1 frequency
     */
    std::uint32_t get_apb1_frequency() const;

    /**
     * @brief Set frequency for APB2 bus
     * @param frequency Desired APB2 frequency
     */
    const rcc& set_apb2_frequency(std::uint32_t frequency) const;

    /**
     * @brief Set frequency for APB2 bus
     * @return APB2 frequency
     */
    std::uint32_t get_apb2_frequency() const;

    /**
     * @brief Enable specified PLL output
     * @param output desired PLL output
     */
    const rcc& enable_pll_output(concepts::pll_output output) const;

    /**
     * @brief Disable specified PLL output
     * @param output desired PLL output
     */
    const rcc& disable_pll_output(concepts::pll_output output) const;

private:
    static constexpr uint8_t min_vco_multiplier = 8;
    static constexpr uint8_t max_vco_multiplier = 86;
};

static_assert(concepts::rcc<rcc>);

/** @} */
} // namespace spl::peripherals::rcc
