#pragma once
#include <cstdint>
#include "spl/peripherals/rcc/concepts/enums.h"

namespace spl::peripherals::rcc {

/**
 * @addtogroup rcc_ll
 * @{
 */

struct peripheral {
    static constexpr auto gpioa = spl::peripherals::rcc::concepts::peripheral{0};
    static constexpr auto gpiob = spl::peripherals::rcc::concepts::peripheral{1};
    static constexpr auto gpioc = spl::peripherals::rcc::concepts::peripheral{2};
    static constexpr auto gpiod = spl::peripherals::rcc::concepts::peripheral{3};
    static constexpr auto uart1 = spl::peripherals::rcc::concepts::peripheral{4};
    static constexpr auto uart2 = spl::peripherals::rcc::concepts::peripheral{5};
    static constexpr auto uart3 = spl::peripherals::rcc::concepts::peripheral{6};
    static constexpr auto dma1 = spl::peripherals::rcc::concepts::peripheral{7};
    static constexpr auto dma2 = spl::peripherals::rcc::concepts::peripheral{8};
    static constexpr auto spi1 = spl::peripherals::rcc::concepts::peripheral{9};
    static constexpr auto spi2 = spl::peripherals::rcc::concepts::peripheral{10};
    static constexpr auto spi3 = spl::peripherals::rcc::concepts::peripheral{11};
    static constexpr auto i2c1 = spl::peripherals::rcc::concepts::peripheral{12};
    static constexpr auto i2c2 = spl::peripherals::rcc::concepts::peripheral{13};
    static constexpr auto i2c3 = spl::peripherals::rcc::concepts::peripheral{14};
    static constexpr auto can1 = spl::peripherals::rcc::concepts::peripheral{15};
    static constexpr auto can2 = spl::peripherals::rcc::concepts::peripheral{16};
    static constexpr auto crc = spl::peripherals::rcc::concepts::peripheral{17};
};

using concepts::clock_source;
using concepts::oscillator;
using concepts::pll_clock_source;
using concepts::pll_input_prescaler;
using concepts::pll_output;
using concepts::pll_prescaler;

/** @} */
} // namespace spl::peripherals::rcc
