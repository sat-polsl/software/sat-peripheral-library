#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/crc/concepts/crc.h"

namespace spl::peripherals::crc {

/**
 * @addtogroup crc_ll
 * @{
 */

/**
 * @brief CRC low level driver class
 */
class crc : private satext::noncopyable {
public:
    /**
     * @brief Reset CRC calculator
     * @return CRC object
     */
    const crc& reset() const;

    /**
     * @brief Write word
     * @return std::uint32_t CRC result
     */
    std::uint32_t feed(std::byte data) const;

    /**
     * @brief Add block and return result
     * @return std::uint32_t CRC calculator value
     */
    std::uint32_t feed(std::span<const std::byte> buffer) const;

    /**
     * @brief Enables/disables reverse input
     * @param set true to enable, false to disable reverse input
     * @return CRC object
     */
    const crc& set_reverse_input(bool set) const;

    /**
     * @brief Enables/disables reverse output
     * @param set true to enable, false to disable reverse output
     * @return CRC object
     */
    const crc& set_reverse_output(bool set) const;

    /**
     * @brief Sets final xor value
     * @param value value to set
     * @return CRC object
     */
    const crc& set_final_xor_value(std::uint32_t value);

private:
    std::uint32_t final_xor_value_{};
};

static_assert(concepts::crc<crc>);
/** @} */
} // namespace spl::peripherals::crc
