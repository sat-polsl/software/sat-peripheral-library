#include "spl/peripherals/crc/crc.h"
#include "libopencm3/stm32/crc.h"

namespace spl::peripherals::crc {

using namespace satext;

const crc& crc::reset() const {
    crc_reset();
    return *this;
}

std::uint32_t crc::feed(std::byte data) const {
    return crc_feed(static_cast<std::uint8_t>(data)) ^ final_xor_value_;
}

std::uint32_t crc::feed(std::span<const std::byte> buffer) const {
    return crc_feed_block(reinterpret_cast<std::uint8_t*>(const_cast<std::byte*>(buffer.data())),
                          buffer.size()) ^
           final_xor_value_;
}

const crc& crc::set_reverse_input(bool set) const {
    crc_set_reverse_input(set ? CRC_CR_REV_IN_WORD : CRC_CR_REV_IN_NONE);
    return *this;
}

const crc& crc::set_reverse_output(bool set) const {
    if (set) {
        crc_reverse_output_enable();
    } else {
        crc_reverse_output_disable();
    }
    return *this;
}

const crc& crc::set_final_xor_value(std::uint32_t value) {
    final_xor_value_ = value;
    return *this;
}

} // namespace spl::peripherals::crc
