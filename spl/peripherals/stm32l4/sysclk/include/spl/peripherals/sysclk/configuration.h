#pragma once
#include "spl/peripherals/rcc/concepts/enums.h"

namespace spl::peripherals::sysclk {
/**
 * @addtogroup sysclk
 * @{
 */

/**
 * @brief Clock configuration
 */
struct configuration {
    /**
     * @brief sysclk source
     */
    spl::peripherals::rcc::concepts::clock_source sysclk_src{};

    /**
     * @brief sysclk bus frequency
     */
    std::uint32_t sysclk_hz{};

    /**
     * @brief PLL source
     */
    spl::peripherals::rcc::concepts::pll_clock_source pll_clock_src{};

    /**
     * @brief PLL input clock frequency
     */
    std::uint32_t pll_input_clock_hz{};

    /**
     * @brief PLL input prescaler. f_VCOin = f_pll_clock_src / pll_input_prescaler
     */
    spl::peripherals::rcc::concepts::pll_input_prescaler pll_input_prescaler{};

    /**
     * @brief VCO multiplier. f_VCOout = f_VCO_in * pll_vco_multiplier
     */
    std::uint32_t pll_vco_multiplier{};

    /**
     * @brief PLL output prescaler. f_PLL = f_VCO_out / pll_output_prescaler
     */
    spl::peripherals::rcc::concepts::pll_prescaler pll_output_prescaler{};
};

/** @} */
} // namespace spl::peripherals::sysclk
