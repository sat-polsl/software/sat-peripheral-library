#pragma once
#include "satext/type_traits.h"
#include "satext/units.h"
#include "spl/peripherals/sysclk/configuration.h"

namespace spl::peripherals::sysclk::detail {

constexpr bool verify_configuration(spl::peripherals::sysclk::configuration cfg) {
    if (cfg.sysclk_hz > 80_MHz) {
        return false;
    }

    if (cfg.sysclk_src == spl::peripherals::rcc::concepts::clock_source::pll) {
        std::uint32_t input_prescaler = satext::to_underlying_type(cfg.pll_input_prescaler) + 1;
        std::uint32_t output_prescaler =
            satext::to_underlying_type(cfg.pll_output_prescaler) * 2 + 2;

        std::uint32_t pll_hz =
            cfg.pll_input_clock_hz / input_prescaler * cfg.pll_vco_multiplier / output_prescaler;

        if (cfg.sysclk_hz != pll_hz) {
            return false;
        }
    }

    return true;
}

} // namespace spl::peripherals::sysclk::detail
