#pragma once
#include "satext/units.h"
#include "spl/peripherals/flash/concepts/flash.h"
#include "spl/peripherals/flash/enums.h"
#include "spl/peripherals/rcc/concepts/rcc.h"
#include "spl/peripherals/rcc/enums.h"
#include "spl/peripherals/sysclk/configuration.h"
#include "spl/peripherals/sysclk/detail/verify_configuration.h"

namespace spl::peripherals::sysclk {
/**
 * @addtogroup sysclk
 * @{
 */

/**
 * @brief Class providing compiletime checked sysclk configuration.
 * @tparam RCC RCC peripheral.
 * @tparam Flash Flash peripheral.
 */
template<spl::peripherals::rcc::concepts::rcc RCC, spl::peripherals::flash::concepts::flash Flash>
class sysclk {
public:
    /**
     * @brief Constructror.
     * @param rcc RCC peripheral reference.
     * @param flash Flash peripheral reference.
     */
    sysclk(RCC& rcc, Flash& flash) : rcc_{rcc}, flash_{flash} {}

    /**
     * @brief Setups clock with given configuration
     * @tparam cfg clocks configuration
     */
    template<spl::peripherals::sysclk::configuration cfg>
    void setup() const {
        static_assert(detail::verify_configuration(cfg), "sysclk configuration error!");
        setup_wait_states<cfg.sysclk_hz>();

        if constexpr (cfg.sysclk_src == spl::peripherals::rcc::clock_source::pll) {
            setup_pll<cfg>();
        }

        rcc_.set_ahb_frequency(cfg.sysclk_hz);
        rcc_.set_apb1_frequency(cfg.sysclk_hz);
        rcc_.set_apb2_frequency(cfg.sysclk_hz);

        rcc_.set_sysclk_source(cfg.sysclk_src);
    }

private:
    template<spl::peripherals::sysclk::configuration cfg>
    void setup_pll() const {
        if constexpr (cfg.pll_clock_src == spl::peripherals::rcc::pll_clock_source::hsi) {
            rcc_.enable_oscillator(spl::peripherals::rcc::oscillator::hsi);
            while (!rcc_.is_oscillator_ready(spl::peripherals::rcc::oscillator::hsi)) {}
            rcc_.set_pll_source(spl::peripherals::rcc::pll_clock_source::hsi);
        } else if constexpr (cfg.pll_clock_src == spl::peripherals::rcc::pll_clock_source::hse) {
            rcc_.enable_oscillator(spl::peripherals::rcc::oscillator::hse);
            while (!rcc_.is_oscillator_ready(spl::peripherals::rcc::oscillator::hse)) {}
            rcc_.set_pll_source(spl::peripherals::rcc::pll_clock_source::hse);
        }

        rcc_.set_pll_input_clock_prescaler(cfg.pll_input_prescaler);
        rcc_.set_pll_vco_multiplier(cfg.pll_vco_multiplier);
        rcc_.set_pll_clock_prescaler(cfg.pll_output_prescaler);
        rcc_.enable_oscillator(spl::peripherals::rcc::oscillator::pll);

        while (!rcc_.is_oscillator_ready(spl::peripherals::rcc::oscillator::pll)) {}

        rcc_.enable_pll_output(spl::peripherals::rcc::pll_output::pllclk);
    }

    template<std::uint32_t sysclk_hz>
    void setup_wait_states() const {
        if constexpr (sysclk_hz <= 16_MHz) {
            flash_.set_wait_states(spl::peripherals::flash::ws0);
        } else if constexpr (sysclk_hz <= 32_MHz) {
            flash_.set_wait_states(spl::peripherals::flash::ws1);
        } else if constexpr (sysclk_hz <= 48_MHz) {
            flash_.set_wait_states(spl::peripherals::flash::ws2);
        } else if constexpr (sysclk_hz <= 64_MHz) {
            flash_.set_wait_states(spl::peripherals::flash::ws3);
        } else if constexpr (sysclk_hz <= 80_MHz) {
            flash_.set_wait_states(spl::peripherals::flash::ws4);
        }
    }

    RCC& rcc_;
    Flash& flash_;
};

/** @} */
} // namespace spl::peripherals::sysclk
