#pragma once
#include "gmock/gmock.h"
#include "satext/expected.h"
#include "spl/drivers/dma/concepts/dma.h"

namespace spl::drivers::dma::mock {
namespace detail {
struct dma_channel {
    MOCK_METHOD(void, enable, (), (const));
    MOCK_METHOD(void, disable, (), (const));
    MOCK_METHOD(void,
                set_peripheral_address,
                (spl::peripherals::dma::concepts::peripheral),
                (const));
    MOCK_METHOD(void, set_memory_address, (std::uint32_t), (const));

    template<typename T>
    void set_memory_address(const T* ptr) const {
        return set_memory_address(std::bit_cast<std::uint32_t>(ptr));
    }

    MOCK_METHOD(void, set_transfer_size, (std::uint16_t), (const));
    MOCK_METHOD(void, set_memory_increment, (bool), (const));
    MOCK_METHOD(void, set_peripheral_increment, (bool), (const));
    MOCK_METHOD(void, set_direction, (spl::peripherals::dma::concepts::direction), (const));
    MOCK_METHOD(void, set_request_mapping, (spl::peripherals::dma::concepts::request), (const));
    MOCK_METHOD(void, set_m2m, (bool), (const));
    MOCK_METHOD(void, set_m2m_source, (std::uint32_t), (const));

    template<typename T>
    void set_m2m_source(const T* ptr) const {
        return set_m2m_source(std::bit_cast<std::uint32_t>(ptr));
    }

    MOCK_METHOD(void, set_m2m_destination, (std::uint32_t), (const));

    template<typename T>
    void set_m2m_destination(const T* ptr) const {
        return set_m2m_destination(std::bit_cast<std::uint32_t>(ptr));
    }

    MOCK_METHOD(void, enable_irq, (spl::peripherals::dma::concepts::irq), (const));
    MOCK_METHOD(void, disable_irq, (spl::peripherals::dma::concepts::irq), (const));

    MOCK_METHOD(spl::drivers::dma::status, transfer, (satos::clock::duration), (const));

    spl::drivers::dma::status transfer() const { return transfer(satos::clock::duration::max()); }

    MOCK_METHOD(spl::drivers::dma::status, transfer, (satos::clock::time_point), (const));

    MOCK_METHOD(spl::drivers::dma::status, wait_for_completion, (satos::clock::duration), (const));

    spl::drivers::dma::status wait_for_completion() const {
        return wait_for_completion(satos::clock::duration::max());
    }

    MOCK_METHOD(spl::drivers::dma::status,
                wait_for_completion,
                (satos::clock::time_point),
                (const));
};
} // namespace detail

struct dma_channel {
    dma_channel(spl::drivers::dma::mock::detail::dma_channel* dma_channel_mock) {
        mock = dma_channel_mock;
    }

    const dma_channel& enable() const {
        mock->enable();
        return *this;
    }

    const dma_channel& disable() const {
        mock->disable();
        return *this;
    }

    const dma_channel&
    set_peripheral_address(spl::peripherals::dma::concepts::peripheral peripheral) const {
        mock->set_peripheral_address(peripheral);
        return *this;
    }

    const dma_channel& set_memory_address(std::uint32_t address) const {
        mock->set_memory_address(address);
        return *this;
    }

    template<typename T>
    const dma_channel& set_memory_address(const T* address) const {
        mock->set_memory_address(address);
        return *this;
    }

    const dma_channel& set_transfer_size(std::uint16_t size) const {
        mock->set_transfer_size(size);
        return *this;
    }

    const dma_channel& set_memory_increment(bool increment) const {
        mock->set_memory_increment(increment);
        return *this;
    }

    const dma_channel& set_peripheral_increment(bool increment) const {
        mock->set_peripheral_increment(increment);
        return *this;
    }

    const dma_channel& set_direction(spl::peripherals::dma::concepts::direction direction) const {
        mock->set_direction(direction);
        return *this;
    }

    const dma_channel& set_request_mapping(spl::peripherals::dma::concepts::request request) const {
        mock->set_request_mapping(request);
        return *this;
    }

    const dma_channel& set_m2m(bool m2m) const {
        mock->set_m2m(m2m);
        return *this;
    }

    const dma_channel& set_m2m_source(std::uint32_t address) const {
        mock->set_m2m_source(address);
        return *this;
    }

    template<typename T>
    const dma_channel& set_m2m_source(const T* address) const {
        mock->set_m2m_source(address);
        return *this;
    }

    const dma_channel& set_m2m_destination(std::uint32_t address) const {
        mock->set_m2m_destination(address);
        return *this;
    }

    template<typename T>
    const dma_channel& set_m2m_destination(const T* address) const {
        mock->set_m2m_destination(address);
        return *this;
    }

    const dma_channel& enable_irq(spl::peripherals::dma::concepts::irq irq) const {
        mock->enable_irq(irq);
        return *this;
    }

    const dma_channel& disable_irq(spl::peripherals::dma::concepts::irq irq) const {
        mock->disable_irq(irq);
        return *this;
    }

    spl::drivers::dma::status
    transfer(satos::clock::duration timeout = satos::clock::duration::max()) {
        return mock->transfer(timeout);
    }

    spl::drivers::dma::status transfer(satos::clock::time_point time_point) {
        return mock->transfer(time_point);
    }

    spl::drivers::dma::status
    wait_for_completion(satos::clock::duration timeout = satos::clock::duration::max()) {
        return mock->wait_for_completion(timeout);
    }

    spl::drivers::dma::status wait_for_completion(satos::clock::time_point time_point) {
        return mock->wait_for_completion(time_point);
    }

    spl::drivers::dma::mock::detail::dma_channel* mock{};
};

static_assert(spl::drivers::dma::concepts::dma_channel<dma_channel>);

struct dma {
    using channel = dma_channel;
    using result = satext::expected<dma_channel, spl::drivers::dma::status>;
    MOCK_METHOD(result,
                request_mock,
                (spl::peripherals::dma::concepts::channel, satos::clock::duration));

    result request(spl::peripherals::dma::concepts::channel channel,
                   satos::clock::duration timeout = satos::clock::duration::max()) {
        return request_mock(channel, timeout);
    }

    MOCK_METHOD(result,
                request,
                (spl::peripherals::dma::concepts::channel, satos::clock::time_point));
};

static_assert(spl::drivers::dma::concepts::dma<dma>);

} // namespace spl::drivers::dma::mock
