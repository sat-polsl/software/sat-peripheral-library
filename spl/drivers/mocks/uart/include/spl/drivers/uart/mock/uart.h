#pragma once
#include "gmock/gmock.h"
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/uart/concepts/uart.h"

namespace spl::drivers::uart::mock {
struct uart {
    using read_result = satext::expected<std::span<const std::byte>, spl::drivers::uart::status>;
    MOCK_METHOD(read_result, read_mock, (std::span<std::byte>, satos::clock::duration));
    MOCK_METHOD(read_result,
                read_until_mock,
                (std::span<std::byte>, std::byte, satos::clock::duration));
    MOCK_METHOD(spl::drivers::uart::status,
                write_mock,
                (std::span<const std::byte>, satos::clock::duration));

    read_result read(std::span<std::byte> output,
                     satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_mock(output, timeout);
    }

    read_result read_until(std::span<std::byte> output,
                           std::byte expected,
                           satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_until_mock(output, expected, timeout);
    }

    spl::drivers::uart::status
    write(std::span<const std::byte> input,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_mock(input, timeout);
    }
};

static_assert(spl::drivers::uart::concepts::uart<uart>);

} // namespace spl::drivers::uart::mock
