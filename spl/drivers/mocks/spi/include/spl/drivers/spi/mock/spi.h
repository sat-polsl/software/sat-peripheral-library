#pragma once
#include "gmock/gmock.h"
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/peripherals/gpio/mock/gpio.h"

namespace spl::drivers::spi::mock {
struct spi {
    using result = satext::expected<std::span<const std::byte>, spl::drivers::spi::status>;
    MOCK_METHOD(status,
                write_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<const std::byte>,
                 satos::clock::duration));
    MOCK_METHOD(result,
                read_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<std::byte>,
                 satos::clock::duration));
    MOCK_METHOD(result,
                write_read_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<const std::byte>,
                 std::span<std::byte>,
                 satos::clock::duration));
    MOCK_METHOD(result,
                transfer_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<std::byte>,
                 satos::clock::duration));

    status write(spl::peripherals::gpio::mock::gpio& gpio,
                 std::span<const std::byte> buffer,
                 satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_mock(gpio, buffer, timeout);
    }

    result read(spl::peripherals::gpio::mock::gpio& gpio,
                std::span<std::byte> buffer,
                satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_mock(gpio, buffer, timeout);
    }

    result write_read(spl::peripherals::gpio::mock::gpio& gpio,
                      std::span<const std::byte> in,
                      std::span<std::byte> out,
                      satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_read_mock(gpio, in, out, timeout);
    }

    result transfer(spl::peripherals::gpio::mock::gpio& gpio,
                    std::span<std::byte> buffer,
                    satos::clock::duration timeout = satos::clock::duration::max()) {
        return transfer_mock(gpio, buffer, timeout);
    }
};

static_assert(spl::drivers::spi::concepts::spi<spi, spl::peripherals::gpio::mock::gpio>);

struct spi_vectored {
    MOCK_METHOD(status,
                write_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<std::span<const std::byte>>,
                 satos::clock::duration));
    MOCK_METHOD(status,
                read_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<std::span<std::byte>>,
                 satos::clock::duration));
    MOCK_METHOD(status,
                write_read_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<std::span<const std::byte>>,
                 std::span<std::span<std::byte>>,
                 satos::clock::duration));
    MOCK_METHOD(status,
                transfer_mock,
                (spl::peripherals::gpio::mock::gpio&,
                 std::span<std::span<std::byte>>,
                 satos::clock::duration));

    status write(spl::peripherals::gpio::mock::gpio& gpio,
                 std::span<std::span<const std::byte>> buffer,
                 satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_mock(gpio, buffer, timeout);
    }

    status read(spl::peripherals::gpio::mock::gpio& gpio,
                std::span<std::span<std::byte>> buffer,
                satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_mock(gpio, buffer, timeout);
    }

    status write_read(spl::peripherals::gpio::mock::gpio& gpio,
                      std::span<std::span<const std::byte>> in,
                      std::span<std::span<std::byte>> out,
                      satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_read_mock(gpio, in, out, timeout);
    }

    status transfer(spl::peripherals::gpio::mock::gpio& gpio,
                    std::span<std::span<std::byte>> buffer,
                    satos::clock::duration timeout = satos::clock::duration::max()) {
        return transfer_mock(gpio, buffer, timeout);
    }
};

static_assert(
    spl::drivers::spi::concepts::spi_vectored<spi_vectored, spl::peripherals::gpio::mock::gpio>);

} // namespace spl::drivers::spi::mock
