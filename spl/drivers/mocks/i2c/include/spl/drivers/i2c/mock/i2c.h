#pragma once
#include "gmock/gmock.h"
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/i2c/concepts/i2c.h"

namespace spl::drivers::i2c::mock {
struct i2c {
    using read_result = satext::expected<std::span<const std::byte>, spl::drivers::i2c::status>;
    MOCK_METHOD(read_result,
                read_mock,
                (std::uint8_t, std::span<std::byte>, satos::clock::duration));
    MOCK_METHOD(spl::drivers::i2c::status,
                write_mock,
                (std::uint8_t, std::span<const std::byte>, satos::clock::duration));
    MOCK_METHOD(
        read_result,
        write_read_mock,
        (std::uint8_t, std::span<const std::byte>, std::span<std::byte>, satos::clock::duration));

    read_result read(std::uint8_t address,
                     std::span<std::byte> out,
                     satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_mock(address, out, timeout);
    }

    spl::drivers::i2c::status
    write(std::uint8_t address,
          std::span<const std::byte> in,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_mock(address, in, timeout);
    }

    read_result write_read(std::uint8_t address,
                           std::span<const std::byte> in,
                           std::span<std::byte> out,
                           satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_read_mock(address, in, out, timeout);
    }
};

struct i2c_vectored {
    MOCK_METHOD(spl::drivers::i2c::status,
                read_mock,
                (std::uint8_t, std::span<std::span<std::byte>>, satos::clock::duration));
    MOCK_METHOD(spl::drivers::i2c::status,
                write_mock,
                (std::uint8_t, std::span<std::span<const std::byte>>, satos::clock::duration));
    MOCK_METHOD(spl::drivers::i2c::status,
                write_read_mock,
                (std::uint8_t,
                 std::span<std::span<const std::byte>>,
                 std::span<std::span<std::byte>>,
                 satos::clock::duration));

    spl::drivers::i2c::status read(std::uint8_t address,
                                   std::span<std::span<std::byte>> out,
                                   satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_mock(address, out, timeout);
    }

    spl::drivers::i2c::status
    write(std::uint8_t address,
          std::span<std::span<const std::byte>> in,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_mock(address, in, timeout);
    }

    spl::drivers::i2c::status
    write_read(std::uint8_t address,
               std::span<std::span<const std::byte>> in,
               std::span<std::span<std::byte>> out,
               satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_read_mock(address, in, out, timeout);
    }
};

static_assert(spl::drivers::i2c::concepts::i2c<i2c>);
static_assert(spl::drivers::i2c::concepts::i2c_vectored<i2c_vectored>);

} // namespace spl::drivers::i2c::mock
