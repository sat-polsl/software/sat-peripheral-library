#pragma once
#include "gmock/gmock.h"
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/can/concepts/can.h"

namespace spl::drivers::can::mock {
struct can {
    using read_result = satext::expected<spl::peripherals::can::concepts::message, status>;
    MOCK_METHOD(status,
                write_mock,
                (const spl::peripherals::can::concepts::message&, satos::clock::duration));
    MOCK_METHOD(read_result,
                read_mock,
                (spl::peripherals::can::concepts::rx_fifo, satos::clock::duration));

    status write(const spl::peripherals::can::concepts::message& msg,
                 satos::clock::duration timeout = satos::clock::duration::max()) {
        return write_mock(msg, timeout);
    }

    read_result read(spl::peripherals::can::concepts::rx_fifo fifo,
                     satos::clock::duration timeout = satos::clock::duration::max()) {
        return read_mock(fifo, timeout);
    }
};

static_assert(concepts::can<can>);
} // namespace spl::drivers::can::mock
