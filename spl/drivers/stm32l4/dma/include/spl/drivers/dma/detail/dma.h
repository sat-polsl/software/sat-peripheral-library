#pragma once

#include <bit>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satext/type_traits.h"
#include "satos/clock.h"
#include "satos/flags.h"
#include "satos/isr.h"
#include "spl/drivers/dma/enums.h"
#include "spl/peripherals/dma/concepts/dma.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/nvic/concepts/nvic.h"

namespace spl::drivers::dma::detail {
/**
 * @addtogroup dma
 * @{
 */

/**
 * @brief DMA driver class
 * @tparam DMA DMA low level peripheral
 * @tparam NVIC NVIC low level peripheral
 */
template<peripherals::dma::concepts::dma DMA, peripherals::nvic::concepts::nvic NVIC>
class dma : private satext::noncopyable {
public:
    /**
     * @brief Class representing single DMA channel
     */
    class channel {
    public:
        /**
         * @brief Copy constructor
         */
        channel(const channel&) = delete;

        /**
         * @brief Copy assigment operator
         */
        channel& operator=(const channel&) = delete;

        /**
         * @brief Move constructor
         */
        channel(channel&& other) noexcept : dma_{other.dma_}, channel_{other.channel_} {
            other.dma_ = nullptr;
        }

        /**
         * @brief Move assigment operator
         */
        channel& operator=(channel&& other) noexcept {
            if (*this != &other) {
                dma_ = other.dma_;
                channel_ = other.channel_;
                other.dma_ = nullptr;
            }
            return *this;
        }

        /**
         * @brief Destructor
         */
        ~channel() {
            if (dma_ != nullptr) {
                dma_->dma_.reset_channel(channel_);
                dma_->channel_flags_.set(1 << satext::to_underlying_type(channel_));
            }
        }

        /**
         * @brief Enables channel
         * @return This DMA channel
         */
        const channel& enable() const {
            dma_->dma_.enable_channel(channel_);
            return *this;
        }

        /**
         * @brief Disable channel
         * @return This DMA channel
         */
        const channel& disable() const {
            dma_->dma_.disable_channel(channel_);
            return *this;
        }

        /**
         * @brief Set channel peripheral address in P2M/M2P mode
         * @param peripheral Peripheral address to set
         * @return This DMA channel
         */
        const channel&
        set_peripheral_address(peripherals::dma::concepts::peripheral peripheral) const {
            dma_->dma_.set_peripheral_address(channel_, peripheral);
            return *this;
        }

        /**
         * @brief Set channel memory address in P2M/M2P mode
         * @param peripheral Memory address to set
         * @return This DMA channel
         */
        const channel& set_memory_address(std::uint32_t address) const {
            dma_->dma_.set_memory_address(channel_, address);
            return *this;
        }

        /**
         * @brief Set channel memory address in P2M/M2P
         * @tparam T Pointer type
         * @param address Pointer to memory buffer to set
         * @return This DMA channel
         */
        template<typename T>
        const channel& set_memory_address(const T* address) const {
            return set_memory_address(std::bit_cast<std::uint32_t>(address));
        }

        /**
         * @brief Set channel transfer size
         * @param size Transfer size to set
         * @return This DMA channel
         */
        const channel& set_transfer_size(std::uint16_t size) const {
            dma_->dma_.set_transfer_size(channel_, size);
            return *this;
        }

        /**
         * @brief Set memory increment mode
         * @param increment true to enable increment mode, false to disable it
         * @return This DMA channel
         */
        const channel& set_memory_increment(bool increment) const {
            dma_->dma_.set_memory_increment(channel_, increment);
            return *this;
        }

        /**
         * @brief Set peripheral increment mode
         * @param increment true to enable increment mode, false to disable it
         * @return This DMA channel
         */
        const channel& set_peripheral_increment(bool increment) const {
            dma_->dma_.set_peripheral_increment(channel_, increment);
            return *this;
        }

        /**
         * @brief Set transfer direction
         * @param dir Transfer direction
         * @return This DMA channel
         */
        const channel& set_direction(peripherals::dma::concepts::direction direction) const {
            dma_->dma_.set_direction(channel_, direction);
            return *this;
        }

        /**
         * @brief Set request mapping
         * @param request DMA request mapping
         * @return This DMA channel
         */
        const channel& set_request_mapping(peripherals::dma::concepts::request request) const {
            dma_->dma_.set_request_mapping(channel_, request);
            return *this;
        }

        /**
         * @brief Set memory-to-memory DMA mode
         * @param m2m true to enable m2m, false to disable
         * @return This DMA channel
         */
        const channel& set_m2m(bool m2m) const {
            dma_->dma_.set_m2m(channel_, m2m);
            return *this;
        }

        /**
         * @brief Set memory-to-memory transfer source
         * @param address Source memory address
         * @return This DMA channel
         */
        const channel& set_m2m_source(std::uint32_t address) const {
            dma_->dma_.set_m2m_source(channel_, address);
            return *this;
        }

        /**
         * @brief Set memory-to-memory transfer source
         * @tparam T pointer type
         * @param address Pointer to source memory buffer
         * @return This DMA channel
         */
        template<typename T>
        const channel& set_m2m_source(const T* address) const {
            return set_m2m_source(std::bit_cast<std::uint32_t>(address));
        }

        /**
         * @brief Set memory-to-memory transfer destination
         * @param address Source memory destination
         * @return This DMA channel
         */
        const channel& set_m2m_destination(std::uint32_t address) const {
            dma_->dma_.set_m2m_destination(channel_, address);
            return *this;
        }

        /**
         * @brief Set memory-to-memory transfer destination
         * @tparam T pointer type
         * @param address Pointer to destination memory buffer
         * @return This DMA channel
         */
        template<typename T>
        const channel& set_m2m_destination(const T* address) const {
            return set_m2m_destination(std::bit_cast<std::uint32_t>(address));
        }

        /**
         * @brief Enable IRQ
         * @param irq IRQ to enable
         * @return This DMA chanel
         */
        const channel& enable_irq(peripherals::dma::concepts::irq irq) const {
            dma_->dma_.enable_irq(channel_, irq);
            return *this;
        }

        /**
         * @brief Enable IRQ
         * @param irq IRQ to enable
         * @return This DMA chanel
         */
        const channel& disable_irq(peripherals::dma::concepts::irq irq) const {
            dma_->dma_.disable_irq(channel_, irq);
            return *this;
        }

        /**
         * @brief Waits for completion of started transfer
         * @param timeout Waiting timeout
         * @return Transfer status
         */
        drivers::dma::status
        wait_for_completion(satos::clock::duration timeout = satos::clock::duration::max()) const {
            auto result =
                dma_->notify_flags_.wait(1 << satext::to_underlying_type(channel_), true, timeout);

            dma_->dma_.disable_channel(channel_);
            dma_->dma_.disable_irq(channel_, peripherals::dma::irq::transfer_completed);
            dma_->dma_.disable_irq(channel_, peripherals::dma::irq::transfer_error);

            if (result.has_value()) {
                if ((dma_->transfer_error_ & (1 << satext::to_underlying_type(channel_))) != 0) {
                    dma_->transfer_error_ &= ~(1 << satext::to_underlying_type(channel_));
                    return drivers::dma::status::transfer_error;
                } else {
                    return drivers::dma::status::ok;
                }
            } else {
                return drivers::dma::status::timeout;
            }
        }

        /**
         * @brief Waits for completion of started transfer
         * @param time_point Waiting absolute timeout
         * @return Transfer status
         */
        drivers::dma::status wait_for_completion(satos::clock::time_point time_point) const {
            return wait_for_completion(time_point - satos::clock::now());
        }

        /**
         * @brief Performs DMA transfer
         * @param timeout Transfer timeout
         * @return Transfer status
         */
        drivers::dma::status
        transfer(satos::clock::duration timeout = satos::clock::duration::max()) const {
            dma_->dma_.enable_irq(channel_, peripherals::dma::irq::transfer_completed);
            dma_->dma_.enable_irq(channel_, peripherals::dma::irq::transfer_error);
            dma_->dma_.enable_channel(channel_);

            return wait_for_completion(timeout);
        }

        drivers::dma::status transfer(satos::clock::time_point time_point) const {
            return transfer(time_point - satos::clock::now());
        }

    private:
        friend dma;

        channel(dma* dma, peripherals::dma::concepts::channel channel) :
            dma_{dma},
            channel_{channel} {}

        dma* dma_;
        peripherals::dma::concepts::channel channel_;
    };

    /**
     * @brief Constructor
     * @param dma Reference to low level DMA
     * @param nvic Reference to low level NVIC
     */
    dma(DMA& dma, NVIC& nvic) :
        dma_{dma},
        nvic_{nvic},
        transfer_error_{},
        channel_flags_{},
        notify_flags_{} {}

    /**
     * @brief Initializes DMA. Must be called in RTOS context
     */
    void initialize() { channel_flags_.set(0xffffff); }

    /**
     * @brief Requests given DMA channel
     * @param dma_channel DMA channel
     * @param timeout Request timeout
     * @return DMA Channel on success, status otherwise
     */
    satext::expected<channel, drivers::dma::status>
    request(spl::peripherals::dma::concepts::channel dma_channel,
            satos::clock::duration timeout = satos::clock::duration::max()) {
        auto result =
            channel_flags_.wait(1 << satext::to_underlying_type(dma_channel), true, timeout);
        if (result.has_value()) {
            return channel{this, dma_channel};
        } else {
            return satext::unexpected{drivers::dma::status::timeout};
        }
    }

    /**
     * @brief Requests given DMA channel
     * @param dma_channel DMA channel
     * @param time_point Request absolute timeout
     * @return DMA Channel on success, status otherwise
     */
    satext::expected<channel, drivers::dma::status>
    request(spl::peripherals::dma::concepts::channel dma_channel,
            satos::clock::time_point time_point) {
        return request(dma_channel, time_point - satos::clock::now());
    }

    void isr_handler(peripherals::nvic::concepts::irq irq,
                     peripherals::dma::concepts::channel channel) {
        satos::isr isr;

        if (dma_.get_irq_status(channel, spl::peripherals::dma::irq::transfer_completed)) {
            dma_.clear_irq(channel, spl::peripherals::dma::irq::transfer_completed)
                .disable_channel(channel);
            notify_flags_.set(1 << satext::to_underlying_type(channel));
        }
        if (dma_.get_irq_status(channel, spl::peripherals::dma::irq::half_transfer)) {
            dma_.clear_irq(channel, spl::peripherals::dma::irq::half_transfer);
        }
        if (dma_.get_irq_status(channel, spl::peripherals::dma::irq::transfer_error)) {
            dma_.clear_irq(channel, spl::peripherals::dma::irq::transfer_error)
                .disable_channel(channel);

            transfer_error_ |= 1 << satext::to_underlying_type(channel);
            notify_flags_.set(1 << satext::to_underlying_type(channel));
        }
        nvic_.clear_pending_irq(irq);
    }

private:
    DMA& dma_;
    NVIC& nvic_;
    std::uint32_t transfer_error_;
    satos::flags channel_flags_;
    satos::flags notify_flags_;
};

/** @} */
} // namespace spl::drivers::dma::detail
