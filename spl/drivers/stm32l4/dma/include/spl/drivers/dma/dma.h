#pragma once
#include "satos/flags.h"
#include "spl/drivers/dma/concepts/dma.h"
#include "spl/drivers/dma/detail/dma.h"
#include "spl/peripherals/dma/dma.h"
#include "spl/peripherals/nvic/nvic.h"

namespace spl::drivers::dma {
/**
 * @addtogroup dma
 * @{
 */

/**
 * @brief Hardware specialization of DMA
 */
using dma = detail::dma<spl::peripherals::dma::dma, spl::peripherals::nvic::nvic>;

static_assert(concepts::dma<dma>);

/** @} */
} // namespace spl::drivers::dma
