#pragma once
#include <span>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satos/clock.h"
#include "spl/drivers/dma/concepts/dma.h"
#include "spl/drivers/i2c/enums.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/i2c/concepts/i2c.h"
#include "spl/peripherals/i2c/enums.h"

namespace spl::drivers::i2c::detail {
/**
 * @addtogroup i2c
 * @{
 */

/**
 * @brief I2C RTOS-aware high level driver providing scatter read/gather write DMA transfers.
 * @tparam I2C I2C peripheral
 * @tparam DMA DMA driver
 * @tparam RxPeripheralAddress I2C Rx Peripheral address
 * @tparam TxPeripheralAddress I2C Tx Peripheral address
 * @tparam RxDMA DMA Rx channel
 * @tparam TxDMA DMA Tx channel
 * @tparam Request DMA request
 */
template<spl::peripherals::i2c::concepts::i2c I2C,
         spl::drivers::dma::concepts::dma DMA,
         spl::peripherals::dma::concepts::peripheral RxPeripheralAddress,
         spl::peripherals::dma::concepts::peripheral TxPeripheralAddress,
         spl::peripherals::dma::concepts::channel RxDMA,
         spl::peripherals::dma::concepts::channel TxDMA,
         spl::peripherals::dma::concepts::request Request>
class i2c_dma_vectored : private satext::noncopyable {
public:
    /**
     * @brief Constructor.
     * @param i2c I2C peripheral reference
     * @param dma DMA driver reference
     */
    i2c_dma_vectored(I2C& i2c, DMA& dma) : i2c_{i2c}, dma_{dma} {}

    /**
     * @brief Performs scatter read to output buffers from I2C device with given address
     * @param address I2C device address
     * @param out Non-contiguous output buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    spl::drivers::i2c::status read(std::uint8_t address,
                                   std::span<std::span<std::byte>> out,
                                   satos::clock::duration timeout = satos::clock::duration::max()) {
        if (out.empty()) {
            return spl::drivers::i2c::status::buffer_is_empty;
        }

        std::size_t transfer_size{};

        for (auto buffer : out) {
            transfer_size += buffer.size();
        }

        if (transfer_size > buffer_size_limit) {
            return spl::drivers::i2c::status::buffer_size_out_of_range;
        }

        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;

        auto result =
            dma_.request(RxDMA, timeout_abs)
                .and_then([this, address, out, timeout_abs, transfer_size](dma_channel&& channel)
                              -> satext::expected<dma_channel, spl::drivers::dma::status> {
                    setup_rx_channel(channel);

                    i2c_.enable()
                        .set_7bit_address(address)
                        .set_transfer_dir(spl::peripherals::i2c::direction::read)
                        .set_bytes_to_transfer(transfer_size)
                        .enable_dma(spl::peripherals::i2c::dma::rx);

                    i2c_.send_start();
                    auto dma_status = scatter_read(out, channel, timeout_abs);
                    i2c_.send_stop();
                    if (dma_status != spl::drivers::dma::status::ok) {
                        return satext::unexpected{dma_status};
                    } else {
                        return channel;
                    }
                })
                .map_error(map_status);

        i2c_.disable().disable_dma(spl::peripherals::i2c::concepts::dma::rx);
        return result.has_value() ? status::ok : result.error();
    }

    /**
     * @brief Performs gather write from input buffers to I2C device with given address
     * @param address I2C device address
     * @param in Non-contiguous input buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    spl::drivers::i2c::status
    write(std::uint8_t address,
          std::span<std::span<const std::byte>> in,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        if (in.empty()) {
            return spl::drivers::i2c::status::buffer_is_empty;
        }

        std::size_t transfer_size{};

        for (auto buffer : in) {
            transfer_size += buffer.size();
        }

        if (transfer_size > buffer_size_limit) {
            return spl::drivers::i2c::status::buffer_size_out_of_range;
        }

        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;

        auto result =
            dma_.request(TxDMA, timeout_abs)
                .and_then([this, address, in, timeout_abs, transfer_size](dma_channel&& channel)
                              -> satext::expected<dma_channel, spl::drivers::dma::status> {
                    setup_tx_channel(channel);

                    i2c_.enable()
                        .set_7bit_address(address)
                        .set_transfer_dir(spl::peripherals::i2c::direction::write)
                        .set_bytes_to_transfer(transfer_size)
                        .enable_dma(spl::peripherals::i2c::dma::tx);

                    i2c_.send_start();

                    auto dma_status = gather_write(in, channel, timeout_abs);

                    i2c_.send_stop();
                    if (dma_status != spl::drivers::dma::status::ok) {
                        return satext::unexpected{dma_status};
                    } else {
                        return channel;
                    }
                })
                .map_error(map_status);

        i2c_.disable().disable_dma(spl::peripherals::i2c::concepts::dma::tx);
        return result.has_value() ? status::ok : result.error();
    }

    /**
     * @brief Performs gather write from input buffers and then scatter read to output buffers
     * to/from I2C device with given address
     * @param address I2C device address
     * @param in Non-contiguous input buffers
     * @param out Non-contiguous output buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    spl::drivers::i2c::status
    write_read(std::uint8_t address,
               std::span<std::span<const std::byte>> in,
               std::span<std::span<std::byte>> out,
               satos::clock::duration timeout = satos::clock::duration::max()) {
        if (in.empty() || out.empty()) {
            return spl::drivers::i2c::status::buffer_is_empty;
        }

        std::size_t tx_size{};
        std::size_t rx_size{};

        for (auto buffer : in) {
            tx_size += buffer.size();
        }

        for (auto buffer : out) {
            rx_size += buffer.size();
        }

        if (tx_size > buffer_size_limit || rx_size > buffer_size_limit) {
            return spl::drivers::i2c::status::buffer_size_out_of_range;
        }

        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;

        auto result =
            dma_.request(TxDMA, timeout_abs)
                .and_then([this, address, in, out, timeout_abs, rx_size, tx_size](
                              dma_channel&& tx_channel)
                              -> satext::expected<dma_channel, spl::drivers::dma::status> {
                    return dma_.request(RxDMA, timeout_abs)
                        .and_then(
                            [this, address, in, out, timeout_abs, rx_size, tx_size, &tx_channel](
                                dma_channel&& rx_channel)
                                -> satext::expected<dma_channel, spl::drivers::dma::status> {
                                setup_tx_channel(tx_channel);
                                setup_rx_channel(rx_channel);

                                i2c_.enable()
                                    .set_7bit_address(address)
                                    .set_transfer_dir(spl::peripherals::i2c::direction::write)
                                    .set_bytes_to_transfer(tx_size)
                                    .enable_dma(spl::peripherals::i2c::dma::tx);
                                i2c_.send_start();

                                auto tx_dma_status = gather_write(in, tx_channel, timeout_abs);

                                if (tx_dma_status != spl::drivers::dma::status::ok) {
                                    i2c_.send_stop();
                                    return satext::unexpected{tx_dma_status};
                                }

                                i2c_.disable_dma(spl::peripherals::i2c::dma::tx)
                                    .set_bytes_to_transfer(rx_size)
                                    .set_transfer_dir(spl::peripherals::i2c::direction::read)
                                    .enable_dma(spl::peripherals::i2c::dma::rx)
                                    .send_start();

                                auto rx_dma_status = scatter_read(out, rx_channel, timeout_abs);

                                i2c_.send_stop();
                                if (rx_dma_status != spl::drivers::dma::status::ok) {
                                    return satext::unexpected{rx_dma_status};
                                } else {
                                    return rx_channel;
                                }
                            });
                })
                .map_error(map_status);

        i2c_.disable()
            .disable_dma(spl::peripherals::i2c::dma::rx)
            .disable_dma(spl::peripherals::i2c::dma::tx);
        return result.has_value() ? status::ok : result.error();
    }

private:
    static constexpr std::size_t buffer_size_limit = 256;
    using dma_channel = typename DMA::channel;

    I2C& i2c_;
    DMA& dma_;

    inline static status map_status(spl::drivers::dma::status dma_status) {
        if (dma_status == spl::drivers::dma::status::timeout) {
            return status::timeout;
        } else if (dma_status == spl::drivers::dma::status::transfer_error) {
            return status::transfer_error;
        } else {
            return status::ok;
        }
    }

    spl::drivers::dma::status gather_write(std::span<std::span<const std::byte>> vector,
                                           dma_channel& channel,
                                           satos::clock::time_point timeout_abs) {
        spl::drivers::dma::status dma_status{};
        for (auto buffer : vector) {
            channel.set_memory_address(buffer.data()).set_transfer_size(buffer.size());

            dma_status = channel.transfer(timeout_abs);
            if (dma_status != spl::drivers::dma::status::ok) {
                break;
            }
        }
        return dma_status;
    }

    spl::drivers::dma::status scatter_read(std::span<std::span<std::byte>> vector,
                                           dma_channel& channel,
                                           satos::clock::time_point timeout_abs) {

        spl::drivers::dma::status dma_status{};
        for (auto buffer : vector) {
            channel.set_memory_address(buffer.data()).set_transfer_size(buffer.size());

            dma_status = channel.transfer(timeout_abs);
            if (dma_status != spl::drivers::dma::status::ok) {
                break;
            }
        }
        return dma_status;
    }

    static void setup_rx_channel(dma_channel& channel) {
        channel.set_memory_increment(true)
            .set_peripheral_address(RxPeripheralAddress)
            .set_direction(spl::peripherals::dma::direction::peripheral_to_memory)
            .set_request_mapping(Request);
    }

    static void setup_tx_channel(dma_channel& channel) {
        channel.set_memory_increment(true)
            .set_peripheral_address(TxPeripheralAddress)
            .set_direction(spl::peripherals::dma::direction::memory_to_peripheral)
            .set_request_mapping(Request);
    }
};

/** @} */

} // namespace spl::drivers::i2c::detail
