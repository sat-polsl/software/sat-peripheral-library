#pragma once
#include <mutex>
#include <span>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satos/clock.h"
#include "satos/event.h"
#include "satos/isr.h"
#include "satos/mutex.h"
#include "spl/drivers/i2c/enums.h"
#include "spl/peripherals/i2c/concepts/i2c.h"
#include "spl/peripherals/i2c/enums.h"
#include "spl/peripherals/nvic/concepts/nvic.h"

namespace spl::drivers::i2c::detail {

/**
 * @addtogroup i2c
 * @{
 */

/**
 * @brief I2C driver
 * @tparam I2C I2C peripheral
 * @tparam Event RTOS event
 * @tparam irq I2C IRQ
 */
template<spl::peripherals::i2c::concepts::i2c I2C,
         spl::peripherals::nvic::concepts::nvic NVIC,
         spl::peripherals::nvic::concepts::irq EvIrq,
         spl::peripherals::nvic::concepts::irq ErIrq>
class i2c : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param i2c I2C peripheral
     */
    explicit i2c(I2C& i2c, NVIC& nvic) : i2c_{i2c}, nvic_{nvic}, status_{status::ok} {}

    /**
     * @brief Reads data received over I2C into given buffer
     * @param buffer Buffer where received data will be saved
     * @param timeout Duration timeout
     * @return On success subspan with received data. Status on failure.
     */
    satext::expected<std::span<const std::byte>, status>
    read(std::uint8_t address,
         std::span<std::byte> buffer,
         satos::clock::duration timeout = satos::clock::duration::max()) {
        if (buffer.empty()) {
            return satext::unexpected(status::buffer_is_empty);
        }

        if (i2c_.is_busy()) {
            return satext::unexpected(status::busy);
        }

        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{rw_mtx_, timeout_abs};

        if (!lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        status_ = status::ok;

        read_begin_ = buffer.begin();
        read_end_ = buffer.end();

        i2c_.enable()
            .set_7bit_address(address)
            .set_transfer_dir(spl::peripherals::i2c::direction::read)
            .set_bytes_to_transfer(buffer.size_bytes())
            .enable_interrupt(spl::peripherals::i2c::irq::nack_received)
            .send_start();

        satos::event_guard event(rw_event_);
        i2c_.enable_interrupt(spl::peripherals::i2c::irq::rx_not_empty);
        auto no_timeout = event.wait_until(timeout_abs);

        i2c_.disable_interrupt(spl::peripherals::i2c::irq::nack_received).disable();

        if (!no_timeout) {
            return satext::unexpected(status::timeout);
        }
        if (status_ != status::ok) {
            return satext::unexpected(status_);
        }
        return buffer.subspan(0, std::distance(buffer.begin(), read_begin_));
    }

    /**
     * @brief Transmit given buffer over I2C
     * @tparam rep Duration ratio
     * @tparam period Clock period
     * @return I2C Status
     */
    spl::drivers::i2c::status
    write(std::uint8_t address,
          std::span<const std::byte> buffer,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{rw_mtx_, timeout_abs};

        if (i2c_.is_busy()) {
            return status::busy;
        }

        if (!lck.owns_lock()) {
            return status::timeout;
        }

        status_ = status::ok;

        write_begin_ = buffer.begin();
        write_end_ = buffer.end();

        read_begin_ = iterator{};
        read_end_ = iterator{};

        i2c_.enable()
            .set_7bit_address(address)
            .set_transfer_dir(spl::peripherals::i2c::direction::write)
            .set_bytes_to_transfer(buffer.size_bytes())
            .enable_interrupt(spl::peripherals::i2c::irq::nack_received)
            .send_start();

        satos::event_guard event(rw_event_);
        i2c_.enable_interrupt(spl::peripherals::i2c::irq::tx_empty);
        auto no_timeout = event.wait_until(timeout_abs);

        i2c_.disable_interrupt(spl::peripherals::i2c::irq::nack_received).disable();

        if (!no_timeout) {
            return status::timeout;
        }
        if (status_ != status::ok) {
            return status_;
        }
        return status::ok;
    }

    /**
     * @brief Transmit given tx_buffer over I2C, read rx_buffer afterwards
     * @param tx_buffer Buffer to transmit
     * @param rx_buffer Buffer to transmit
     * @param timeout Duration timeout
     * @return I2C Status
     */
    satext::expected<std::span<const std::byte>, status>
    write_read(std::uint8_t address,
               std::span<const std::byte> tx_buffer,
               std::span<std::byte> rx_buffer,
               satos::clock::duration timeout = satos::clock::duration::max()) {
        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{rw_mtx_, timeout_abs};

        if (i2c_.is_busy()) {
            return satext::unexpected(status::busy);
        }

        if (!lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        status_ = status::ok;

        write_begin_ = tx_buffer.begin();
        write_end_ = tx_buffer.end();

        read_begin_ = rx_buffer.begin();
        read_end_ = rx_buffer.end();

        i2c_.enable()
            .set_7bit_address(address)
            .set_transfer_dir(spl::peripherals::i2c::direction::write)
            .set_bytes_to_transfer(tx_buffer.size_bytes())
            .enable_interrupt(spl::peripherals::i2c::irq::nack_received)
            .send_start();

        satos::event_guard event(rw_event_);
        i2c_.enable_interrupt(spl::peripherals::i2c::irq::tx_empty);
        auto no_timeout = event.wait_until(timeout_abs);

        i2c_.disable_interrupt(spl::peripherals::i2c::irq::nack_received).disable();

        if (!no_timeout) {
            return satext::unexpected(status::timeout);
        }
        if (status_ != status::ok) {
            return satext::unexpected(status_);
        }
        return rx_buffer.subspan(0, std::distance(rx_buffer.begin(), read_begin_));
    }

    void isr_ev_handler() {
        satos::isr isr;

        if (i2c_.is_interrupt_enabled(spl::peripherals::i2c::irq::rx_not_empty) &&
            i2c_.is_rx_not_empty()) {
            *read_begin_++ = static_cast<std::byte>(i2c_.read());
            if (read_begin_ == read_end_) {
                i2c_.disable_interrupt(spl::peripherals::i2c::irq::rx_not_empty).send_stop();
                rw_event_.notify();
            }
        }

        if (i2c_.is_interrupt_enabled(spl::peripherals::i2c::irq::tx_empty) && i2c_.is_tx_empty()) {
            if (write_begin_ != write_end_) {
                i2c_.write(static_cast<std::uint8_t>(*write_begin_++));
            }
            if (write_begin_ == write_end_) {
                i2c_.disable_interrupt(spl::peripherals::i2c::irq::tx_empty);
                if (read_begin_ != read_end_) {
                    i2c_.set_bytes_to_transfer(std::distance(read_begin_, read_end_))
                        .set_transfer_dir(spl::peripherals::i2c::direction::read)
                        .enable_interrupt(spl::peripherals::i2c::irq::rx_not_empty)
                        .send_start();
                } else {
                    i2c_.send_stop();
                    rw_event_.notify();
                }
            }
        }

        if (i2c_.is_interrupt_enabled(spl::peripherals::i2c::irq::nack_received) &&
            i2c_.nack_received()) {
            i2c_.disable_interrupt(spl::peripherals::i2c::irq::nack_received);
            status_ = status::nack_received;
            rw_event_.notify();
        }

        nvic_.clear_pending_irq(EvIrq);
    }

    void isr_er_handler() { nvic_.clear_pending_irq(ErIrq); }

private:
    using iterator = std::span<std::byte>::iterator;
    using const_iterator = std::span<const std::byte>::iterator;

    I2C& i2c_;
    NVIC& nvic_;
    satos::event rw_event_{};
    satos::mutex rw_mtx_{};
    iterator read_begin_{};
    iterator read_end_{};
    const_iterator write_begin_{};
    const_iterator write_end_{};
    status status_{};
};

} // namespace spl::drivers::i2c::detail
