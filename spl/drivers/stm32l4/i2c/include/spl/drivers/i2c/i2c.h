#pragma once
#include "spl/drivers/dma/dma.h"
#include "spl/drivers/i2c/concepts/i2c.h"
#include "spl/drivers/i2c/detail/i2c.h"
#include "spl/drivers/i2c/detail/i2c_dma_vectored.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/i2c/i2c.h"
#include "spl/peripherals/nvic/nvic.h"

namespace spl::drivers::i2c {

/**
 * @addtogroup i2c
 * @{
 */

namespace i2c {
/**
 * @brief MCU I2C1 specialization
 */
using i2c1 = detail::i2c<spl::peripherals::i2c::i2c,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::i2c1_ev,
                         spl::peripherals::nvic::irq::i2c1_er>;

/**
 * @brief MCU I2C2 specialization
 */
using i2c2 = detail::i2c<spl::peripherals::i2c::i2c,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::i2c2_ev,
                         spl::peripherals::nvic::irq::i2c2_er>;

/**
 * @brief MCU I2C3 specialization
 */
using i2c3 = detail::i2c<spl::peripherals::i2c::i2c,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::i2c3_ev,
                         spl::peripherals::nvic::irq::i2c3_er>;
} // namespace i2c

namespace i2c_dma_vectored {
/**
 * @brief MCU I2C1 DMA1 vectored specialization
 */
using i2c1 = detail::i2c_dma_vectored<spl::peripherals::i2c::i2c,
                                      spl::drivers::dma::dma,
                                      spl::peripherals::dma::peripheral::i2c1_rx,
                                      spl::peripherals::dma::peripheral::i2c1_tx,
                                      spl::peripherals::dma::channel7,
                                      spl::peripherals::dma::channel6,
                                      spl::peripherals::dma::request4>;

/**
 * @brief MCU I2C2 DMA1 vectored specialization
 */
using i2c2 = detail::i2c_dma_vectored<spl::peripherals::i2c::i2c,
                                      spl::drivers::dma::dma,
                                      spl::peripherals::dma::peripheral::i2c2_rx,
                                      spl::peripherals::dma::peripheral::i2c2_tx,
                                      spl::peripherals::dma::channel5,
                                      spl::peripherals::dma::channel4,
                                      spl::peripherals::dma::request4>;

/**
 * @brief MCU I2C3 DMA1 vectored specialization
 */
using i2c3 = detail::i2c_dma_vectored<spl::peripherals::i2c::i2c,
                                      spl::drivers::dma::dma,
                                      spl::peripherals::dma::peripheral::i2c3_rx,
                                      spl::peripherals::dma::peripheral::i2c3_tx,
                                      spl::peripherals::dma::channel3,
                                      spl::peripherals::dma::channel2,
                                      spl::peripherals::dma::request4>;
} // namespace i2c_dma_vectored

/** @} */

static_assert(concepts::i2c<i2c::i2c1>);
static_assert(concepts::i2c<i2c::i2c2>);
static_assert(concepts::i2c<i2c::i2c3>);

static_assert(concepts::i2c_vectored<i2c_dma_vectored::i2c1>);
static_assert(concepts::i2c_vectored<i2c_dma_vectored::i2c2>);
static_assert(concepts::i2c_vectored<i2c_dma_vectored::i2c3>);

} // namespace spl::drivers::i2c
