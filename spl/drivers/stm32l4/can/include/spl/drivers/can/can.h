#pragma once
#include "spl/drivers/can/concepts/can.h"
#include "spl/drivers/can/detail/can.h"
#include "spl/peripherals/can/can.h"
#include "spl/peripherals/nvic/nvic.h"

namespace spl::drivers::can {

/**
 * @addtogroup can
 * @{
 */

/**
 * @brief CAN1 MCU specialization
 */
using can1 = detail::can<spl::peripherals::can::can,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::can1_tx,
                         spl::peripherals::nvic::irq::can1_rx0,
                         spl::peripherals::nvic::irq::can1_rx1>;

/**
 * @brief CAN2 MCU specialization
 */
using can2 = detail::can<spl::peripherals::can::can,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::can2_tx,
                         spl::peripherals::nvic::irq::can2_rx0,
                         spl::peripherals::nvic::irq::can2_rx1>;

static_assert(concepts::can<can1>);
static_assert(concepts::can<can2>);

/** @} */
} // namespace spl::drivers::can
