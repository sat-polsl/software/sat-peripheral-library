#pragma once
#include <mutex>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satos/clock.h"
#include "satos/event.h"
#include "satos/isr.h"
#include "satos/mutex.h"
#include "spl/drivers/can/enums.h"
#include "spl/peripherals/can/concepts/can.h"
#include "spl/peripherals/can/enums.h"
#include "spl/peripherals/nvic/concepts/nvic.h"

#include <cstdio>

namespace spl::drivers::can::detail {
/**
 * @addtogroup can
 * @{
 */

/**
 * @brief CAN driver
 * @tparam CAN CAN peripheral
 * @tparam NVIC NVIC peripheral
 * @tparam TxIrq TX CAN IRQ
 * @tparam Rx0Irq RX0 CAN IRQ
 * @tparam Rx1Irq RX1 CAN IRQ
 */
template<spl::peripherals::can::concepts::can CAN,
         spl::peripherals::nvic::concepts::nvic NVIC,
         spl::peripherals::nvic::concepts::irq TxIrq,
         spl::peripherals::nvic::concepts::irq Rx0Irq,
         spl::peripherals::nvic::concepts::irq Rx1Irq>
class can : private satext::noncopyable {
public:
    /**
     * Constructor
     * @param can CAN low level driver reference
     * @param nvic NVIC low level driver reference
     */
    can(CAN& can, NVIC& nvic) : can_{can}, nvic_{nvic} {}

    /**
     * @brief Writes CAN message to CAN TX mailbox
     * @param msg Message to write
     * @param timeout Timeout
     * @return operation status
     */
    status write(const spl::peripherals::can::concepts::message& msg,
                 satos::clock::duration timeout = satos::clock::duration::max()) {

        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{tx_mtx_, timeout_abs};

        if (!lck.owns_lock()) {
            return status::timeout;
        }

        bool no_timeout = true;
        if (!can_.is_tx_mailbox_empty()) {
            satos::event_guard event(tx_event_);
            can_.enable_irq(spl::peripherals::can::irq::tx_mailbox_released);
            no_timeout = event.wait_until(timeout_abs);
            can_.disable_irq(spl::peripherals::can::irq::tx_mailbox_released);
        }

        if (no_timeout) {
            can_.write(msg);
            return status::ok;
        } else {
            return status::timeout;
        }
    };

    /**
     * @brief Reads CAN message from RX CAN FIFO
     * @param fifo RX FIFO to read
     * @param timeout timeout
     * @return CAN message on success, operation status otherwise
     */
    satext::expected<spl::peripherals::can::concepts::message, status>
    read(spl::peripherals::can::concepts::rx_fifo fifo,
         satos::clock::duration timeout = satos::clock::duration::max()) {
        auto& mtx = (fifo == spl::peripherals::can::rx_fifo0) ? rx0_mtx_ : rx1_mtx_;
        auto& evt = (fifo == spl::peripherals::can::rx_fifo0) ? rx0_event_ : rx1_event_;
        auto irq = (fifo == spl::peripherals::can::rx_fifo0)
                       ? spl::peripherals::can::irq::rx0_pending
                       : spl::peripherals::can::irq::rx1_pending;

        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{mtx, timeout_abs};

        if (!lck.owns_lock()) {
            return satext::unexpected{status::timeout};
        }

        satos::event_guard event(evt);
        can_.enable_irq(irq);
        auto no_timeout = event.wait_until(timeout_abs);

        can_.disable_irq(irq);

        if (no_timeout) {
            return can_.read(fifo);
        } else {
            return satext::unexpected{status::timeout};
        }
    }

    void tx_isr_handler() {
        satos::isr isr;
        if (can_.is_irq_enabled(spl::peripherals::can::irq::tx_mailbox_released) &&
            can_.tx_mailbox_released()) {
            can_.disable_irq(spl::peripherals::can::irq::tx_mailbox_released);
            can_.clear_tx_mailbox_released();
            tx_event_.notify();
        }
        nvic_.clear_pending_irq(TxIrq);
    }

    void rx0_isr_handler() {
        satos::isr isr;
        if (can_.is_irq_enabled(spl::peripherals::can::irq::rx0_pending) &&
            can_.is_rx0_fifo_not_empty()) {
            can_.disable_irq(spl::peripherals::can::irq::rx0_pending);
            rx0_event_.notify();
        }
        nvic_.clear_pending_irq(Rx0Irq);
    }

    void rx1_isr_handler() {
        satos::isr isr;
        if (can_.is_irq_enabled(spl::peripherals::can::irq::rx1_pending) &&
            can_.is_rx1_fifo_not_empty()) {
            can_.disable_irq(spl::peripherals::can::irq::rx1_pending);
            rx0_event_.notify();
        }
        nvic_.clear_pending_irq(Rx1Irq);
    }

private:
    CAN& can_;
    NVIC& nvic_;
    satos::mutex tx_mtx_;
    satos::mutex rx0_mtx_;
    satos::mutex rx1_mtx_;
    satos::event tx_event_;
    satos::event rx0_event_;
    satos::event rx1_event_;
};

/** @} */
} // namespace spl::drivers::can::detail
