#pragma once
#include "spl/drivers/uart/concepts/uart.h"
#include "spl/drivers/uart/detail/uart.h"
#include "spl/peripherals/nvic/nvic.h"
#include "spl/peripherals/uart/uart.h"

namespace spl::drivers::uart {

/**
 * @addtogroup uart
 * @{
 */

/**
 * @brief MCU UART1 specialization
 */
using uart1 = detail::uart<spl::peripherals::uart::uart,
                           spl::peripherals::nvic::nvic,
                           spl::peripherals::nvic::irq::uart1>;

/**
 * @brief MCU UART2 specialization
 */
using uart2 = detail::uart<spl::peripherals::uart::uart,
                           spl::peripherals::nvic::nvic,
                           spl::peripherals::nvic::irq::uart2>;

/**
 * @brief MCU UART3 specialization
 */
using uart3 = detail::uart<spl::peripherals::uart::uart,
                           spl::peripherals::nvic::nvic,
                           spl::peripherals::nvic::irq::uart3>;

/** @} */

static_assert(concepts::uart<uart1>);
static_assert(concepts::uart<uart2>);
static_assert(concepts::uart<uart3>);

} // namespace spl::drivers::uart
