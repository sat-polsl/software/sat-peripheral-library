#pragma once
#include <mutex>
#include <span>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satos/event.h"
#include "satos/isr.h"
#include "satos/mutex.h"
#include "spl/drivers/uart/enums.h"
#include "spl/peripherals/nvic/concepts/nvic.h"
#include "spl/peripherals/uart/concepts/uart.h"
#include "spl/peripherals/uart/enums.h"

namespace spl::drivers::uart::detail {

/**
 * @addtogroup uart
 * @{
 */

/**
 * @brief UART driver
 * @ingroup uart
 * @tparam UART UART peripheral
 * @tparam NVIC NVIC peripheral
 * @tparam Irq UART IRQ
 */
template<spl::peripherals::uart::concepts::uart UART,
         spl::peripherals::nvic::concepts::nvic NVIC,
         spl::peripherals::nvic::concepts::irq Irq>
class uart : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param uart UART peripheral
     * @param nvic NVIC peripheral
     */
    uart(UART& uart, NVIC& nvic) : uart_{uart}, nvic_(nvic) {}

    /**
     * @brief Reads data received over UART into given buffer
     * @tparam rep Duration ratio
     * @tparam period Clock period
     * @param buffer Buffer where received data will be saved
     * @param timeout Duration timeout
     * @return On success subspan with received data. Status on failure.
     */
    satext::expected<std::span<const std::byte>, status>
    read(std::span<std::byte> buffer,
         satos::clock::duration timeout = satos::clock::duration::max()) {
        if (buffer.empty()) {
            return satext::unexpected(status::buffer_is_empty);
        }

        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{read_mtx_, timeout_abs};

        if (!lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        read_begin_ = buffer.begin();
        read_end_ = buffer.end();

        satos::event_guard event(read_event_);
        uart_.enable_irq(spl::peripherals::uart::irq::rx_not_empty);
        if (!event.wait_until(timeout_abs)) {
            return satext::unexpected(status::timeout);
        }

        return buffer.subspan(0, std::distance(buffer.begin(), read_begin_));
    }

    satext::expected<std::span<const std::byte>, status>
    read_until(std::span<std::byte> buffer,
               std::byte expected,
               satos::clock::duration timeout = satos::clock::duration::max()) {
        if (buffer.empty()) {
            return satext::unexpected(status::buffer_is_empty);
        }

        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{read_mtx_, timeout_abs};

        if (!lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        is_expected_read_ = true;
        expected_ = expected;
        read_begin_ = buffer.begin();
        read_end_ = buffer.end();

        satos::event_guard event(read_event_);
        uart_.enable_irq(spl::peripherals::uart::irq::rx_not_empty);
        bool no_timeout = event.wait_until(timeout_abs);

        is_expected_read_ = false;
        if (!no_timeout) {
            return satext::unexpected(status::timeout);
        }

        return buffer.subspan(0, std::distance(buffer.begin(), read_begin_));
    }

    /**
     * @brief Transmit given buffer over UART
     * @tparam rep Duration ratio
     * @tparam period Clock period
     * @param buffer Buffer to transmit
     * @param timeout Duration timeout
     * @return UART Status
     */
    spl::drivers::uart::status
    write(std::span<const std::byte> buffer,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{write_mtx_, timeout_abs};

        if (!lck.owns_lock()) {
            return status::timeout;
        }

        write_begin_ = buffer.begin();
        write_end_ = buffer.end();

        satos::event_guard event(write_event_);
        uart_.enable_irq(spl::peripherals::uart::irq::tx_empty);
        if (!event.wait_until(timeout_abs)) {
            return status::timeout;
        } else {
            return status::ok;
        }
    }

    void isr_handler() {
        satos::isr isr;
        if (uart_.is_irq_enabled(spl::peripherals::uart::irq::tx_empty) && uart_.is_tx_empty()) {
            if (write_begin_ != write_end_) {
                uart_.write(static_cast<std::uint16_t>(*write_begin_++));
            } else {
                uart_.disable_irq(spl::peripherals::uart::irq::tx_empty);
                write_event_.notify();
            }
        }
        if (uart_.is_irq_enabled(spl::peripherals::uart::irq::rx_not_empty) &&
            uart_.is_rx_not_empty()) {
            auto data = static_cast<std::byte>(uart_.read());
            *read_begin_++ = data;
            if (read_begin_ == read_end_ || (is_expected_read_ && data == expected_)) {
                uart_.disable_irq(spl::peripherals::uart::irq::rx_not_empty);
                read_event_.notify();
            }
        }
        if (uart_.is_irq_enabled(spl::peripherals::uart::irq::rx_not_empty) &&
            uart_.overrun_error()) {
            uart_.clear_irq_flag(spl::peripherals::uart::clear_irq::overrun);
        }
        nvic_.clear_pending_irq(Irq);
    }

private:
    using iterator = std::span<std::byte>::iterator;
    using const_iterator = std::span<const std::byte>::iterator;

    UART& uart_;
    NVIC& nvic_;
    satos::event write_event_{};
    satos::event read_event_{};
    satos::mutex write_mtx_{};
    satos::mutex read_mtx_{};
    iterator read_begin_{};
    iterator read_end_{};
    const_iterator write_begin_{};
    const_iterator write_end_{};
    bool is_expected_read_{};
    std::byte expected_{};
};
} // namespace spl::drivers::uart::detail
