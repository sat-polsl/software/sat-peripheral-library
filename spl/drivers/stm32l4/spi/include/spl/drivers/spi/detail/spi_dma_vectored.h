#pragma once
#include <span>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "spl/drivers/dma/concepts/dma.h"
#include "spl/drivers/spi/chip_select_guard.h"
#include "spl/drivers/spi/enums.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/gpio/concepts/gpio.h"
#include "spl/peripherals/spi/concepts/spi.h"
#include "spl/peripherals/spi/enums.h"

namespace spl::drivers::spi::detail {
/**
 * @addtogroup spi
 * @{
 */

/**
 * @brief SPI RTOS-aware high level driver providing scatter read/gather write DMA transfers.
 * @tparam SPI SPI peripheral
 * @tparam GPIO GPIO peripheral
 * @tparam DMA DMA driver
 * @tparam PeripheralAddress SPI Peripheral address
 * @tparam RxDmaChannel DMA RX channel
 * @tparam TxDmaChannel DMA TX channel
 * @tparam DmaRequest DMA request
 */
template<spl::peripherals::spi::concepts::spi SPI,
         spl::peripherals::gpio::concepts::gpio GPIO,
         spl::drivers::dma::concepts::dma DMA,
         spl::peripherals::dma::concepts::peripheral PeripheralAddress,
         spl::peripherals::dma::concepts::channel RxDmaChannel,
         spl::peripherals::dma::concepts::channel TxDmaChannel,
         spl::peripherals::dma::concepts::request DmaRequest>
class spi_dma_vectored : private satext::noncopyable {
public:
    /**
     * @brief Constructor.
     * @param spi SPI peripheral reference
     * @param dma DMA driver reference
     */
    spi_dma_vectored(SPI& spi, DMA& dma) : spi_{spi}, dma_{dma} {}

    /**
     * @brief Initializes SPI in master mode with software chip select
     */
    void initialize() {
        spi_.set_slave_management(spl::peripherals::spi::slave_management::software)
            .set_mode(spl::peripherals::spi::mode::master)
            .set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8)
            .set_internal_slave_select(true);
    }

    /**
     * @brief Performs gather write from input buffers over SPI to device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param input Non-contiguous input buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    status write(GPIO& chip_select,
                 std::span<std::span<const std::byte>> input,
                 satos::clock::duration timeout = satos::clock::duration::max()) {
        if (input.empty()) {
            return status::buffer_is_empty;
        }
        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;
        auto result =
            dma_.request(TxDmaChannel, timeout_abs)
                .and_then([this, timeout_abs, input, &chip_select](dma_channel&& channel)
                              -> satext::expected<dma_channel, spl::drivers::dma::status> {
                    setup_tx_channel(channel);
                    spi_.enable_dma(spl::peripherals::spi::dma::tx);

                    spl::drivers::dma::status dma_status;

                    {
                        chip_select_guard cs(chip_select);
                        spi_.enable();
                        dma_status = gather_write(input, channel, timeout_abs);
                    }

                    spi_.disable().disable_dma(spl::peripherals::spi::dma::tx);
                    while (!spi_.is_rx_fifo_empty()) {
                        static_cast<void>(spi_.read());
                    }

                    if (dma_status != spl::drivers::dma::status::ok) {
                        return satext::unexpected{dma_status};
                    } else {
                        return channel;
                    }
                })
                .map_error(map_status);
        return result.has_value() ? status::ok : result.error();
    }

    /**
     * @brief Performs scatter read to output buffers over SPI from device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param output Non-contiguous output buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    status read(GPIO& chip_select,
                std::span<std::span<std::byte>> output,
                satos::clock::duration timeout = satos::clock::duration::max()) {
        if (output.empty()) {
            return status::buffer_is_empty;
        }
        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;

        auto result =
            dma_.request(TxDmaChannel, timeout_abs)
                .and_then([this, timeout_abs, output, &chip_select](dma_channel&& tx_channel) {
                    return dma_.request(RxDmaChannel, timeout_abs)
                        .and_then([this, timeout_abs, output, &chip_select, &tx_channel](
                                      dma_channel&& rx_channel)
                                      -> satext::expected<dma_channel, spl::drivers::dma::status> {
                            spi_.enable_dma(spl::peripherals::spi::dma::rx);
                            setup_tx_channel(tx_channel);
                            setup_rx_channel(rx_channel);

                            std::byte tx_dummy{0xff};
                            tx_channel.set_memory_address(&tx_dummy).set_memory_increment(false);

                            spi_.enable_dma(spl::peripherals::spi::dma::tx);

                            spl::drivers::dma::status dma_status;

                            {
                                chip_select_guard cs(chip_select);
                                spi_.enable();

                                dma_status =
                                    scatter_read(output, rx_channel, tx_channel, timeout_abs);
                            }

                            if (dma_status != spl::drivers::dma::status::ok) {
                                return satext::unexpected{dma_status};
                            } else {
                                return rx_channel;
                            }
                        });
                })
                .map_error(map_status);

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);

        return result.has_value() ? status::ok : result.error();
    }

    /**
     * @brief Performs gather write from input buffers and then scatter read to output buffers over
     * SPI to/from device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param input Non-contiguous input buffers
     * @param output Non-contiguous output buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    status write_read(GPIO& chip_select,
                      std::span<std::span<const std::byte>> input,
                      std::span<std::span<std::byte>> output,
                      satos::clock::duration timeout = satos::clock::duration::max()) {
        if (input.empty() || output.empty()) {
            return status::buffer_is_empty;
        }

        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;

        auto result =
            dma_.request(TxDmaChannel, timeout_abs)
                .and_then([this, timeout_abs, input, output, &chip_select](
                              dma_channel&& tx_channel) {
                    return dma_.request(RxDmaChannel, timeout_abs)
                        .and_then([this, timeout_abs, input, output, &chip_select, &tx_channel](
                                      dma_channel&& rx_channel)
                                      -> satext::expected<dma_channel, spl::drivers::dma::status> {
                            setup_tx_channel(tx_channel);
                            setup_rx_channel(rx_channel);

                            std::byte tx_dummy{0xff};
                            spi_.enable_dma(spl::peripherals::spi::dma::tx);

                            spl::drivers::dma::status dma_status;
                            {
                                chip_select_guard cs(chip_select);
                                spi_.enable();
                                dma_status = gather_write(input, tx_channel, timeout_abs);

                                spi_.disable();
                                while (!spi_.is_rx_fifo_empty()) {
                                    static_cast<void>(spi_.read());
                                }

                                if (dma_status != spl::drivers::dma::status::ok) {
                                    return satext::unexpected{dma_status};
                                }

                                tx_channel.set_memory_address(&tx_dummy);
                                tx_channel.set_memory_increment(false);

                                spi_.enable_dma(spl::peripherals::spi::dma::rx);
                                spi_.enable();
                                dma_status =
                                    scatter_read(output, rx_channel, tx_channel, timeout_abs);
                            }

                            if (dma_status != spl::drivers::dma::status::ok) {
                                return satext::unexpected{dma_status};
                            } else {
                                return rx_channel;
                            }
                        });
                })
                .map_error(map_status);

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);

        return result.has_value() ? status::ok : result.error();
    }

    /**
     * @brief Performs gather write/scatter read from/to input/output buffers over SPI to/from
     * device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param input_output Non-contiguous input/output buffers
     * @param timeout Operation timeout. By default waits indefinitely.
     * @return Operation status
     */
    status transfer(GPIO& chip_select,
                    std::span<std::span<std::byte>> input_output,
                    satos::clock::duration timeout = satos::clock::duration::max()) {
        if (input_output.empty()) {
            return status::buffer_is_empty;
        }

        auto timeout_abs = (timeout == satos::clock::duration::max())
                               ? satos::clock::time_point::max()
                               : satos::clock::now() + timeout;

        auto result =
            dma_.request(TxDmaChannel, timeout_abs)
                .and_then([this, timeout_abs, input_output, &chip_select](
                              dma_channel&& tx_channel) {
                    return dma_.request(RxDmaChannel, timeout_abs)
                        .and_then([this, timeout_abs, input_output, &chip_select, &tx_channel](
                                      dma_channel&& rx_channel)
                                      -> satext::expected<dma_channel, spl::drivers::dma::status> {
                            spi_.enable_dma(spl::peripherals::spi::dma::rx);
                            setup_tx_channel(tx_channel);
                            setup_rx_channel(rx_channel);
                            spi_.enable_dma(spl::peripherals::spi::dma::tx);

                            spl::drivers::dma::status dma_status;
                            {
                                chip_select_guard cs(chip_select);
                                spi_.enable();

                                for (auto& buffer : input_output) {
                                    rx_channel.set_memory_address(buffer.data())
                                        .set_transfer_size(buffer.size())
                                        .enable();
                                    tx_channel.set_memory_address(buffer.data())
                                        .set_transfer_size(buffer.size());

                                    dma_status = tx_channel.transfer(timeout_abs);

                                    while (spi_.is_busy()) {};
                                    rx_channel.disable();

                                    if (dma_status != spl::drivers::dma::status::ok) {
                                        break;
                                    }
                                }
                            }

                            if (dma_status != spl::drivers::dma::status::ok) {
                                return satext::unexpected{dma_status};
                            } else {
                                return rx_channel;
                            }
                        });
                })
                .map_error(map_status);

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);

        return result.has_value() ? status::ok : result.error();
    }

private:
    SPI& spi_;
    DMA& dma_;

    using dma_channel = typename DMA::channel;

    inline static status map_status(spl::drivers::dma::status dma_status) {
        if (dma_status == spl::drivers::dma::status::timeout) {
            return status::timeout;
        } else if (dma_status == spl::drivers::dma::status::transfer_error) {
            return status::transfer_error;
        } else {
            return status::ok;
        }
    }

    static void setup_tx_channel(dma_channel& channel) {
        channel.set_memory_increment(true)
            .set_peripheral_address(PeripheralAddress)
            .set_direction(spl::peripherals::dma::direction::memory_to_peripheral)
            .set_request_mapping(DmaRequest);
    }

    static void setup_rx_channel(dma_channel& channel) {
        channel.set_memory_increment(true)
            .set_peripheral_address(PeripheralAddress)
            .set_direction(spl::peripherals::dma::direction::peripheral_to_memory)
            .set_request_mapping(DmaRequest);
    }

    spl::drivers::dma::status gather_write(std::span<std::span<const std::byte>> vector,
                                           dma_channel& channel,
                                           satos::clock::time_point timeout_abs) {
        spl::drivers::dma::status dma_status{};
        for (auto& buffer : vector) {
            channel.set_memory_address(buffer.data());
            channel.set_transfer_size(buffer.size());
            dma_status = channel.transfer(timeout_abs);
            if (dma_status != spl::drivers::dma::status::ok) {
                break;
            }
        }
        return dma_status;
    }

    spl::drivers::dma::status scatter_read(std::span<std::span<std::byte>> vector,
                                           dma_channel& rx_channel,
                                           dma_channel& tx_channel,
                                           satos::clock::time_point timeout_abs) {
        spl::drivers::dma::status dma_status{};

        for (auto& buffer : vector) {
            rx_channel.set_memory_address(buffer.data())
                .set_transfer_size(buffer.size())
                .enable_irq(spl::peripherals::dma::irq::transfer_completed)
                .enable_irq(spl::peripherals::dma::irq::transfer_error)
                .enable();
            tx_channel.set_transfer_size(buffer.size());
            tx_channel.enable();

            dma_status = rx_channel.wait_for_completion(timeout_abs);

            tx_channel.disable();
            rx_channel.disable();

            if (dma_status != spl::drivers::dma::status::ok) {
                break;
            }
        }

        return dma_status;
    }
};
/** @} */
} // namespace spl::drivers::spi::detail
