#pragma once
#include <span>
#include "baremetal/irq_event.h"
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "spl/drivers/spi/chip_select_guard.h"
#include "spl/drivers/spi/enums.h"
#include "spl/peripherals/dma/concepts/dma.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/gpio/concepts/gpio.h"
#include "spl/peripherals/nvic/concepts/nvic.h"
#include "spl/peripherals/spi/concepts/spi.h"
#include "spl/peripherals/spi/enums.h"

namespace spl::drivers::spi::detail {

/**
 * @addtogroup spi
 * @{
 */

/**
 * @brief SPI high level driver with DMA and without RTOS.
 * @tparam SPI SPI peripheral
 * @tparam DMA DMA peripheral
 * @tparam GPIO GPIO peripheral
 * @tparam NVIC NVIC peripheral
 * @tparam Event Event
 * @tparam PeripheralAddress SPI Peripheral address
 * @tparam RxChannel DMA RX channel
 * @tparam TxChannel DMA TX channel
 * @tparam RxIrq DMA RX IRQ
 * @tparam TxIrq DMA TX IRQ
 * @tparam DmaRequest DMA request
 */
template<spl::peripherals::spi::concepts::spi SPI,
         spl::peripherals::dma::concepts::dma DMA,
         spl::peripherals::gpio::concepts::gpio GPIO,
         spl::peripherals::nvic::concepts::nvic NVIC,
         baremetal::irq_event_concept Event,
         spl::peripherals::dma::concepts::peripheral PeripheralAddress,
         spl::peripherals::dma::concepts::channel RxChannel,
         spl::peripherals::dma::concepts::channel TxChannel,
         spl::peripherals::nvic::concepts::irq RxIrq,
         spl::peripherals::nvic::concepts::irq TxIrq,
         spl::peripherals::dma::concepts::request DmaRequest>
class spi_dma_lite : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param spi SPI peripheral
     * @param dma DMA peripheral
     * @param nvic NVIC peripheral
     */
    spi_dma_lite(SPI& spi, DMA& dma, NVIC& nvic) : spi_{spi}, dma_{dma}, nvic_{nvic} {}

    /**
     * @brief Initializes SPI in master mode with software chip select
     */
    void initialize() {
        spi_.set_slave_management(spl::peripherals::spi::slave_management::software)
            .set_mode(spl::peripherals::spi::mode::master)
            .set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8)
            .set_internal_slave_select(true);
    }

    /**
     * @brief Writes data over SPI to device selected with chip_select GPIO
     * @param chip_select Chip select GPIO
     * @param buffer Buffer to write
     * @return Operation status
     */
    status write(GPIO& chip_select, std::span<const std::byte> buffer) {
        if (buffer.empty()) {
            return status::buffer_is_empty;
        }
        status_ = status::ok;
        setup_tx_dma(buffer);
        dma_.enable_channel(TxChannel);
        spi_.enable_dma(spl::peripherals::spi::dma::tx);
        nvic_.enable_irq(TxIrq);

        {
            chip_select_guard cs(chip_select);

            spi_.enable();

            event_.wait();

            while (spi_.is_busy()) {};
        }

        spi_.disable().disable_dma(spl::peripherals::spi::dma::tx);
        while (!spi_.is_rx_fifo_empty()) {
            static_cast<void>(spi_.read());
        }
        dma_.reset_channel(TxChannel);

        return status_;
    }

    /**
     * @brief Read data received over SPI from device selected with chip_select GPIO
     * @param chip_select Chip select GPIO
     * @param buffer Buffer where received data will be saved
     * @return On success subspan with received data. Status on failure.
     */
    satext::expected<std::span<const std::byte>, status> read(GPIO& chip_select,
                                                              std::span<std::byte> buffer) {
        if (buffer.empty()) {
            return satext::unexpected{status::buffer_is_empty};
        }
        status_ = status::ok;
        spi_.enable_dma(spl::peripherals::spi::dma::rx);
        setup_rx_dma(buffer);
        dma_.enable_channel(RxChannel);
        nvic_.enable_irq(RxIrq);
        spi_.set_receive_only_mode();

        {
            chip_select_guard cs(chip_select);

            spi_.enable();

            event_.wait();
            spi_.disable();
        }

        spi_.disable_dma(spl::peripherals::spi::dma::rx).set_full_duplex_mode();
        while (!spi_.is_rx_fifo_empty()) {
            static_cast<void>(spi_.read());
        }
        dma_.reset_channel(RxChannel);

        if (status_ != status::ok) {
            return satext::unexpected{status_};
        } else {
            return buffer;
        }
    }

    /**
     * @brief Performs write and then read to/from SPI device selected with chip_select GPIO
     * @param chip_select Chip select GPIO
     * @param input_buffer Buffer to write
     * @param output_buffer Buffer where received data will be saved
     * @return On success subspan with received data. Status on failure.
     */
    satext::expected<std::span<const std::byte>, status>
    write_read(GPIO& chip_select,
               std::span<const std::byte> input_buffer,
               std::span<std::byte> output_buffer) {
        if (input_buffer.empty() || output_buffer.empty()) {
            return satext::unexpected{status::buffer_is_empty};
        }
        status_ = status::ok;
        setup_tx_dma(input_buffer);
        setup_rx_dma(output_buffer);
        dma_.enable_channel(TxChannel);
        spi_.enable_dma(spl::peripherals::spi::dma::tx);
        nvic_.enable_irq(TxIrq).enable_irq(RxIrq);

        {
            chip_select_guard cs(chip_select);

            spi_.enable();

            event_.wait();

            while (spi_.is_busy()) {};

            spi_.disable();
            spi_.disable_dma(spl::peripherals::spi::dma::tx);
            while (!spi_.is_rx_fifo_empty()) {
                static_cast<void>(spi_.read());
            }

            if (status_ != status::ok) {
                dma_.reset_channel(RxChannel).reset_channel(TxChannel);
                return satext::unexpected{status_};
            }

            spi_.enable_dma(spl::peripherals::spi::dma::rx);
            dma_.enable_channel(RxChannel);

            spi_.set_receive_only_mode();
            spi_.enable();

            event_.wait();
            spi_.disable();
        }

        spi_.disable_dma(spl::peripherals::spi::dma::rx).set_full_duplex_mode();
        while (!spi_.is_rx_fifo_empty()) {
            static_cast<void>(spi_.read());
        }
        dma_.reset_channel(RxChannel).reset_channel(TxChannel);

        if (status_ != status::ok) {
            return satext::unexpected{status_};
        } else {
            return output_buffer;
        }
    }

    /**
     * @brief Performs data transfer over SPI to device selected with chip_select GPIO
     * @param buffer
     * @param buffer Buffer with data to write and where received data will be saved
     * @return On success subspan with received data. Status on failure.
     */
    satext::expected<std::span<const std::byte>, status> transfer(GPIO& chip_select,
                                                                  std::span<std::byte> buffer) {
        if (buffer.empty()) {
            return satext::unexpected{status::buffer_is_empty};
        }
        status_ = status::ok;
        spi_.enable_dma(spl::peripherals::spi::dma::rx);
        setup_rx_dma(buffer);
        dma_.enable_channel(RxChannel);
        setup_tx_dma(buffer);
        dma_.enable_channel(TxChannel);
        spi_.enable_dma(spl::peripherals::spi::dma::tx);

        nvic_.enable_irq(RxIrq).enable_irq(TxIrq);

        {
            chip_select_guard cs(chip_select);
            spi_.enable();

            event_.wait();

            while (spi_.is_busy()) {};
        }

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);
        dma_.reset_channel(RxChannel).reset_channel(TxChannel);

        if (status_ != status::ok) {
            return satext::unexpected{status_};
        } else {
            return buffer;
        }
    }

    void isr_tx_handler() {
        if (dma_.get_irq_status(TxChannel, spl::peripherals::dma::irq::transfer_completed)) {
            dma_.disable_irq(TxChannel, spl::peripherals::dma::irq::transfer_completed)
                .clear_irq(TxChannel, spl::peripherals::dma::irq::transfer_completed)
                .disable_channel(TxChannel);
            nvic_.disable_irq(TxIrq);
            event_.notify();
        }
        if (dma_.get_irq_status(TxChannel, spl::peripherals::dma::irq::half_transfer)) {
            dma_.clear_irq(TxChannel, spl::peripherals::dma::irq::half_transfer);
        }
        if (dma_.get_irq_status(TxChannel, spl::peripherals::dma::irq::transfer_error)) {
            dma_.clear_irq(TxChannel, spl::peripherals::dma::irq::transfer_error)
                .disable_channel(TxChannel);
            nvic_.disable_irq(TxIrq);
            status_ = status::transfer_error;
            event_.notify();
        }
        nvic_.clear_pending_irq(TxIrq);
    }

    void isr_rx_handler() {
        if (dma_.get_irq_status(RxChannel, spl::peripherals::dma::irq::transfer_completed)) {
            dma_.disable_irq(RxChannel, spl::peripherals::dma::irq::transfer_completed)
                .clear_irq(RxChannel, spl::peripherals::dma::irq::transfer_completed)
                .disable_channel(RxChannel);
            nvic_.disable_irq(RxIrq);
            event_.notify();
        }
        if (dma_.get_irq_status(RxChannel, spl::peripherals::dma::irq::half_transfer)) {
            dma_.clear_irq(RxChannel, spl::peripherals::dma::irq::half_transfer);
        }
        if (dma_.get_irq_status(RxChannel, spl::peripherals::dma::irq::transfer_error)) {
            dma_.clear_irq(RxChannel, spl::peripherals::dma::irq::transfer_error)
                .disable_channel(RxChannel);
            nvic_.disable_irq(RxIrq);
            status_ = status::transfer_error;
            event_.notify();
        }
        nvic_.clear_pending_irq(RxIrq);
    }

private:
    void setup_tx_dma(std::span<const std::byte> buffer) {
        dma_.set_memory_address(TxChannel, buffer.data())
            .set_memory_increment(TxChannel, true)
            .set_peripheral_address(TxChannel, PeripheralAddress)
            .set_direction(TxChannel, spl::peripherals::dma::direction::memory_to_peripheral)
            .set_transfer_size(TxChannel, buffer.size())
            .set_request_mapping(TxChannel, DmaRequest)
            .enable_irq(TxChannel, spl::peripherals::dma::irq::transfer_completed)
            .enable_irq(TxChannel, spl::peripherals::dma::irq::transfer_error);
    }

    void setup_rx_dma(std::span<std::byte> buffer) {
        dma_.set_memory_address(RxChannel, buffer.data())
            .set_memory_increment(RxChannel, true)
            .set_peripheral_address(RxChannel, PeripheralAddress)
            .set_direction(RxChannel, spl::peripherals::dma::direction::peripheral_to_memory)
            .set_transfer_size(RxChannel, buffer.size())
            .set_request_mapping(RxChannel, DmaRequest)
            .enable_irq(RxChannel, spl::peripherals::dma::irq::transfer_completed)
            .enable_irq(RxChannel, spl::peripherals::dma::irq::transfer_error);
    }

    SPI& spi_;
    DMA& dma_;
    NVIC& nvic_;
    Event event_;
    status status_{status::ok};
};

/** @} */
} // namespace spl::drivers::spi::detail
