#pragma once
#include "satext/noncopyable.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace spl::drivers::spi {
/**
 * @addtogroup spi
 * @{
 */

template<spl::peripherals::gpio::concepts::gpio gpio>
class chip_select_guard : private satext::noncopyable {
public:
    explicit chip_select_guard(gpio& g) : gpio_(g) { gpio_.clear(); }

    ~chip_select_guard() { gpio_.set(); }

private:
    gpio& gpio_;
};

/** @} */
} // namespace spl::drivers::spi
