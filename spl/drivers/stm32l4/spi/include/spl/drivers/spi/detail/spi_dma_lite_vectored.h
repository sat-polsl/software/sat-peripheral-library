#pragma once
#include <span>
#include "baremetal/irq_event.h"
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "spl/drivers/spi/chip_select_guard.h"
#include "spl/drivers/spi/enums.h"
#include "spl/peripherals/dma/concepts/dma.h"
#include "spl/peripherals/dma/enums.h"
#include "spl/peripherals/gpio/concepts/gpio.h"
#include "spl/peripherals/nvic/concepts/nvic.h"
#include "spl/peripherals/spi/concepts/spi.h"
#include "spl/peripherals/spi/enums.h"

namespace spl::drivers::spi::detail {

/**
 * @addtogroup spi
 * @{
 */

/**
 * @brief SPI high level driver providing scatter read/gather write DMA transfers intended for use
 * on baremetal target.
 * @tparam SPI SPI peripheral
 * @tparam DMA DMA peripheral
 * @tparam GPIO GPIO peripheral
 * @tparam NVIC NVIC peripheral
 * @tparam Event Event
 * @tparam PeripheralAddress SPI Peripheral address
 * @tparam RxChannel DMA RX channel
 * @tparam TxChannel DMA TX channel
 * @tparam RxIrq DMA RX IRQ
 * @tparam TxIrq DMA TX IRQ
 * @tparam DmaRequest DMA request
 */
template<spl::peripherals::spi::concepts::spi SPI,
         spl::peripherals::dma::concepts::dma DMA,
         spl::peripherals::gpio::concepts::gpio GPIO,
         spl::peripherals::nvic::concepts::nvic NVIC,
         baremetal::irq_event_concept Event,
         spl::peripherals::dma::concepts::peripheral PeripheralAddress,
         spl::peripherals::dma::concepts::channel RxChannel,
         spl::peripherals::dma::concepts::channel TxChannel,
         spl::peripherals::nvic::concepts::irq RxIrq,
         spl::peripherals::nvic::concepts::irq TxIrq,
         spl::peripherals::dma::concepts::request DmaRequest>
class spi_dma_lite_vectored : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param spi SPI peripheral
     * @param dma DMA peripheral
     * @param nvic NVIC peripheral
     */
    spi_dma_lite_vectored(SPI& spi, DMA& dma, NVIC& nvic) : spi_{spi}, dma_{dma}, nvic_{nvic} {}

    /**
     * @brief Initializes SPI in master mode with software chip select
     */
    void initialize() {
        spi_.set_slave_management(spl::peripherals::spi::slave_management::software)
            .set_mode(spl::peripherals::spi::mode::master)
            .set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8)
            .set_internal_slave_select(true);
    }

    /**
     * @brief Performs gather write from input buffers over SPI to device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param input Non-contiguous input buffers
     * @return Operation status
     */
    status write(GPIO& chip_select, std::span<std::span<const std::byte>> input) {
        if (input.empty()) {
            return status::buffer_is_empty;
        }
        status_ = status::ok;
        setup_tx_dma();
        spi_.enable_dma(spl::peripherals::spi::dma::tx);
        nvic_.enable_irq(TxIrq);

        {
            chip_select_guard cs(chip_select);
            spi_.enable();

            gather_write(input);
            if (status_ != status::ok) {
                return status_;
            }
        }

        spi_.disable().disable_dma(spl::peripherals::spi::dma::tx);
        while (!spi_.is_rx_fifo_empty()) {
            static_cast<void>(spi_.read());
        }
        nvic_.disable_irq(TxIrq);
        dma_.reset_channel(TxChannel);

        return status_;
    }

    /**
     * @brief Performs scatter read to output buffers over SPI from device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param output Non-contiguous output buffers
     * @return Operation status
     */
    status read(GPIO& chip_select, std::span<std::span<std::byte>> output) {
        if (output.empty()) {
            return status::buffer_is_empty;
        }
        status_ = status::ok;
        spi_.enable_dma(spl::peripherals::spi::dma::rx);
        setup_rx_dma();
        setup_tx_dma();
        std::byte tx_dummy{0xff};
        dma_.set_memory_address(TxChannel, &tx_dummy) //
            .set_memory_increment(TxChannel, false);
        spi_.enable_dma(spl::peripherals::spi::dma::tx);
        nvic_.enable_irq(RxIrq);

        {
            chip_select_guard cs(chip_select);
            spi_.enable();

            scatter_read(output);
            if (status_ != status::ok) {
                return status_;
            }
        }

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);
        nvic_.disable_irq(RxIrq);
        dma_.reset_channel(RxChannel).reset_channel(TxChannel);

        return status_;
    }

    /**
     * @brief Performs gather write from input buffers and then scatter read to output buffers over
     * SPI to/from device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param input Non-contiguous input buffers
     * @param output Non-contiguous output buffers
     * @return Operation status
     */
    status write_read(GPIO& chip_select,
                      std::span<std::span<const std::byte>> input,
                      std::span<std::span<std::byte>> output) {
        if (input.empty() || output.empty()) {
            return status::buffer_is_empty;
        }
        status_ = status::ok;
        std::byte tx_dummy{0xff};
        setup_tx_dma();
        setup_rx_dma();
        spi_.enable_dma(spl::peripherals::spi::dma::tx);
        nvic_.enable_irq(TxIrq);

        {
            chip_select_guard cs(chip_select);

            spi_.enable();

            gather_write(input);
            if (status_ != status::ok) {
                return status_;
            }

            spi_.disable();
            while (!spi_.is_rx_fifo_empty()) {
                static_cast<void>(spi_.read());
            }
            nvic_.disable_irq(TxIrq).enable_irq(RxIrq);

            dma_.set_memory_address(TxChannel, &tx_dummy) //
                .set_memory_increment(TxChannel, false)
                .disable_irq(TxChannel, spl::peripherals::dma::irq::transfer_completed)
                .disable_irq(TxChannel, spl::peripherals::dma::irq::transfer_error);
            spi_.enable_dma(spl::peripherals::spi::dma::rx);
            spi_.enable();

            scatter_read(output);
            if (status_ != status::ok) {
                return status_;
            }
        }

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);
        nvic_.disable_irq(RxIrq);
        dma_.reset_channel(RxChannel).reset_channel(TxChannel);

        return status_;
    }

    /**
     * @brief Performs gather write/scatter read from/to input/output buffers over SPI to/from
     * device selected with chip_select
     * @param chip_select Chip select GPIO
     * @param input_output Non-contiguous input/output buffers
     * @return Operation status
     */
    status transfer(GPIO& chip_select, std::span<std::span<std::byte>> input_output) {
        if (input_output.empty()) {
            return status::buffer_is_empty;
        }
        status_ = status::ok;
        spi_.enable_dma(spl::peripherals::spi::dma::rx);
        setup_rx_dma();
        setup_tx_dma();
        spi_.enable_dma(spl::peripherals::spi::dma::tx);

        nvic_.enable_irq(TxIrq);

        {
            chip_select_guard cs(chip_select);
            spi_.enable();

            for (auto& buffer : input_output) {
                dma_.set_memory_address(RxChannel, buffer.data())
                    .set_transfer_size(RxChannel, buffer.size())
                    .enable_channel(RxChannel);
                dma_.set_memory_address(TxChannel, buffer.data())
                    .set_transfer_size(TxChannel, buffer.size())
                    .enable_channel(TxChannel);

                event_.wait();

                if (status_ != status::ok) {
                    return status_;
                }

                while (spi_.is_busy()) {};
            }
        }

        spi_.disable()
            .disable_dma(spl::peripherals::spi::dma::rx)
            .disable_dma(spl::peripherals::spi::dma::tx);
        nvic_.disable_irq(TxIrq);
        dma_.reset_channel(RxChannel).reset_channel(TxChannel);

        return status_;
    }

    void isr_tx_handler() {
        if (dma_.get_irq_status(TxChannel, spl::peripherals::dma::irq::transfer_completed)) {
            dma_.clear_irq(TxChannel, spl::peripherals::dma::irq::transfer_completed)
                .disable_channel(TxChannel);
            event_.notify();
        }
        if (dma_.get_irq_status(TxChannel, spl::peripherals::dma::irq::half_transfer)) {
            dma_.clear_irq(TxChannel, spl::peripherals::dma::irq::half_transfer);
        }
        if (dma_.get_irq_status(TxChannel, spl::peripherals::dma::irq::transfer_error)) {
            dma_.reset_channel(TxChannel).reset_channel(RxChannel);
            nvic_.disable_irq(TxIrq).disable_irq(RxIrq);
            spi_.disable().set_full_duplex_mode();
            while (!spi_.is_rx_fifo_empty()) {
                static_cast<void>(spi_.read());
            }

            status_ = status::transfer_error;
            event_.notify();
        }
        nvic_.clear_pending_irq(TxIrq);
    }

    void isr_rx_handler() {
        if (dma_.get_irq_status(RxChannel, spl::peripherals::dma::irq::transfer_completed)) {
            dma_.clear_irq(RxChannel, spl::peripherals::dma::irq::transfer_completed)
                .disable_channel(RxChannel);
            event_.notify();
        }
        if (dma_.get_irq_status(RxChannel, spl::peripherals::dma::irq::half_transfer)) {
            dma_.clear_irq(RxChannel, spl::peripherals::dma::irq::half_transfer);
        }
        if (dma_.get_irq_status(RxChannel, spl::peripherals::dma::irq::transfer_error)) {
            dma_.reset_channel(TxChannel).reset_channel(RxChannel);
            nvic_.disable_irq(TxIrq).disable_irq(RxIrq);
            spi_.disable().set_full_duplex_mode();
            while (!spi_.is_rx_fifo_empty()) {
                static_cast<void>(spi_.read());
            }

            status_ = status::transfer_error;
            event_.notify();
        }
        nvic_.clear_pending_irq(RxIrq);
    }

private:
    void setup_tx_dma() {
        dma_.set_memory_increment(TxChannel, true)
            .set_peripheral_address(TxChannel, PeripheralAddress)
            .set_direction(TxChannel, spl::peripherals::dma::direction::memory_to_peripheral)
            .set_request_mapping(TxChannel, DmaRequest)
            .enable_irq(TxChannel, spl::peripherals::dma::irq::transfer_completed)
            .enable_irq(TxChannel, spl::peripherals::dma::irq::transfer_error);
    }

    void setup_rx_dma() {
        dma_.set_memory_increment(RxChannel, true)
            .set_peripheral_address(RxChannel, PeripheralAddress)
            .set_direction(RxChannel, spl::peripherals::dma::direction::peripheral_to_memory)
            .set_request_mapping(RxChannel, DmaRequest)
            .enable_irq(RxChannel, spl::peripherals::dma::irq::transfer_completed)
            .enable_irq(RxChannel, spl::peripherals::dma::irq::transfer_error);
    }

    void gather_write(std::span<std::span<const std::byte>> vector) {
        for (auto& buffer : vector) {
            dma_.set_memory_address(TxChannel, buffer.data())
                .set_transfer_size(TxChannel, buffer.size())
                .enable_channel(TxChannel);

            event_.wait();
            if (status_ != status::ok) {
                break;
            }
            while (spi_.is_busy()) {};
        }
    }

    void scatter_read(std::span<std::span<std::byte>> vector) {
        for (auto& buffer : vector) {
            dma_.set_memory_address(RxChannel, buffer.data())
                .set_transfer_size(RxChannel, buffer.size())
                .enable_channel(RxChannel);
            dma_.set_transfer_size(TxChannel, buffer.size()) //
                .enable_channel(TxChannel);

            event_.wait();
            if (status_ != status::ok) {
                break;
            }
            dma_.disable_channel(TxChannel);
        }
    }

    SPI& spi_;
    DMA& dma_;
    NVIC& nvic_;
    Event event_;
    status status_{status::ok};
};

/** @} */
} // namespace spl::drivers::spi::detail
