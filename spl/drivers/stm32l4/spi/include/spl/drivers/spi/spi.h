#pragma once
#include "spl/drivers/dma/dma.h"
#include "spl/drivers/spi/concepts/spi.h"
#include "spl/drivers/spi/detail/spi.h"
#include "spl/drivers/spi/detail/spi_dma_lite.h"
#include "spl/drivers/spi/detail/spi_dma_lite_vectored.h"
#include "spl/drivers/spi/detail/spi_dma_vectored.h"
#include "spl/peripherals/dma/dma.h"
#include "spl/peripherals/gpio/gpio.h"
#include "spl/peripherals/nvic/nvic.h"
#include "spl/peripherals/spi/spi.h"

namespace spl::drivers::spi {

namespace spi_dma_lite {
using spi1 = detail::spi_dma_lite<spl::peripherals::spi::spi,
                                  spl::peripherals::dma::dma,
                                  spl::peripherals::gpio::gpio,
                                  spl::peripherals::nvic::nvic,
                                  baremetal::irq_event,
                                  spl::peripherals::dma::peripheral::spi1,
                                  spl::peripherals::dma::channel2,
                                  spl::peripherals::dma::channel3,
                                  spl::peripherals::nvic::irq::dma1_channel2,
                                  spl::peripherals::nvic::irq::dma1_channel3,
                                  spl::peripherals::dma::request2>;

using spi2 = detail::spi_dma_lite<spl::peripherals::spi::spi,
                                  spl::peripherals::dma::dma,
                                  spl::peripherals::gpio::gpio,
                                  spl::peripherals::nvic::nvic,
                                  baremetal::irq_event,
                                  spl::peripherals::dma::peripheral::spi2,
                                  spl::peripherals::dma::channel4,
                                  spl::peripherals::dma::channel5,
                                  spl::peripherals::nvic::irq::dma1_channel4,
                                  spl::peripherals::nvic::irq::dma1_channel5,
                                  spl::peripherals::dma::request2>;

using spi3 = detail::spi_dma_lite<spl::peripherals::spi::spi,
                                  spl::peripherals::dma::dma,
                                  spl::peripherals::gpio::gpio,
                                  spl::peripherals::nvic::nvic,
                                  baremetal::irq_event,
                                  spl::peripherals::dma::peripheral::spi3,
                                  spl::peripherals::dma::channel1,
                                  spl::peripherals::dma::channel2,
                                  spl::peripherals::nvic::irq::dma2_channel1,
                                  spl::peripherals::nvic::irq::dma2_channel2,
                                  spl::peripherals::dma::request4>;
} // namespace spi_dma_lite

namespace spi_dma_lite_vectored {
using spi1 = detail::spi_dma_lite_vectored<spl::peripherals::spi::spi,
                                           spl::peripherals::dma::dma,
                                           spl::peripherals::gpio::gpio,
                                           spl::peripherals::nvic::nvic,
                                           baremetal::irq_event,
                                           spl::peripherals::dma::peripheral::spi1,
                                           spl::peripherals::dma::channel2,
                                           spl::peripherals::dma::channel3,
                                           spl::peripherals::nvic::irq::dma1_channel2,
                                           spl::peripherals::nvic::irq::dma1_channel3,
                                           spl::peripherals::dma::request2>;

using spi2 = detail::spi_dma_lite_vectored<spl::peripherals::spi::spi,
                                           spl::peripherals::dma::dma,
                                           spl::peripherals::gpio::gpio,
                                           spl::peripherals::nvic::nvic,
                                           baremetal::irq_event,
                                           spl::peripherals::dma::peripheral::spi2,
                                           spl::peripherals::dma::channel4,
                                           spl::peripherals::dma::channel5,
                                           spl::peripherals::nvic::irq::dma1_channel4,
                                           spl::peripherals::nvic::irq::dma1_channel5,
                                           spl::peripherals::dma::request2>;

using spi3 = detail::spi_dma_lite_vectored<spl::peripherals::spi::spi,
                                           spl::peripherals::dma::dma,
                                           spl::peripherals::gpio::gpio,
                                           spl::peripherals::nvic::nvic,
                                           baremetal::irq_event,
                                           spl::peripherals::dma::peripheral::spi3,
                                           spl::peripherals::dma::channel1,
                                           spl::peripherals::dma::channel2,
                                           spl::peripherals::nvic::irq::dma2_channel1,
                                           spl::peripherals::nvic::irq::dma2_channel2,
                                           spl::peripherals::dma::request4>;
} // namespace spi_dma_lite_vectored

namespace spi_dma_vectored {
using spi1 = detail::spi_dma_vectored<spl::peripherals::spi::spi,
                                      spl::peripherals::gpio::gpio,
                                      spl::drivers::dma::dma,
                                      spl::peripherals::dma::peripheral::spi1,
                                      spl::peripherals::dma::channel2,
                                      spl::peripherals::dma::channel3,
                                      spl::peripherals::dma::request2>;

using spi2 = detail::spi_dma_vectored<spl::peripherals::spi::spi,
                                      spl::peripherals::gpio::gpio,
                                      spl::drivers::dma::dma,
                                      spl::peripherals::dma::peripheral::spi2,
                                      spl::peripherals::dma::channel4,
                                      spl::peripherals::dma::channel5,
                                      spl::peripherals::dma::request2>;

using spi3 = detail::spi_dma_vectored<spl::peripherals::spi::spi,
                                      spl::peripherals::gpio::gpio,
                                      spl::drivers::dma::dma,
                                      spl::peripherals::dma::peripheral::spi3,
                                      spl::peripherals::dma::channel1,
                                      spl::peripherals::dma::channel2,
                                      spl::peripherals::dma::request4>;
} // namespace spi_dma_vectored

namespace spi {
/**
 * @brief MCU SPI1 specialization
 */
using spi1 = detail::spi<spl::peripherals::spi::spi,
                         spl::peripherals::gpio::gpio,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::spi1>;

/**
 * @brief MCU SPI3 specialization
 */
using spi2 = detail::spi<spl::peripherals::spi::spi,
                         spl::peripherals::gpio::gpio,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::spi2>;

/**
 * @brief MCU SPI2 specialization
 */
using spi3 = detail::spi<spl::peripherals::spi::spi,
                         spl::peripherals::gpio::gpio,
                         spl::peripherals::nvic::nvic,
                         spl::peripherals::nvic::irq::spi3>;
} // namespace spi

static_assert(concepts::spi<spi_dma_lite::spi1, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi<spi_dma_lite::spi2, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi<spi_dma_lite::spi3, spl::peripherals::gpio::gpio>);

static_assert(concepts::spi_vectored<spi_dma_lite_vectored::spi1, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi_vectored<spi_dma_lite_vectored::spi2, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi_vectored<spi_dma_lite_vectored::spi3, spl::peripherals::gpio::gpio>);

static_assert(concepts::spi<spi::spi1, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi<spi::spi2, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi<spi::spi3, spl::peripherals::gpio::gpio>);

static_assert(concepts::spi_vectored<spi_dma_vectored::spi1, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi_vectored<spi_dma_vectored::spi2, spl::peripherals::gpio::gpio>);
static_assert(concepts::spi_vectored<spi_dma_vectored::spi3, spl::peripherals::gpio::gpio>);

/** @} */
} // namespace spl::drivers::spi
