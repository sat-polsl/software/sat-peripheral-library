#pragma once

#include <mutex>
#include <span>
#include "satext/expected.h"
#include "satext/noncopyable.h"
#include "satos/clock.h"
#include "satos/event.h"
#include "satos/isr.h"
#include "satos/mutex.h"
#include "satos/thread.h"
#include "spl/drivers/spi/chip_select_guard.h"
#include "spl/drivers/spi/enums.h"
#include "spl/peripherals/gpio/concepts/gpio.h"
#include "spl/peripherals/nvic/concepts/nvic.h"
#include "spl/peripherals/spi/concepts/spi.h"
#include "spl/peripherals/spi/enums.h"

namespace spl::drivers::spi::detail {

/**
 * @addtogroup spi
 * @{
 */

/**
 * @brief SPI high level driver.
 * @tparam SPI SPI peripheral
 * @tparam GPIO GPIO peripheral (chip select)
 * @tparam NVIC NVIC peripheral
 * @tparam Irq NVIC irq
 */
template<spl::peripherals::spi::concepts::spi SPI,
         spl::peripherals::gpio::concepts::gpio GPIO,
         spl::peripherals::nvic::concepts::nvic NVIC,
         spl::peripherals::nvic::concepts::irq Irq>
class spi : private satext::noncopyable {
public:
    /**
     * @brief Constructor
     * @param spi SPI peripheral
     * @param nvic NVIC peripheral
     */
    explicit spi(SPI& spi, NVIC& nvic) : spi_{spi}, nvic_{nvic} {}

    /**
     * @brief Initialize SPI peripheral
     */
    void initialize() {
        spi_.set_slave_management(spl::peripherals::spi::slave_management::software)
            .set_mode(spl::peripherals::spi::mode::master)
            .set_fifo_reception_threshold(spl::peripherals::spi::fifo_threshold::bit8)
            .set_internal_slave_select(true);
    }

    /**
     * @brief Read data from SPI
     * @param chip_select Chip select for device to read from
     * @param buffer Buffer to store read information
     * @param timeout Optional timeout
     * @return Buffer with read data if successful, satext::unexpected(status) if not
     */
    satext::expected<std::span<const std::byte>, status>
    read(GPIO& chip_select,
         std::span<std::byte> buffer,
         satos::clock::duration timeout = satos::clock::duration::max()) {
        if (spi_.is_busy()) {
            return satext::unexpected(status::busy);
        }

        if (buffer.empty()) {
            return satext::unexpected(status::buffer_is_empty);
        }

        auto timeout_abs = satos::clock::from_now(timeout);

        if (std::unique_lock lck{mtx_, timeout_abs}; !lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        status_ = status::ok;
        read_begin_ = buffer.begin();
        read_end_ = buffer.end();
        read_only_mode_ = true;

        spi_.enable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
        spi_.enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
        {
            chip_select_guard cs(chip_select);
            satos::event_guard event(event_);
            spi_.enable();
            if (!event.wait_until(timeout_abs)) {
                status_ = status::timeout;
            }
        }
        read_only_mode_ = false;

        if (status_ != status::ok) {
            spi_.disable();
            spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
            spi_.disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
            return satext::unexpected{status_};
        } else {
            return buffer.subspan(0, std::distance(buffer.begin(), read_begin_));
        }
    }

    /**
     * @brief Write data onto SPI bus
     * @param chip_select Chip select for device to write to
     * @param buffer Buffer with information to send
     * @param timeout Optional timeout
     * @return Status of operation
     */
    spl::drivers::spi::status
    write(GPIO& chip_select,
          std::span<const std::byte> buffer,
          satos::clock::duration timeout = satos::clock::duration::max()) {
        using namespace satos::chrono_literals;
        if (spi_.is_busy()) {
            return status::busy;
        }

        if (buffer.empty()) {
            return status::buffer_is_empty;
        }

        auto timeout_abs = satos::clock::from_now(timeout);
        std::unique_lock lck{mtx_, timeout_abs};
        if (!lck.owns_lock()) {
            return status::timeout;
        }

        status_ = status::ok;
        write_begin_ = buffer.begin();
        write_end_ = buffer.end();

        spi_.enable_irq(spl::peripherals::spi::irq::tx_buffer_empty);

        {
            chip_select_guard cs(chip_select);
            satos::event_guard event(event_);
            spi_.enable();
            if (!event.wait_until(timeout_abs)) {
                status_ = status::timeout;
            }

            if (status_ == status::ok) {
                satos::this_thread::sleep_for(10_ms);
            }
        }
        spi_.disable();
        clean_rx_fifo();

        if (status_ != status::ok) {
            spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
        }
        return status_;
    }

    /**
     * @brief First write to, then read from SPI bus
     * @param chip_select Chip select for device to write to and read from
     * @param read_buffer Buffer to store read information
     * @param write_buffer Buffer with information to send
     * @param timeout Optional timeout
     * @return Buffer with read data if successful, satext::unexpected(status) if not
     */
    satext::expected<std::span<const std::byte>, status>
    write_read(GPIO& chip_select,
               std::span<const std::byte> write_buffer,
               std::span<std::byte> read_buffer,
               satos::clock::duration timeout = satos::clock::duration::max()) {
        using namespace satos::chrono_literals;
        if (spi_.is_busy()) {
            return satext::unexpected(status::busy);
        }

        if (read_buffer.empty() || write_buffer.empty()) {
            return satext::unexpected(status::buffer_is_empty);
        }

        auto timeout_abs = satos::clock::from_now(timeout);

        if (std::unique_lock lck{mtx_, timeout_abs}; !lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        status_ = status::ok;
        write_begin_ = write_buffer.begin();
        write_end_ = write_buffer.end();
        read_begin_ = read_buffer.begin();
        read_end_ = read_buffer.end();

        spi_.enable_irq(spl::peripherals::spi::irq::tx_buffer_empty);

        {
            chip_select_guard cs(chip_select);
            satos::event_guard event(event_);

            spi_.enable();
            if (!event.wait_until(timeout_abs)) {
                status_ = status::timeout;
            }

            if (status_ == status::ok) {
                satos::this_thread::sleep_for(10_ms);

                spi_.disable();
                clean_rx_fifo();
                spi_.enable();

                read_only_mode_ = true;
                spi_.enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
                spi_.enable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
                if (!event.wait_until(timeout_abs)) {
                    status_ = status::timeout;
                }
            }
        }
        read_only_mode_ = false;

        if (status_ != status::ok) {
            spi_.disable();
            spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
            spi_.disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
            return satext::unexpected(status_);
        } else {
            return read_buffer.subspan(0, std::distance(read_buffer.begin(), read_begin_));
        }
    }

    /**
     * @brief Transfer data on SPI bus
     * @param chip_select Chip select for device to write to and read from
     * @param buffer Buffer with data to send and to store read information
     * @param timeout Optional timeout
     * @return Buffer with new data if successful, satext::unexpected(status) if not
     */
    satext::expected<std::span<const std::byte>, status>
    transfer(GPIO& chip_select,
             std::span<std::byte> buffer,
             satos::clock::duration timeout = satos::clock::duration::max()) {
        if (spi_.is_busy()) {
            return satext::unexpected(status::busy);
        }

        if (buffer.empty()) {
            return satext::unexpected(status::buffer_is_empty);
        }

        auto timeout_abs = satos::clock::from_now(timeout);

        if (std::unique_lock lck{mtx_, timeout_abs}; !lck.owns_lock()) {
            return satext::unexpected(status::timeout);
        }

        status_ = status::ok;
        auto write_buffer = std::span<const std::byte>(buffer);
        write_begin_ = write_buffer.begin();
        write_end_ = write_buffer.end();
        read_begin_ = buffer.begin();
        read_end_ = buffer.end();
        transfer_mode_ = true;

        spi_.enable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
        spi_.enable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);

        {
            chip_select_guard cs(chip_select);
            satos::event_guard event(event_);
            spi_.enable();
            if (!event.wait_until(timeout_abs)) {
                status_ = status::timeout;
            }
        }

        transfer_mode_ = false;

        if (status_ != status::ok) {
            spi_.disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
            spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
            spi_.disable();
            return satext::unexpected(status_);
        } else {
            return buffer.subspan(0, std::distance(buffer.begin(), read_begin_));
        }
    }

    void isr_handler(void) {
        satos::isr isr;
        if (spi_.is_irq_enabled(spl::peripherals::spi::irq::rx_buffer_not_empty) &&
            spi_.is_rx_not_empty()) {
            *read_begin_++ = static_cast<std::byte>(spi_.read());
            if (read_begin_ == read_end_) {
                spi_.disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
                spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
                spi_.disable();
                event_.notify();
            }
        }

        if (spi_.is_irq_enabled(spl::peripherals::spi::irq::tx_buffer_empty) &&
            spi_.is_tx_empty()) {
            if (read_only_mode_ && std::distance(read_begin_, read_end_) > 1) {
                spi_.write(0xff);
            } else if (write_begin_ != write_end_) {
                spi_.write(static_cast<std::uint8_t>(*write_begin_++));
            } else {
                spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
                if (!transfer_mode_) {
                    event_.notify();
                }
            }
        }

        if (spi_.is_irq_enabled(spl::peripherals::spi::irq::error) && spi_.overrun_error()) {
            spi_.clear_error_flag(spl::peripherals::spi::error::overrun);
            spi_.disable_irq(spl::peripherals::spi::irq::rx_buffer_not_empty);
            spi_.disable_irq(spl::peripherals::spi::irq::tx_buffer_empty);
            spi_.disable();
            status_ = status::transfer_error;
            event_.notify();
        }

        nvic_.clear_pending_irq(Irq);
    }

private:
    SPI& spi_;
    NVIC& nvic_;
    satos::event event_{};
    satos::mutex mtx_{};
    std::span<std::byte>::iterator read_begin_{};
    std::span<std::byte>::iterator read_end_{};
    std::span<const std::byte>::iterator write_begin_;
    std::span<const std::byte>::iterator write_end_;
    status status_{status::ok};
    bool read_only_mode_{false};
    bool transfer_mode_{false};

    void clean_rx_fifo() {
        while (!spi_.is_rx_fifo_empty()) {
            static_cast<void>(spi_.read());
        }
    }
};

} // namespace spl::drivers::spi::detail
