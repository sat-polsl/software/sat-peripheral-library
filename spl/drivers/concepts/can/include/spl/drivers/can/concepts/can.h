#pragma once
#include <variant>
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/can/enums.h"
#include "spl/peripherals/can/concepts/enums.h"
#include "spl/peripherals/can/concepts/types.h"

namespace spl::drivers::can::concepts {

// clang-format off
template<typename T>
concept can = requires(T can,
                       spl::peripherals::can::concepts::message msg,
                       spl::peripherals::can::concepts::rx_fifo rx_fifo,
                       satos::clock::duration duration) {
    { can.write(msg, duration) } -> std::same_as<status>;
    { can.read(rx_fifo, duration) } -> std::same_as<satext::expected<spl::peripherals::can::concepts::message, status>>;
};
// clang-format on

} // namespace spl::drivers::can::concepts
