#pragma once

namespace spl::drivers::can {
/**
 * @addtogroup can
 * @{
 */

/**
 * @brief CAN operation status enumeration
 */
enum class status { ok, timeout };

/** @} */
} // namespace spl::drivers::can
