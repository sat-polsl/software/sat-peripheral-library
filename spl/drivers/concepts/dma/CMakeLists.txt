set(TARGET dma_concept)

add_library(${TARGET} INTERFACE)
add_library(spl::dma_concept ALIAS ${TARGET})

target_include_directories(${TARGET}
    INTERFACE
    include
    )

target_link_libraries(${TARGET}
    INTERFACE
    spl::dma_ll_concept
    satext::satext
    satos::api
    )
