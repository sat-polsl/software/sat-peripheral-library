#pragma once
#include <cstdint>

namespace spl::drivers::dma {

/**
 * @addtogroup dma
 * @{
 */

/**
 * @brief DMA status enumeration
 */
enum class status { ok, timeout, transfer_error };

/** @} */
} // namespace spl::drivers::dma
