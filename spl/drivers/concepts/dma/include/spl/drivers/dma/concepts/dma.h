#pragma once
#include <concepts>
#include <type_traits>
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/dma/enums.h"
#include "spl/peripherals/dma/concepts/enums.h"

namespace spl::drivers::dma::concepts {

// clang-format off
template<typename T>
concept dma_channel = requires(T dma_channel,
                               spl::peripherals::dma::concepts::peripheral peripheral,
                               std::uint32_t address, std::uint32_t* pointer,
                               std::uint16_t size, bool increment,
                               spl::peripherals::dma::concepts::direction direction,
                               spl::peripherals::dma::concepts::request request,
                               spl::peripherals::dma::concepts::irq irq,
                               satos::clock::duration duration,
                               satos::clock::time_point time_point) {
    { dma_channel.enable() } -> std::same_as<const T&>;
    { dma_channel.disable() } -> std::same_as<const T&>;
    { dma_channel.enable_irq(irq) } -> std::same_as<const T&>;
    { dma_channel.disable_irq(irq) } -> std::same_as<const T&>;
    { dma_channel.set_peripheral_address(peripheral) } -> std::same_as<const T&>;
    { dma_channel.set_memory_address(address) } -> std::same_as<const T&>;
    { dma_channel.set_memory_address(pointer) } -> std::same_as<const T&>;
    { dma_channel.set_transfer_size(size) } -> std::same_as<const T&>;
    { dma_channel.set_memory_increment(increment) } -> std::same_as<const T&>;
    { dma_channel.set_peripheral_increment(increment) } -> std::same_as<const T&>;
    { dma_channel.set_direction(direction) } -> std::same_as<const T&>;
    { dma_channel.set_request_mapping(request) } -> std::same_as<const T&>;
    { dma_channel.set_m2m(increment) } -> std::same_as<const T&>;
    { dma_channel.set_m2m_source(address) } -> std::same_as<const T&>;
    { dma_channel.set_m2m_source(pointer) } -> std::same_as<const T&>;
    { dma_channel.set_m2m_destination(address) } -> std::same_as<const T&>;
    { dma_channel.set_m2m_destination(pointer) } -> std::same_as<const T&>;
    { dma_channel.wait_for_completion(duration) } -> std::same_as<spl::drivers::dma::status>;
    { dma_channel.wait_for_completion(time_point) } -> std::same_as<spl::drivers::dma::status>;
    { dma_channel.transfer(duration) } -> std::same_as<spl::drivers::dma::status>;
    { dma_channel.transfer(time_point) } -> std::same_as<spl::drivers::dma::status>;
};

template<typename T>
concept dma = requires(T dma, spl::peripherals::dma::concepts::channel channel,
                               satos::clock::duration duration, satos::clock::time_point time_point) {
    { dma.request(channel, duration) } -> std::same_as<satext::expected<typename T::channel, spl::drivers::dma::status>>;
    { dma.request(channel, time_point) } -> std::same_as<satext::expected<typename T::channel, spl::drivers::dma::status>>;
} && dma_channel<typename T::channel>;
// clang-format on

} // namespace spl::drivers::dma::concepts
