#pragma once
#include <span>
#include "satext/expected.h"
#include "spl/drivers/uart/enums.h"

namespace spl::drivers::uart::concepts {

// clang-format off
template<typename T>
concept uart = requires(T uart, std::span<std::byte> buffer,
                        std::span<const std::byte> const_buffer,
                        std::byte expected) {
    { uart.write(const_buffer) } -> std::same_as<status>;
    { uart.read(buffer) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
    { uart.read_until(buffer, expected) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
};
// clang-format on

} // namespace spl::drivers::uart::concepts
