#pragma once

namespace spl::drivers::uart {

/**
 * @addtogroup uart
 * @{
 */

/**
 * @brief UART status enumeration
 */
enum class status { ok, buffer_is_empty, timeout };

/** @} */

} // namespace spl::drivers::uart
