#pragma once
#include <span>
#include <type_traits>
#include "satext/expected.h"
#include "spl/drivers/spi/enums.h"
#include "spl/peripherals/gpio/concepts/gpio.h"

namespace spl::drivers::spi::concepts {

// clang-format off
template<typename T, typename U>
concept spi = requires(T spi, U& gpio, std::span<std::byte> buffer,
                       std::span<const std::byte> const_buffer) {
    { spi.write(gpio, const_buffer) } -> std::same_as<status>;
    { spi.read(gpio, buffer) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
    { spi.write_read(gpio, const_buffer, buffer) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
    { spi.transfer(gpio, buffer) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
} && spl::peripherals::gpio::concepts::gpio<U>;

template<typename T, typename U>
concept spi_vectored = requires(T spi, U& gpio, std::span<std::span<const std::byte>> input,
                                std::span<std::span<std::byte>> output) {
    { spi.write(gpio, input) } -> std::same_as<status>;
    { spi.read(gpio, output) } -> std::same_as<status>;
    { spi.write_read(gpio, input, output) } -> std::same_as<status>;
    { spi.transfer(gpio, output) } -> std::same_as<status>;
} && spl::peripherals::gpio::concepts::gpio<U>;

// clang-format on

} // namespace spl::drivers::spi::concepts
