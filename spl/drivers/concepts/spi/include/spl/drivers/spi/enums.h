#pragma once

namespace spl::drivers::spi {
/**
 * @addtogroup spi
 * @{
 */

/**
 * @brief SPI status enumeration
 */

enum class status { ok, buffer_is_empty, transfer_error, busy, timeout };

/** @} */
} // namespace spl::drivers::spi
