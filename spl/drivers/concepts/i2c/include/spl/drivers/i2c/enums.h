#pragma once

namespace spl::drivers::i2c {

/**
 * @addtogroup i2c
 * @{
 */

/**
 * @brief I2C status enumeration
 */
enum class status {
    ok,
    buffer_is_empty,
    busy,
    timeout,
    nack_received,
    transfer_error,
    buffer_size_out_of_range
};

/** @} */

} // namespace spl::drivers::i2c
