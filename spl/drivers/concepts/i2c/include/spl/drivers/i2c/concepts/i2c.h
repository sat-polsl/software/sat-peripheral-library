#pragma once
#include <span>
#include "satext/expected.h"
#include "satos/clock.h"
#include "spl/drivers/i2c/enums.h"

namespace spl::drivers::i2c::concepts {

// clang-format off
template<typename T>
concept i2c = requires(T i2c, std::uint8_t address, std::span<std::byte> buffer,
                                std::span<const std::byte> const_buffer, satos::clock::duration duration) {
    { i2c.write(address, const_buffer, duration) } -> std::same_as<status>;
    { i2c.read(address, buffer, duration) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
    { i2c.write_read(address, const_buffer, buffer, duration) } -> std::same_as<satext::expected<std::span<const std::byte>, status>>;
};

template<typename T>
concept i2c_vectored = requires(T i2c, std::uint8_t address, std::span<std::span<const std::byte>> input,
                                        std::span<std::span<std::byte>> output, satos::clock::duration duration) {
    { i2c.write(address, input, duration) } -> std::same_as<status>;
    { i2c.read(address, output, duration) } -> std::same_as<status>;
    { i2c.write_read(address, input, output, duration) } -> std::same_as<status>;
};
// clang-format on

} // namespace spl::drivers::i2c::concepts
